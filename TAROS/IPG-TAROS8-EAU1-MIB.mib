IPG-TAROS8-EAU1-MIB DEFINITIONS ::= BEGIN


IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE, Integer32
        FROM SNMPv2-SMI
    DisplayString
       FROM SNMPv2-TC
    OBJECT-GROUP
        FROM SNMPv2-CONF
    devices, StatusType
        FROM IPG-TAROS-MIB;

eau1 MODULE-IDENTITY
    LAST-UPDATED    "201103080000Z"
    ORGANIZATION    "IPG - NTO IRE-Polus"
    CONTACT-INFO
            "IPG Russia, NTO IRE-Polus,
            Telecommunications department,
            TAROS Family Devices
            http://www.ntoire-polus.ru/products_telecom_analog.html

            E-mail:
            Alexander Gutor <agutor@ntoire-polus.ru>"
    DESCRIPTION
        "The Structure of Management Information for the
        IPG Taros equipment enterprise."
    REVISION        "200909220000Z"
    DESCRIPTION
        "Initial version of this MIB module."
    ::= { devices 8 }


--
-- TAROS-5, EAU-BS1 MIB
--

information    OBJECT IDENTIFIER 	::= { eau1 1 }
variables      OBJECT IDENTIFIER 	::= { eau1 2 }
configure      OBJECT IDENTIFIER 	::= { eau1 3 }
thresholds     OBJECT IDENTIFIER 	::= { eau1 4 }

--
-- information BEGIN
--
devID OBJECT-TYPE
	SYNTAX  DisplayString
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"Device identifier"
	::= { information 1 }

devSN OBJECT-TYPE
	SYNTAX  DisplayString
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"Device Serial Numbers"
	::= { information 2 }

ampHwNumber OBJECT-TYPE
	SYNTAX  DisplayString
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"Hardware Component Number"
	::= { information 3 }

ampSwNumber OBJECT-TYPE
	SYNTAX  DisplayString
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"Firmware version"
	::= { information 4 }




--
-- information END.
--

--
-- Variables BEGIN
--


ampStatus OBJECT-TYPE
	SYNTAX  StatusType
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"Common status of te device"
	::= { variables 1 }

ampPin  OBJECT-TYPE
    SYNTAX  Integer32
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "Pin*10, input optical power, dBm*10"
    ::= { variables 2 }

ampPout  OBJECT-TYPE
    SYNTAX  Integer32
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "Pout*10, Output optical power, dBm*10"
    ::= { variables 3 }

ampIPrLD  OBJECT-TYPE
    SYNTAX  Integer32 (0..6000)
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "IPrLD, Preliminary Pump Laser Diode Current, mA"
    ::= { variables 4 }

ampILD  OBJECT-TYPE
    SYNTAX  Integer32 (0..6000)
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "ILD, Amplifier Pump Laser Diod Current, mA"
    ::= { variables 5 }


ampGain  OBJECT-TYPE
    SYNTAX  Integer32
    MAX-ACCESS	read-only
    STATUS  current
    DESCRIPTION
            "Gain*10, Amplifier Gain, equal Pout-Pin, dBm*10"
    ::= { variables  6 }

ampTCase  OBJECT-TYPE
    SYNTAX  Integer32
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
            "TCase*10, Pump Laser Diod Radiator Temperature, centigrade degree*10"
    ::= { variables  7 }

devIFan  OBJECT-TYPE
    SYNTAX Integer32
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "IFan, Fan Current, mA"
    ::= { variables  8 }

devPFan  OBJECT-TYPE
    SYNTAX  Integer32 (0..100)
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "Fan Air Flow, "
    ::= { variables  9 }

devFanState  OBJECT-TYPE
    SYNTAX   INTEGER {
			manual(0),
            low(1),
            linear(2),
            high(3)
			}
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "Current Fan Operation Mode.  manual(1) - forced set value,
            low(2), linear(3) or high(4) - the current condition of automatic Fan mode Operation."
    ::= { variables  10 }

devTime  OBJECT-TYPE
    SYNTAX  Integer32
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "Time of Work from Switching On, sec"
    ::= { variables  11 }

--
-- Variables END
--


--
-- Mgmt BEGIN
--

ampIPrLDSet OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDSet, Set Value of Preliminary Pump Laser Diode Current, mA"
	::= { configure 1 }

ampILDSet OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDSet, Set Value of Amplifier Laser Diode Pump Current, mA"
	::= { configure 2 }

ampPoutSet OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutSet*10, Set Value of Output Optical Power, dBm*10"
	::= { configure 3 }

ampGainSet OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainSet*10, Set Value of Amplifier Gain, dB*10"
	::= { configure 4 }

ampStMode OBJECT-TYPE
	SYNTAX  INTEGER {
            ild(0),
            pout(1),
            gain(2)
            }
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"StMode, Amplifier Stabilization Mode, 
        ild(0)  - Amplifier ILD Stabilization Mode,
        pout(1) - Amplifier Pout Stabilization Mode,
        gain(2) - Amplifier Gain Stabilization Mode"
	::= { configure 5 }

ampAmpAPS OBJECT-TYPE
	SYNTAX  INTEGER {
            off(0),
            on(1)
            }
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"AmpAPS, Amplifier APS Mode On/Off"
	::= { configure 6 }
	
ampAmpEnable OBJECT-TYPE
	SYNTAX  INTEGER {
            off(0),
            on(1)
            }
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"AmpEnable, Amplifier Pump Current On/Off"
	::= { configure 7 }

devPFanMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PFanMax, Max Value of Fan Air Flow for Auto Mode, %"
	::= { configure 8 }

devPFanMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PFanMin, Min Value of Fan Air Flow for Auto Mode, %"
	::= { configure 9 }

devTCaseMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseMax, Max Value of Case Temperature for Auto Mode, centigrade degree"
	::= { configure 10 }

devTCaseMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseMin, Min Value of Case Temperature for Auto Mode, centigrade degree"
	::= { configure 11 }

devPFanSet OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PFanSet, Set Value of Fan Air Flow for Manual Mode, %"
	::= { configure 12 }

devFanMode OBJECT-TYPE
	SYNTAX  INTEGER {
            off(0),
            on(1)
            }
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"FanMode, Auto Mode On/Off"
	::= { configure 13 }
	

--
-- Mgmt END.
--



--
-- Thresholds BEGIN
--
ampPinHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinHMax, dBm*10"
	::= { thresholds 1 }
ampPinCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinCMax, dBm*10"
	::= { thresholds 2 }
ampPinWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinWMax, dBm*10"
	::= { thresholds 3 }
ampPinWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinWMin, dBm*10"
	::= { thresholds 4 }
ampPinCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinCMin, dBm*10"
	::= { thresholds 5 }
ampPinHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PinHMin, dBm*10"
	::= { thresholds 6 }


ampPoutHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutHMax, dBm*10"
	::= { thresholds 7 }
ampPoutCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutCMax, dBm*10"
	::= { thresholds 8 }
ampPoutWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutWMax, dBm*10"
	::= { thresholds 9 }
ampPoutWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutWMin, dBm*10"
	::= { thresholds 10 }
ampPoutCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutCMin, dBm*10"
	::= { thresholds 11 }
ampPoutHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"PoutHMin, dBm*10"
	::= { thresholds 12 }


ampGainHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainHMax, dB*10"
	::= { thresholds 13 }
ampGainCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainCMax, dB*10"
	::= { thresholds 14 }
ampGainWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainWMax, dB*10"
	::= { thresholds 15 }
ampGainWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainWMin, dB*10"
	::= { thresholds 16 }
ampGainCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainCMin, dB*10"
	::= { thresholds 17 }
ampGainHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"GainHMin, dB*10"
	::= { thresholds 18 }


ampIPrLDHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDMax, mA"
	::= { thresholds 19 }
ampIPrLDCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDCMax, mA"
	::= { thresholds 20 }
ampIPrLDWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDWMax, mA"
	::= { thresholds 21 }
ampIPrLDWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDWMin, mA"
	::= { thresholds 22 }
ampIPrLDCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDCMin, mA"
	::= { thresholds 23 }
ampIPrLDHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IPrLDHMin, mA"
	::= { thresholds 24 }

ampILDHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDHMax, mA"
	::= { thresholds 25 }
ampILDCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDCMax, mA"
	::= { thresholds 26 }
ampILDWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDWMax, mA"
	::= { thresholds 27 }
ampILDWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDWMin, mA"
	::= { thresholds 28 }
ampILDCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDCMin, mA"
	::= { thresholds 29 }
ampILDHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"ILDHMin, mA"
	::= { thresholds 30 }




ampTCaseHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseHMax, centigrade degree*10"
	::= { thresholds 31 }
ampTCaseCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseCMax, centigrade degree*10"
	::= { thresholds 32 }
ampTCaseWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseWMax, centigrade degree*10"
	::= { thresholds 33 }
ampTCaseWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseWMin, centigrade degree*10"
	::= { thresholds 34 }
ampTCaseCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseCMin, centigrade degree*10"
	::= { thresholds 35 }
ampTCaseHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"TCaseHMin, centigrade degree*10"
	::= { thresholds 36 }


ampIFanHMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanHMax, mA"
	::= { thresholds 37 }
ampIFanCMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanCMax, mA"
	::= { thresholds 38 }
ampIFanWMax OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanWMax, mA"
	::= { thresholds 39 }
ampIFanWMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanWMin, mA"
	::= { thresholds 40 }
ampIFanCMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanCMin, mA"
	::= { thresholds 41 }
ampIFanHMin OBJECT-TYPE
	SYNTAX  Integer32
	MAX-ACCESS read-only
	STATUS current
	DESCRIPTION
		"IFanHMin, mA"
	::= { thresholds 42 }

--
-- Thresholds END.
--




localConformance OBJECT IDENTIFIER 	::= { eau1 9 }
localGroups OBJECT IDENTIFIER 	::= { localConformance 2 }


varGroup OBJECT-GROUP
	OBJECTS {

    ampStatus,
    ampPin,
    ampPout,
    ampIPrLD,
    ampILD,
    ampGain,
    ampTCase,
    devIFan,
    devPFan,
    devFanState,
    devTime
    }
    STATUS current
	DESCRIPTION
		"A collection of objects providing remote configuration of
		management target translation parameters for use by
		proxy forwarder applications."
	::= { localGroups 1 }



setGroup OBJECT-GROUP
	OBJECTS {
    ampIPrLDSet,
    ampILDSet,
    ampPoutSet,
    ampGainSet,
    ampStMode,
    ampAmpAPS,
    ampAmpEnable,
    devPFanMax,
    devPFanMin,
    devTCaseMax,
    devTCaseMin,
    devPFanSet,
    devFanMode
    }
    STATUS current
	DESCRIPTION
		"A collection of objects providing remote configuration of
		management target translation parameters for use by
		proxy forwarder applications."
	::= { localGroups 2 }

infoGroup OBJECT-GROUP
	OBJECTS {
	devID,
	devSN,
	ampHwNumber,
	ampSwNumber
    }
    STATUS current
	DESCRIPTION
		"A collection of objects providing remote configuration of
		management target translation parameters for use by
		proxy forwarder applications."
	::= { localGroups 3 }


thGroup OBJECT-GROUP
	OBJECTS {
	ampPinHMax,
	ampPinCMax,
	ampPinWMax,
	ampPinWMin,
	ampPinCMin,
	ampPinHMin,

	ampPoutHMax,
	ampPoutCMax,
	ampPoutWMax,
	ampPoutWMin,
	ampPoutCMin,
	ampPoutHMin,

	ampGainHMax,
	ampGainCMax,
	ampGainWMax,
	ampGainWMin,
	ampGainCMin,
	ampGainHMin,

    ampIPrLDHMax,
    ampIPrLDCMax,
    ampIPrLDWMax,
    ampIPrLDWMin,
    ampIPrLDCMin,
    ampIPrLDHMin,

    ampILDHMax,
	ampILDCMax,
	ampILDWMax,
	ampILDWMin,
	ampILDCMin,
	ampILDHMin,

	ampTCaseHMax,
	ampTCaseCMax,
	ampTCaseWMax,
	ampTCaseWMin,
	ampTCaseCMin,
	ampTCaseHMin,

	ampIFanHMax,
	ampIFanCMax,
	ampIFanWMax,
	ampIFanWMin,
	ampIFanCMin,
	ampIFanHMin
    }
    STATUS current
	DESCRIPTION
		"A collection of objects providing remote configuration of
		management target translation parameters for use by
		proxy forwarder applications."
	::= { localGroups 4 }

END

