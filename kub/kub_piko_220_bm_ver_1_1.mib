Technotronics DEFINITIONS ::= BEGIN

IMPORTS
   mgmt, enterprises, experimental, IpAddress, Gauge, TimeTicks     FROM RFC1155-SMI
   DisplayString                                FROM RFC1213-MIB
   OBJECT-TYPE                                  FROM RFC-1212
   TRAP-TYPE                                    FROM RFC-1215
   SnmpSecurityModel,
   SnmpMessageProcessingModel,
   SnmpSecurityLevel,
   SnmpAdminString				      FROM SNMP-FRAMEWORK-MIB
   KeyChange						FROM SNMP-USER-BASED-SM-MIB
   TEXTUAL-CONVENTION			FROM SNMPv2-TC;

mib-2      			    OBJECT IDENTIFIER ::= { mgmt 1 }

-- textual conventions

DisplayString ::= OCTET STRING
-- This data type is used to model textual information taken
-- from the NVT ASCII character set.  By convention, objects
-- with this syntax are declared as having SIZE (0..255)

PhysAddress ::= OCTET STRING
-- This data type is used to model media addresses.  For many
-- types of media, this will be in a binary representation.
-- For example, an ethernet address would be represented as
-- a string of 6 octets.

-- groups in MIB-II

system       			    OBJECT IDENTIFIER ::= { mib-2 1 }
ttxDevices                          OBJECT IDENTIFIER ::= { experimental 55 }
technotronics                       OBJECT IDENTIFIER ::= { ttxDevices 1 } 
control                             OBJECT IDENTIFIER ::= { technotronics 2 }
snmpPort                            OBJECT IDENTIFIER ::= { technotronics 4 }

  ON-OFF          ::=   INTEGER { ON(1), OFF(0) }

MCHPUsmAuthPrivProtocol ::=  TEXTUAL-CONVENTION
       STATUS  current
       DESCRIPTION
           "This textual convention enumerates the authentication and privledge 
		protocol for USM configuration.
           "
       SYNTAX    INTEGER
 			{
				noAuthProtocol(1),
				hmacMD5Auth(2),
				hmacSHAAuth(3),
				noPrivProtocol(4),
				desPrivProtocol(5),
				aesPrivProtocol(6)
			}

sysDescr OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..255))
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION
            "A textual description of the entity.  This value
            should include the full name and version
            identification of the system's hardware type,
            software operating-system, and networking
            software.  It is mandatory that this only contain
            printable ASCII characters."
    ::= { system 1 }

sysObjectID OBJECT-TYPE
    SYNTAX  OBJECT IDENTIFIER
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION
            "The vendor's authoritative identification of the
            network management subsystem contained in the
            entity.  This value is allocated within the SMI
            enterprises subtree (1.3.6.1.4.1) and provides an
            easy and unambiguous means for determining `what
            kind of box' is being managed.  For example, if
            vendor `Flintstones, Inc.' was assigned the
            subtree 1.3.6.1.4.1.4242, it could assign the
            identifier 1.3.6.1.4.1.4242.1.1 to its `Fred
            Router'."
    ::= { system 2 }

sysUpTime OBJECT-TYPE
    SYNTAX  TimeTicks
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION
            "The time (in hundredths of a second) since the
            network management portion of the system was last
            re-initialized."
    ::= { system 3 }

sysContact OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..255))
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION
            "The textual identification of the contact person
            for this managed node, together with information
            on how to contact this person."
    ::= { system 4 }

sysName OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..15))
    ACCESS  read-write
    STATUS  mandatory
    DESCRIPTION
            "An administratively-assigned name for this
            managed node.  By convention, this is the node's
            fully-qualified domain name."
    ::= { system 5 }

sysLocation OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..15))
    ACCESS  read-write
    STATUS  mandatory
    DESCRIPTION
            "The physical location of this node (e.g.,
            `telephone closet, 3rd floor')."
    ::= { system 6 }

sysServices OBJECT-TYPE
    SYNTAX  INTEGER (0..127)
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION
            "A value which indicates the set of services that
            this entity primarily offers.

            The value is a sum.  This sum initially takes the
            value zero, Then, for each layer, L, in the range
            1 through 7, that this node performs transactions
            for, 2 raised to (L - 1) is added to the sum.  For
            example, a node which performs primarily routing
            functions would have a value of 4 (2^(3-1)).  In
            contrast, a node which is a host offering
            application services would have a value of 72
            (2^(4-1) + 2^(7-1)).  Note that in the context of
            the Internet suite of protocols, values should be
            calculated accordingly:

                 layer  functionality
                     1  physical (e.g., repeaters)
                     2  datalink/subnetwork (e.g., bridges)
                     3  internet (e.g., IP gateways)
                     4  end-to-end  (e.g., IP hosts)
                     7  applications (e.g., mail relays)

            For systems including OSI protocols, layers 5 and
            6 may also be counted."
    ::= { system 7 }

values OBJECT-TYPE
    SYNTAX SEQUENCE OF TrapEntry
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Таблица трапов"
    ::= { ttxDevices 2 }        
    
trapEntry OBJECT-TYPE
    SYNTAX TrapEntry
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Информация о получателе трапов"
    INDEX { trapReceiverNumber }
     ::= { values 1 }
     
trapEntry ::=
    SEQUENCE {
        trapReceiverNumber
            INTEGER,
        trapEnabled
            INTEGER,
        trapReceiverIPAddress
            IpAddress,
        trapCommunity
            DisplayString
    }            
    
trapReceiverNumber  OBJECT-TYPE
    SYNTAX INTEGER (0.. 4)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс приёмника трапов"
    ::= { trapEntry 1 }
    
trapEnabled OBJECT-TYPE
    SYNTAX INTEGER { Yes(1), No(0) }
    ACCESS read-write
    STATUS mandatory
    DESCRIPTION
        "Указывает, включена ли отправка трапов"
    ::= { trapEntry 2 }


trapReceiverIPAddress OBJECT-TYPE
    SYNTAX  IpAddress
    ACCESS  read-write
    STATUS mandatory
    DESCRIPTION
        "IP адрес получателя трапов"
    ::= { trapEntry 3 }

trapCommunity OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..7))
    ACCESS  read-write
    STATUS mandatory
    DESCRIPTION
        "Community, который будет использован при отправке трапов"
    ::= { trapEntry 4 }
  
-- Температура

extTemp OBJECT-TYPE
  SYNTAX  INTEGER (-70..120)
  ACCESS  read-only
  STATUS  mandatory
  DESCRIPTION
    "Температура выносного датчика, *С"
  ::= { control 1 }


-- МАК адрес

MACAddr OBJECT-TYPE
  SYNTAX  DisplayString (SIZE (17))
  ACCESS  read-only
  STATUS  mandatory
  DESCRIPTION
    "MAC адрес устройства"
  ::= { control 4 }


valuesPort OBJECT-TYPE
    SYNTAX SEQUENCE OF PortEntry
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Таблица портов"
    ::= { ttxDevices 3 }        
    
portEntry OBJECT-TYPE
    SYNTAX PortEntry
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Информация о данных портов"
    INDEX { portIndex }
     ::= { valuesPort 3 }
     
portEntry ::=
    SEQUENCE {
        portIndex
            INTEGER,
        portType
            INTEGER,
        portConfig
            INTEGER,
        portValue
            INTEGER,
        portMaxValue
            INTEGER,
        portMinValue
            INTEGER,
        portParam1
            INTEGER,
        portParam2
            INTEGER
    }            
    
portIndex  OBJECT-TYPE
    SYNTAX INTEGER (0.. 5)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 1 }  

portType  OBJECT-TYPE
    SYNTAX INTEGER (0.. 255)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 2 }  

portConfig  OBJECT-TYPE
    SYNTAX INTEGER (0.. 255)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 3 }  

portValue  OBJECT-TYPE
    SYNTAX INTEGER (0.. 4294967295)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 4 }  

portMaxValue  OBJECT-TYPE
    SYNTAX INTEGER (0.. 4294967295)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 5 }  

portMinValue  OBJECT-TYPE
    SYNTAX INTEGER (0.. 4294967295)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 6 }  

portParam1  OBJECT-TYPE
    SYNTAX INTEGER (0.. 65536)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 7 }  

portParam2  OBJECT-TYPE
    SYNTAX INTEGER (0.. 255)
    ACCESS not-accessible
    STATUS mandatory
    DESCRIPTION
        "Индекс портов"
    ::= { portEntry 8 }  

   
END   
