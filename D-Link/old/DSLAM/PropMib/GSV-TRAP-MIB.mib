GSV-TRAP-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE, NOTIFICATION-TYPE, mib-2,
    IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
        FROM SNMPv2-SMI
    TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress
	  FROM SNMPv2-TC
    OBJECT-GROUP, MODULE-COMPLIANCE
        FROM SNMPv2-CONF
    columbiaPackets
	  FROM GSV-ENTERPRISE-INFO-MIB
    adslAturPerfCurr1DayESs, adslAturPerfPrev1DayESs,
    adslAturPerfCurr1DayLprs, adslAturPerfPrev1DayLprs,
    adslAturPerfCurr1DayLoss, adslAturPerfPrev1DayLoss, 
    adslAturPerfCurr1DayLofs, adslAturPerfPrev1DayLofs,
    adslAtucPerfCurr1DayESs, adslAtucPerfPrev1DayESs,
    adslAtucPerfCurr1DayLprs, adslAtucPerfPrev1DayLprs,
    adslAtucPerfCurr1DayLols, adslAtucPerfPrev1DayLols,
    adslAtucPerfCurr1DayLoss, adslAtucPerfPrev1DayLoss,
    adslAtucPerfCurr1DayLofs, adslAtucPerfPrev1DayLofs
	  FROM ADSL-LINE-MIB
    adslAtucPerfCurr1DayUasL, adslAtucPerfPrev1DayUasL,
    adslAtucPerfCurr1DaySesL, adslAtucPerfPrev1DaySesL, 
    adslAturPerfCurr1DayUasL, adslAturPerfPrev1DayUasL,
    adslAturPerfCurr1DaySesL, adslAturPerfPrev1DaySesL
	  FROM ADSL-LINE-EXT-MIB
    gsvSystemConfigCfgStatus
	  FROM GSV-SYS-MIB
	ifIndex
	  FROM IF-MIB	
	gsvAdslAtucPerfDataExtnCurr15MinFecsL,
	gsvAdslAtucPerfDataExtnCurr1DayFecsL, gsvAdslAtucPerfDataExtnPrev1DayFecsL,
	gsvAdslAturPerfDataExtnCurr15MinFecsL,
	gsvAdslAturPerfDataExtnCurr1DayFecsL, gsvAdslAturPerfDataExtnPrev1DayFecsL,
	gsvAdslAlarmConfProfileExtAtucThresh15MinFecsL,
	gsvAdslAlarmConfProfileExtAturThresh15MinFecsL,	  
	gsvHdsl2ShdslFramerOHAndDefects 
	  FROM GSV-DSL-MIB
	gsvAtmInterfaceConfExtnCurrOrl,gsvAtmVcCurrentSensedEncapType,gsvAtmVclExtnVciIfIndex
		FROM GSV-ATM-MIB
		hdsl2ShdslEndpointCurrStatus
		FROM HDSL2-SHDSL-LINE-MIB;	  

gsvTrapMIB OBJECT IDENTIFIER ::= { columbiaPackets 500 }

gsvTrapObjectGroup		OBJECT IDENTIFIER ::= { gsvTrapMIB 1 }

gsvTrapAdslAtucOpstateChangePortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the Port-id of the port for which operational status has changed."
	::= { gsvTrapObjectGroup 1 }


gsvIpaddrGetFailIfIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the interface index for that DHCP client could not get an ip address from DHCP server."
	::= { gsvTrapObjectGroup 2}


gsvControlQueueIfIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the Interface Index for ethernet or atm interface that is in/out-of congestion."
	::= { gsvTrapObjectGroup 3 }


gsvStatsResetIfIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the ifIndex of Interface for which interface stats has been reset."
	::= { gsvTrapObjectGroup 4 }


gsvAdslChipLockUpStartXcvrIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the Xcvr Index for Xcvr Lock-Up related traps, or start of Xcvr Index for Adsl Chip Lock-Up."
	::= { gsvTrapObjectGroup 5 }


gsvAdslChipLockUpEndXcvrIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the end of Xcvr Index for Adsl Chip Lock-Up related traps."
	::= { gsvTrapObjectGroup 6 }
 
gsvTrapShdslOpStateChangePortId  OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the Port-id of the SHDSL port for which operational status has changed."
	::= { gsvTrapObjectGroup 7 }

gsvTrapShdslRmtAtmCellStatus  OBJECT-TYPE
	SYNTAX       BITS
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the atm cell status bits."
	::= { gsvTrapObjectGroup 8 }

gsvTrapShdslHecErrorCnt	OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the HEC error count."
	::= { gsvTrapObjectGroup 9 }

gsvTrapShdslUTCRecvd	OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the SHDSL UTC recieved in response of STU-R Config Request."
	::= { gsvTrapObjectGroup 10 }

gsvTrapShdslEOCMsgId	OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the message id of remote EOC request."
	::= { gsvTrapObjectGroup 11 }

gsvTrapShdslCommandClass	OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the command class in case of generic trap."
	::= { gsvTrapObjectGroup 12 }

gsvTrapShdslCommandResult	OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the result in case of generic trap."
	::= { gsvTrapObjectGroup 13 }

gsvAtmMinSchPrflReq	OBJECT-TYPE	
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the minimum bandwidth for a profile ."
	::= { gsvTrapObjectGroup 14 }

gsvAdslAtucChipLbusAccessFailedIfIndex 	OBJECT-TYPE	
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates ifIndex for gsvAdslAtucChipLbusAccessFailedTrap."
	::= { gsvTrapObjectGroup 15 }

gsvTrapVdslOpstateChangePortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the Port-id of the port for which operational status has changed."
	::= { gsvTrapObjectGroup 16 }

gsvAdslAtucIntChanCurrTxRate OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the actual transmit rate on this channel."
	::= { gsvTrapObjectGroup 17 }

gsvAdslAtucIntChanPrevTxRate OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the rate at the time of the last adslAtucInterChanRateChangeTrap event."
	::= { gsvTrapObjectGroup 18 }
	
gsvAdslAtucFastChanCurrTxRate OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the actual transmit rate on this channel."
	::= { gsvTrapObjectGroup 19 }	
	
gsvAdslAtucFastChanPrevTxRate OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the rate at the time of the last adslAtucFastChanRateChangeTrap event."
	::= { gsvTrapObjectGroup 20 }	
		
gsvCreateDslSystemStatus 	OBJECT-TYPE	
	SYNTAX		 INTEGER       
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object specifies the cause of failure and zero value signifies the success in create dsl system."
	::= { gsvTrapObjectGroup 21 }

gsvCreateDslChipId 	OBJECT-TYPE	
	SYNTAX		 INTEGER       
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the chipid."
	::= { gsvTrapObjectGroup 22 }
	
gsvCreateDslChipStatus 	OBJECT-TYPE	
	SYNTAX		 INTEGER       
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object specifies the cause of failure if it holds non zero value and zero value signifies the success in create dsl system."
	::= { gsvTrapObjectGroup 23 }

gsvPortMacAddrFirstTrackTrapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvPortMacAddrFirstTrackTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains the information pertaining to Mac Address First trap."
	::= { gsvTrapMIB 2 }


gsvPortMacAddrFirstTrackTrapEntry OBJECT-TYPE
	SYNTAX       GsvPortMacAddrFirstTrackTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvPortMacAddrFirstTrackTrapTable.
		The Table is indexed by gsvPortMacAddrFirstTrackTrapMacAddress."
	INDEX        { gsvPortMacAddrFirstTrackTrapMacAddress }
	::= { gsvPortMacAddrFirstTrackTrapTable 1 }

GsvPortMacAddrFirstTrackTrapEntry ::= SEQUENCE {
		gsvPortMacAddrFirstTrackTrapMacAddress  MacAddress,
		gsvPortMacAddrChangeTrackTrapPortId     Unsigned32
	}

 gsvPortMacAddrFirstTrackTrapMacAddress OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This object indicates the MacAddress value for 'gsvPortMacAddrFirstTrackTrap'."
	::= { gsvPortMacAddrFirstTrackTrapEntry 1}


gsvPortMacAddrChangeTrackTrapPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		"This object indicates the PortId value for 'gsvPortMacAddrFirstTrackTrap'."
	::= { gsvPortMacAddrFirstTrackTrapEntry 2}



gsvPortBindingChangedTrapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvPortBindingChangedTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains the information pertaining to port binding changed trap."
	::= { gsvTrapMIB 3}


gsvPortBindingChangedTrapEntry OBJECT-TYPE
	SYNTAX       GsvPortBindingChangedTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvPortBindingChangedTrapTable.
		The Table is indexed by gsvPortBindingChangedTrapFdbId,
		gsvPortBindingChangedTrapMacAddress."
	INDEX        { gsvPortBindingChangedTrapFdbId, gsvPortBindingChangedTrapMacAddress }
	::= { gsvPortBindingChangedTrapTable 1 }

GsvPortBindingChangedTrapEntry ::= SEQUENCE {
		gsvPortBindingChangedTrapFdbId     Unsigned32,
		gsvPortBindingChangedTrapMacAddress     MacAddress,
		gsvPortBindingChangedTrapPrevPortId     Unsigned32,
		gsvPortBindingChangedTrapCurrPortId     Unsigned32
	}


gsvPortBindingChangedTrapFdbId OBJECT-TYPE
	SYNTAX       Unsigned32 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Unique identifier to indicate Vlan Identifier to which this entry belongs."
	::= { gsvPortBindingChangedTrapEntry 1 }

gsvPortBindingChangedTrapMacAddress OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		" This object indicates the MAC address of this entry."
	::= { gsvPortBindingChangedTrapEntry 2 }


gsvPortBindingChangedTrapPrevPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the port on which this entry was learnt previously."
	::= { gsvPortBindingChangedTrapEntry 3 }


gsvPortBindingChangedTrapCurrPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the port on which this entry has been learned currently."
	::= { gsvPortBindingChangedTrapEntry 4 }



gsvPortMacAddrChangeTrackTrapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvPortMacAddrChangeTrackTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains the information pertaining to Mac Address changed trap."
	::= { gsvTrapMIB 4}


gsvPortMacAddrChangeTrackTrapEntry OBJECT-TYPE
	SYNTAX       GsvPortMacAddrChangeTrackTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvPortMacAddrChangeTrackTrapTable.
		The Table is indexed by gsvPortMacAddrChangeTrackTrapMacAddress."
	INDEX        { gsvPortMacAddrChangeTrackTrapMacAddress }
	::= { gsvPortMacAddrChangeTrackTrapTable 1 }

GsvPortMacAddrChangeTrackTrapEntry ::= SEQUENCE {
		gsvPortMacAddrChangeTrackTrapMacAddress     MacAddress,
		gsvPortMacAddrChangeTrackTrapPrevPortId     Unsigned32,
		gsvPortMacAddrChangeTrackTrapCurrPortId     Unsigned32
	}


gsvPortMacAddrChangeTrackTrapMacAddress OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		" This object indicates the MAC address of this entry."
	::= { gsvPortMacAddrChangeTrackTrapEntry 1 }


gsvPortMacAddrChangeTrackTrapPrevPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the port on which this entry was learnt previously."
	::= { gsvPortMacAddrChangeTrackTrapEntry 2 }


gsvPortMacAddrChangeTrackTrapCurrPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the port on which this entry has been learned currently."
	::= { gsvPortMacAddrChangeTrackTrapEntry 3 }



gsvAdslAtucPmStateTrapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAdslAtucPmStateTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains the information pertaining to Adsl Atuc Pm State Trap."
	::= { gsvTrapMIB 5}

gsvAdslAtucPmStateTrapEntry OBJECT-TYPE
	SYNTAX       GsvAdslAtucPmStateTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAdslAtucPmStateTrapTable.
		The Table is indexed by ifIndex."
	INDEX        { ifIndex }
	::= { gsvAdslAtucPmStateTrapTable 1 }

GsvAdslAtucPmStateTrapEntry ::= SEQUENCE {
		gsvAdslAtucPmStateCurrValue     Unsigned32,
		gsvAdslAtucPmStatePrevValue     Unsigned32
	}


gsvAdslAtucPmStateCurrValue   OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		" This object indicates the Current Value for PM State Trap."
	::= { gsvAdslAtucPmStateTrapEntry 1 }

gsvAdslAtucPmStatePrevValue    OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		" This object indicates the Previous Value for PM State Trap."
	::= { gsvAdslAtucPmStateTrapEntry 2 }


gsvAbondLinkStatusChngTrapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAbondLinkStatusChngTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains the information pertaining to abond Link Status Change Trap."
	::= { gsvTrapMIB 6}


gsvAbondLinkStatusChngTrapEntry OBJECT-TYPE
	SYNTAX       GsvAbondLinkStatusChngTrapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAbondLinkStatusChngTrapTable.
		The Table is indexed by gsvAbondLinkStatusIntfIndex , gsvAbondLinkStatusIndex."
	INDEX        {gsvAbondLinkStatusIntfIndex , gsvAbondLinkStatusIndex  }
	::= { gsvAbondLinkStatusChngTrapTable 1 }

GsvAbondLinkStatusChngTrapEntry ::= SEQUENCE {
		gsvAbondLinkStatusIntfIndex     INTEGER,
		gsvAbondLinkStatusIndex     INTEGER,
		gsvAbondASMLinkStatusCurrValue      INTEGER,
		gsvAbondASMLinkStatusPrevValue     INTEGER
	}


gsvAbondLinkStatusIntfIndex OBJECT-TYPE
	SYNTAX       INTEGER 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the ATM Based Multi pair Bonding type of interfaces."
	::= { gsvAbondLinkStatusChngTrapEntry 1 }

gsvAbondLinkStatusIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		" This specifies the interface index used for the abond link (DSL) interfaces."
	::= { gsvAbondLinkStatusChngTrapEntry 2 }


gsvAbondASMLinkStatusCurrValue OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the current value of Link Status as in the ASM Messages."
	::= { gsvAbondLinkStatusChngTrapEntry 3 }


gsvAbondASMLinkStatusPrevValue OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   accessible-for-notify
	STATUS       current
	DESCRIPTION
		" This object indicates the previous value of Link Status as in the ASM Messages."
	::= { gsvAbondLinkStatusChngTrapEntry 4 } 


gsvTraps  OBJECT IDENTIFIER ::= { gsvTrapMIB 200 }

gsvTrapPrefix  OBJECT IDENTIFIER ::= { gsvTraps  0 }


gsvAdslAtucOpstateChangeTrap      NOTIFICATION-TYPE
OBJECTS { gsvTrapAdslAtucOpstateChangePortId }
             STATUS  current
             DESCRIPTION
                 "This trap indicates the change in the operational status of the port."
         ::= { gsvTrapPrefix  1 }




gsvPortBindingInFdbChangedTrap	NOTIFICATION-TYPE
OBJECTS { gsvPortBindingChangedTrapPrevPortId, gsvPortBindingChangedTrapCurrPortId }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the port on which the mac address has been learned has changed."
         ::= { gsvTrapPrefix  2 }


gsvPortMacAddrChangeTrackTrap      NOTIFICATION-TYPE
OBJECTS { gsvPortMacAddrChangeTrackTrapPrevPortId, gsvPortMacAddrChangeTrackTrapCurrPortId }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the port on which the tracked MAC address is being received has changed."
         ::= { gsvTrapPrefix  3 }



gsvPortMacAddrFirstTrackTrap      NOTIFICATION-TYPE
OBJECTS { gsvPortMacAddrChangeTrackTrapPortId }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the particular mac address has been received for the first time. This trap will also be received if the tracked MAC address is received from an existing port and the port from which it was earlier received has been deleted by now."
         ::= { gsvTrapPrefix  4 }


gsvIpaddrGetFailTrap      NOTIFICATION-TYPE
OBJECTS { gsvIpaddrGetFailIfIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that DHCP client could not get an ip address from DHCP server."
         ::= { gsvTrapPrefix  5 }


gsvControlQueueCongestionStartTrap      NOTIFICATION-TYPE
OBJECTS { gsvControlQueueIfIndex }
             STATUS  current
             DESCRIPTION
                 "For ethernet or atm interface, this trap indicates that the interface is in congestion."
         ::= { gsvTrapPrefix  6 }

gsvControlQueueCongestionStopTrap      NOTIFICATION-TYPE
OBJECTS { gsvControlQueueIfIndex }
             STATUS  current
             DESCRIPTION
                 "For ethernet or atm interface, this trap indicates that the congestion on this interface has eased."
         ::= { gsvTrapPrefix  7 }

gsvInterfaceStatsResetTrap	      NOTIFICATION-TYPE
OBJECTS { gsvStatsResetIfIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that interface stats has been reset for an interface."
         ::= { gsvTrapPrefix  8 }

gsvAdslChipLockUpDetectedTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex, gsvAdslChipLockUpEndXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that all the Xcvrs in the chip have locked up."
         ::= { gsvTrapPrefix  9 }

gsvAdslChipLockUpRecoveryTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex, gsvAdslChipLockUpEndXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the chip has successfully recovered from the lock up condition."
         ::= { gsvTrapPrefix  10 }

gsvAdslChipLockUpRecoveryFailedTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex, gsvAdslChipLockUpEndXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the recovery from lockup condition of the chip has failed."
         ::= { gsvTrapPrefix  11 }

gsvAdslChipPreInitChkSumFailedTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex, gsvAdslChipLockUpEndXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that the pre-init checksum calculation for the chip failed."
         ::= { gsvTrapPrefix  12 }

gsvAdslXcvrLockUpDetectedTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that an Xcvr lockup has been detected."
         ::= { gsvTrapPrefix  13 }

gsvAdslXcvrLockUpRecoveryTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates successful recovery of an Xcvr from the lockup condition."
         ::= { gsvTrapPrefix  14 }

gsvAdslXcvrLockUpRecoveryFailedTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslChipLockUpStartXcvrIndex }
             STATUS  current
             DESCRIPTION
                 "This trap indicates the failure of Xcvr's recovery from lockup."
         ::= { gsvTrapPrefix  15 }


gsvAdslAtucPerfLofsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayLofs, adslAtucPerfPrev1DayLofs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Framing 1-Day interval threshold for ATUC has reached."
         ::= { gsvTrapPrefix  16 }


gsvAdslAtucPerfLossThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayLoss, adslAtucPerfPrev1DayLoss }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Signal 1-Day interval threshold for ATUC has reached."
         ::= { gsvTrapPrefix  17 }


gsvAdslAtucPerfLolsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayLols, adslAtucPerfPrev1DayLols }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Link 1-Day interval threshold for ATUC has reached."
         ::= { gsvTrapPrefix  18 }


gsvAdslAtucPerfLprsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayLprs, adslAtucPerfPrev1DayLprs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Power 1-Day interval threshold for ATUC has reached."
         ::= { gsvTrapPrefix  19 }


gsvAdslAtucPerfESsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayESs, adslAtucPerfPrev1DayESs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Errored Second 1-Day interval threshold for ATUC has reached."
         ::= { gsvTrapPrefix  20 }


gsvAdslAtucPerfSesLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DaySesL, adslAtucPerfPrev1DaySesL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Severely Errored Seconds-line 1-Day threshold for ATUC has reached."
         ::= { gsvTrapPrefix  21 }


gsvAdslAtucPerfUasLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAtucPerfCurr1DayUasL, adslAtucPerfPrev1DayUasL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Unavailable Seconds-line 1-Day threshold for ATUC has reached."
         ::= { gsvTrapPrefix  22 }



gsvAdslAturPerfLofsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DayLofs, adslAturPerfPrev1DayLofs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Framing 1-Day interval threshold for ATUR has reached."
         ::= { gsvTrapPrefix  23 }


gsvAdslAturPerfLossThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DayLoss, adslAturPerfPrev1DayLoss }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Signal 1-Day interval threshold for ATUR has reached."
         ::= { gsvTrapPrefix  24 }


gsvAdslAturPerfLprsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DayLprs, adslAturPerfPrev1DayLprs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Loss of Power 1-Day interval threshold for ATUR has reached."
         ::= { gsvTrapPrefix  25 }


gsvAdslAturPerfESsThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DayESs, adslAturPerfPrev1DayESs }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Errored Second 1-Day interval threshold for ATUR has reached."
         ::= { gsvTrapPrefix  26 }


gsvAdslAturPerfSesLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DaySesL, adslAturPerfPrev1DaySesL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Severely Errored Seconds-line 1-Day threshold for ATUR has reached."
         ::= { gsvTrapPrefix  27 }


gsvAdslAturPerfUasLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { adslAturPerfCurr1DayUasL, adslAturPerfPrev1DayUasL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Unavailable Seconds-line 1-Day threshold for ATUR has reached."
         ::= { gsvTrapPrefix  28 }



-- **********************************************************************
-- ********** gsvCommitStatusTrap is currently not supported ************
-- **********************************************************************

gsvCommitStatusTrap	      NOTIFICATION-TYPE
OBJECTS     { gsvSystemConfigCfgStatus }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the status of commit operation. Object 'gsvSystemConfigCfgStatus' specifies if the last configuration save was a success or failure."
         ::= { gsvTrapPrefix  29 }

gsvPppoeMaxDiscDoneTrap	      NOTIFICATION-TYPE
OBJECTS     { ifIndex }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the maximum retries in discovery stage have exceeded for a PPPoE interface."
         ::= { gsvTrapPrefix  30}       

gsvAdslAtucPerfFecsLThreshTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslAtucPerfDataExtnCurr15MinFecsL, gsvAdslAlarmConfProfileExtAtucThresh15MinFecsL }
             STATUS  current
             DESCRIPTION       
                  "This trap indicates that Forward error correction seconds 15-Min threshold for ATUR has reached."
         ::= { gsvTrapPrefix  31 }
         
gsvAdslAtucPerfFecsLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslAtucPerfDataExtnCurr1DayFecsL, gsvAdslAtucPerfDataExtnPrev1DayFecsL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Forward error correction seconds 15-Min threshold for ATUC has reached."
         ::= { gsvTrapPrefix  32 }
         
gsvAdslAturPerfFecsLThreshTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslAturPerfDataExtnCurr15MinFecsL,	gsvAdslAlarmConfProfileExtAturThresh15MinFecsL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Forward error correction seconds 15-Min threshold for ATUR has reached."                  
         ::= { gsvTrapPrefix  33 }
         
gsvAdslAturPerfFecsLThresh1DayTrap	      NOTIFICATION-TYPE
OBJECTS { gsvAdslAturPerfDataExtnCurr1DayFecsL, gsvAdslAturPerfDataExtnPrev1DayFecsL }
             STATUS  current
             DESCRIPTION
                 "This trap indicates that Forward error correction seconds 1-Day threshold for ATUR has reached."
         ::= { gsvTrapPrefix  34 }

gsvHdsl2ShdslFramerOHAndDefects	      NOTIFICATION-TYPE
OBJECTS     { gsvHdsl2ShdslFramerOHAndDefects }
          STATUS  current
          DESCRIPTION
                "This trap indicates the Framer Overhead and Defects"
         ::= { gsvTrapPrefix  35 }

gsvTrapShdslOpStateChangePortId	      NOTIFICATION-TYPE
OBJECTS     { gsvTrapShdslOpStateChangePortId }
          STATUS  current
          DESCRIPTION
                "This trap indicates the change in the operational status of the port."
         ::= { gsvTrapPrefix  36 }

gsvShdslRmtAtmCellStatusTrap	      NOTIFICATION-TYPE
OBJECTS     { gsvTrapShdslRmtAtmCellStatus , gsvTrapShdslHecErrorCnt}
          STATUS  current
          DESCRIPTION
                "This trap indicates the SHDSL Remote ATM Cell Status Response."
         ::= { gsvTrapPrefix  37 }
                                        
gsvShdslConfReqUtcTrap	      NOTIFICATION-TYPE
OBJECTS     { gsvTrapShdslUTCRecvd }
          STATUS  current
          DESCRIPTION
                "This trap indicates the SHDSL UTC recieved in response of STU-R Config Request."
         ::= { gsvTrapPrefix  38 }
                                        
gsvShdslRmtEOCUtcTrap	      NOTIFICATION-TYPE
OBJECTS     { gsvTrapShdslEOCMsgId }
          STATUS  current
          DESCRIPTION
                "This trap indicates the SHDSL UTC Recieved in response of Remote EOC request."
         ::= { gsvTrapPrefix  39 }
         
gsvShdslGenericFailureTrap	      NOTIFICATION-TYPE
OBJECTS     { gsvTrapShdslCommandClass, gsvTrapShdslCommandResult }
          STATUS  current
          DESCRIPTION
                "This trap indicates the SHDSL Generic Failure Trap."
         ::= { gsvTrapPrefix  40 }

gsvAtmPortUnderDeficitTrap	      NOTIFICATION-TYPE
OBJECTS     {gsvAtmInterfaceConfExtnCurrOrl , gsvAtmMinSchPrflReq }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the atm port is under deficit as per rate required by it's classes based on the scheduling profile applied to the ATM port."
         ::= { gsvTrapPrefix  41 }

gsvAtmPortOutOfDeficitTrap	      NOTIFICATION-TYPE
OBJECTS     {gsvAtmInterfaceConfExtnCurrOrl , gsvAtmMinSchPrflReq }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the atm port has come out of deficit."
         ::= { gsvTrapPrefix  42 }

gsvAsaAtmVcEncapTypeChangedTrap		      NOTIFICATION-TYPE
OBJECTS     { gsvAtmVclExtnVciIfIndex , gsvAtmVcCurrentSensedEncapType }
          STATUS  current
          DESCRIPTION
                "This trap indicates that Autosensing agent has changed the ATM VC AAL5 Encapsulation Type."
         ::= { gsvTrapPrefix  43 }
         
gsvAsaCfgTearDownFailTrap      NOTIFICATION-TYPE
OBJECTS     { gsvAtmVclExtnVciIfIndex }
          STATUS  current
          DESCRIPTION
                "This trap indicates that Auto Sensing Agent is unable to Tear Down the current stack due to configuration change."
         ::= { gsvTrapPrefix  44 }         
               
gsvAdslAtucPmStateChangeTrap      NOTIFICATION-TYPE
OBJECTS     { gsvAdslAtucPmStateCurrValue, gsvAdslAtucPmStatePrevValue}
          STATUS  current
          DESCRIPTION
                "This trap indicates that Auto Sensing Agent is unable to Tear Down the current stack due to configuration change."
         ::= { gsvTrapPrefix  45 }

gsvDslChipLbusAccessFailedTrap      NOTIFICATION-TYPE
OBJECTS     { gsvAdslAtucChipLbusAccessFailedIfIndex }
          STATUS  current
          DESCRIPTION
                "This trap indicates that Auto Sensing Agent is unable to Tear Down the current stack due to configuration change."
         ::= { gsvTrapPrefix  46 }               
         
gsvVdslOpstateChangeTrap      NOTIFICATION-TYPE
OBJECTS { gsvTrapVdslOpstateChangePortId }
             STATUS  current
             DESCRIPTION
                 "This trap indicates the change in the operational status of the port."
         ::= { gsvTrapPrefix  47 }               
         
gsvAbondLinkTxStatusChngTrap		NOTIFICATION-TYPE
OBJECTS     { gsvAbondASMLinkStatusCurrValue, gsvAbondASMLinkStatusPrevValue }
          STATUS  current
          DESCRIPTION
                "This trap indicates that Abond Link Tx Status Changed."
         ::= { gsvTrapPrefix  48 }         
         
gsvAbondLinkRxStatusChngTrap		NOTIFICATION-TYPE
OBJECTS     { gsvAbondASMLinkStatusCurrValue, gsvAbondASMLinkStatusPrevValue }
          STATUS  current
          DESCRIPTION
                "This trap indicates that Abond Link Rx Status Changed."
         ::= { gsvTrapPrefix  49 }   
         
gsvAdslAtucInterChanRateChangeTrap		NOTIFICATION-TYPE
OBJECTS     { gsvAdslAtucIntChanCurrTxRate, gsvAdslAtucIntChanPrevTxRate }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the ADSL ATUCs Interleave channel transmit rate has changed"
         ::= { gsvTrapPrefix  50 } 
         
gsvAdslAtucFastChanRateChangeTrap		NOTIFICATION-TYPE
OBJECTS     { gsvAdslAtucFastChanCurrTxRate, gsvAdslAtucFastChanPrevTxRate }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the ADSL ATUCs Fast channel transmit rate has changed"
         ::= { gsvTrapPrefix  51 }

gsvCreateDslSystemStatusTrap      NOTIFICATION-TYPE
OBJECTS     { gsvCreateDslSystemStatus }
          STATUS  current
          DESCRIPTION
                "This trap indicates that the whether DSL System is successfully created or not."
         ::= { gsvTrapPrefix  52 }
         
gsvCreateDslChipStatusTrap      NOTIFICATION-TYPE
OBJECTS     { gsvCreateDslChipId, gsvCreateDslChipStatus }
          STATUS  current
          DESCRIPTION
                "This trap indicates that whether the DSL Chip Creation is successful or not."
         ::= { gsvTrapPrefix  53 }                                         

gsvShdslLineLoswFailureTrap      NOTIFICATION-TYPE
OBJECTS     { hdsl2ShdslEndpointCurrStatus }
          STATUS  current
          DESCRIPTION
                "This trap indicates that a LOSW Failure event has occured on the DSL line."
         ::= { gsvTrapPrefix  54 }   
END
