GSV-SNTP-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2  
		      FROM SNMPv2-SMI
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TRAP-TYPE
		      FROM RFC-1215
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress
    		      FROM SNMPv2-TC
	  	RowStatus
		      FROM SNMPv2-TC
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvSntpMIB MODULE-IDENTITY
    LAST-UPDATED "200405110000Z"	       -- 11 May 2004
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO "Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
     " The MIB module that provides objects that are proprietary to GSPN. The MIB includes following propritery Groups/Tables.
	1.)gsvSntpClientConfGroup
	2.)gsvSntpClientStatsGroup
	3.)gsvSntpServerListTable"		

-- Revision History

       REVISION	    "200305200000Z"	       -- 20 May 2003
       DESCRIPTION  "This is the initial version of this MIB."
       
       REVISION	    "200405110000Z"	       -- 11 May 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."
       
    ::= { columbiaPackets 11 }


gsvSntpClientConfGroup		OBJECT IDENTIFIER ::= { gsvSntpMIB 1 }


gsvSntpClientConfStatusEnable OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This specifies whether the SNTP service is enabled or disabled. 'true' means that SNTP is enabled and 'false' means that SNTP is disabled."
	DEFVAL   { false }
	::= { gsvSntpClientConfGroup 1 }

gsvSntpClientStatsGroup		OBJECT IDENTIFIER ::= { gsvSntpMIB 2 }


gsvSntpClientStatsNumRequest OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the number of requests sent to SNTP Server."
	::= { gsvSntpClientStatsGroup 1 }


gsvSntpClientStatsNumResponse OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the Number of responses received from SNTP Server."
	::= { gsvSntpClientStatsGroup 2 }


gsvSntpClientStatsNumInvalidResponse OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the Number of responses received from SNTP Server."
	::= { gsvSntpClientStatsGroup 3 }


gsvSntpClientStatsNumLostResponse OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the number of responses which do not come within time limit."
	::= { gsvSntpClientStatsGroup 4 }


gsvSntpClientStatsLastSetSysTime OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies time at which the local clock was last set or corrected. The display format shall be mm/dd/yyyy:hr:min:sec."
	::= { gsvSntpClientStatsGroup 5 }


gsvSntpClientStatsReset OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This shall reset the stats. Only true value is supported for this field."
	::= { gsvSntpClientStatsGroup 6 }


gsvSntpServerListTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvSntpServerListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains information of sntp server."
	::= { gsvSntpMIB 3 }


gsvSntpServerListEntry OBJECT-TYPE
	SYNTAX       GsvSntpServerListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvSntpServerListTable.
		The Table is indexed by gsvSntpServerListIPAddress and gsvSntpServerListName."
	INDEX        { gsvSntpServerListIPAddress }
	::= { gsvSntpServerListTable 1 }

GsvSntpServerListEntry ::= SEQUENCE {
		gsvSntpServerListIPAddress     IpAddress,
		gsvSntpServerListName     DisplayString,
		gsvSntpServerListOperStatus     TruthValue,
		gsvSntpServerListRowStatus     RowStatus }


gsvSntpServerListIPAddress OBJECT-TYPE
	SYNTAX       IpAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the IP Address of the SNTP Server."
	::= { gsvSntpServerListEntry 1 }


gsvSntpServerListName OBJECT-TYPE
	SYNTAX       DisplayString (SIZE ( 0..64 ) )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the IP Address of the SNTP Server. Currently this object is not in use and can not be modified."
	::= { gsvSntpServerListEntry 2 }


gsvSntpServerListOperStatus OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the operational status of the server. 'true' means Server is in Use. 'false' means Server is in standby mode i.e. not in use."
	::= { gsvSntpServerListEntry 3 }


gsvSntpServerListRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvSntpServerListEntry 4 }
END 
