GSV-LACP-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,  
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress,
	  	RowStatus
		      FROM SNMPv2-TC
		dot3adAggIndex, dot3adAggPortIndex
			FROM IEEE8023-LAG-MIB
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvLacpMIB MODULE-IDENTITY
    LAST-UPDATED "200411160000Z"	       -- 16 Nov 2004
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO "Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
     " The MIB module that provides objects that are proprietary and extention to tables in IEEE803-LAG-MIB. The MIB includes extension to following Tables.
	1.) dot3adAggTable
	2.) dot3adAggPortTable
	3.) dot3adAggPortStatsTable"

-- Revision History

       REVISION	    "200112030000Z"	       -- 03 December 2002
       DESCRIPTION  "This is the initial version of this MIB."
       
       REVISION	    "200405110000Z"	       -- 11 May 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."

       REVISION	    "200411160000Z"	       -- 16 Nov 2004
       DESCRIPTION  "This MIB was updated for 'VC Aggrigation support' changes."
       
    ::= { columbiaPackets 5 }




gsvDot3adAggExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvDot3adAggExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains information about every Aggregator that is associated with this System. It is an extension to dot3adAggTable defined in IEEE Std 802.3, 2000 Edition. All LACP related snmp operations shall fail, if aggregator interface is not created. LACP aggregator shall not be created, if Redundancy aggregator is created. All LACP related snmp operations shall fail, if LACP Aggregator is not created."
	::= { gsvLacpMIB 1 }


gsvDot3adAggExtnEntry OBJECT-TYPE
	SYNTAX       GsvDot3adAggExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvDot3adAggExtnTable.
		The Table is indexed by dot3adAggIndex."
	INDEX        { dot3adAggIndex }
	::= { gsvDot3adAggExtnTable 1 }

GsvDot3adAggExtnEntry ::= SEQUENCE {
		gsvDot3adAggExtnAggregationType     INTEGER,
		gsvDot3adAggExtnRowStatus     RowStatus }


gsvDot3adAggExtnAggregationType OBJECT-TYPE
	SYNTAX       INTEGER{
			static(1),
			lacp(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Aggregation type done over the aggregator."
	DEFVAL   { 1 }
	::= { gsvDot3adAggExtnEntry 1 }


gsvDot3adAggExtnRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvDot3adAggExtnEntry 2 }


gsvDot3adAggPortExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvDot3adAggPortExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains contains Link Aggregation Control configuration information about every Aggregation Port associated with this device. This is an extension to dot3adAggPortTable defined in IEEE Std 802.3, 2000 Edition."
	::= { gsvLacpMIB 2 }


gsvDot3adAggPortExtnEntry OBJECT-TYPE
	SYNTAX       GsvDot3adAggPortExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvDot3adAggPortExtnTable.
		The Table is indexed by dot3adAggPortIndex."
	INDEX        { dot3adAggPortIndex }
	::= { gsvDot3adAggPortExtnTable 1 }

GsvDot3adAggPortExtnEntry ::= SEQUENCE {
		gsvDot3adAggPortExtnAdminStatus     INTEGER,
		gsvDot3adAggPortPktPriority     INTEGER }


gsvDot3adAggPortExtnAdminStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"Whether or not aggregation(bonding) is to be enabled over this Aggregation Port."
	DEFVAL   { 1 }
	::= { gsvDot3adAggPortExtnEntry 1 }


gsvDot3adAggPortPktPriority OBJECT-TYPE
	SYNTAX       INTEGER ( 0..7 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"For LACP PDUs, this priority shall be used for choice of traffic class/Queue on outgoing interface."
	DEFVAL   { 0 }
	::= { gsvDot3adAggPortExtnEntry 2 }


gsvDot3adAggPortStatsExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvDot3adAggPortStatsExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table is an extension to dot3adAggPortStatsTable defined in IEEE8023-LAG-MIB."
	::= { gsvLacpMIB 3 }


gsvDot3adAggPortStatsExtnEntry OBJECT-TYPE
	SYNTAX       GsvDot3adAggPortStatsExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvDot3adAggPortStatsExtnTable.
		The Table is indexed by dot3adAggPortIndex."
	INDEX        { dot3adAggPortIndex }
	::= { gsvDot3adAggPortStatsExtnTable 1 }

GsvDot3adAggPortStatsExtnEntry ::= SEQUENCE {
		gsvDot3adAggPortStatsExtnResetStats     TruthValue }

gsvDot3adAggPortStatsExtnResetStats OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The setting of this parameter results in statistics reset.The only value supported is 'true'."
	::= { gsvDot3adAggPortStatsExtnEntry 1 }
END 
