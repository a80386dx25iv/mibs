
GSV-PPPR-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress,
	  	RowStatus
		      FROM SNMPv2-TC
		ifIndex
            	FROM IF-MIB
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvPpprMIB MODULE-IDENTITY
    LAST-UPDATED "200504120000Z"	    -- 12 April 2005
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
   " The MIB module that provides objects that are proprietary to GSPN. The MIB includes following propritery Tables.
	1.) gsvPpprIfCfgTable "

-- Revision History

       REVISION	    "200404050000Z"	       -- 05 April 2004
       DESCRIPTION  "This is the initial version of this MIB."

       REVISION	    "200404240000Z"	       -- 24 April 2004
       DESCRIPTION  "PPPoA to PPPoE Tunneling- architectural and bridging changes."

       REVISION	    "200407290000Z"	       -- 29 July 2004
       DESCRIPTION  "Updated the Organization and Contact-Info." 

       REVISION	    "200411160000Z"	       -- 16 Nov 2004
       DESCRIPTION  "This MIB was updated for 'VC Aggrigation support' changes."
       REVISION	    "200504120000Z"	    -- 12 April 2005
       DESCRIPTION  "Updated the MIB for feature 'Packet VDSL interface on the CPE side for Columbia'."       
       
    ::= { columbiaPackets 19 }




gsvPpprIfCfgTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvPpprIfCfgEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table stores the configuration of a PPP relay Interface."
	::= { gsvPpprMIB 1 }


gsvPpprIfCfgEntry OBJECT-TYPE
	SYNTAX       GsvPpprIfCfgEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvPpprIfCfgTable.
		The Table is indexed by gsvPpprIfCfgIfIndex."
	INDEX        { gsvPpprIfCfgIfIndex }
	::= { gsvPpprIfCfgTable 1 }

GsvPpprIfCfgEntry ::= SEQUENCE {
		gsvPpprIfCfgIfIndex     Unsigned32,
		gsvPpprIfCfgLowerIntf     Unsigned32,
		gsvPpprIfCfgMaxPDUSize     Integer32,
		gsvPpprIfCfgAckTimer     Integer32,
		gsvPpprIfCfgLowIfToggleTimerTO     Integer32,
		gsvPpprIfCfgNature     INTEGER,
		gsvPpprIfCfgConfigMode     BITS,
		gsvPpprIfCfgPktPriority     INTEGER,
		gsvPpprIfCfgRowStatus     RowStatus }


gsvPpprIfCfgIfIndex OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"The Interface Index of PPP relay interface."
	::= { gsvPpprIfCfgEntry 1 }


gsvPpprIfCfgLowerIntf OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the lower interface index over which PPP relay interface can be built. The valid values are the interface indices of the AAL5 interface. This object must be specified at the time of row creation and can not be modified once the 'gsvppprIfCfgRowStatus' is made 'active'. The lower ATM VC interface cannot have atmVCCAAL5EncapType  as Ethernet"
	::= { gsvPpprIfCfgEntry 2 }


gsvPpprIfCfgMaxPDUSize OBJECT-TYPE
      SYNTAX       Integer32 ( 0..1492 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the Maximum PDU size on a PPP relay interface. This object can be specified at the time of row creation and can not be modified once the 'gsvppprIfCfgRowStatus' is made 'active'."
	DEFVAL   { 1492 }
	::= { gsvPpprIfCfgEntry 3 }


gsvPpprIfCfgAckTimer OBJECT-TYPE
	SYNTAX       Integer32 ( 0..10 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Time  in seconds to wait for LCP terminate Ack, after sending terminate request."
	DEFVAL   { 5 }
	::= { gsvPpprIfCfgEntry 4 }


gsvPpprIfCfgLowIfToggleTimerTO OBJECT-TYPE
	SYNTAX       Integer32 ( 0..10 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Time in seconds to wait for lower interface to comeup without tearing down the PPP relay session."
	DEFVAL   { 5 }
	::= { gsvPpprIfCfgEntry 5 }


gsvPpprIfCfgNature OBJECT-TYPE
	SYNTAX       INTEGER{
			dynamic(0),
			static(1)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Specifies if the interface is dynamic or static in nature."
	DEFVAL   { 0 }
	::= { gsvPpprIfCfgEntry 6 }


gsvPpprIfCfgConfigMode OBJECT-TYPE
	SYNTAX       BITS{
			auto(0)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This mode describes the auto sensing status for this interface. If the 'auto' sensing bit is set then this interface shall be sensed. If none of the bits is specified, then this interaface shall not be sensed."
	::= { gsvPpprIfCfgEntry 7 }


gsvPpprIfCfgPktPriority OBJECT-TYPE
	SYNTAX       INTEGER ( 0..7 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Priority to be set in tagged  PPPOE  frames  or  PPP packets sent over this port  from Control Plane .This priority shall also be used for choice of traffic class/Queue on outgoing interface whether the frame is tagged or not.In case the bridge port is over an Aggregated ATM VC,  this will also be used to identify the VC, on which the packet is to be sent."
	DEFVAL   { 0 }
	::= { gsvPpprIfCfgEntry 8 }


gsvPpprIfCfgRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvPpprIfCfgEntry 9 }

END 
