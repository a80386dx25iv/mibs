GSV-ATM-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress,
	  	RowStatus
		      FROM SNMPv2-TC
		ifIndex
            	FROM IF-MIB
		atmVclVpi, atmVclVci
			FROM ATM-MIB
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvAtmMIB MODULE-IDENTITY
    LAST-UPDATED "200504130000Z"	       -- 13 Apr 2005
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
     " The MIB module that provides objects that are proprietary and extension to ATM-MIB and ATM2-MIB. The MIB include extension to following Tables.
	1.) atmAal5VclStatTable
	2.) atmInterfaceConfTable
	3.) atmVclTable
	4.) atmVclStatTable "

-- Revision History

       REVISION	    "200212030000Z"	       -- 03 December 2002
       DESCRIPTION  "This is the initial version of this MIB."

       REVISION	    "200312040000Z"	       -- 04 December 2003
       DESCRIPTION  "The reset support for atmVclStatTable and atmAal5VclStatTable deprecated."

       REVISION	    "200404240000Z"	       -- 24 April 2004
       DESCRIPTION  "PPPoA to PPPoE Tunneling- architectural and bridging changes."

       REVISION	    "200405110000Z"	       -- 11 May 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."

       REVISION	    "200406280000Z"	       -- 28 June 2004
       DESCRIPTION  "Updated the mib description of few fields of table gsvAtmVclExtnTable."
       
       REVISION	    "200407220000Z"	       -- 22 July 2004
       DESCRIPTION  "Auto Detection of LLC and VC changes."

       REVISION	    "200407270000Z"	    -- 27 July 2004
       DESCRIPTION  "Updated the MIB for feature 'Rate Limiting - Qos Requirements for Egress rate Limiting per queue'."      

       REVISION	    "200409010000Z"	    -- 01 September 2004
       DESCRIPTION  "Updated the MIB for feature 'DP-CP Queues Configuration'."

       REVISION	    "200409150000Z"	    -- 15 Sept 2004
       DESCRIPTION  "Updated gsvAtmInterfaceConfExtnTable table  for Qos feature."       

       REVISION	    "200410290000Z"	    -- 29 Oct 2004
       DESCRIPTION  "Updated for IPOA/IPOE support."       

	   REVISION	    "200504040000Z"	    -- 04 April 2005
       DESCRIPTION  "Updated for new field of CtlPkt GroupId and VDSL Packet"       
       
	   REVISION	    "200504120000Z"	    -- 12 April 2005
       DESCRIPTION  "Updated the MIB for feature 'Packet VDSL interface on the CPE side for Columbia'."       

	   REVISION	    "200504130000Z"	       -- 13 Apr 2005
       DESCRIPTION  "MIB Updated for Filtering changes."

    ::= { columbiaPackets 1 }


gsvAtmAal5VclStatExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAtmAal5VclStatExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       deprecated
	DESCRIPTION
		"This table provides an object used for reseting the stats information in atmAal5VclStatTable and aal5VccTable."
	::= { gsvAtmMIB 1 }


gsvAtmAal5VclStatExtnEntry OBJECT-TYPE
	SYNTAX       GsvAtmAal5VclStatExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       deprecated
	DESCRIPTION
		"An entry (conceptual row) in the gsvAtmAal5VclStatExtnTable.
		The Table is indexed by ifIndex, atmVclVpi and atmVclVci."
	INDEX        { ifIndex, atmVclVpi, atmVclVci }
	::= { gsvAtmAal5VclStatExtnTable 1 }

GsvAtmAal5VclStatExtnEntry ::= SEQUENCE {
		gsvAtmAal5VclStatExtnResetStats     TruthValue }

gsvAtmAal5VclStatExtnResetStats OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       deprecated
	DESCRIPTION
		"The setting of this parameter results in statistics reset. The reset can only be done in case the AAL5 interface is administratively down.The only value supported is 'true'."
	::= { gsvAtmAal5VclStatExtnEntry 1 }



gsvAtmInterfaceConfExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAtmInterfaceConfExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains ATM local interface configuration parameters, one entry per ATM interface port. This Table is an extension to atmInterfaceConfTable."
	::= { gsvAtmMIB 2 }


gsvAtmInterfaceConfExtnEntry OBJECT-TYPE
	SYNTAX       GsvAtmInterfaceConfExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAtmInterfaceConfExtnTable.
		The Table is indexed by ifIndex."
	INDEX        { ifIndex }
	::= { gsvAtmInterfaceConfExtnTable 1 }

GsvAtmInterfaceConfExtnEntry ::= SEQUENCE {
		gsvAtmInterfaceConfExtnLowerIfIndex     INTEGER,
		gsvAtmInterfaceConfExtnOAMSourceID     OCTET STRING,
		gsvAtmInterfaceConfExtnOrl     INTEGER,
		gsvAtmInterfaceConfExtnTrfClassPrflId     INTEGER,
		gsvAtmInterfaceConfExtnUnknownVpi     INTEGER,
		gsvAtmInterfaceConfExtnUnknownVci     INTEGER,
		gsvAtmInterfaceConfExtnSchdPrflName     DisplayString,  
		gsvAtmInterfaceConfExtnCurrOrl     INTEGER,
		gsvAtmInterfaceConfExtnCtrlPktInstanceId     INTEGER,
		gsvAtmInterfaceConfExtnTransportType     INTEGER,	
		gsvAtmInterfaceConfExtnMirrorMode     INTEGER,
		gsvAtmInterfaceConfExtnRowStatus     RowStatus }


gsvAtmInterfaceConfExtnLowerIfIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This is the IfIndex of the Low interface (  DSL port or Abond interface) on which this ATM port is configured. This object is mandatory at the time of row creation but can not be modified once the row is created."
	::= { gsvAtmInterfaceConfExtnEntry 1 }


gsvAtmInterfaceConfExtnOAMSourceID OBJECT-TYPE
	SYNTAX       OCTET STRING (SIZE ( 0..16 ) )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the loop back source id used for the OAM loop back testing."
	DEFVAL   { "0xffffffffffffffffffffffffffffffff" }
	::= { gsvAtmInterfaceConfExtnEntry 2 }


gsvAtmInterfaceConfExtnOrl OBJECT-TYPE
	SYNTAX       INTEGER ( 64..50000 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This object specifies the output rate limiting value KBPS to be applied on this interface."
	DEFVAL   { 24000 }
	::= { gsvAtmInterfaceConfExtnEntry 3 }


gsvAtmInterfaceConfExtnTrfClassPrflId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..10 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the traffic class profile identifier to be associated with the ATM port."
	DEFVAL   { 1 }
	::= { gsvAtmInterfaceConfExtnEntry 4 }


gsvAtmInterfaceConfExtnUnknownVpi OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This parameter specifies the last seen unknown VPI on this ATM interface."
	::= { gsvAtmInterfaceConfExtnEntry 5 }


gsvAtmInterfaceConfExtnUnknownVci OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This parameter specifies the last seen unknown VCI on this ATM interface."
	::= { gsvAtmInterfaceConfExtnEntry 6 }


gsvAtmInterfaceConfExtnSchdPrflName OBJECT-TYPE
	SYNTAX       DisplayString (SIZE ( 0..32 ) )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the scheduling profile to be associated with the ATM port."
	DEFVAL   { "SPPROFILE" }
	::= { gsvAtmInterfaceConfExtnEntry 7 }


gsvAtmInterfaceConfExtnCurrOrl OBJECT-TYPE
	SYNTAX       INTEGER ( 0..50000 )
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This parameter specifies the current output rate value in KBPS that is available on this interface, based on the minimum of DSL trained rate and OutPut Rate limit configured for the ATM port."
	::= { gsvAtmInterfaceConfExtnEntry 8 }

gsvAtmInterfaceConfExtnCtrlPktInstanceId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..50 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the control packet instance identifier associated with this interface. If the user does not provide any instance identifier while creating an interface an instance is created internally from the default profile governed by the macro GS_CFG_CTRL_PKTS_DEF_ATM_PROF_ID and associated to the interface. This will reduce the total number to instances that can be now created by one. The default instance is governed by macro GS_CFG_CTRL_PKTS_DEF_INSTANCE_ID."
	DEFVAL   { 0 }
	::= { gsvAtmInterfaceConfExtnEntry 9 }

gsvAtmInterfaceConfExtnTransportType OBJECT-TYPE
	SYNTAX       INTEGER{
			cell(1),
			packet(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the transport type of the atm interface. This can be either Cell(1) which means that actual Atm Cells shall be received over the UTOPIA interface, or Packet(2), which means that Pseudo Cells corresponding to Packet VDSL shall be received over this ATM interface. This is not modifiable if any ATM VC is created on top of this ATM port."
	DEFVAL   { 1 }
	::= { gsvAtmInterfaceConfExtnEntry 10 }
	
gsvAtmInterfaceConfExtnMirrorMode OBJECT-TYPE
	SYNTAX       INTEGER{
			data(1),
			mirror(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This field configures ATM port in data mode or mirror mode. In mirror mode, only the mirrored packets are allowed to go out of the port and regular customer data is forbidden. Scheduling profile field is ignored in mirror mode."
	DEFVAL   { 1 }
	::= { gsvAtmInterfaceConfExtnEntry 11 }


gsvAtmInterfaceConfExtnRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This object specifies the RowStatus."
	::= { gsvAtmInterfaceConfExtnEntry 12 }


gsvAtmVclStatExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAtmVclStatExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       deprecated
	DESCRIPTION
		"This table is an extension of atmVclStatTable defined in 'draft-ietf-atommib-atm2-17'."
	::= { gsvAtmMIB 3 }


gsvAtmVclStatExtnEntry OBJECT-TYPE
	SYNTAX       GsvAtmVclStatExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       deprecated
	DESCRIPTION
		"An entry (conceptual row) in the gsvAtmVclStatExtnTable.
		The Table is indexed by ifIndex, atmVclVpi and atmVclVci."
	INDEX        { ifIndex, atmVclVpi, atmVclVci }
	::= { gsvAtmVclStatExtnTable 1 }

GsvAtmVclStatExtnEntry ::= SEQUENCE {
		gsvAtmVclStatExtnResetStats     TruthValue }


gsvAtmVclStatExtnResetStats OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       deprecated
	DESCRIPTION
		"The setting of this parameter results in statistics reset. This can only be reset in case the entry is administratively down. The only value supported is 'true'."
	::= { gsvAtmVclStatExtnEntry 1 }




gsvAtmVclExtnTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAtmVclExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table is an extension to the Virtual Channel Link (VCL) table defined in 'rfc2515'. The object gsvAtmVclExtnVciIfIndex is a mandatory parameter for creation of an entry in this table."
	::= { gsvAtmMIB 4 }


gsvAtmVclExtnEntry OBJECT-TYPE
	SYNTAX       GsvAtmVclExtnEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAtmVclExtnTable.
		The Table is indexed by ifIndex, atmVclVpi and atmVclVci."
	INDEX        { ifIndex, atmVclVpi, atmVclVci }
	::= { gsvAtmVclExtnTable 1 }

GsvAtmVclExtnEntry ::= SEQUENCE {
		gsvAtmVclExtnDslChannel     INTEGER,
		gsvAtmVclExtnOamLoopbackType     INTEGER,
		gsvAtmVclExtnOamLoopBackLocation     OCTET STRING,
		gsvAtmVclExtnOamLoopBackResult     INTEGER,
		gsvAtmVclExtnOamLoopBackTestEnable     TruthValue,
		gsvAtmVclExtnCCAction     INTEGER,
		gsvAtmVclExtnCCDirection     INTEGER,
		gsvAtmVclExtnCCSinkOperState     INTEGER,
		gsvAtmVclExtnCCSourceOperState     INTEGER,
		gsvAtmVclExtnCCInitiator     INTEGER,
		gsvAtmVclExtnCCMode     INTEGER,
		gsvAtmVclExtnVciIfIndex     INTEGER,
		gsvAtmVclExtnAtmMgmtMode     BITS,
		gsvAtmVclExtnProtoNumSup     INTEGER,
		gsvAtmVclExtnAutoMode     INTEGER,
		gsvAtmVcAutoSupportedProtocols     BITS,
		gsvAtmVcMuxForcedProtocol     INTEGER,
		gsvAtmVcAutoSenseTriggerType     INTEGER,
		gsvAtmVcCurrentSensedEncapType     INTEGER,
		gsvAtmVcCtlPktGroupId     INTEGER }


gsvAtmVclExtnDslChannel OBJECT-TYPE
	SYNTAX       INTEGER{
			fast(1),
			interleaved(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This extension specifies the type of channel on which, the ATM VC's cells have to be transmitted/received. 'fast(1)' means fast channel and 'interleaved(2)' means interleaved channel. This field is deprecated and currently not in use."
	DEFVAL   { 2 }
	::= { gsvAtmVclExtnEntry 1 }


gsvAtmVclExtnOamLoopbackType OBJECT-TYPE
	SYNTAX       INTEGER{
			endToEnd(0),
			segment(1)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This specifies the loopback type to be used. Both 'endToEnd' and 'segment' loopback are supported in Columbia. If this field is 'endToEnd', then gsvAtmVclExtnOamLoopBackLocation field cannot be 0x0.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { 0 }
	::= { gsvAtmVclExtnEntry 2 }


gsvAtmVclExtnOamLoopBackLocation OBJECT-TYPE
	SYNTAX       OCTET STRING (SIZE ( 0..16 ) )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This defines the loopback site, which will loop back the cell. It is used for the loopback testing. The default, always, is 1. If gsvAtmVclExtnOamLoopbackType field is 'endToEnd', then this field cannot be 0x0. All 0s is allowed only for 'segment' loopback cells.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { 4294967295 }
	::= { gsvAtmVclExtnEntry 3 }


gsvAtmVclExtnOamLoopBackResult OBJECT-TYPE
	SYNTAX       INTEGER{
			segLbSuccess(0),
			segLbFailure(1),
			endToEndLbSuccess(2),
			endToEndLbFailure(3),
			lbTestAborted(4),
			lbTestInProgress(5),
			lbResultNotAvailable(4294967295)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the result of the loopback test. This variable should be read 5 seconds after setting the OAM Loopback test enable. Currently, only End-To-End loop back is supported in Columbia. This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 4 }


gsvAtmVclExtnOamLoopBackTestEnable OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This enables the OAM LoopBack test on this VC just for once. The parameters gsvAtmVclExtnOamLoopbackType and gsvAtmVclExtnOamLoopBackLocation shall be used to do the loopback testing. The only value supported for this field is 'true'.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 5 }


gsvAtmVclExtnCCAction OBJECT-TYPE
	SYNTAX       INTEGER{
			activate(1),
			deactivate(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies the CC action to be taken. This is used along with gsvAtmVclExtnCCDirection field. This object is modifiable only when the value of atmVclRowStatus is 'active'. The modification done on this object will be ineffective for atmVclRowStatus value set to 'notInService' or 'notReady'. Also setting this object along with atmVclRowStatus value set to 'createAndWait' will have no meaning.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 6 }


gsvAtmVclExtnCCDirection OBJECT-TYPE
	SYNTAX       INTEGER{
			sink(1),
			source(2),
			both(3)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies the direction for CC activation/Deactivation. This object is modifiable only when the value of atmVclRowStatus is 'active'. The modification done on this object will be ineffective for atmVclRowStatus value set to 'notInService' or 'notReady'. Also, setting this object along with atmVclRowStatus value set to 'createAndWait' will have no meaning.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 7 }


gsvAtmVclExtnCCSinkOperState OBJECT-TYPE
	SYNTAX       INTEGER{
			activated(1),
			deactivated(2),
			inProgress(3),
			deactInProg(4),
			loc(5),
			actFailed(6),
			deactFailed(7)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This field specifies the current operational state of sink point of the VCC.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 8 }


gsvAtmVclExtnCCSourceOperState OBJECT-TYPE
	SYNTAX       INTEGER{
			activated(1),
			deactivated(2),
			inProgress(3),
			deactInProgress(4),
			loc(5),
			actFailed(6),
			deactFailed(7)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This field specifies the current operational state of source point of the VCC.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 9 }


gsvAtmVclExtnCCInitiator OBJECT-TYPE
	SYNTAX       INTEGER{
			self(1),
			peer(2),
			invalid(3)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This field is valid only in auto mode and it specifies the current initiator of CC Activation/Deactivation.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	::= { gsvAtmVclExtnEntry 10 }


gsvAtmVclExtnCCMode OBJECT-TYPE
	SYNTAX       INTEGER{
			autoMode(1),
			manualMode(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This specifies the Activation/Deactivation capability at a VCC. This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { 1 }
	::= { gsvAtmVclExtnEntry 11 }


gsvAtmVclExtnVciIfIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies the AAL5 Interface Index, which is associated with the VPI/VCI port. For creating an entry in this table using atmVclRowStatus with its value set to 'createAndWait' or 'createAndGo', this object must be specified in the same PDU. If this object is not specified along with atmVclRowStatus in single PDU, agent will return 'inconsistentValue'/'badValue' error. The field gsvAtmVclExtnVciIfIndex cannot be modified for atmVclRowStatus value 'active', 'notInService', or 'notReady'."
	::= { gsvAtmVclExtnEntry 12 }


gsvAtmVclExtnAtmMgmtMode OBJECT-TYPE
	SYNTAX       BITS{
			data(0),
			mgmt(1),
			rawAtm(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"It denotes the Management Mode of the ATM VC. If it is 'data', then only data transmission can take place. If it is 'mgmt', then management of remote CPE device can happen on that ATM VC and packets on that ATM VC shall start coming to Control Plane. In 'data' + 'Mgmt' mode, data transmission as well as remote CPE management can happen on the same ATM VC interface. In 'data' + 'Mgmt' mode, the only acceptable value for 'atmVCCAAL5EncapType' is 'llc'. In Mgmt mode, EoA interface can't be created on the ATM VC and both Ethernet as well as non-Ethernet packets on that ATM VC shall be received at Control Plane. In 'data' + 'mgmt' mode, if EoA is created then only non-Ethernet packets on that ATM VC shall be received at Control Plane. However, if EoA is not created then all the packets on that ATM VC shall be received at Control Plane. However, to configure ATM VC in 'data' + 'mgmt' mode, good practice is to create ATM VC in disable mode till EoA is created on it, to prevent flooding at Control Plane. In order 
to run STP, the mode has to be 'data' + 'mgmt'. If the mode is 'rawATM', ATM cells are given to Control Plane. In this mode, EoA interface cannot be created on the ATM VC. If EoA interface is already created on the ATM VC, its mode cannot be changed to either 'mgmt' or 'rawATM'.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { 0 }
	::= { gsvAtmVclExtnEntry 13 }


gsvAtmVclExtnProtoNumSup OBJECT-TYPE
	SYNTAX       INTEGER ( 1..1 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies the maximum number of simultaneous active protocols stacks supported on this interface. Currently, only one protocol stack is supported."
	DEFVAL   { 1 }
	::= { gsvAtmVclExtnEntry 14 }


gsvAtmVclExtnAutoMode OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies whether Auto mode is to be enabled or not. In Auto mode, the stack above this interface will be determined and created based on the protocol packets sensed on this interface. For example, if the protocol packet sensed above this interface is an EoA packet, then the corresponding EoA stack will be created above this interface. However, the corresponding EoA interface must have been created with 'gsvEoaConfigMode' field's bit corresponding to 'Auto' set.  This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { disable }
	::= { gsvAtmVclExtnEntry 15 }


gsvAtmVcAutoSupportedProtocols OBJECT-TYPE
	SYNTAX       BITS{
			pppoa(0),
			eoa(1),
			ipoa(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies higher layer protocols which are supported for auto detection on the given ATM VC. Only the packets, if the protocols mentioned in this field can lead to Auto detection. This field is meaningful only when the autostatus flag is set as ?enable?."
	DEFVAL   { pppoa }
	::= { gsvAtmVclExtnEntry 16 }


gsvAtmVcMuxForcedProtocol OBJECT-TYPE
	SYNTAX       INTEGER{
			none(0),
			pppoa(1),
			eoa(2),
			ipoa(3)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies if the encap type detected is VCMux, the user can configure to build a specific protocol stack automatically. This field is meaningful only when the autostatus flag is set as ?enable?. In case of conflict with autoSupportedProtocols, its value will override."
	DEFVAL   { 0 }
	::= { gsvAtmVclExtnEntry 17 }


gsvAtmVcAutoSenseTriggerType OBJECT-TYPE
	SYNTAX       INTEGER{
			dynamic(0),
			opstatechange(1)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies at what time autodetection of Encapsulation type or higher protocol layers is to be done - all the time or only when Operational Status of ATM VC is changed to UP. If its value is 0, then detection can happen anytime a packet is received. If its value is 1, then autodetection happens only when Operational status of ATM VC changes to UP. This field is not valid if atmVCCAAL5EncapsType is Ethernet."
	DEFVAL   { 0 }
	::= { gsvAtmVclExtnEntry 18 }


gsvAtmVcCurrentSensedEncapType OBJECT-TYPE
	SYNTAX       INTEGER{
			vccmux(1),
			llcmux(7),
			none(10)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This field specifies the current sensed Encapsulation Type in case Encapsulation type is being autodetected. The value of this will be the same as the object Aal5EncapsulationType if Encapsulation Type is preconfigured. This is a read-only field for all agents, except the Auto Sense Agent."
	DEFVAL   { 10 }
	::= { gsvAtmVclExtnEntry 19 }


gsvAtmVcCtlPktGroupId OBJECT-TYPE
	SYNTAX       INTEGER{
			nD(10)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The Control packet instance group associated with this VC. The flows for this interface shall be mapped to control packet instances as mapped for the flows corresponding to the groupid configured in gsvCtlPktInstanceGroupTbl. If this group does not have entries for some of the flows, then those flows shall be mapped to the gsvAtmInterfaceConfExtnCtrl PktInstanceId of ATM port, for which this VC is being created. If the group id is 0, then all the flows shall be mapped to gsvAtmInterfaceConfExtnCtrl PktInstanceId of ATM port, for which this VC is being created."
	DEFVAL   { 0 }
	::= { gsvAtmVclExtnEntry 20 }

gsvAtmVcl2Table OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAtmVcl2Entry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains VPI and VCI information for the corresponding VC interface index."
	::= { gsvAtmMIB 5 }


gsvAtmVcl2Entry OBJECT-TYPE
	SYNTAX       GsvAtmVcl2Entry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAtmVcl2Table.
		The Table is indexed by gsvAtmVcl2VcIfIndex."
	INDEX        { gsvAtmVcl2VcIfIndex }
	::= { gsvAtmVcl2Table 1 }

GsvAtmVcl2Entry ::= SEQUENCE {
		gsvAtmVcl2VcIfIndex     INTEGER,
		gsvAtmVcl2Vpi     INTEGER,
		gsvAtmVcl2Vci     INTEGER }


gsvAtmVcl2VcIfIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Interface Index of the ATM port, on which this VC is getting configured"
	::= { gsvAtmVcl2Entry 1 }


gsvAtmVcl2Vpi OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The VPI value of the VCL."
	::= { gsvAtmVcl2Entry 2 }


gsvAtmVcl2Vci OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The VCI value of the VCL."
	::= { gsvAtmVcl2Entry 3 }

END 


