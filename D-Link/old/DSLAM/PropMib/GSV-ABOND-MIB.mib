GSV-ABOND-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TruthValue,
	  	RowStatus
		      FROM SNMPv2-TC
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvAbondMIB MODULE-IDENTITY
    LAST-UPDATED "200506090000Z"	       -- 09 June 2005
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
   " The MIB module that provides proprietary objects. The MIB includes following propritery Tables.
	1.) gsvAbondGroupStatsTable
	2.) gsvAbondGroupIntfTable
	3.)	gsvAbondLinkTable
	4.) gsvAbondLinkStatsTable"
	
-- Revision History
       

       REVISION	    "200410130000Z"	       -- 13 October 2004
       DESCRIPTION  "This is the initial version of this MIB."   
       
       REVISION	    "200503010000Z"	       -- 01 March 2005
       DESCRIPTION  "This MIB was updated for 'DSL Bonding and Dual Latency' changes." 
		
       REVISION	    "200505310000Z"	       -- 31 May 2005
       DESCRIPTION  "This MIB was updated for making the changes in gsvAbondGroupIntfTable viz. adding new field AsmIrlThreshold and making the field SIDFormat configurable per group." 
	
       REVISION	    "200505310000Z"	       -- 09 June 2005
       DESCRIPTION  "This MIB was updated for making the changes in gsvAbondGroupIntfTable viz. adding new object gsvAbondGroupIntfMaxAtmPortUSRate." 
	

    ::= { columbiaPackets 25 }



gsvAbondLinkStatsTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAbondLinkStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table governs the statistics of the individual Links in the Abond Groups."
	::= { gsvAbondMIB 1 }


gsvAbondLinkStatsEntry OBJECT-TYPE
	SYNTAX       GsvAbondLinkStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAbondLinkStatsTable.
		The Table is indexed by gsvAbondLinkStatsIntfIndex and gsvAbondLinkStatsLinkIfIndex."
	INDEX        { gsvAbondLinkStatsIntfIndex, gsvAbondLinkStatsLinkIfIndex }
	::= { gsvAbondLinkStatsTable 1 }

GsvAbondLinkStatsEntry ::= SEQUENCE {
		gsvAbondLinkStatsIntfIndex     INTEGER,
		gsvAbondLinkStatsLinkIfIndex     INTEGER,
		gsvAbondLinkStatsASMTxCount     Integer32,
		gsvAbondLinkStatsASMRxCount     Integer32,
		gsvAbondLinkStatsTxLinkFailureReason     INTEGER,
		gsvAbondLinkStatsRxLinkFailureReason     INTEGER,
		gsvAbondLinkStatsReset     TruthValue }


gsvAbondLinkStatsIntfIndex OBJECT-TYPE
	SYNTAX       INTEGER 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the ATM Based Multi pair Bonding type of interfaces. The min and max values for ' gsvAbondLinkStatsIntfIndex 'is given by macros 'IAD_ABONDGR_MIN_IFINDEX' and 'IAD_ABONDGR_MIN_IFINDEX ' respectively. These macros are defined in 'controlplane\common\inc\iad_interface_id.h'."
	::= { gsvAbondLinkStatsEntry 1 }


gsvAbondLinkStatsLinkIfIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the abond link (DSL) interfaces. "
	::= { gsvAbondLinkStatsEntry 2 }


gsvAbondLinkStatsASMTxCount OBJECT-TYPE
	SYNTAX       Integer32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Per-link ASM Tx count."
	::= { gsvAbondLinkStatsEntry 3 }


gsvAbondLinkStatsASMRxCount OBJECT-TYPE
	SYNTAX       Integer32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Per-link ASM Rx count."
	::= { gsvAbondLinkStatsEntry 4 }


gsvAbondLinkStatsTxLinkFailureReason OBJECT-TYPE
	SYNTAX       INTEGER{                           
			hecLimitExceeded(4),
			latencyMismatch(1),
			dslTypeMismatch(2),
			cpeRequest(3),
			linkDown(5),
			noAsmOnLink(6),
			linkAdminDown(8),
			linkMakesDifferentialDelayHighInDnStrm(9)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Failure reason for the abond link in Tx direction. "
	::= { gsvAbondLinkStatsEntry 5 }


gsvAbondLinkStatsRxLinkFailureReason OBJECT-TYPE
	SYNTAX       INTEGER{
			hecLimitExceeded(4),
			latencyMismatch(1),
			dslTypeMismatch(2),
			cpeRequest(3),
			linkDown(5),
			noAsmOnLink(6),
			linkMakesRxBitRateRatioHigh(7),
			linkAdminDown(8),
			linkMakesDifferentialDelayHighInUpStrm(10)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Failure reason for the abond link in Rx direction. "
	::= { gsvAbondLinkStatsEntry 6 }


gsvAbondLinkStatsReset OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The setting of this parameter results in statistics reset.The only value supported is 'true'."
	::= { gsvAbondLinkStatsEntry 7 }
     
     


gsvAbondLinkTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAbondLinkEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table is used to create the abond links. This table is indexed by interface index used for Abond Group interfaces and Index used for the abond links."
	::= { gsvAbondMIB 2 }


gsvAbondLinkEntry OBJECT-TYPE
	SYNTAX       GsvAbondLinkEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAbondLinkTable.
		The Table is indexed by gsvAbondLinkIntfIndex and gsvAbondLinkIfIndex."
	INDEX        { gsvAbondLinkIntfIndex, gsvAbondLinkIfIndex }
	::= { gsvAbondLinkTable 1 }

GsvAbondLinkEntry ::= SEQUENCE {
		gsvAbondLinkIntfIndex     INTEGER,
		gsvAbondLinkIfIndex     INTEGER,
		gsvAbondLinkTxLinkAdminStatus     INTEGER,
		gsvAbondLinkRxLinkAdminStatus     INTEGER,
		gsvAbondLinkTxLinkOperStatus     INTEGER,
		gsvAbondLinkRxLinkOperStatus     INTEGER,   
		gsvAbondASMTxLinkStatus     INTEGER,
		gsvAbondASMRxLinkStatus     INTEGER,
		gsvAbondLinkRowStatus     RowStatus }


gsvAbondLinkIntfIndex OBJECT-TYPE
	SYNTAX       INTEGER 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the ATM Based Multi pair Bonding type of interfaces. The min and max values for ' gsvAbondLinkIntfIndex 'is given by macros 'IAD_ABONDGR_MIN_IFINDEX' and 'IAD_ABONDGR_MIN_IFINDEX ' respectively. These macros are defined in 'controlplane\common\inc\iad_interface_id.h'."
	::= { gsvAbondLinkEntry 1 }


gsvAbondLinkIfIndex OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the abond link (DSL) interfaces. "
	::= { gsvAbondLinkEntry 2 }


gsvAbondLinkTxLinkAdminStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This specifies the Tx Status for the link in a Group."
	DEFVAL   { 1 }
	::= { gsvAbondLinkEntry 3 }


gsvAbondLinkRxLinkAdminStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This specifies the Rx Status for the link in a Group."
	DEFVAL   { 1 }
	::= { gsvAbondLinkEntry 4 }


gsvAbondLinkTxLinkOperStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies the tx operational Status for the link in a Group."
	::= { gsvAbondLinkEntry 5 }


gsvAbondLinkRxLinkOperStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies the rx operational Status for the link in a Group."
	::= { gsvAbondLinkEntry 6 }

gsvAbondASMTxLinkStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			notProvisioned(0),
			shouldNotBeUsed(1),
			acceptableToCarryBondingTraffic(2),
			selectedToCarryBondingTraffic(3)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies the Tx Link Status as in the ASM Messages."
	::= { gsvAbondLinkEntry 7 }


gsvAbondASMRxLinkStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			notProvisioned(0),
			shouldNotBeUsed(1),
			acceptableToCarryBondingTraffic(2),
			selectedToCarryBondingTraffic(3)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies the Rx Link Status as in the ASM Messages."
	::= { gsvAbondLinkEntry 8 }

gsvAbondLinkRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvAbondLinkEntry 9 }




gsvAbondGroupStatsTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAbondGroupStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"It governs the statistics of the Abond Groups."
	::= { gsvAbondMIB 3 }


gsvAbondGroupStatsEntry OBJECT-TYPE
	SYNTAX       GsvAbondGroupStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAbondGroupStatsTable.
		The Table is indexed by gsvAbondGroupStatsIfIndex."
	INDEX        { gsvAbondGroupStatsIfIndex }
	::= { gsvAbondGroupStatsTable 1 }

GsvAbondGroupStatsEntry ::= SEQUENCE {
		gsvAbondGroupStatsIfIndex     INTEGER,
		gsvAbondGroupStatsAchievedAggrRateUpstrm     Unsigned32,
		gsvAbondGroupStatsAchievedAggrRateDnstrm     Unsigned32,
		gsvAbondGroupStatsCellLossUpstrmCurrent     Counter32,
		gsvAbondGroupStatsCellLossDnstrmCurrent     Counter32,
		gsvAbondGroupStatsCellLossUpstrmPrv15min     Counter32,
		gsvAbondGroupStatsCellLossDnstrmPrev15Min     Counter32,
		gsvAbondGroupStatsCellLossUpstrmCurrentDay     Counter32,
		gsvAbondGroupStatsCellLossDnstrmCurrentDay     Counter32,
		gsvAbondGroupStatsCellLossUpstrmPrevDay     Counter32,
		gsvAbondGroupStatsCellLossDnstrmPrevDay     Counter32,
		gsvAbondGroupStatsGroupFailureCntCurrent     Counter32,
		gsvAbondGroupStatsGroupFailureCntPrev15Min     Counter32,
		gsvAbondGroupStatsGroupFailureCntCurrentDay     Counter32,
		gsvAbondGroupStatsGroupFailureCntPrevDay     Counter32,
		gsvAbondGroupStatsGroupUnavailableSecCurrent     Counter32,
		gsvAbondGroupStatsGroupUnavailableSecPrev15Min     Counter32,
		gsvAbondGroupStatsGroupUnavailableSecCurrentDay     Counter32,
		gsvAbondGroupStatsGroupUnavailableSecPrevDay     Counter32,
		gsvAbondGroupStatsASMTxCount     Counter32,
		gsvAbondGroupStatsASMRxCount     Counter32,
		gsvAbondGroupStatsGroupFailureReason     INTEGER,
		gsvAbondGroupStatsAsmRxCrcErrorCount     Counter32,
		gsvAbondGroupStatsReset     TruthValue }


gsvAbondGroupStatsIfIndex OBJECT-TYPE
	SYNTAX       INTEGER 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the ATM Based Multi pair Bonding type of interfaces. The min and max values for ' gsvAbondGroupStatsIfIndex 'is given by macros 'IAD_ABONDGR_MIN_IFINDEX' and 'IAD_ABONDGR_MIN_IFINDEX ' respectively. These macros are defined in 'controlplane\common\inc\iad_interface_id.h'."
	::= { gsvAbondGroupStatsEntry 1 }


gsvAbondGroupStatsAchievedAggrRateUpstrm OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Achieved aggregate data rate in bits per sec in upstream direction."
	::= { gsvAbondGroupStatsEntry 2 }


gsvAbondGroupStatsAchievedAggrRateDnstrm OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Achieved aggregate data rate in bits per sec in downstream direction."
	::= { gsvAbondGroupStatsEntry 3 }


gsvAbondGroupStatsCellLossUpstrmCurrent OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count upstream for current 15 minutes."
	::= { gsvAbondGroupStatsEntry 4 }


gsvAbondGroupStatsCellLossDnstrmCurrent OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count downstream for current 15 minutes."
	::= { gsvAbondGroupStatsEntry 5 }


gsvAbondGroupStatsCellLossUpstrmPrv15min OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count upstream for Last 15 minutes."
	::= { gsvAbondGroupStatsEntry 6 }


gsvAbondGroupStatsCellLossDnstrmPrev15Min OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count downstream for Last 15 minutes."
	::= { gsvAbondGroupStatsEntry 7 }


gsvAbondGroupStatsCellLossUpstrmCurrentDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count upstream for current Day."
	::= { gsvAbondGroupStatsEntry 8 }


gsvAbondGroupStatsCellLossDnstrmCurrentDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count downstream for current Day."
	::= { gsvAbondGroupStatsEntry 9 }


gsvAbondGroupStatsCellLossUpstrmPrevDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count upstream for Previous Day."
	::= { gsvAbondGroupStatsEntry 10 }


gsvAbondGroupStatsCellLossDnstrmPrevDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Rx cell loss count downstream for Previous Day."
	::= { gsvAbondGroupStatsEntry 11 }


gsvAbondGroupStatsGroupFailureCntCurrent OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group failure count for current 15 minutes."
	::= { gsvAbondGroupStatsEntry 12 }


gsvAbondGroupStatsGroupFailureCntPrev15Min OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group failure count for previous 15 minutes."
	::= { gsvAbondGroupStatsEntry 13 }


gsvAbondGroupStatsGroupFailureCntCurrentDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group failure count for current Day."
	::= { gsvAbondGroupStatsEntry 14 }


gsvAbondGroupStatsGroupFailureCntPrevDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group failure count for previous Day."
	::= { gsvAbondGroupStatsEntry 15 }


gsvAbondGroupStatsGroupUnavailableSecCurrent OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group unavailable second current."
	::= { gsvAbondGroupStatsEntry 16 }


gsvAbondGroupStatsGroupUnavailableSecPrev15Min OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group unavailable second previous 15 Min."
	::= { gsvAbondGroupStatsEntry 17 }


gsvAbondGroupStatsGroupUnavailableSecCurrentDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group unavailable second current Day."
	::= { gsvAbondGroupStatsEntry 18 }


gsvAbondGroupStatsGroupUnavailableSecPrevDay OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group unavailable second for previous Day."
	::= { gsvAbondGroupStatsEntry 19 }


gsvAbondGroupStatsASMTxCount OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group ASM Tx count."
	::= { gsvAbondGroupStatsEntry 20 }


gsvAbondGroupStatsASMRxCount OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group ASM Rx count."
	::= { gsvAbondGroupStatsEntry 21 }


gsvAbondGroupStatsGroupFailureReason OBJECT-TYPE
	SYNTAX       INTEGER{
			minRateNotAchievedUpAndDn(1),
			ddtExceededUpAndDn(4),
			minRateNotAchievedUp(2),
			ddtExceededUp(5),
			minRateNotAchievedDn(3),
			ddtExceededDn(6),
			bufferInsufficientLocal(7),
			bufferInsufficientRemote(8),
			notAllProvisionedLinksUp(9),
			rxBitRateRatioHigh(10),
			provisionedLinksLessThanTwo(11),
			invalidLinkNoInAsm(12),
			invalidGroupIdInAsm (13),
			invalidNoOfLinksInAsm (14),
			adminStatusNotEnabled(15),
			allLinksAreDown(16),
			noAsmCpeResponse(17),
			noAsmCpeResponseOnAllLinks(18),
			cpeRequestedInitialization(19)			
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"Failure reason for the abond Group. "
	::= { gsvAbondGroupStatsEntry 22 }


gsvAbondGroupStatsAsmRxCrcErrorCount OBJECT-TYPE
	SYNTAX       Counter32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" Group Asm Rx crc error count."
	::= { gsvAbondGroupStatsEntry 23 }


gsvAbondGroupStatsReset OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The setting of this parameter results in statistics reset.The only value supported is 'true'."
	::= { gsvAbondGroupStatsEntry 24 }
      



gsvAbondGroupIntfTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAbondGroupIntfEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table is used to create the Abond Group interfaces. This table is indexed by gsvAbondGroupIntfIfIndex that is the Interface Index of the Abond Group interface."
	::= { gsvAbondMIB 4 }


gsvAbondGroupIntfEntry OBJECT-TYPE
	SYNTAX       GsvAbondGroupIntfEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAbondGroupIntfTable.
		The Table is indexed by gsvAbondGroupIntfIfIndex."
	INDEX        { gsvAbondGroupIntfIfIndex }
	::= { gsvAbondGroupIntfTable 1 }

GsvAbondGroupIntfEntry ::= SEQUENCE {
		gsvAbondGroupIntfIfIndex     INTEGER,
		gsvAbondGroupIntfGroupId     INTEGER,
		gsvAbondGroupIntfMinAggrRateUpstrm     Unsigned32,
		gsvAbondGroupIntfMinAggrRateDnstrm     Unsigned32,
		gsvAbondGroupIntfDiffDelayTolUpstrm     INTEGER,
		gsvAbondGroupIntfDiffDelayTolDnstrm     INTEGER,
		gsvAbondGroupIntfAsmProtocol     INTEGER,
		gsvAbondGroupIntfSidFormat     INTEGER,
		gsvAbondGroupIntfMaxRxBitRateRatio     Integer32,    
		gsvAbondGroupIntfLinkHECThrsld     INTEGER,
		gsvAbondGroupIntfCtrlVPI     INTEGER,
		gsvAbondGroupIntfCtrlVCI     INTEGER,
		gsvAbondGroupIntfLinksUpForGrpUp     INTEGER,		
		gsvAbondGroupIntfAsmIrlThreshold     INTEGER,
		gsvAbondGroupIntfMaxAtmPortUSRate     Unsigned32,
		gsvAbondGroupIntfRowStatus     RowStatus }


gsvAbondGroupIntfIfIndex OBJECT-TYPE
	SYNTAX       INTEGER 
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This specifies the interface index used for the ATM Based Multi pair Bonding type of interfaces. The min and max values for ' gsvAbondGroupIntfIfIndex 'is given by macros 'IAD_ABONDGR_MIN_IFINDEX' and 'IAD_ABONDGR_MIN_IFINDEX ' respectively. These macros are defined in 'controlplane\common\inc\iad_interface_id.h'."
	::= { gsvAbondGroupIntfEntry 1 }


gsvAbondGroupIntfGroupId OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This specifies the group id configured for this interface. This field is configured statically when the bonded group is provisioned and must not be changed while the group is in service. These fields may be used by an operator to help identify mis-configuration or to assist in management or debugging of the link."
	::= { gsvAbondGroupIntfEntry 2 }


gsvAbondGroupIntfMinAggrRateUpstrm OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" Minimum Aggregate Data Rate in bits per  second in US direction. "
	DEFVAL   { 0 }
	::= { gsvAbondGroupIntfEntry 3 }


gsvAbondGroupIntfMinAggrRateDnstrm OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" Minimum Aggregate Data Rate in bits per  second in DS direction. "
	DEFVAL   { 0 }
	::= { gsvAbondGroupIntfEntry 4 }


gsvAbondGroupIntfDiffDelayTolUpstrm OBJECT-TYPE
	SYNTAX       INTEGER ( 0..4 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" The maximum differential delay among member links in a bonding group in US direction."
	DEFVAL   { 4 }
	::= { gsvAbondGroupIntfEntry 5 }


gsvAbondGroupIntfDiffDelayTolDnstrm OBJECT-TYPE
	SYNTAX       INTEGER ( 0..24 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" The maximum differential delay among member links in a bonding group in DS direction."
	DEFVAL   { 4 }
	::= { gsvAbondGroupIntfEntry 6 }


gsvAbondGroupIntfAsmProtocol OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" Abond Group Asm Exchange Enable. This specifies whether ASM messages is to be exchanged between CPE and CO for a bonded group after bonding group has become operational."
	DEFVAL   { 1 }
	::= { gsvAbondGroupIntfEntry 7 }


gsvAbondGroupIntfSidFormat OBJECT-TYPE
	SYNTAX       INTEGER{
			eightBitSID(0),
			twelveBitSID(1)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This specifies the Message Type eight bit or twelve bit SID."
	DEFVAL   { 1 }
	::= { gsvAbondGroupIntfEntry 8 }


gsvAbondGroupIntfMaxRxBitRateRatio OBJECT-TYPE
	SYNTAX       Integer32 ( 1..4 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" The maximum bit rate ratio among member links in a bonding group in upstream direction. "
	DEFVAL   { 4 }
	::= { gsvAbondGroupIntfEntry 9 }

gsvAbondGroupIntfLinkHECThrsld OBJECT-TYPE
	SYNTAX       INTEGER ( 1..10 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This specified Threshold of HEC error in terms of Percentage of Link Rate"
	DEFVAL   { 2 }
	::= { gsvAbondGroupIntfEntry 10 }


gsvAbondGroupIntfCtrlVPI OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies control channel VPI value"
	::= { gsvAbondGroupIntfEntry 11 }


gsvAbondGroupIntfCtrlVCI OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		" This specifies control channel VCI value"
	::= { gsvAbondGroupIntfEntry 12 }


gsvAbondGroupIntfLinksUpForGrpUp OBJECT-TYPE
	SYNTAX       INTEGER{
			one(0),
			all(1)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" This field specifies the number of links required to be up for bonding to start ASM protocol "
	DEFVAL   { 0 }
	::= { gsvAbondGroupIntfEntry 13 }


gsvAbondGroupIntfAsmIrlThreshold OBJECT-TYPE
	SYNTAX       INTEGER ( 1..8 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		" IRL Threshold for ASM messages"
	DEFVAL   { 8 }
	::= { gsvAbondGroupIntfEntry 14 }


gsvAbondGroupIntfMaxAtmPortUSRate OBJECT-TYPE
	SYNTAX       Unsigned32 ( 0..8000 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"Maximum ATM port Upstream Rate"
	DEFVAL   { 8000 }
	::= { gsvAbondGroupIntfEntry 15 }


gsvAbondGroupIntfRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvAbondGroupIntfEntry 16 }
END      

