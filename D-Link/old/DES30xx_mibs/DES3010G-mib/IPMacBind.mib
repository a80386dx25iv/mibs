-- *****************************************************************************
-- IP-MAC mib
-- *****************************************************************************
-- Version 1.2
-- 2005/9/9 03:26
-- add object swIpMacBindingPorts in swIpMacBindingTable .
-- *****************************************************************************
-- Version 1.1
-- 2005/7/21 11:45 
-- Insert the swIpMacBindingInfo subtree for further extension .
-- *****************************************************************************
-- Version 1.0 
-- 2005/6/24 10:44 
-- modify the MAX-ACCESS of AllPortState from write-only to read-write for mib compiler issue
-- and add some description
-- *****************************************************************************
-- 0.9 Draft 
-- 2005/5/16 10:44 
-- Create IP-MAC-MIB
-- *****************************************************************************

IP-MAC-BIND-MIB DEFINITIONS ::= BEGIN

    IMPORTS
        MODULE-IDENTITY,OBJECT-TYPE,IpAddress, Unsigned32
                                        FROM SNMPv2-SMI
        MacAddress, RowStatus           FROM SNMPv2-TC
        DisplayString                   FROM RFC1213-MIB
    	SnmpAdminString					FROM SNMP-FRAMEWORK-MIB

        dlink-common-mgmt				FROM DLINK-ID-REC-MIB;
        
		VlanId					::= INTEGER (1..4094)        
    	
    swIpMacBindMIB MODULE-IDENTITY
	    LAST-UPDATED "0007150000Z"
	    ORGANIZATION " "
	    CONTACT-INFO
	        "    "
	    DESCRIPTION
		    "The Structure of IP-MAC binding management for the
		    proprietary enterprise."
        ::= { dlink-common-mgmt 23 }


    PortList                ::= OCTET STRING(SIZE (0..32))


	swIpMacBindingCtrl           OBJECT IDENTIFIER ::= { swIpMacBindMIB 1 }
	swIpMacBindingInfo           OBJECT IDENTIFIER ::= { swIpMacBindMIB 2 }	
    swIpMacBindingPortMgmt       OBJECT IDENTIFIER ::= { swIpMacBindMIB 3 }
	swIpMacBindingMgmt           OBJECT IDENTIFIER ::= { swIpMacBindMIB 4 }

	swIpMacBindingAllPortState OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						enable(2),
						disable(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"This object enable\disable the IP-MAC binding on the all ports of the system.
			This object is meaningful and take action only while write demand,
			 and 'other(1)' will return while read this object.  "
		::= { swIpMacBindingPortMgmt 1 }


	swIpMacBindingPortTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingPortEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "The table specifies port's ip-mac binding function."
		::= { swIpMacBindingPortMgmt 2 }

	swIpMacBindingPortEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingPortEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "A list of information about port's ip-mac binding function."
		INDEX { swIpMacBindingPortIndex }
		::= { swIpMacBindingPortTable 1 }
	
	SwIpMacBindingPortEntry ::=
        SEQUENCE {
            swIpMacBindingPortIndex
                INTEGER,
			swIpMacBindingPortState
				INTEGER
       }

	swIpMacBindingPortIndex OBJECT-TYPE
		SYNTAX		INTEGER(0..65535)
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"This object indicates the module's port number.(1..Max port
			 number in the module)."
		::= { swIpMacBindingPortEntry 1 }
	
	swIpMacBindingPortState OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						enable(2),
						disable(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"This object enable\disable the IP-MAC binding on the specified port."
		::= { swIpMacBindingPortEntry 2 }
		
-- *****************************************************************************	
--	swIpMacBindingMgmt          OBJECT IDENTIFIER ::= { swIpMacBindMIB 3 }	
-- *****************************************************************************
	swIpMacBindingTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "This table specifies IP-MAC binding information."
		::= { swIpMacBindingMgmt 1 }

	swIpMacBindingEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "IP-MAC binding entry used to add\delete\configure the address 
			  pair to the switch's authorized user database."
		INDEX { swIpMacBindingIpIndex }
		::= { swIpMacBindingTable 1 }
	
	SwIpMacBindingEntry ::=
        SEQUENCE {
            swIpMacBindingIpIndex
                IpAddress,
			swIpMacBindingMac
				MacAddress,
			swIpMacBindingStatus
				RowStatus,
			swIpMacBindingPorts
        		PortList				
       }
	
	swIpMacBindingIpIndex OBJECT-TYPE
		SYNTAX		IpAddress
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The IP address of IP-MAC binding."
		::= { swIpMacBindingEntry 1 }

	swIpMacBindingMac OBJECT-TYPE
		SYNTAX		MacAddress
		MAX-ACCESS	read-create
		STATUS		current
		DESCRIPTION
			"The MAC address of IP-MAC binding."
		::= { swIpMacBindingEntry 2 }
	
	swIpMacBindingStatus OBJECT-TYPE
		SYNTAX		RowStatus
		MAX-ACCESS	read-create
		STATUS		current
		DESCRIPTION
			"The status of this entry."
		::= { swIpMacBindingEntry 3 }

   swIpMacBindingPorts OBJECT-TYPE
    SYNTAX    PortList
    MAX-ACCESS  read-create
    STATUS    current
    DESCRIPTION
      "The port members of this entry."
   	    ::= { swIpMacBindingEntry 4 }


-- *****************************************************************************


	swIpMacBindingBlockedTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingBlockedEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "The table specifies information of MAC address which was blocked."
		::= { swIpMacBindingMgmt 2 }

	swIpMacBindingBlockedEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingBlockedEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "The entry can not create\configure. It can be deleted only."
		INDEX { swIpMacBindingBlockedVID, swIpMacBindingBlockedMac }
		::= { swIpMacBindingBlockedTable 1 }
	
	SwIpMacBindingBlockedEntry ::=
        SEQUENCE {
            swIpMacBindingBlockedVID
                	VlanId,
			swIpMacBindingBlockedMac
				MacAddress,
			swIpMacBindingBlockedVlanName
				DisplayString,
			swIpMacBindingBlockedPort
				INTEGER,
			swIpMacBindingBlockedType
				INTEGER
       }

	swIpMacBindingBlockedVID OBJECT-TYPE
		SYNTAX		VlanId
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The object specifies VLAN ID."
		::= { swIpMacBindingBlockedEntry 1 }

	swIpMacBindingBlockedMac OBJECT-TYPE
		SYNTAX		MacAddress
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The MAC address which was blocked."
		::= { swIpMacBindingBlockedEntry 2 }
	
	swIpMacBindingBlockedVlanName OBJECT-TYPE
		SYNTAX		DisplayString
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"This object specifies VLAN name."
		::= { swIpMacBindingBlockedEntry 3 }

	swIpMacBindingBlockedPort OBJECT-TYPE
		SYNTAX		INTEGER(0..65535)
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The port with which the MAC is associated."
		::= { swIpMacBindingBlockedEntry 4 }
	
	swIpMacBindingBlockedType OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						blockByAddrBind(2),
						delete(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"The value is always blockByAddrBind. This entry will be delete by setting value delete(3)."
		::= { swIpMacBindingBlockedEntry 5 }
		
		
END		
