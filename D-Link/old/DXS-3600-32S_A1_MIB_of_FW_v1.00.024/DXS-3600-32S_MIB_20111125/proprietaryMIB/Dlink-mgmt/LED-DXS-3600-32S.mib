-- -----------------------------------------------------------------------------
-- MIB NAME : LED-DXS-3600-32S-MIB mib
-- FILE NAME: LED-DXS-3600-32S.mib
-- DATE     : 2011/6/24
-- VERSION  : 1.00
-- PURPOSE  : To construct the MIB structure of LED information for proprietary
--        enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version 1.00, 2011/6/24, Lina Zhao
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
LED-DXS-3600-32S-MIB  DEFINITIONS ::= BEGIN

	IMPORTS
		MODULE-IDENTITY , OBJECT-TYPE   FROM SNMPv2-SMI
		dXS-3600-32S         	FROM SW3600PRIMGMT-MIB ;


	swLedMIB MODULE-IDENTITY
		LAST-UPDATED "201106240000Z"
			ORGANIZATION "D-Link Corp."
			CONTACT-INFO
			"http://support.dlink.com"
			DESCRIPTION
			"The Structure of LED Information for the proprietary enterprise."
			     ::= { dXS-3600-32S 1 }

	swLedMIBObject OBJECT IDENTIFIER ::={swLedMIB 1}
			
swLedInfoTable OBJECT-TYPE
        SYNTAX  SEQUENCE OF SwLedInfoEntry
        MAX-ACCESS  not-accessible
        STATUS  current
        DESCRIPTION
            "This table contains the LED information."
        ::= { swLedMIBObject 1 }
        
	swLedInfoEntry OBJECT-TYPE
        SYNTAX  SwLedInfoEntry
        MAX-ACCESS  not-accessible
        STATUS  current
        DESCRIPTION
            "A list of front panel LED information for each unit in the system."
        INDEX  { swLedInfoUnitId }
        ::= { swLedInfoTable 1 }
        
	SwLedInfoEntry ::=
        SEQUENCE {
				swLedInfoUnitId
					INTEGER,
				swLedInfoFrontPanelLedStatus					
					OCTET STRING
		}

	swLedInfoUnitId OBJECT-TYPE
        SYNTAX  INTEGER (1..13)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The object indicates the unit ID."
        ::= { swLedInfoEntry 1 }
        
	swLedInfoFrontPanelLedStatus OBJECT-TYPE
        SYNTAX  OCTET STRING 
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
			"This object is a set of system LED indicators. The first 32 octets are defined as next,
			and following octets are for logical port LED.

				Octet 1 is Power1 LED:
					0x01 = Power On.
					0x02 = Power supply failures, over voltage, over current, over temperature.
                    0x03 = Power present, but AC does not work.
                    0x04 = Power and fan are running on different air flow.
                    0x05 = Power Off.  
                    
				Octet 2 is Power2 LED:
					0x01 = Power On.
					0x02 = Power supply failures, over voltage, over current, over temperature.
                    0x03 = Power present, but AC does not work.
                    0x04 = Power and fan are running on different air flow.
                    0x05 = Power Off. 
                    								
				Octet 3 is FAN1 LED:
					0x01 = All diagnostics pass. The module is operational..
					0x02 = The module is booting or running diagnostics.
					0x03 = If the module fails during initial reset, the LED continues to blink and the module does not come online.
							The module has runtime failure and is brought offline
					0x04 = The module is not receiving power       

				Octet 4 is FAN2 LED:
					same with Octet 3
										
				Octet 5 is FAN3 LED:
					same with Octet 3

				Octet 6 is Console LED:
					0x01 = Power On.
                    0x02 = Power Off.                      			
                    			
				Octet 7 is MGMT LED:
					0x01 = Link present but not sending or receiving data
					0x02 = Activity. Port is sending or receiving data.
					0x03 = Link fault. Error frames can affect connectivity, and errors such as excessive collisions, 
							CRC1 errors, and alignment and jabber errors2 are monitored for a link-fault indication.
					0x04 = No link, or port was administratively shutdown.

				Octet 8 is SDCard LED:
					0x01 = Plug-in
					0x02 = Read/Write
					0x03 = Read/Write failed
					0x04 = No link

					                    										
				Octet 9 is MGMT port LED:
					bit 7:
						0B = port is not activity.
						1B = port is activity.

					bit 6~4:
						reserved(it will be set to zero).

					bit 3:
						0B = half duplex
						1B = full duplex

					bit 2~0:
						0x0 = link fail
						0x1 = 10Mbps
						0x2 = 100Mbps
						0x3 = 1000Mbps

				The continued octets are the logic port led. Each octet presents one port.
				For each octet:
				link/activity/speed mode:
					bit 7:
						0B = port is not activity.
						1B = port is activity.

					bit 6:
						0B = not combo port
						1B = combo port

					bit 5:
						0B = fiber linkup
						1B = copper linkup
						(Note: This bit is available only when the port is combo port and linkup,
						otherwise, this bit will be set to zero.)

					bit 4: reserved(will be set to zero).

					bit 3:
						0B = half duplex
						1B = full duplex

					bit 2~0:
						0x0 = link fail
						0x1 = 10Mbps
						0x2 = 100Mbps
						0x3 = 1000Mbps         
						0x4 = 10Gbps
					"
		 ::= { swLedInfoEntry 2 }

END
