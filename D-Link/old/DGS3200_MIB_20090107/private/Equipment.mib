-- -----------------------------------------------------------------------------
-- MIB NAME : Equipment Common mib
-- FILE NAME: Equipment.mib
-- DATE     : 2008/10/16
-- VERSION  : 2.03
-- PURPOSE  : To construct the MIB structure of equipments management
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Version 2.03,2008/10/16, Jespersen cai
-- 1. add swEquipmentPowerSaving for dgs32xx.
--
-- Version 2.02, 2008/03/10, Kelvin Tao
-- 1. add swExternalAlarmMgmt,
-- 2. add notifications swExternalAlarm.
--
-- Version 2.01, 2007/10/24, Scott
-- Modify the descriptions of "swPowerStatus", "swPowerStatusChg", 
-- "swPowerFailure", and "swPowerRecover" to detail each cases.
--
-- Version 2.00, 2007/03/27, Yedda
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------

EQUIPMENT-MIB DEFINITIONS ::= BEGIN

     IMPORTS
        MODULE-IDENTITY, OBJECT-TYPE
            FROM SNMPv2-SMI
        DateAndTime, TruthValue ,DisplayString
            FROM SNMPv2-TC
		AgentNotifyLevel, dlink-common-mgmt
		    FROM DLINK-ID-REC-MIB;


     swEquipmentMIB MODULE-IDENTITY
          LAST-UPDATED "0810160000Z"
          ORGANIZATION "D-Link Corp."
          CONTACT-INFO
            "http://support.dlink.com"
          DESCRIPTION
            " equipments MIB."
          ::= { dlink-common-mgmt 11 }

	MacAddress ::= OCTET STRING (SIZE (6))    -- a 6 octet address
                                                  -- in the
                                                  -- "canonical"
                                                  -- order, copy from RFC1493



-- -----------------------------------------------------------------------------
-- OID Tree Allocation
-- -----------------------------------------------------------------------------
    swEquipment         OBJECT IDENTIFIER ::= { swEquipmentMIB 1 }
    swEquipmentNotify   OBJECT IDENTIFIER ::= { swEquipmentMIB 2 }

-- -----------------------------------------------------------------------------
-- Object Definition
-- -----------------------------------------------------------------------------
    swEquipmentCapacity OBJECT-TYPE
    SYNTAX      BITS {
                    fanCapable(0),
                        --
                    redundantPowerCapable(1),
                        --
                    tempteratureDetection(2),
                    stackingCapable(3),
                    chassisCapable(4)
                }
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Indicates the equipment capability supported in the system."

    ::= { swEquipment 1 }

-- -----------------------------------------------------------------------------
    swPowerTable OBJECT-TYPE
        SYNTAX          SEQUENCE OF SwPowerEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A list of Power information values."
        ::= { swEquipment 6 }

    swPowerEntry OBJECT-TYPE
        SYNTAX          SwPowerEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A entry of Power information values."
        INDEX           { swPowerUnitIndex , swPowerID}
        ::= { swPowerTable 1 }

    SwPowerEntry ::= SEQUENCE {
    			swPowerUnitIndex		      INTEGER,
                swPowerID	                  INTEGER,
                swPowerStatus   			  INTEGER
                }

    swPowerUnitIndex OBJECT-TYPE
        SYNTAX INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the unit ID in the System."
        ::= { swPowerEntry 1 }
    swPowerID OBJECT-TYPE
        SYNTAX INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates ID of the power
            1 : main power
            2 : redundant power ."


        ::= { swPowerEntry 2 }

    swPowerStatus OBJECT-TYPE
           SYNTAX  INTEGER {
              other(0),
              lowVoltage(1),
              overCurrent(2),
              working(3),
              fail(4),
              connect(5),
              disconnect(6)
     }

        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the current power status."
        ::= { swPowerEntry 3 }

-- -----------------------------------------------------------------------------
    swFanTable OBJECT-TYPE
        SYNTAX          SEQUENCE OF SwFanEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A list of fan information values."
        ::= { swEquipment 7 }

    swFanEntry OBJECT-TYPE
        SYNTAX          SwFanEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A entry of fan information values."
        INDEX           { swFanUnitIndex, swFanID}
        ::= { swFanTable 1 }

    SwFanEntry ::= SEQUENCE {
    			swFanUnitIndex		      INTEGER,
                swFanID	                  INTEGER,
				swFanStatus				  INTEGER
                }

    swFanUnitIndex OBJECT-TYPE
        SYNTAX INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the unit ID in the System."
        ::= { swFanEntry 1 }
    swFanID OBJECT-TYPE
        SYNTAX INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the unit ID."
        ::= { swFanEntry 2 }

    swFanStatus OBJECT-TYPE
           SYNTAX  INTEGER {
              other(0),
              working(1),
              fail(2)
     }

        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the current fan status."
        ::= { swFanEntry 3 }


-- -----------------------------------------------------------------------------
    swTemperatureTable OBJECT-TYPE
        SYNTAX          SEQUENCE OF SwTemperatureEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A list of temperature values."
        ::= { swEquipment 8 }

    swTemperatureEntry OBJECT-TYPE
        SYNTAX          SwTemperatureEntry
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION     "A entry of temperature values."
        INDEX           { swTemperatureUnitIndex }
        ::= { swTemperatureTable 1 }

    SwTemperatureEntry ::= SEQUENCE {
    			swTemperatureUnitIndex	      INTEGER,
                swTemperatureCurrent          INTEGER,
                swTemperatureHighThresh       INTEGER,
                swTemperatureLowThresh        INTEGER
                }

    swTemperatureUnitIndex OBJECT-TYPE
        SYNTAX INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS current
        DESCRIPTION
            "Indicates the unit ID in the System"
        ::= { swTemperatureEntry 1 }

    swTemperatureCurrent OBJECT-TYPE
        SYNTAX          INTEGER(-500..500)
        MAX-ACCESS      read-only
        STATUS          current
        DESCRIPTION     "The shelf current temperature."
        ::= { swTemperatureEntry 2 }

    swTemperatureHighThresh OBJECT-TYPE
        SYNTAX          INTEGER (-500..500)
        MAX-ACCESS      read-write
        STATUS          current
        DESCRIPTION     "The high threshold of shelf temperature."
        ::= { swTemperatureEntry 3 }

    swTemperatureLowThresh OBJECT-TYPE
        SYNTAX          INTEGER(-500..500)
        MAX-ACCESS      read-write
        STATUS          current
        DESCRIPTION     "The low threshold of shelf temperature."
        ::= { swTemperatureEntry 4 }

-- -----------------------------------------------------------------------------
    swUnitMgmt            OBJECT IDENTIFIER ::= { swEquipment 9 }

    swUnitStackingVersion OBJECT-TYPE
        SYNTAX  INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the version of this stacking system ."
        ::= { swUnitMgmt 1 }

    swUnitMaxSupportedUnits OBJECT-TYPE
        SYNTAX  INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Maximum number of units are supported in the system."
        ::= { swUnitMgmt 2 }

    swUnitNumOfUnit OBJECT-TYPE
        SYNTAX  INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The current number of units."
        ::= { swUnitMgmt 3 }

    swUnitMgmtTable OBJECT-TYPE
        SYNTAX  SEQUENCE OF SwUnitMgmtEntry
        MAX-ACCESS  not-accessible
        STATUS  current
        DESCRIPTION
            "This table contains the unit information."
        ::= { swUnitMgmt 4 }

    swUnitMgmtEntry OBJECT-TYPE
        SYNTAX  SwUnitMgmtEntry
        MAX-ACCESS  not-accessible
        STATUS  current
        DESCRIPTION
            "A list of management information for each unit in the system."
        INDEX  { swUnitMgmtId }
        ::= { swUnitMgmtTable 1 }

    SwUnitMgmtEntry ::=
        SEQUENCE {
            swUnitMgmtId
                INTEGER,
            swUnitMgmtMacAddr
                MacAddress,
            swUnitMgmtStartPort
                INTEGER,
            swUnitMgmtPortRange
                INTEGER,
			swUnitMgmtFrontPanelLedStatus
				OCTET STRING,
            swUnitMgmtCtrlMode
                INTEGER,
            swUnitMgmtCurrentMode
                INTEGER,
            swUnitMgmtVersion
                DisplayString,
            swUnitMgmtModuleName
            	DisplayString,
            swUnitMgmtPromVersion
                DisplayString,
            swUnitMgmtFirmwareVersion
                DisplayString,
            swUnitMgmtHardwareVersion
                DisplayString,
            swUnitMgmtPriority
                INTEGER,
            swUnitMgmtUserSetState
                INTEGER,
            swUnitMgmtExistState
            	INTEGER,
            swUnitMgmtBoxId
                INTEGER
            }

    swUnitMgmtId OBJECT-TYPE
        SYNTAX  INTEGER (1..13)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the specific entry in the stacking/chassis
            table."
        ::= { swUnitMgmtEntry 1 }

    swUnitMgmtMacAddr OBJECT-TYPE
        SYNTAX  MacAddress
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The MAC address of this unit."
        ::= { swUnitMgmtEntry 2 }

    swUnitMgmtStartPort OBJECT-TYPE
        SYNTAX  INTEGER (1..65535)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the starting port of this unit."
        ::= { swUnitMgmtEntry 3 }

    swUnitMgmtPortRange OBJECT-TYPE
        SYNTAX  INTEGER (0..65535)
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the total ports of this unit."
        ::= { swUnitMgmtEntry 4 }

    swUnitMgmtFrontPanelLedStatus OBJECT-TYPE
        SYNTAX  OCTET STRING (SIZE (0..255))
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object is a set of system LED indications. The first four
             octets are defined as system LED. The first LED is power LED.
             The second LED in stacking is master LED but in chassis is
             status LED. The third LED is console LED.The fourth LED is
             RPS (Redundancy Power Supply) LED. The other octets are the
             logical port LED(following dot1dBasePort ordering). Every two
             bytes are presented to a port. The first byte is presented as the
             Link/Activity LED. The second byte is presented as the Speed LED.

        	 system LED:
             01 = fail/error/non existence.
             02 = work normal.


             Link/Activity LED :
             The most significant bit is used for blink/solid:
                8 = The LED blinks.

             The second most significant bit is used for link status:
             	1 = link fail.
             	2 = link pass.

             Speed LED :
             	01 = 10Mbps.
             	02 = 100Mbps.
             	03 = 1000Mbps.

            The four remaining bits are currently unused and must be set to 0."
        ::= { swUnitMgmtEntry 5 }


    swUnitMgmtCtrlMode OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    auto(2),
                    stand-alone(3),
                    master(4),
                    slave(5)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the stacking mode that user configed for
            this unit.  The object only can be configed when the device is
            stand alone.

            other(1) - This object indicates the stacking mode that the user
                has configured this unit. This object can only be configured
                when the device is in stand alone mode..
            auto(2) - The system will auto assign this stack role of this
                unit to be stand-alone(3), master(4), or slave(5).
            stand-alone(3) - The unit is forced to be in stand alone mode.
            master(4) - The unit is forced to be in master mode. If this unit is
                selected to be master, the unit can modify the configuration of the
                stacking system.
            slave(5) - The unit is forced to be in slave mode. If this unit is
                selected to be slave, it can only view the configuration of
                the stacking system."
        ::= { swUnitMgmtEntry 6 }

    swUnitMgmtCurrentMode OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    auto(2),
                    stand-alone(3),
                    master(4),
                    slave(5)
                }
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The current stacking role of this unit."
        ::= { swUnitMgmtEntry 7 }

    swUnitMgmtVersion OBJECT-TYPE
        SYNTAX  DisplayString (SIZE (0..32))
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the version of this stacking unit."
        ::= { swUnitMgmtEntry 8 }

	swUnitMgmtModuleName OBJECT-TYPE
        SYNTAX      DisplayString (SIZE (0..32))
        MAX-ACCESS  read-only
        STATUS      current
        DESCRIPTION
               "A textual string containing the stacking unit module name. "
        ::= { swUnitMgmtEntry 9 }

        swUnitMgmtPromVersion OBJECT-TYPE
        SYNTAX      DisplayString (SIZE (0..255))
        MAX-ACCESS  read-only
        STATUS      current
        DESCRIPTION
               "A textual string containing the PROM version of the
                stacking unit. "
        ::= { swUnitMgmtEntry 10 }

    swUnitMgmtFirmwareVersion  OBJECT-TYPE
        SYNTAX      DisplayString (SIZE (0..255))
        MAX-ACCESS  read-only
        STATUS      current
        DESCRIPTION
               "A textual string containing the firmware version of the
               stacking unit. "
        ::= { swUnitMgmtEntry 11 }

    swUnitMgmtHardwareVersion  OBJECT-TYPE
        SYNTAX      DisplayString (SIZE (0..255))
        MAX-ACCESS  read-only
        STATUS      current
        DESCRIPTION
               "A textual string containing the hardware version of the
               stacking unit. "
        ::= { swUnitMgmtEntry 12 }

    swUnitMgmtPriority  OBJECT-TYPE
        SYNTAX      INTEGER(1..63)
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The Priority of the  stacking unit. "
        ::= { swUnitMgmtEntry 13 }

    swUnitMgmtUserSetState OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    auto(2),
                    user(3)
                }
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the user set state of this unit."
        ::= { swUnitMgmtEntry 14 }

    swUnitMgmtExistState OBJECT-TYPE
        SYNTAX  INTEGER {
                    exist(1),
                    no-exist(2)
                }
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The state of existence of this unit."
        ::= { swUnitMgmtEntry 15 }

     swUnitMgmtBoxId  OBJECT-TYPE
        SYNTAX      INTEGER {
                    box-1(1),
                    box-2(2),
                    box-3(3),
                    box-4(4),
                    box-5(5),
                    box-6(6),
                    box-7(7),
                    box-8(8),
                    box-9(9),
                    box-10(10),
                    box-11(11),
                    box-12(12),
                    auto(13)
                }
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The box ID of the stacking unit.
                When show, it shows the current box ID of this unit;
                When set, it sets the new box ID, and the new box ID will
                take effect after the next boot."
        ::= { swUnitMgmtEntry 16 }

    swUnitTopology OBJECT-TYPE
        SYNTAX  INTEGER {
                    stand-alone(1),
                    duplex-chain(2),
                    duplex-ring(3),
                    star(4),
                    unstable(5)
                }
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "The stacking topology state."
        ::= { swUnitMgmt 5 }

-- --------------------------------------------------------------------------------
-- swEquipmentPowerSaving
-- --------------------------------------------------------------------------------
	swEquipmentPowerSaving OBJECT IDENTIFIER ::= { swEquipment 11 }
	 
   		swEquipmentPowerSavingState OBJECT-TYPE
       		 SYNTAX  INTEGER {
              		enabled(1),
              		disabled(2)
     			}
    		MAX-ACCESS  read-write
    		STATUS      current
    		DESCRIPTION  "Indicates the equipment reduce power consumption state."
    		::= { swEquipmentPowerSaving 1 }


-- -----------------------------------------------------------------------------
	swEquipmentNotifyMgmt           OBJECT IDENTIFIER ::= { swEquipmentNotify 1 }
	swEquipmentNotification         OBJECT IDENTIFIER ::= { swEquipmentNotify 2 }


	swEquipUnitNotification         OBJECT IDENTIFIER ::= { swEquipmentNotification 1 }
	swEquipPowerNotification        OBJECT IDENTIFIER ::= { swEquipmentNotification 2 }
	swEquipFanNotification          OBJECT IDENTIFIER ::= { swEquipmentNotification 3 }
	swEquipTemperatureNotification  OBJECT IDENTIFIER ::= { swEquipmentNotification 4 }


	swEquipUnitNotifyMgmt           OBJECT IDENTIFIER ::= { swEquipmentNotifyMgmt 1 }
	swEquipPowerNotifyMgmt          OBJECT IDENTIFIER ::= { swEquipmentNotifyMgmt 2 }
	swEquipFanNotifyMgmt            OBJECT IDENTIFIER ::= { swEquipmentNotifyMgmt 3 }
	swEquipTemperatureNotifyMgmt    OBJECT IDENTIFIER ::= { swEquipmentNotifyMgmt 4 }

-- -----------------------------------------------------------------------------
    swUnitInsertSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swUnitInsert detection level."
        ::= { swEquipUnitNotifyMgmt 1 }

    swUnitRemoveSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swUnitRemove detection level."
        ::= { swEquipUnitNotifyMgmt 2 }

    swUnitFailureSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swUnitFailure detection level."
        ::= { swEquipUnitNotifyMgmt 3 }

-- -----------------------------------------------------------------------------
    swPowerStatusChgSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swPowerStatusChg detection level."
        ::= { swEquipPowerNotifyMgmt 1 }

    swPowerFailureSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swPowerFailure detection level."
        ::= { swEquipPowerNotifyMgmt 2 }

    swPowerRecoverSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swPowerRecover detection level."
        ::= { swEquipPowerNotifyMgmt 3 }

-- -----------------------------------------------------------------------------
    swFanFailureSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swFanFailure detection level."
        ::= { swEquipFanNotifyMgmt 1 }

    swFanRecoverSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swFanRecover detection level."
        ::= { swEquipFanNotifyMgmt 2 }

-- -----------------------------------------------------------------------------
    swHighTemperatureSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swHighTemperature detection level."
        ::= { swEquipTemperatureNotifyMgmt 1 }

    swHighTemperatureRecoverSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swHighTemperatureRecover detection level."
        ::= { swEquipTemperatureNotifyMgmt 2 }

   swLowTemperatureSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swLowTemperature detection level."
        ::= { swEquipTemperatureNotifyMgmt 3 }

    swLowTemperatureRecoverSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "Indicates the swLowTemperatureRecover detection level."
        ::= { swEquipTemperatureNotifyMgmt 4 }


-- -----------------------------------------------------------------------------
	swEquipUnitNotifyPrefix 	 OBJECT IDENTIFIER ::= { swEquipUnitNotification 0 }

    swUnitInsert NOTIFICATION-TYPE
        OBJECTS         { swUnitMgmtId

                        }
        STATUS          current
        DESCRIPTION     "Unit Hot Insert notification."
        ::= { swEquipUnitNotifyPrefix 1 }

    swUnitRemove NOTIFICATION-TYPE
        OBJECTS         { swUnitMgmtId

                        }
        STATUS          current
        DESCRIPTION     "Unit Hot Remove notification."
        ::= { swEquipUnitNotifyPrefix 2 }

    swUnitFailure NOTIFICATION-TYPE
        OBJECTS         { swUnitMgmtId
                        }
        STATUS          current
        DESCRIPTION     "Unit Failure notification."
        ::= { swEquipUnitNotifyPrefix 3 }

-- -----------------------------------------------------------------------------
	swEquipPowerNotifyPerfix 	 OBJECT IDENTIFIER ::= { swEquipPowerNotification 0 }

    swPowerStatusChg  NOTIFICATION-TYPE
        OBJECTS         { swPowerUnitIndex,
        					swPowerID,
        					swPowerStatus
                        }
        STATUS          current
        DESCRIPTION     "Power Status change notification."
        ::= { swEquipPowerNotifyPerfix  1 }


    swPowerFailure  NOTIFICATION-TYPE
        OBJECTS         { swPowerUnitIndex,
        					swPowerID,
        					swPowerStatus
                        }
        STATUS          current
        DESCRIPTION     "Power Failure notification."
        ::= { swEquipPowerNotifyPerfix  2 }

    swPowerRecover NOTIFICATION-TYPE
        OBJECTS         {swPowerUnitIndex,
        					 swPowerID,
        					 swPowerStatus
                        }
        STATUS          current
        DESCRIPTION     "Power Recover notification."
        ::= { swEquipPowerNotifyPerfix  3 }

-- -----------------------------------------------------------------------------
    swEquipFanNotifyPrefix 	 OBJECT IDENTIFIER ::= { swEquipFanNotification 0 }

    swFanFailure NOTIFICATION-TYPE
        OBJECTS         { swFanUnitIndex,
        					swFanID

                        }
        STATUS          current
        DESCRIPTION     "Fan Failure notification."
        ::= { swEquipFanNotifyPrefix 1 }

    swFanRecover NOTIFICATION-TYPE
        OBJECTS         { swFanUnitIndex,
        					swFanID
                        }
        STATUS          current
        DESCRIPTION     "Fan Recover notification."
        ::= { swEquipFanNotifyPrefix 2 }

-- -----------------------------------------------------------------------------
    swEquipTemperatureNotifyPrefix 	 OBJECT IDENTIFIER ::= { swEquipTemperatureNotification 0 }

    swHighTemperature NOTIFICATION-TYPE
        OBJECTS         { swTemperatureUnitIndex,
        				  swTemperatureCurrent
                        }
        STATUS          current
        DESCRIPTION     "High Temperature notification."
        ::= { swEquipTemperatureNotifyPrefix 1 }

    swHighTemperatureRecover NOTIFICATION-TYPE
        OBJECTS         { swTemperatureUnitIndex,
        				  swTemperatureCurrent
                        }
        STATUS          current
        DESCRIPTION     "High Temperature notification."
        ::= { swEquipTemperatureNotifyPrefix 2 }

    swLowTemperature NOTIFICATION-TYPE
        OBJECTS         { swTemperatureUnitIndex,
        				  swTemperatureCurrent
                        }
        STATUS          current
        DESCRIPTION     "Low Temperature notification."
        ::= { swEquipTemperatureNotifyPrefix 3 }

    swLowTemperatureRecover NOTIFICATION-TYPE
        OBJECTS         { swTemperatureUnitIndex,
        				  swTemperatureCurrent
                        }
        STATUS          current
        DESCRIPTION     "Low Temperature notification."
        ::= { swEquipTemperatureNotifyPrefix 4 }

--- -----------------------------------------------------------------------------
    swNotificationBindings OBJECT IDENTIFIER ::= { swEquipmentNotify 3 }


END
