-- -----------------------------------------------------------------------------
-- MIB NAME : IP-MAC Binding Common mib
-- FILE NAME: IPMacBind.mib
-- DATE     : 2007/05/23
-- VERSION  : 2.01
-- PURPOSE  : To construct the MIB structure of IP-MAC Binding
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Revision 2.01 2007/5/23 14:35PM by Nic Liu
-- [New Object]
-- [1]Add object swIpMacBindingBlockedTime in swIpMacBindingBlockedTable
-- Request by Nic for DES30XXR4.1
--
-- Version 2.00, 2007/03/27, Yedda
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------

IP-MAC-BIND-MIB DEFINITIONS ::= BEGIN

    IMPORTS
        MODULE-IDENTITY,OBJECT-TYPE,IpAddress, Unsigned32
                                        FROM SNMPv2-SMI
        MacAddress, RowStatus, DateAndTime           FROM SNMPv2-TC
        DisplayString                   FROM RFC1213-MIB
    	SnmpAdminString					FROM SNMP-FRAMEWORK-MIB

        dlink-common-mgmt				FROM DLINK-ID-REC-MIB;

		VlanId					::= INTEGER (1..4094)

    swIpMacBindMIB MODULE-IDENTITY
	    LAST-UPDATED "0705230000Z"
	    ORGANIZATION "D-Link Crop."
	    CONTACT-INFO
	        "http://support.dlink.com"
	    DESCRIPTION
		    "The Structure of IP-MAC binding management for the
		    proprietary enterprise."
        ::= { dlink-common-mgmt 23 }

    PortList                ::= OCTET STRING(SIZE (0..127))



	swIpMacBindingCtrl           OBJECT IDENTIFIER ::= { swIpMacBindMIB 1 }
	swIpMacBindingInfo           OBJECT IDENTIFIER ::= { swIpMacBindMIB 2 }
    swIpMacBindingPortMgmt       OBJECT IDENTIFIER ::= { swIpMacBindMIB 3 }
	swIpMacBindingMgmt           OBJECT IDENTIFIER ::= { swIpMacBindMIB 4 }
	swIpMacBindingNotify 	     OBJECT IDENTIFIER ::= { swIpMacBindMIB 5 }

-- -----------------------------------------------------------------------------
-- swIpMacBindingCtrl           OBJECT IDENTIFIER ::= { swIpMacBindMIB 1 }
-- -----------------------------------------------------------------------------
	swIpMacBindingTrapLogState OBJECT-TYPE
    		SYNTAX  INTEGER {
                	other(1),
                	enable(2),
                	disable(3)
            	}
    		MAX-ACCESS  read-write
    		STATUS  current
    		DESCRIPTION
            		"When enabled(2),whenever there is a new MAC that violates the pre-defined
            		 IP MAC Binding configuration, a trap will be sent out and the relevant information
            		 will be logged into the system."
        	::= { swIpMacBindingCtrl 1 }

   	swIpMacBindingACLMode OBJECT-TYPE
    		SYNTAX  INTEGER {
                	other(1),
                	enable(2),
                	disable(3)
            	}
    		MAX-ACCESS  read-write
    		STATUS  current
    		DESCRIPTION
            		"When enabled(2), the IP-MAC Binding function will use the ACL mode."
        	::= { swIpMacBindingCtrl 2 }

-- -----------------------------------------------------------------------------
-- swIpMacBindingPortMgmt       OBJECT IDENTIFIER ::= { swIpMacBindMIB 3 }
-- -----------------------------------------------------------------------------
	swIpMacBindingAllPortState OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						enable(2),
						disable(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"This object enables\disables IP-MAC binding on all ports of the system.
			 This action is only applicable when users have write privileges,
			 and can only be viewed when users have read privileges."
		::= { swIpMacBindingPortMgmt 1 }


	swIpMacBindingPortTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingPortEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "The table specifies the IP-MAC binding function of a specified port."
		::= { swIpMacBindingPortMgmt 2 }

	swIpMacBindingPortEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingPortEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "A list of information about the IP-MAC binding function of a port."
		INDEX { swIpMacBindingPortIndex }
		::= { swIpMacBindingPortTable 1 }

	SwIpMacBindingPortEntry ::=
        SEQUENCE {
            swIpMacBindingPortIndex
                INTEGER,
			swIpMacBindingPortState
				INTEGER,
			swIpMacBindingPortZeroIPState
				INTEGER
       }

	swIpMacBindingPortIndex OBJECT-TYPE
		SYNTAX		INTEGER(0..65535)
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"This object indicates the module's port number.(1..Max port
			 number in the module)."
		::= { swIpMacBindingPortEntry 1 }

	swIpMacBindingPortState OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						enable(2),
						disable(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"This object enables/disables IP-MAC binding on the specified port."
		::= { swIpMacBindingPortEntry 2 }

	swIpMacBindingPortZeroIPState OBJECT-TYPE
		SYNTAX		INTEGER{
						enabled(1),
						disabled(2)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"Specifies whether to allow ARP packets with the SIP address 0.0.0.0,
			 regardless if the IP address 0.0.0.0 is set in the binding list or not.
			 When set to enable, the ARP with SIP 0.0.0.0 is allowed. When set to disable,
			 ARP with SIP 0.0.0.0 is dropped.
			Note:
			 This option does not affect the IP-MAC-Port binding ACL Mode.
			"
		::= { swIpMacBindingPortEntry 3 }

-- -----------------------------------------------------------------------------
-- swIpMacBindingMgmt          OBJECT IDENTIFIER ::= { swIpMacBindMIB 4 }
-- -----------------------------------------------------------------------------
	swIpMacBindingTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "This table specifies IP-MAC binding information."
		::= { swIpMacBindingMgmt 1 }

	swIpMacBindingEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "IP-MAC binding entry used to add\delete\configure the address
			  pair of the switch's authorized user database."
		INDEX { swIpMacBindingIpIndex }
		::= { swIpMacBindingTable 1 }

	SwIpMacBindingEntry ::=
        SEQUENCE {
            	swIpMacBindingIpIndex
                	IpAddress,
			    swIpMacBindingMac
				    MacAddress,
			    swIpMacBindingStatus
				    RowStatus,
      			swIpMacBindingPorts
        			PortList,
      			swIpMacBindingAction
      				INTEGER,
      			swIpMacBindingMode
      				INTEGER
       }

	swIpMacBindingIpIndex OBJECT-TYPE
		SYNTAX		IpAddress
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The IP address of IP-MAC binding."
		::= { swIpMacBindingEntry 1 }

	swIpMacBindingMac OBJECT-TYPE
		SYNTAX		MacAddress
		MAX-ACCESS	read-create
		STATUS		current
		DESCRIPTION
			"The MAC address of IP-MAC binding."
		::= { swIpMacBindingEntry 2 }

	swIpMacBindingStatus OBJECT-TYPE
		SYNTAX		RowStatus
		MAX-ACCESS	read-create
		STATUS		current
		DESCRIPTION
			"The status of this entry."
		::= { swIpMacBindingEntry 3 }

  	swIpMacBindingPorts OBJECT-TYPE
    		SYNTAX    PortList
    		MAX-ACCESS  read-create
    		STATUS    current
    		DESCRIPTION
      			"The port members of this entry."
    		::= { swIpMacBindingEntry 4 }

  	swIpMacBindingAction OBJECT-TYPE
    		SYNTAX    INTEGER{
            		inactive(1),
            		active(2)
            	}
    		MAX-ACCESS  read-only
    		STATUS    current
    		DESCRIPTION
      			"The action of this entry."
    		::= { swIpMacBindingEntry 5 }

  	swIpMacBindingMode OBJECT-TYPE
    		SYNTAX    INTEGER{
            		arp(1),
            		acl(2)
            	}
    		MAX-ACCESS  read-create
    		STATUS    current
    		DESCRIPTION
      			"The mode of this entry."
    		::= { swIpMacBindingEntry 6 }

	swIpMacBindingBlockedTable OBJECT-TYPE
		SYNTAX		SEQUENCE OF SwIpMacBindingBlockedEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "This table displays information regarding blocked MAC addresses."
		::= { swIpMacBindingMgmt 2 }

	swIpMacBindingBlockedEntry OBJECT-TYPE
		SYNTAX		SwIpMacBindingBlockedEntry
		MAX-ACCESS  not-accessible
		STATUS		current
		DESCRIPTION
		      "The entry cannot be created or configured. It can be deleted only."
		INDEX { swIpMacBindingBlockedVID, swIpMacBindingBlockedMac }
		::= { swIpMacBindingBlockedTable 1 }

	SwIpMacBindingBlockedEntry ::=
        SEQUENCE {
            swIpMacBindingBlockedVID
                	VlanId,
			swIpMacBindingBlockedMac
				MacAddress,
			swIpMacBindingBlockedVlanName
				DisplayString,
			swIpMacBindingBlockedPort
				INTEGER,
			swIpMacBindingBlockedType
				INTEGER,
			swIpMacBindingBlockedTime
			  DateAndTime
       }

	swIpMacBindingBlockedVID OBJECT-TYPE
		SYNTAX		VlanId
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The object specifies the VLAN ID."
		::= { swIpMacBindingBlockedEntry 1 }

	swIpMacBindingBlockedMac OBJECT-TYPE
		SYNTAX		MacAddress
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The MAC address which was blocked."
		::= { swIpMacBindingBlockedEntry 2 }

	swIpMacBindingBlockedVlanName OBJECT-TYPE
		SYNTAX		DisplayString
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"This object specifies the VLAN name."
		::= { swIpMacBindingBlockedEntry 3 }

	swIpMacBindingBlockedPort OBJECT-TYPE
		SYNTAX		INTEGER(0..65535)
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"The port with which the MAC is associated."
		::= { swIpMacBindingBlockedEntry 4 }

	swIpMacBindingBlockedType OBJECT-TYPE
		SYNTAX		INTEGER{
						other(1),
						blockByAddrBind(2),
						delete(3)
						}
		MAX-ACCESS	read-write
		STATUS		current
		DESCRIPTION
			"The value is always blockByAddrBind. This entry will be deleted when the value is set to 'delete'."
		::= { swIpMacBindingBlockedEntry 5 }

	swIpMacBindingBlockedTime OBJECT-TYPE
		SYNTAX		DateAndTime
		MAX-ACCESS	read-only
		STATUS		current
		DESCRIPTION
			"This object specifies the last time that this entry was generated."
		::= { swIpMacBindingBlockedEntry 6 }

-- -----------------------------------------------------------------------------
-- swIpMacBindingNotify        OBJECT IDENTIFIER ::= { swIpMacBindMIB 5 }
-- -----------------------------------------------------------------------------
	swIpMacBindingNotifyPrefix   OBJECT IDENTIFIER ::= { swIpMacBindingNotify 0 }

	swIpMacBindingViolationTrap NOTIFICATION-TYPE
    	OBJECTS  { 	  swIpMacBindingPorts,
    		  		  swIpMacBindingViolationIP,
    		  		  swIpMacBindingViolationMac
                 }
        STATUS  current
        	DESCRIPTION
            		"When the IP-MAC Binding trap is enabled, if there's a new MAC that violates the pre-defined
            		port security configuration, a trap will be sent out."
        	::= { swIpMacBindingNotifyPrefix 1 }

	swIpMacBindingNotificationBidings   OBJECT IDENTIFIER ::= { swIpMacBindingNotify 2 }

    	swIpMacBindingViolationIP OBJECT-TYPE
        	SYNTAX  MacAddress
        	MAX-ACCESS  accessible-for-notify
        	STATUS  current
        	DESCRIPTION
            		"This object indicates the MAC address that violates the IP-MAC Binding configuration."
        	::= { swIpMacBindingNotificationBidings 1 }

    	swIpMacBindingViolationMac OBJECT-TYPE
        	SYNTAX  MacAddress
        	MAX-ACCESS  accessible-for-notify
        	STATUS  current
        	DESCRIPTION
            		"This object indicates the IP address that violates the IP-MAC Binding configuration."
        	::= { swIpMacBindingNotificationBidings 2 }

END
