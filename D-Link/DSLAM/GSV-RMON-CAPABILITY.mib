-- *****************************************************************
-- GSPN RmonCapabilities MIB
--
-- AGENT-CAPABILITIES mib for RMON-MIB 
--
-- May 2003, Subodh Kumar
--
-- Copyright(c) 2002 by GlobeSpanVirata, Inc.
--
-- *****************************************************************

GSV-RMON-CAPABILITY DEFINITIONS ::= BEGIN

IMPORTS
        MODULE-IDENTITY
                FROM SNMPv2-SMI
        AGENT-CAPABILITIES
                FROM SNMPv2-CONF
     	  columbiaPackets
	          FROM GSV-ENTERPRISE-INFO-MIB;
   
gsvRmonCapability MODULE-IDENTITY
        LAST-UPDATED    "200407290000Z"
        ORGANIZATION    "Conexant Systems, Inc."
        CONTACT-INFO
                " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
        DESCRIPTION
                "The Agent Capabilities for RMON-MIB."
        REVISION "200306330000Z"
        DESCRIPTION
                "Initial Version of the MIB module."
                
       REVISION	    "200407290000Z"	       -- 29 July 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."                
        ::= { columbiaPackets 9910}

gsvRmonCapabilityV2R50 AGENT-CAPABILITIES

PRODUCT-RELEASE "Columbia Release 2.5"
STATUS          current
DESCRIPTION     "RMON MIB Capabilities"

SUPPORTS RMON-MIB
INCLUDES { rmonEtherStatsGroup }



--no VARIATION for  etherStatsIndex

VARIATION   etherStatsDataSource
ACCESS	read-only
DESCRIPTION "Write access is not supported and has a fixed value of '0.0'"

VARIATION   etherStatsDropEvents
ACCESS      not-implemented
DESCRIPTION "Not supported."

--no VARIATION for  etherStatsOctets

--no VARIATION for  etherStatsPkts

--no VARIATION for  etherStatsBroadcastPkts

--no VARIATION for  etherStatsMulticastPkts

VARIATION   etherStatsCRCAlignErrors
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION 	etherStatsUndersizePkts
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION 	etherStatsOversizePkts
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION 	etherStatsFragments
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION 	etherStatsJabbers
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION   etherStatsCollisions
ACCESS      not-implemented
DESCRIPTION "Not supported."

--no VARIATION for  etherStatsPkts64Octets

--no VARIATION for  etherStatsPkts65to127Octets

--no VARIATION for  etherStatsPkts128to255Octets

--no VARIATION for  etherStatsPkts256to511Octets

--no VARIATION for  etherStatsPkts512to1023Octets

--no VARIATION for  etherStatsPkts1024to1518Octets

VARIATION	etherStatsOwner
ACCESS	read-create
DESCRIPTION "This object is mandatory at the time of row creation but can not be modified once 
		the row is created"

VARIATION	etherStatsStatus
SYNTAX   	INTEGER { valid(1), createRequest(2), invalid(4) }
--no		{ underCreation(3) }
DESCRIPTION "Unable to detect all states."

    ::= { gsvRmonCapability 1 }

END

