-- *****************************************************************
-- GSPN AdslLineCapabilities MIB
--
-- AGENT-CAPABILITIES mib for ADSL-LINE-MIB 
--
-- May 2003, Subodh Kumar
--
-- Copyright(c) 2002 by GlobeSpanVirata, Inc.
--
-- *****************************************************************

GSV-ADSL-LINE-CAPABILITY DEFINITIONS ::= BEGIN

IMPORTS
        MODULE-IDENTITY
                FROM SNMPv2-SMI
        AGENT-CAPABILITIES
                FROM SNMPv2-CONF
     	  columbiaPackets
	          FROM GSV-ENTERPRISE-INFO-MIB;
   
gsvAdslLineCapability MODULE-IDENTITY
        LAST-UPDATED    "200407290000Z"
        ORGANIZATION    "Conexant Systems, Inc."
        CONTACT-INFO
                " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
        DESCRIPTION
                "The Agent Capabilities for ADSL-LINE-MIB."
        REVISION "200305130000Z"
        DESCRIPTION
                "Initial Version of the MIB module."
                
       REVISION	    "200407290000Z"	       -- 29 July 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."                 
        ::= { columbiaPackets 9902 }

gsvAdslLineCapabilityV2R00 AGENT-CAPABILITIES

PRODUCT-RELEASE "Columbia Release 2.00"
STATUS          current
DESCRIPTION     "ADSL-LINE MIB Capabilities"

SUPPORTS ADSL-LINE-MIB
INCLUDES { adslLineGroup, adslLineConfProfileControlGroup , adslPhysicalGroup, adslChannelGroup, adslAtucPhysPerfIntervalGroup, adslAturAtucPhysPerfIntervalGroup, adslAtucChanPerformanceGroup, adslLineAlarmConfProfileGroup, adslAturChanPerformanceGroup, adslAturPhysPerfRawCounterGroup, adslAturPhysPerfIntervalGroup, adslLineConfProfileGroup }

--no VARIATION for  adslLineCoding

--no VARIATION for  adslLineType

VARIATION   adslLineSpecific
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION   adslLineConfProfile
SYNTAX      SnmpAdminString (SIZE (0..32))
DESCRIPTION "Supported with limitation. Agent supports (0..32) instead of (1..32).
		 This object is optional at the time of row creation but can not be modified once 
		 the row is created."

VARIATION   adslLineAlarmConfProfile
SYNTAX      SnmpAdminString (SIZE (0..32))
DESCRIPTION "Supported with limitation. Agent supports (0..32) instead of (1..32).
		 This object is optional at the time of row creation but can not be modified once 
		 the row is created."

VARIATION   adslLineConfProfileRowStatus
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION   adslLineAlarmConfProfileRowStatus
ACCESS      not-implemented
DESCRIPTION "Not supported."



--no VARIATION for  adslAtucInvSerialNumber

--no VARIATION for  adslAtucInvVendorID

--no VARIATION for  adslAtucInvVersionNumber

--no VARIATION for  adslAtucCurrSnrMgn

--no VARIATION for  adslAtucCurrAtn

VARIATION   adslAtucCurrStatus
DESCRIPTION "The length of BITS will be 4 octets instead of 2 octet."

--no VARIATION for  adslAtucCurrOutputPwr

--no VARIATION for  adslAtucCurrAttainableRate

--no VARIATION for  adslAturInvSerialNumber

--no VARIATION for  adslAturInvVendorID

--no VARIATION for  adslAturInvVersionNumber

--no VARIATION for  adslAturCurrSnrMgn

--no VARIATION for  adslAturCurrAtn

VARIATION   adslAturCurrStatus
DESCRIPTION "The length of BITS will be 4 octets instead of 1 octet."

--no VARIATION for  adslAturCurrOutputPwr

--no VARIATION for  adslAturCurrAttainableRate



--no VARIATION for  adslAtucChanInterleaveDelay

--no VARIATION for  adslAtucChanCurrTxRate

--no VARIATION for  adslAtucChanPrevTxRate

--no VARIATION for  adslAtucChanCrcBlockLength

--no VARIATION for  adslAturChanInterleaveDelay

--no VARIATION for  adslAturChanCurrTxRate

--no VARIATION for  adslAturChanPrevTxRate

--no VARIATION for  adslAturChanCrcBlockLength

            

--no VARIATION for  adslAtucPerfValidIntervals

--no VARIATION for  adslAtucPerfInvalidIntervals

--no VARIATION for  adslAtucPerfCurr15MinTimeElapsed

--no VARIATION for  adslAtucPerfCurr15MinLofs

--no VARIATION for  adslAtucPerfCurr15MinLoss

--no VARIATION for  adslAtucPerfCurr15MinLols

--no VARIATION for  adslAtucPerfCurr15MinLprs

--no VARIATION for  adslAtucPerfCurr15MinESs

--no VARIATION for  adslAtucPerfCurr15MinInits

--no VARIATION for  adslAtucPerfCurr1DayTimeElapsed

--no VARIATION for  adslAtucPerfCurr1DayLofs

--no VARIATION for  adslAtucPerfCurr1DayLoss

--no VARIATION for  adslAtucPerfCurr1DayLols

--no VARIATION for  adslAtucPerfCurr1DayLprs

--no VARIATION for  adslAtucPerfCurr1DayESs

--no VARIATION for  adslAtucPerfCurr1DayInits

--no VARIATION for  adslAtucPerfPrev1DayMoniSecs

--no VARIATION for  adslAtucPerfPrev1DayLofs

--no VARIATION for  adslAtucPerfPrev1DayLoss

--no VARIATION for  adslAtucPerfPrev1DayLols

--no VARIATION for  adslAtucPerfPrev1DayLprs

--no VARIATION for  adslAtucPerfPrev1DayESs

--no VARIATION for  adslAtucPerfPrev1DayInits

--no VARIATION for  adslAtucIntervalLofs

--no VARIATION for  adslAtucIntervalLoss

--no VARIATION for  adslAtucIntervalLols

--no VARIATION for  adslAtucIntervalLprs

--no VARIATION for  adslAtucIntervalESs

--no VARIATION for  adslAtucIntervalInits

--no VARIATION for  adslAtucIntervalValidData


--no VARIATION for  adslAtucChanReceivedBlks

--no VARIATION for  adslAtucChanTransmittedBlks

--no VARIATION for  adslAtucChanCorrectedBlks

--no VARIATION for  adslAtucChanUncorrectBlks

--no VARIATION for  adslAtucChanPerfValidIntervals

--no VARIATION for  adslAtucChanPerfInvalidIntervals

--no VARIATION for  adslAtucChanPerfCurr15MinTimeElapsed

--no VARIATION for  adslAtucChanPerfCurr15MinReceivedBlks

--no VARIATION for  adslAtucChanPerfCurr15MinTransmittedBlks

--no VARIATION for  adslAtucChanPerfCurr15MinCorrectedBlks

--no VARIATION for  adslAtucChanPerfCurr15MinUncorrectBlks

--no VARIATION for  adslAtucChanPerfCurr1DayTimeElapsed

--no VARIATION for  adslAtucChanPerfCurr1DayReceivedBlks

--no VARIATION for  adslAtucChanPerfCurr1DayTransmittedBlks

--no VARIATION for  adslAtucChanPerfCurr1DayCorrectedBlks

--no VARIATION for  adslAtucChanPerfCurr1DayUncorrectBlks

--no VARIATION for  adslAtucChanPerfPrev1DayMoniSecs

--no VARIATION for  adslAtucChanPerfPrev1DayReceivedBlks

--no VARIATION for  adslAtucChanPerfPrev1DayTransmittedBlks

--no VARIATION for  adslAtucChanPerfPrev1DayCorrectedBlks

--no VARIATION for  adslAtucChanPerfPrev1DayUncorrectBlks

--no VARIATION for  adslAtucChanIntervalReceivedBlks

--no VARIATION for  adslAtucChanIntervalTransmittedBlks

--no VARIATION for  adslAtucChanIntervalCorrectedBlks

--no VARIATION for  adslAtucChanIntervalUncorrectBlks

--no VARIATION for  adslAtucChanIntervalValidData


--no VARIATION for  adslAtucThresh15MinLofs

--no VARIATION for  adslAtucThresh15MinLoss

--no VARIATION for  adslAtucThresh15MinLols

--no VARIATION for  adslAtucThresh15MinLprs

--no VARIATION for  adslAtucThresh15MinESs

--no VARIATION for  adslAtucThreshFastRateUp

--no VARIATION for  adslAtucThreshInterleaveRateUp

--no VARIATION for  adslAtucThreshFastRateDown

--no VARIATION for  adslAtucThreshInterleaveRateDown

--no VARIATION for  adslAtucInitFailureTrapEnable

--no VARIATION for  adslAturThresh15MinLofs

--no VARIATION for  adslAturThresh15MinLoss

--no VARIATION for  adslAturThresh15MinLprs

--no VARIATION for  adslAturThresh15MinESs

--no VARIATION for  adslAturThreshFastRateUp

--no VARIATION for  adslAturThreshInterleaveRateUp

--no VARIATION for  adslAturThreshFastRateDown

--no VARIATION for  adslAturThreshInterleaveRateDown


--no VARIATION for  adslAturChanReceivedBlks

--no VARIATION for  adslAturChanTransmittedBlks

--no VARIATION for  adslAturChanCorrectedBlks

--no VARIATION for  adslAturChanUncorrectBlks

--no VARIATION for  adslAturChanPerfValidIntervals

--no VARIATION for  adslAturChanPerfInvalidIntervals

--no VARIATION for  adslAturChanPerfCurr15MinTimeElapsed

--no VARIATION for  adslAturChanPerfCurr15MinReceivedBlks

--no VARIATION for  adslAturChanPerfCurr15MinTransmittedBlks

--no VARIATION for  adslAturChanPerfCurr15MinCorrectedBlks

--no VARIATION for  adslAturChanPerfCurr15MinUncorrectBlks

--no VARIATION for  adslAturChanPerfCurr1DayTimeElapsed

--no VARIATION for  adslAturChanPerfCurr1DayReceivedBlks

--no VARIATION for  adslAturChanPerfCurr1DayTransmittedBlks

--no VARIATION for  adslAturChanPerfCurr1DayCorrectedBlks

--no VARIATION for  adslAturChanPerfCurr1DayUncorrectBlks

--no VARIATION for  adslAturChanPerfPrev1DayMoniSecs

--no VARIATION for  adslAturChanPerfPrev1DayReceivedBlks

--no VARIATION for  adslAturChanPerfPrev1DayTransmittedBlks

--no VARIATION for  adslAturChanPerfPrev1DayCorrectedBlks

--no VARIATION for  adslAturChanPerfPrev1DayUncorrectBlks


--no VARIATION for  adslAturChanIntervalReceivedBlks

--no VARIATION for  adslAturChanIntervalTransmittedBlks

--no VARIATION for  adslAturChanIntervalCorrectedBlks

--no VARIATION for  adslAturChanIntervalUncorrectBlks

--no VARIATION for  adslAturChanIntervalValidData


--no VARIATION for  adslAturPerfLofs

--no VARIATION for  adslAturPerfLoss

--no VARIATION for  adslAturPerfLprs

--no VARIATION for  adslAturPerfESs


--no VARIATION for  adslAturPerfValidIntervals

--no VARIATION for  adslAturPerfInvalidIntervals

--no VARIATION for  adslAturPerfCurr15MinTimeElapsed

--no VARIATION for  adslAturPerfCurr15MinLofs

--no VARIATION for  adslAturPerfCurr15MinLoss

--no VARIATION for  adslAturPerfCurr15MinLprs

--no VARIATION for  adslAturPerfCurr15MinESs

--no VARIATION for  adslAturPerfCurr1DayTimeElapsed

--no VARIATION for  adslAturPerfCurr1DayLofs

--no VARIATION for  adslAturPerfCurr1DayLoss

--no VARIATION for  adslAturPerfCurr1DayLprs

--no VARIATION for  adslAturPerfCurr1DayESs

--no VARIATION for  adslAturPerfPrev1DayMoniSecs

--no VARIATION for  adslAturPerfPrev1DayLofs

--no VARIATION for  adslAturPerfPrev1DayLoss

--no VARIATION for  adslAturPerfPrev1DayLprs

--no VARIATION for  adslAturPerfPrev1DayESs

--no VARIATION for  adslAturIntervalLofs

--no VARIATION for  adslAturIntervalLoss 

--no VARIATION for  adslAturIntervalLprs

--no VARIATION for  adslAturIntervalESs

--no VARIATION for  adslAturIntervalValidData


--no VARIATION for  adslAtucConfRateMode

VARIATION   adslAtucConfRateChanRatio
ACCESS      not-implemented
DESCRIPTION "Not supported."

--no VARIATION for  adslAtucConfTargetSnrMgn
--no VARIATION for  adslAtucConfMaxSnrMgn

VARIATION   adslAtucConfMinSnrMgn
ACCESS      not-implemented
DESCRIPTION "Not supported."

--no VARIATION for  adslAtucConfDownshiftSnrMgn

--no VARIATION for  adslAtucConfUpshiftSnrMgn

--no VARIATION for  adslAtucConfMinUpshiftTime

--no VARIATION for  adslAtucConfMinDownshiftTime

--no VARIATION for  adslAtucChanConfFastMinTxRate

--no VARIATION for  adslAtucChanConfInterleaveMinTxRate

--no VARIATION for  adslAtucChanConfFastMaxTxRate

--no VARIATION for  adslAtucChanConfInterleaveMaxTxRate

--no VARIATION for  adslAtucChanConfMaxInterleaveDelay

VARIATION   adslAturConfRateMode
ACCESS      not-implemented
DESCRIPTION "Not supported."

VARIATION   adslAturConfRateChanRatio
ACCESS      not-implemented
DESCRIPTION "Not supported."

--no VARIATION for  adslAturConfTargetSnrMgn

--no VARIATION for  adslAturConfMaxSnrMgn

--no VARIATION for  adslAturConfMinSnrMgn

--no VARIATION for  adslAturConfDownshiftSnrMgn

--no VARIATION for  adslAturConfUpshiftSnrMgn

--no VARIATION for  adslAturConfMinUpshiftTime

--no VARIATION for  adslAturConfMinDownshiftTime

--no VARIATION for  adslAturChanConfFastMinTxRate

--no VARIATION for  adslAturChanConfInterleaveMinTxRate

--no VARIATION for  adslAturChanConfFastMaxTxRate

--no VARIATION for  adslAturChanConfInterleaveMaxTxRate

--no VARIATION for  adslAturChanConfMaxInterleaveDelay


    ::= { gsvAdslLineCapability 1 }

END

