GSV-IGMP-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress,
	  	RowStatus
		      FROM SNMPv2-TC
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvIgmpMIB MODULE-IDENTITY
    LAST-UPDATED "200406150000Z"	       -- 15 June 2004
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
     " The MIB module that provides objects that are proprietary GSPN. The MIB includes following proprietary Groups / Tables.
	1.) gsvIgmpSnoopGroup
	2.)gsvIgmpSnoopPortConfigTable 
	3.)gsvIgmpSnoopQuerierConfigTable
	4.)gsvIgmpSnoopStatsTable"		

-- Revision History

       REVISION	    "200304100000Z"	       -- 10 April 2003
       DESCRIPTION  "This is the initial version of this MIB."

       REVISION	    "200401070000Z"	       -- 07 Jan 2004
       DESCRIPTION  "The MIB is modified for IGMP Snoop Enhancement for Columbia Release 3.0."

       REVISION	    "200404240000Z"	       -- 24 April 2004
       DESCRIPTION  "PPPoA to PPPoE Tunneling- architectural and bridging changes."

       REVISION	    "200405110000Z"	       -- 11 May 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."
       
       REVISION	    "200405280000Z"	       -- 28 May 2004
       DESCRIPTION  "Auto sensing changes."

       REVISION	    "200406110000Z"	       -- 11 June 2004
       DESCRIPTION  "Incorporated the change for Vlan Transparency."
       
       REVISION	    "200406150000Z"	       -- 15 June 2004
       DESCRIPTION  "Updated the description of gsvIgmpSnoopQuerierConfigVlanId of table gsvIgmpSnoopQuerierConfigTable."

    ::= { columbiaPackets 8 }

gsvIgmpSnoopGroup		OBJECT IDENTIFIER ::= { gsvIgmpMIB 1 }


gsvIgmpSnoopQueryInterval OBJECT-TYPE
	SYNTAX       INTEGER ( 1..25 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies the Query Interval timer (in secs) used to calculate entry age out timer, when no Reports or Queries are received on that entry. This value multiplied by 10 should be greater then the Query Interval configured at the Router. The time for which an entry created at Igmpsnoop module exists, if no messages are received for it is approximately (((gsvIgmpSnoopQueryInterval*10)*gsvIgmpSnoopRobustness) + Query Response Time received in Last Query)."
	DEFVAL   { 12 }
	::= { gsvIgmpSnoopGroup 1 }


gsvIgmpSnoopAnxiousTimer OBJECT-TYPE
	SYNTAX       INTEGER ( 1..65535 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This is the maximum time (in seconds) before which IgmpSnoop module will forward all IGMP membership reports received. It is started once, whenever the first membership report is received for a group, to ensure that reports are forwarded for a sufficiently long time, to take care of any lost reports. The unit is seconds."
	DEFVAL   { 125 }
	::= { gsvIgmpSnoopGroup 2 }


gsvIgmpSnoopV1HostTimer OBJECT-TYPE
	SYNTAX       INTEGER ( 1..65535 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This is the maximum time (in seconds) for which IgmpSnooping module can assume that there are Version 1 group members present, for the group for which this timer is running. The unit is seconds."
	DEFVAL   { 130 }
	::= { gsvIgmpSnoopGroup 3 }


gsvIgmpSnoopLastMembQueryIntvl OBJECT-TYPE
	SYNTAX       INTEGER ( 1..255 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The Last Member Query Interval is the Max Response Time inserted into Group-Specific Queries sent in response to Leave Group messages, and is also the amount of time between Group-Specific Query messages.  This value may be tuned to modify the leave latency of the network.  A reduced value results in reduced time to detect the loss of the last member of a group."
	DEFVAL   { 10 }
	::= { gsvIgmpSnoopGroup 4 }


gsvIgmpSnoopRobustness OBJECT-TYPE
	SYNTAX       INTEGER ( 2..255 )
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This allows tuning for the expected packet loss on a subnet. IgmpSnooping module is robust to [RobustnessVar] packet losses."
	DEFVAL   { 2 }
	::= { gsvIgmpSnoopGroup 5 }


gsvIgmpSnoopStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(0)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies whether Igmp Snooping is to be enabled in the system."
	DEFVAL   { 1 }
	::= { gsvIgmpSnoopGroup 6 }


gsvIgmpSnoopReportSuppression OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(2)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies whether Report Suppression is enabled or not."
	DEFVAL   { disable }
	::= { gsvIgmpSnoopGroup 7 }



gsvIgmpSnoopPortConfigTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvIgmpSnoopPortConfigEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains IGMP snooping configuration information for each bridge port. An entry in this table shall not be applicable for a bridge port created over PPPOE interface."
	::= { gsvIgmpMIB 2 }


gsvIgmpSnoopPortConfigEntry OBJECT-TYPE
	SYNTAX       GsvIgmpSnoopPortConfigEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvIgmpSnoopPortConfigTable.
		The Table is indexed by gsvIgmpSnoopPortConfigPortId."
	INDEX        { gsvIgmpSnoopPortConfigPortId }
	::= { gsvIgmpSnoopPortConfigTable 1 }

GsvIgmpSnoopPortConfigEntry ::= SEQUENCE {
		gsvIgmpSnoopPortConfigPortId     INTEGER,
		gsvIgmpSnoopPortConfigStatus     INTEGER,
		gsvIgmpSnoopPortConfigLeaveMode     INTEGER }


gsvIgmpSnoopPortConfigPortId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..65535 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"A Bridge Port, for which IGMP Snooping needs to be enabled or disabled"
	::= { gsvIgmpSnoopPortConfigEntry 1 }


gsvIgmpSnoopPortConfigStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			enable(1),
			disable(0)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies whether or not IGMP Snooping is to be enabled on the port."
	DEFVAL   { enable }
	::= { gsvIgmpSnoopPortConfigEntry 2 }


gsvIgmpSnoopPortConfigLeaveMode OBJECT-TYPE
	SYNTAX       INTEGER{
			normal(1),
			fast(2),
			fastNormal(3)
		}
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"This object specifies Igmp Snooping Leave message processing mode for the port. If the mode is set to 'normal', the Leave message is forwarded to the Querier and then based on the Query received from Querier the Leave processing is triggered. If the mode is set to 'fast', the port is immediately deleted from that multicast group on Leave message reception and then the Leave message is forwarded. The mode should be set to 'fast' for a port only if there is one host behind the port. This is because if there are multiple hosts behind the port then it will lead to traffic disruption for other hosts who might still be listening to that multicast group. If mode is set to 'fastNormal', the Leave message is forwarded and the Leave processing is triggered immediately without waiting for any trigger from the Querier. 'fastNormal' mode thus saves the delay (equal to the time taken for Leave message to reach router and Querier processing time for it and the time taken for Query to reach IGMP Snoop module) in Leave pro


cessing."
	DEFVAL   { normal }
	::= { gsvIgmpSnoopPortConfigEntry 3 }


gsvIgmpSnoopQuerierConfigTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvIgmpSnoopQuerierConfigEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"A table of IGMP Snooping Querier ports configured for a particular vlan. An entry in this table shall not be applicable for a bridge port created over PPPOE interface."
	::= { gsvIgmpMIB 3 }


gsvIgmpSnoopQuerierConfigEntry OBJECT-TYPE
	SYNTAX       GsvIgmpSnoopQuerierConfigEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvIgmpSnoopQuerierConfigTable.
		The Table is indexed by gsvIgmpSnoopQuerierConfigVlanId and gsvIgmpSnoopQuerierConfigPortId."
	INDEX        { gsvIgmpSnoopQuerierConfigVlanId, gsvIgmpSnoopQuerierConfigPortId }
	::= { gsvIgmpSnoopQuerierConfigTable 1 }

GsvIgmpSnoopQuerierConfigEntry ::= SEQUENCE {
		gsvIgmpSnoopQuerierConfigVlanId     INTEGER,
		gsvIgmpSnoopQuerierConfigPortId     INTEGER,
		gsvIgmpSnoopQuerierConfigStatus     INTEGER,        
		gsvIgmpSnoopQuerierConfigRowStatus     RowStatus }


gsvIgmpSnoopQuerierConfigVlanId OBJECT-TYPE
	SYNTAX       INTEGER ( 0..4094 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"VlanId to uniquely identify the vlanid of the entry for which the IgmpSnooping Querier is configured/learnt. In devices supporting 'Shared Vlan for multicast' capability, the information for a Querier port is shared across vlans. Hence vlan id is an optional parameter. In devices supporting 'Independent Vlan for multicast' capability, each vlan can have its own information for a Querier port. Hence vlanid is a mandatory parameter in all the commands other than - get. For No Vlan case, vlan id is not required.This Feature is not supported for vlan with vlanid as GS_UNREGISTERED_VLANID."
	::= { gsvIgmpSnoopQuerierConfigEntry 1 }


gsvIgmpSnoopQuerierConfigPortId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..65535 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"A Bridge Port, belonging to the Vlan (dot1qVlanIndex), on which the Querier exists"
	::= { gsvIgmpSnoopQuerierConfigEntry 2 }


gsvIgmpSnoopQuerierConfigStatus OBJECT-TYPE
	SYNTAX       INTEGER{
			learned(1),
			mgmt(2)
		}
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"Specifies whether Querier Port has been learnt dynamically  or configured by the user."
	::= { gsvIgmpSnoopQuerierConfigEntry 3 }

gsvIgmpSnoopQuerierConfigRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This defines the row-status of the Querier entry. Currenly only the following values can be configured.  CreateAndGo - This is used to create an entry. Destroy - This is used to delete an entry."
	::= { gsvIgmpSnoopQuerierConfigEntry 4 }


gsvIgmpSnoopStatsTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvIgmpSnoopStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains IGMP Snooping statistics for every bridge port belonging to a particular multicast group in a particular VLAN."
	::= { gsvIgmpMIB 4 }


gsvIgmpSnoopStatsEntry OBJECT-TYPE
	SYNTAX       GsvIgmpSnoopStatsEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvIgmpSnoopStatsTable.
		The Table is indexed by gsvIgmpSnoopStatsVlanId, gsvIgmpSnoopStatsMcastAddress and gsvIgmpSnoopStatsPortId."
	INDEX        { gsvIgmpSnoopStatsVlanId, gsvIgmpSnoopStatsMcastAddress, gsvIgmpSnoopStatsPortId }
	::= { gsvIgmpSnoopStatsTable 1 }

GsvIgmpSnoopStatsEntry ::= SEQUENCE {
		gsvIgmpSnoopStatsVlanId     INTEGER,
		gsvIgmpSnoopStatsMcastAddress     MacAddress,
		gsvIgmpSnoopStatsPortId     INTEGER,
		gsvIgmpSnoopStatsQueryReceived     Unsigned32,
		gsvIgmpSnoopStatsReportReceived     Unsigned32,
		gsvIgmpSnoopStatsReset     TruthValue }


gsvIgmpSnoopStatsVlanId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..4094 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"VlanId to uniquely identify the vlanid of the entry, for which the IgmpSnooping statistics are desired"
	::= { gsvIgmpSnoopStatsEntry 1 }


gsvIgmpSnoopStatsMcastAddress OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"A multicast MAC Address, learnt through Igmp Snooping, within the Vlan (igmpVlanIndex), to uniquely identify the entry, for which the IgmpSnooping statistics are desired. The range of accepted values is 01:00:5E:00:00:00 to 01:00:5E:7F:FF:FF."
	::= { gsvIgmpSnoopStatsEntry 2 }


gsvIgmpSnoopStatsPortId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..65535 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"A Bridge Port belonging to the Vlan (igmpVlanIndex) and Group (igmpsnoopMcastAddress), for which the IgmpSnooping statistics are desired."
	::= { gsvIgmpSnoopStatsEntry 3 }


gsvIgmpSnoopStatsQueryReceived OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"The number of Igmp Queries received on the port belonging to a particular multicast group and Vlan."
	::= { gsvIgmpSnoopStatsEntry 4 }


gsvIgmpSnoopStatsReportReceived OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"The number of Membership Reports received on the port belonging to a particular multicast group and Vlan."
	::= { gsvIgmpSnoopStatsEntry 5 }


gsvIgmpSnoopStatsReset OBJECT-TYPE
	SYNTAX       TruthValue
	MAX-ACCESS   read-write
	STATUS       current
	DESCRIPTION
		"The setting of this parameter results in statistics reset.The only value supported is 'true'."
	::= { gsvIgmpSnoopStatsEntry 6 }
END 