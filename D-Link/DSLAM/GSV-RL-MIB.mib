GSV-RL-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TruthValue,
	  	RowStatus
		      FROM SNMPv2-TC
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvRlMIB MODULE-IDENTITY
    LAST-UPDATED "200407290000Z"	       -- 29 July 2004
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
   " The MIB module that provides proprietary objects. The MIB includes following propritery Tables.
	1.) gsvBridgeRlInstanceMapTable
	2.) gsvRlInstanceTable
	3.) gsvRlProfileTable"
	
-- Revision History

       REVISION	    "200407220000Z"	       -- 22 July 2004
       DESCRIPTION  "This is the initial version of this MIB."        

       REVISION	    "200407290000Z"	       -- 29 July 2004
       DESCRIPTION  "Updated the Organization and Contact-Info." 
    ::= { columbiaPackets 21 }


gsvRlProfileTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvRlProfileEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains information of rate limiter profiles created in the sytem."
	::= { gsvRlMIB 1 }


gsvRlProfileEntry OBJECT-TYPE
	SYNTAX       GsvRlProfileEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvRlProfileTable.
		The Table is indexed by gsvRlProfileId."
	INDEX        { gsvRlProfileId }
	::= { gsvRlProfileTable 1 }

GsvRlProfileEntry ::= SEQUENCE {
		gsvRlProfileId     INTEGER,
		gsvRlRate     INTEGER,
		gsvRlMaxBurstSize     INTEGER,
		gsvRlProfileRowStatus     RowStatus }


gsvRlProfileId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..48 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Rate limiter's profile identifier, which uniquely identifies the profile."
	::= { gsvRlProfileEntry 1 }


gsvRlRate OBJECT-TYPE
	SYNTAX       INTEGER ( 0..300000)
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This field defines the maximum rate at which data is allowed per second. Its unit is packets/second."
	DEFVAL   { 300000 }
	::= { gsvRlProfileEntry 2 }


gsvRlMaxBurstSize OBJECT-TYPE
	SYNTAX       INTEGER ( 4..255 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This field defines the maximum burst size in terms of packets."
	DEFVAL   { 255 }
	::= { gsvRlProfileEntry 3 }


gsvRlProfileRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This field specifies the row status."
	::= { gsvRlProfileEntry 4 }


gsvBridgeRlInstanceMapTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvBridgeRlInstanceMapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains information about mapping between bridge port, flow and rate limiter instance. This rate limiter instance is applied only on the incoming traffic. An entry in this table can be created only when the entry corresponding to field 'gsvBridgeRlInstanceMapInstanceId' has been created in the 'gsvRlInstanceTable' table. An entry in this table shall not be applicable for a bridge port created over PPPOE interface."
	::= { gsvRlMIB 2 }


gsvBridgeRlInstanceMapEntry OBJECT-TYPE
	SYNTAX       GsvBridgeRlInstanceMapEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvBridgeRlInstanceMapTable.
		The Table is indexed by gsvBridgeRlInstanceMapPortId and gsvBridgeRlInstanceMapFlowType."
	INDEX        { gsvBridgeRlInstanceMapPortId, gsvBridgeRlInstanceMapFlowType }
	::= { gsvBridgeRlInstanceMapTable 1 }

GsvBridgeRlInstanceMapEntry ::= SEQUENCE {
		gsvBridgeRlInstanceMapPortId     Unsigned32,
		gsvBridgeRlInstanceMapFlowType     INTEGER,
		gsvBridgeRlInstanceMapInstanceId     INTEGER,
		gsvBridgeRlInstanceMapRowStatus     RowStatus }


gsvBridgeRlInstanceMapPortId OBJECT-TYPE
	SYNTAX       Unsigned32
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Port Identifier with which an instance is associated. If the value of this field is -1 (0xFFFFFFFF), it indicates 'all' bridge ports. For a particular flow, instance map cannot be created both for a specific port as well as for 'all' the bridge ports."
	::= { gsvBridgeRlInstanceMapEntry 1 }


gsvBridgeRlInstanceMapFlowType OBJECT-TYPE
	SYNTAX       INTEGER{
			bcast(1),
			unregmcast(2),
			unknownucast(3)
		}
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This field identifies the flow for which this instance is applied."
	::= { gsvBridgeRlInstanceMapEntry 2 }


gsvBridgeRlInstanceMapInstanceId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..144 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This object identifies the Rate limiting instance."
	::= { gsvBridgeRlInstanceMapEntry 3 }

gsvBridgeRlInstanceMapRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This object specifies the row status."
	::= { gsvBridgeRlInstanceMapEntry 4 }


gsvRlInstanceTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvRlInstanceEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table contains information of rate limiter instances created in the sytem. An instance of a rate limiting profile is an entity that can be used to apply the rate limiting on the flows emanating from multiple ports. An instance of a profile, when applied to multiple bridge ports means that the rate limting will be applied on the aggregated flows from all the ports to which the instance has been attached. Also whenever a profile is changed, the changes are reflected in all of its instances. An entry in this table can be created only when the entry corresponding to field 'gsvRlProfileId' has been created in the 'gsvRlProfile' table."
	::= { gsvRlMIB 3 }


gsvRlInstanceEntry OBJECT-TYPE
	SYNTAX       GsvRlInstanceEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvRlInstanceTable.
		The Table is indexed by gsvRlInstanceId."
	INDEX        { gsvRlInstanceId }
	::= { gsvRlInstanceTable 1 }

GsvRlInstanceEntry ::= SEQUENCE {
		gsvRlInstanceId     INTEGER,
		gsvRlInstanceProfileId     INTEGER,
		gsvRlInstanceRowStatus     RowStatus }


gsvRlInstanceId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..144 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Rate limiter's instance identifier, which uniquely identifies a profile instance."
	::= { gsvRlInstanceEntry 1 }


gsvRlInstanceProfileId OBJECT-TYPE
	SYNTAX       INTEGER ( 1..48 )
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This object identifies the Rate limiting profile whose instance is being created."
	::= { gsvRlInstanceEntry 2 }


gsvRlInstanceRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This field specifies the row status. This field cannot be modified after its value is set to 'active'."
	::= { gsvRlInstanceEntry 3 }
END 
