GSV-ACL-MIB DEFINITIONS ::= BEGIN

IMPORTS
		MODULE-IDENTITY, OBJECT-TYPE, OBJECT-IDENTITY, mib-2,
		IpAddress, Integer32, Counter32, Gauge32, TimeTicks, Unsigned32
		      FROM SNMPv2-SMI
		TEXTUAL-CONVENTION, TimeStamp, TruthValue, DisplayString, MacAddress,
	  	RowStatus
		      FROM SNMPv2-TC
	  	columbiaPackets
		      FROM GSV-ENTERPRISE-INFO-MIB;

  gsvAclMIB MODULE-IDENTITY
    LAST-UPDATED "200405280000Z"	       -- 28 May 2004
    ORGANIZATION "Conexant Systems, Inc."
    CONTACT-INFO " Conexant Systems, Inc.
		 100 Schulz Drive,
		 Red Bank, NJ 07701,U.S
		 Phone: +1.732.345.7500"
    DESCRIPTION
     " The MIB module that provides objects that are proprietary to GSPN. The MIB includes following propritery Tables.
	1.) gsvAclGlobalMacListTable
	2.) gsvAclPortMacListTable"		

-- Revision History

       REVISION	    "200306050000Z"	       -- 05 June 2003
       DESCRIPTION  "This is the initial version of this MIB."

       REVISION	    "200404240000Z"	       -- 24 April 2004
       DESCRIPTION  "PPPoA to PPPoE Tunneling- architectural and bridging changes."
       
       REVISION	    "200405110000Z"	       -- 11 May 2004
       DESCRIPTION  "Updated the Organization and Contact-Info."    
       
       REVISION	    "200405280000Z"	       -- 28 May 2004
       DESCRIPTION  "This MIB was updated for autosensing changes."          
    ::= { columbiaPackets 13 }


gsvAclGlobalMacListTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAclGlobalMacListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table stores the configuration information of Global MAC Address list of ACL."
	::= { gsvAclMIB 1 }


gsvAclGlobalMacListEntry OBJECT-TYPE
	SYNTAX       GsvAclGlobalMacListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAclGlobalMacListTable.
		The Table is indexed by gsvAclGlobalMacListEntryVal."
	INDEX        { gsvAclGlobalMacListEntryVal }
	::= { gsvAclGlobalMacListTable 1 }

GsvAclGlobalMacListEntry ::= SEQUENCE {
		gsvAclGlobalMacListEntryVal     MacAddress,
		gsvAclGlobalMacListDeny     INTEGER,
		gsvAclGlobalMacListTrack     INTEGER,
		gsvAclGlobalMacListNumPortChange     INTEGER,
		gsvAclGlobalMacListRowStatus     RowStatus }


gsvAclGlobalMacListEntryVal OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Unicast Source MAC Address, which needs to be tracked/denied access."
	::= { gsvAclGlobalMacListEntry 1 }


gsvAclGlobalMacListDeny OBJECT-TYPE
	SYNTAX       INTEGER{
			disable(1),
			enable(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This parameter specifies if the MAC address is to be denied access."
	DEFVAL   { enable }
	::= { gsvAclGlobalMacListEntry 2 }


gsvAclGlobalMacListTrack OBJECT-TYPE
	SYNTAX       INTEGER{
			disable(1),
			enable(2)
		}
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This parameter specifies if the MAC address is to be tracked accross different ports. A trap is raised in case packet from the address comes over a port for the first time and when it changes the port."
	DEFVAL   { disable }
	::= { gsvAclGlobalMacListEntry 3 }


gsvAclGlobalMacListNumPortChange OBJECT-TYPE
	SYNTAX       INTEGER
	MAX-ACCESS   read-only
	STATUS       current
	DESCRIPTION
		"This specifies the number of times port has been changed by the MAC address."
	DEFVAL   { 0 }
	::= { gsvAclGlobalMacListEntry 4 }


gsvAclGlobalMacListRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status and is used for creating and deleting the row from this table. Currently only the following values can be configured, CreateAndGo - This is used to create an entry. Destroy - This is used to delete an entry."
	::= { gsvAclGlobalMacListEntry 5 }




gsvAclPortMacListTable OBJECT-TYPE
	SYNTAX       SEQUENCE OF GsvAclPortMacListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"This table stores the configuration information of allowed MAC Addresses on a bridge port. An entry in this table shall not be applicable for a bridge port created over PPPOE interface."
	::= { gsvAclMIB 2 }


gsvAclPortMacListEntry OBJECT-TYPE
	SYNTAX       GsvAclPortMacListEntry
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"An entry (conceptual row) in the gsvAclPortMacListTable.
		The Table is indexed by gsvAclPortMacListPortid and gsvAclPortMacListEntryVal."
	INDEX        { gsvAclPortMacListPortid, gsvAclPortMacListEntryVal }
	::= { gsvAclPortMacListTable 1 }

GsvAclPortMacListEntry ::= SEQUENCE {
		gsvAclPortMacListPortid     INTEGER,
		gsvAclPortMacListEntryVal     MacAddress,
		gsvAclPortMacListRowStatus     RowStatus }


gsvAclPortMacListPortid OBJECT-TYPE
	SYNTAX       INTEGER ( 1..386 )
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"The bridge port id, for which the port MAC Address entry is created."
	::= { gsvAclPortMacListEntry 1 }


gsvAclPortMacListEntryVal OBJECT-TYPE
	SYNTAX       MacAddress
	MAX-ACCESS   not-accessible
	STATUS       current
	DESCRIPTION
		"Unicast Source MAC Address, which is to be allowed access over the particular port."
	::= { gsvAclPortMacListEntry 2 }

gsvAclPortMacListRowStatus OBJECT-TYPE
	SYNTAX       RowStatus
	MAX-ACCESS   read-create
	STATUS       current
	DESCRIPTION
		"This specifies the row status."
	::= { gsvAclPortMacListEntry 3 }
END 
