<-- -----------------------------------------------------------------------------
-- MIB NAME : Access Authentication Control(AAC) Common mib
-- FILE NAME: AAC.mib
-- DATE     : 2009/01/21
-- VERSION  : 2.03
-- PURPOSE  : To construct the MIB structure of Access Authentication Control
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Version 2.03, 2009/01/21, Eli
--  [Modification]
--    Revise the range of "swAACServerAuthKey" to 0..254 from 0..255.
-- Notes: Requested by LAB, DES3200
--
-- Version 2.02, 2008/02/12, Ronald
--  [Modification]
--    To add the objects "swAACCtrlEnableAdminSet".
-- Notes: Requested by D-Link, DES3800
--
-- Version 2.01, 2007/04/30, Yedda
--  [Modification]
--    To recover the objects "swAACAPSSHLoginMethod" and "swAACAPSSHEnableMethod".
-- Notes: Requested by CD, DES3400
--
-- Version 2.00, 2007/03/27, Yedda
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
                                                 "The Link AAC module MIB." "http://support.dlink.com"                       Q"This object indicates if the state of TACACS is enabled or 
	        disabled."                       R"This object indicates if the state of XTACACS is enabled or 
	        disabled."                       R"This object indicates if the state of TACACS+ is enabled or 
	        disabled."                       Q"This object indicates if the state of Radius is enabled or 
	        disabled."                       P"This object indicates if the state of Local is enabled or 
	        disabled."                       O"This object indicates if the state of None is enabled or 
	        disabled."                       V"This object indicates if the Access Authentication is enabled or
	        disabled."                           @"The max number of Login method lists supported by the system ."                       A"The max number of Enable method lists supported by the system ."                       <"The max number of Server Group's supported by the system ."                       9"The max number of AAC servers supported by the system ."                       V"A table that contains information about Login authentication method
	        lists."                       '"A list of the Authentication methods."                       l"A human-readable text string of the method list. The string is the
	        same as swAACLoginMethodName."                       �"The type of Login method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific
	        users. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACLoginMethodListType is with the keyword type and it is writeable,
	        just as the swAACLoginMethodListType is in server_group(3)."                       �"The type of Login method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific 
		users. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACLoginMethodListType is with the keyword type and it is writeable,
	        just as the swAACLoginMethodListType is in server_group(3)."                       �"The type of Login method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific
		 users. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACLoginMethodListType is with the keyword type and It is writeable,
	        just as the swAACLoginMethodListType is in server_group(3)."                       �"The type of Login method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific
 		users. The keyword type includes local(6), local-enable(7), and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACLoginMethodListType is for keyword type. And It is writeable,
	        just as the swAACLoginMethodListType is in server_group(3)."                       �"This object indicates the status of this entry. A set request with
	        RowStatus 'CreateAndGo' will fail when the current entry of
	        the table is equal to the maximum number of
	        swAACLoginMethodListIndex."                       W"A table that contains information about Enable authentication method
	        lists."                       '"A list of the Authentication methods."                       m"A human-readable text string of the method list. The string is the
	        same as swAACEnableMethodName."                       �"The type of Enable method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for user
	        specific. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACEnableMethodListType is with the keyword type and it is writeable,
	        just as the swAACEnableMethodListType is in server_group(3)."                       �"The type of Enable method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for user
	        specific. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACEnableMethodListType is with the keyword type and it is writeable,
	        just as the swAACEnableMethodListType is in server_group(3)."                       �"The type of Enable method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific
		users. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACEnableMethodListType is for keyword type and it is writeable,
	        just as the swAACEnableMethodListType is in server_group(3)."                       �"The type of Enable method list. The group type includes tacas(1),
	        xtacacs(2), tacacs-plus(3), radius(4) and server-group(5) for specific 
		users. The keyword type includes local(6), local-enable(7) and
	        none(8)."                       �"A human-readable text string of the server group. It would be null
	        as swAACEnableMethodListType is for keyword type and It is writeable,
	        just as the swAACEnableMethodListType is in server_group(3)."                       �"This object indicates the status of this entry. A set request with
	        RowStatus 'CreateAndGo' will fail when the current entry in
	        the table is equal to the maximum number of
	        swAACEnableMethodListIndex."                               �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via local
	        console"                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via telnet."                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via SSH."                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via HTTP."                           �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via local
	        console."                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via telnet."                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via SSH."                       �"Specifies the way to execute authentication when logging into the
	        system and the method for authentication.Access system via HTTP."                           7"Timeout in seconds for login authentication response."                       s"The amount of login authentication attempts, if login failure 
			 exceeds, connection or access will be locked."                       9"A table that contains information about a server group."                       )"A list of the groups including servers."                       3"A human-readable text string of the method group."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       "The IP address of the Server"                       5"The authentication protocol provided by the Server."                       "The IP address of the Server"                       5"The authentication protocol provided by the Server."                       "The IP address of the Server"                       5"The authentication protocol provided by the Server."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       �"This object indicates the status of this entry. A set request with 
		RowStatus 'CreateAndGo' will fail when the current entry in the table 
		is equal to swAACMaxServerGroup. "                       1"A table that contains information about severs."                       '"A list of the information of server ."                       "The IP address of Server"                       5"The authentication protocol provided by the Server."                       �"The TCP/IP port. The default authentication port is 49 and port numbers
	        5001 to 65535 are valid settings in the system , other values, except
	        49,5001-65535 will work when set in this object."                       C"The key used during the authentication process .It is write-only."                       "Server response timeout ."                       "The client retry count ."                       �"This object indicates the status of this entry. A set request with
	        RowStatus 'CreateAndGo' will fail when the current entry in
	        the table is equal to swAACMaxServer. "                       ="A table that is used to add/delete a server from the group."                       9"A table that is used to add/delete a server from group."                       3"A human-readable text string of the method group."                       "The IP address of the Server"                       5"The authentication protocol provided by the Server."                       K"This object is used to add or delete an entry from swAACServerGroupTable."                      