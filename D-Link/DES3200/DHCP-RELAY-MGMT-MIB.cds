#-- -----------------------------------------------------------------------------
-- MIB NAME : DHCP-RELAY-MGMT-MIB
-- FILE NAME: DHCPRelay.mib
-- DATE     : 2011/03/14
-- VERSION  : 1.05
-- PURPOSE  : To construct the MIB structure of DHCP Relay function for
--            proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Version 1.05, 2011/03/14, Garnet Tang
-- [New Object]
-- Add swDHCPLocalRelayOption82RemoteIDType and swDHCPLocalRelayOption82RemoteID
-- to support configurable remote ID for DHCP Local Relay with Option 82.
-- Notes: requested by Garnet Tang for DES3526/50 R6.
--
-- Version 1.04, 2011/03/01, Jane Lu
-- [New Object]
-- Add swDHCPLocalRelayOption82Table to support per port configure DHCP local 
-- relay option 82 policy.
-- Notes: requested by Jane Lu for DGS3200.
--
-- Version 1.03, 2010/05/28, Easion Tang
-- [New Object]
-- [1]Added swDHCPLocalRelayMgmt to support DHCP relay in local VLAN.
-- [2]Added swDHCPRelayVlanCtrlTable to support per VLAN configure relay server.
-- Notes: requested by Easion Tang for DES3528.
--
-- Version 1.02, 2009/1/8, Kelvin Tao
-- [Modification]
-- Modify the description of swDHCPRelayOption60String and swDHCPRelayOption61ClientID.
-- For fixed bug(string as index, which length exceed the allowed length of
-- an SNMP object identifier, it's impossible to represent in table).
--
-- Version 1.01, 2008/8/04, Marco Visaya
-- Added objects for remote id
-- added swDHCPRelayOption82RemoteIDType
-- added swDHCPRelayOption82RemoteID
-- Notes: requested by Marco Visaya for DES30XXP project.
-- 
-- Version 1.00, 2008/1/31, Kelvin Tao
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
                                                 H"The structure of DHCP relay management for the proprietary enterprise." "http://support.dlink.com"                   k"This object indicates whether the DHCP relay function is enabled or
             disabled.
            "                       w"This object indicates the maximum number of router hops that
             the BOOTP packets can cross.
            "                       �"This object indicates the minimum time in seconds within which
             the switch must relay the DHCP request. If this time is
             exceeded, the switch will drop the DHCP packet.
            "                                   f"This object indicates whether the DHCP relay option 82 function is enabled or
            disabled."                       {"This object indicates whether the DHCP relay option 82 check function is enabled or
             disabled.
            "                       8"This object indicates the DHCP relay option 82 policy."                       �"This object indicates the type of remote ID. If the type is default,
            the remote ID will be the MAC address of the device. Otherwise, the remote
            ID can be defined by writing to the swDHCPRelayOption82RemoteID object."                      S"This object displays the current remote ID of the device. If swDHCPRelayOption82RemoteIDType
            is set to default, the value will be the MAC address of the device, and this object cannot
            be modified. If swDHCPRelayOption82RemoteIDType is set to user-defined, a new value can
            be written to this object."                           u"This object indicates whether the DHCP relay option 60 function is enabled or
             disabled.
            "                      "This object indicates default mode that has no option 60 matching rules.

             relay (1): means to relay the packet to the IP address which is specified
            	by swDHCPRelayOption60DefRelayIp.
             drop (2): means to drop the packet.
            "                       �"This table indicates the default relay servers for the packet
             that has no option 60 matching rules.
            "                       �"A list of information indicates the default relay servers for the packet
             that has no option 60 matching rules.
            "                       C"This object indicates the IP address of the default relay server."                       1"This object indicates the status of this entry."                       L"This table indicates the relay servers for the packet for option 60 rules."                       e"A list of information indicates the relay servers for the packet
             for option 60 rules."                      6"This object indicates the string of this entry.

             Note that it is theoretically possible for a valid string
             to exceed the allowed length of an SNMP object identifier,
             and thus be impossible to represent in tables in this MIB
             that are indexed by string."                       ;"This object indicates the IP address of the relay server."                      O"This object indicates the match state of this entry.
            
             exact (1): Means the option 60 string in the packet must fully
            	match the specified string.
             partial (2): Means the option 60 string in the packet only needs to
            	partially match the specified string.
            "                       1"This object indicates the status of this entry."                           u"This object indicates whether the DHCP relay option 61 function is enabled or
             disabled.
            "                      "This object indicates the default mode has no option 61 matching rules.

             relay (1): Means to relay the packet to the IP address which is specified 
            	by swDHCPRelayOption61DefRelayIp.
             drop (2): Means to drop the packet.
            "                      T"This object indicates the IP address of the default relay server
             has no option 61 matching rules.

             Pay attention to when swDHCPRelayOption61Mode is:
             relay (1): This object must be set together.
             drop (2): This object can not be set, and it always returns to '0.0.0.0'.
            "                       L"This table indicates the relay servers for the packet for option 61 rules."                       s"A list of information indicates the relay servers for the packet
             for option 61 rules.
            "                       "This object indicates the client type of this entry.
            
             mac (1): Means the client-ID which is the MAC address of a client.
             string (2): Means the client-ID which is specified string by an administrator.
            "                      7"This object indicates the string of this entry.
            
             Note:
             When the swDHCPRelayOption61ClientType is mac (1),
             the format of this object should be XX.XX.XX.XX.XX.XX (MAC address).
             
             When the swDHCPRelayOption61ClientType is string (2),
             it is theoretically possible for a valid string
             to exceed the allowed length of an SNMP object identifier,
             and thus be impossible to represent in tables in this MIB
             that are indexed by the string."                       �"This object indicates mode of this entry.
            
             relay (1): Means to relay the packet to the IP address which is specified 
            	by swDHCPRelayOption61RelayIp.
             drop (2): Means to drop the packet.
            "                      *"This object indicates the IP address of the relay server.
            
             Pay attention to when swDHCPRelayOption61Mode is:
             relay (1): This object must be set together.
             drop (2): This object can not be set, and it always returns to '0.0.0.0'.
            "                       1"This object indicates the status of this entry."                       h"This table indicates the IP address as a destination to forward
             (relay) DHCP packets to."                       s"A list of information indicates the IP address as a destination
             to forward (relay) DHCP packets to."                       5"This object indicates the name of the IP interface."                       3"This object indicates the DHCP server IP address."                       1"This object indicates the status of this entry."                       h"This table indicates the IP address as a destination to forward
             (relay) DHCP packets to."                       s"A list of information indicates the IP address as a destination
             to forward (relay) DHCP packets to."                       3"This object indicates the DHCP server IP address."                       T"This object indicates the VLAN range (1-512)
			 	that added the specific server."                       W"This object indicates the VLAN range (513-1024)
			 	that added the specific server."                       X"This object indicates the VLAN range (1025-1536)
			 	that added the specific server."                       X"This object indicates the VLAN range (1537-2048)
			 	that added the specific server."                       X"This object indicates the VLAN range (2049-2560)
			 	that added the specific server."                       X"This object indicates the VLAN range (2561-3072)
			 	that added the specific server."                       X"This object indicates the VLAN range (3073-3584)
			 	that added the specific server."                       X"This object indicates the VLAN range (3585-4096)
			 	that added the specific server."                       1"This object indicates the status of this entry."                           \"This object indicates the state of the DHCP local relay
          function of the switch."                       N"This table is used to manage the DHCP local relay status
	 		for each VLAN."                       k"This object lists the current VLANs in the switch and their
	    	corresponding DHCP local relay status."                       $"This object indicates the VLAN ID."                       ^"This object indicates the state of the DHCP relay function
          of the specified VLAN."                       X"This table is used to manage the DHCP local relay option 82 policy
	 		for each port."                       u"This object lists the current ports in the switch and their
	    	corresponding DHCP local relay option 82 policy."                       S"This object indicates the module's port number.(1..Max port number in the module)"                       ]"This object indicates the policy of the DHCP local relay option 82
          of each port."                      "This object indicates the type of remote ID for DHCP Local Relay.
            If the type is default, the remote ID will be the MAC address of the device,
            otherwise, the remote ID can be defined by writing to the
            swDHCPLocalRelayOption82RemoteID object."                      "This object displays the current remote ID of the device for DHCP Local Relay.
            If swLocalDHCPRelayOption82RemoteIDType is set to default, the value will be the
            MAC address of the device, and this object cannot be modified. If
            swLocalDHCPRelayOption82RemoteIDType is set to user-defined, a new value can
            be written to this object."                      