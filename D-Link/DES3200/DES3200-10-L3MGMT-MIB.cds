	-- -----------------------------------------------------------------------------
-- MIB NAME : L3 Management MIB
-- FILE NAME: des3200-10-L3mgmt.mib
-- DATE     : 2009/11/07
-- VERSION  : 1.02
-- PURPOSE  : To construct the MIB structure of Layer 3 Network Management Information
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)  
--  
-- Version 1.02 2009/11/07,  Eli
-- Description:
-- [New Object]
-- Add swL3IpCtrlIpDhcpOption12State and swL3IpCtrlIpDhcpOption12HostName to 
-- support DHCP option 12 host name function.
--  
-- Version 1.01 2009/05/16, Eli Lin
-- Description:
-- [Remove Object]
-- swL3Ipv6CtrlRaState, swL3Ipv6CtrlRaMinRtrAdvInterval,
-- swL3Ipv6CtrlRaMaxRtrAdvInterval, swL3Ipv6CtrlRaLifeTime,
-- swL3Ipv6CtrlRaReachableTime, swL3Ipv6CtrlRaRetransTime,
-- swL3Ipv6CtrlRaHopLimit, swL3Ipv6CtrlRaManagedFlag,
-- swL3Ipv6CtrlRaOtherConfigFlag, swL3Ipv6AddressCtrlPreferredLifeTime,
-- swL3Ipv6AddressCtrlValidLifeTime, swL3Ipv6AddressCtrlOnLinkFlag,
-- swL3Ipv6AddressCtrlAutonomousFlag for not supporting
-- [New Object]
-- swL3IpCtrlInterfaceName, swL3IpCtrlIpv6LinkLocalAddress,
-- swL3IpCtrlIpv6LinkLocalPrefixLen, swL3IpCtrlIpv6LinkLocalAutoState
-- for IPV6 Automatic Link Local Address
--  
-- Version 1.00 2009/04/28, Eli Lin
-- Description:
-- [New Object]
-- Add swL2IpCtrlAllIpIfState, swL2Ipv6CtrlTable, 
-- swL2Ipv6AddressCtrlTable for IPv6
-- -----------------------------------------------------------------------------
   s"This data type is used to model IPv6 addresses.
		This is a binary string of 16 octets in network
		byte-order."                                                             I"The Structure of Layer 3 Network Management Information for enterprise." "http://support.dlink.com"                           /"This table contains IP interface information."                       6"A list of information about a specific IP interface."                       5"This object indicates the name of the IP interface."                       1"The IPv6 link local address for this interface."                       :"The IPv6 prefix length for this IPv6 link local address."                       ("The state of the IPv6 link local auto."                       f"Enable or disable insertion of option 12 in the DHCPDISCOVER and 
             DHCPREQUEST message."                      �"Specify the host name to be inserted in the DHCPDISCOVER and 
             DHCPREQUEST message. The specified host name must start with a 
             letter, end with a letter or digit, and have only letters, digits, 
             and hyphen as interior characters; the maximal length is 63. By 
             default, the host name is empty.
             When set an empty host name, means to clear the host name setting and 
             use the default value to encode option 12."                       :"This table contains IPv6 information of an IP interface."                       ;"A list of IPv6 information about a specific IP interface."                       5"This object indicates the name of the IP interface."                       ."Maximum Reassembly Size of the IP interface."                       Z"Neighbor solicitation's retransmit timer.
             The unit is set in milliseconds."                       E"This table contains IPv6 address information for each IP interface."                       6"A list of information about a specific IPv6 address."                       6"This object indicates the name of the IP interface. "                       "Specify the IPv6 address."                       3"Indicates the prefix length of this IPv6 address."                       �"This variable displays the status of the entry. The status is used
            for creating, modifying, and deleting instances of the objects 
            in this table."                       C"This object indicates all interface function state of the device."                      