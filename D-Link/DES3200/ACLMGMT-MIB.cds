+j-- -----------------------------------------------------------------------------
-- MIB NAME : Access Control List(ACL) Common mib
-- FILE NAME: ACL.mib
-- DATE     : 2010/03/02
-- VERSION  : 2.17
-- PURPOSE  : To construct the MIB structure of Access Control List
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Version 2.17, 2010/03/02, Stone Chen
-- Description:
-- [New Object]
-- 1. Add swACLEtherRuleMirrorGroupID, swACLIpRuleMirrorGroupID, SwACLPktContRuleMirrorGroupID,
--    swACLIpv6RuleMirrorGroupID, swACLPktContRuleOptionMirrorGroupID, swACLPktContRuleOption2MirrorGroupID,
--    swACLPktContRuleOption3MirrorGroupID to support mirror group ID in rule action.
-- 2. Add swACLIpRuleMaskUserMask to support mask of swACLIpRuleUserMask.
-- [Modification]
-- 1. Delete RSPAN action in related MIB objects.
-- Requested by Stone Chen for project DGS3600.
--
-- Version 2.16, 2009/10/29, Stone Chen
-- Description
-- 1. Change the unit of swAclMeterTrtcmCir, swAclMeterTrtcmPir, swAclMeterSrtcmCir
--    from 1Kbps to 64Kbps.
-- 2. Add swAclMeterTrtcmCir1kUnit, swAclMeterTrtcmPir1kUnit, swAclMeterSrtcmCir1kUnit
--    in swAclMeterTable to support 1Kbps rate unit. 
-- 3. Add swAclRateTable to support single rate two color metering.
-- 4. Obsolete swAclMeterRate, swAclMeterActionForRateExceed, swAclMeterRemarkDscp,
--    swAclMeterBurstSize in swAclMeterEntry. 
-- Requested by Stone Chen for project DES3810.
--
-- Version 2.15, 2009/05/24, Stone Chen
-- Description
-- 1. add swACLPktContMaskOption3
-- 2. add swACLPktContRuleOption3
-- 3. add rspan action
-- 4. add swACLIpv6IcmpOption
-- 5. add swACLIpv6RuleICMPType and swACLIpv6RuleICMPCode
-- 6. add swAclMeterTrtcmUnConformReplaceDscp and swAclMeterSrtcmUnConformReplaceDscp
-- Requested by Stone Chen for project DES3810
--
-- Version 2.14, 2009/03/18, Marco Visaya
-- Description
-- 1. add swACLPktContMaskOption2
-- 2. add swACLPktContRuleOption2
-- 3. add swACLEthernetVlanMask
-- 4. add swACLIpVlanMask
-- 5. add swACLEtherRuleMaskVlan
-- 6. add swACLEtherRuleMaskSrcMacAddress
-- 7. add swACLEtherRuleMaskDstMacAddress
-- 8. add swACLIpRuleMaskVlan
-- 9. add swACLIpRuleMaskSrcIpaddress
-- 10. add swACLIpRuleMaskDstIpaddress
-- 11. add swACLIpRuleMaskSrcPort
-- 12. add swACLIpRuleMaskDstPort
-- 13. add swACLIpv6RuleMaskSrcIpv6Addr
-- 14. add swACLIpv6RuleMaskDstIpv6Addr
-- 15. add swACLIpv6RuleMaskSrcPort
-- 16. add swACLIpv6RuleMaskDstPort
-- Requested by Marco Visaya for project DES3200
--
-- Version 2.13, 2009/01/05, Oran Tang
-- Description:
-- 1.add swACLEtherRuleMatchVID in swACLEtherRuleTable
-- 2.add swACLIpRuleMatchVID in swACLIpRuleTable
-- 3.add swCpuAclEtherRuleMatchVID in swCpuAclEtherRuleTable
-- 4.add swCpuAclIpRuleMatchVID in swCpuAclIpRuleTable
--   for config the VLAN-ID which has the access rule.
-- 5.modify the description of swACLEtherRuleVlan
-- 6.modify the description of swACLEtherRuleVID
-- 7.modify the description of swACLIpRuleVlan
-- 8.modify the description of swACLIpRuleVID
-- 9.modify the description of swCpuAclEtherRuleVlan
-- 10.modify the description of swCpuAclIpRuleVlan
-- Requested by Oran Tang for project DGS3700.
--
-- Revision 2.12 2008/12/26 by Ronald Hsu, Yedda Liao
-- Description:
-- 1.Add 'arp-spoofing(11)' and 'bpdu-tunnel(12)' in the value list of objects swACLEthernetOwner,
--   swACLIpOwner, swACLPktContMaskOwner, swACLIpv6MaskOwner, swACLEtherRuleOwner, swACLIpRuleOwner,
--   swACLPktContRuleOwner and swACLIpv6RuleOwner.
--   For arp spoofing and bpdu tunnel function, we need to add the two types of the owner on these objects. 
-- 2.Add 'pppoe(10)','arp-spoofing(11)' and 'bpdu-tunnel(12)' in the value list of objects
--   swACLPktContMaskOptionOwner,swACLPktContRuleOptionOwner.
--   For PPPoE circuit ID insertion, ARP spoofing and BPDU tunnel functions, we need to add the three types 
--   of the owner on these objects. 
--
-- Revision 2.11 2008/11/21 by Ronald Hsu
-- 1.Add 'set-drop-precedence(5)' in the value list of object swACLEtherRulePermit,
--   swACLIpRulePermit, swACLPktContRulePermit, swACLIpv6RulePermit.
--
-- Revision 2.10 2008/10/16 by Ronald Hsu
-- Description:
-- 1.Add pppoe(10) in the value list of objects swACLEthernetOwner, swACLIpOwner, swACLPktContMaskOwner,
-- swACLIpv6MaskOwner, swACLEtherRuleOwner, swACLIpRuleOwner, swACLPktContRuleOwner and swACLIpv6RuleOwner.
-- Requested by project DES3500.
--
-- Version 2.09, 2008/05/05, Bonnnie
-- Description:
-- 1.add ismvlan(8) and dhcp-relay(9) in the value list of objects swACLEthernetOwner,swACLIpOwner,swACLPktContMaskOwner,
-- swACLIpv6MaskOwner, swACLPktContMaskOptionOwner,swACLEtherRuleOwner,swACLIpRuleOwner,swACLPktContRuleOwner,
-- swACLIpv6RuleOwner and swACLPktContRuleOptionOwner.
-- Requested by Bonnnie cheng for project DHS3628.
--
-- Version 2.08, 2008/04/18, Marco
-- Description:
-- [New Object]
-- [Modification]
-- 1. change range of the ff nodes to include case node is not active:
--      swACLEtherRule8021P
--      swACLIpRuleDscp
--      swAclIpRuleType
--      swAclIpRuleCode
--      swACLIpRuleSrcPort
--      swACLIpRuleDstPort
--      swACLIpRuleProtoID
--      swCpuAclEtherRule8021P
--      swCpuAclIpRuleDscp
--      swCpuAclIpRuleType
--      swCpuAclIpRuleCode
--      swCpuAclIpRuleSrcPort
--      swCpuAclIpRuleDstPort
--      swCpuAclIpRuleProtoID
-- removed *replaceprioritywith objects
-- Requested by Marco Visaya for project DES30XXP.
--
-- Version 2.07, 2008/04/11, Marco
-- Description:
-- [New Object]
-- 1. Added swACLEtherRuleReplacePriorityWith
-- 2. Added swACLIPRuleReplacePriorityWith
-- [Modification]
-- 1. Remove the range of xxxProfileID, and xxxRxRate. The maximum value of the objects depend on the device.
-- Requested by Marco Visaya for project DES30XXP.
--
--
-- Version 2.06, 2008/04/02, Kelvin
-- Description:
-- [New Object]
-- 1.add objects swACLIpv6MaskUseProtoType, swACLIpv6MaskTcpOption, swACLIpv6MaskUdpOption
-- swACLIpv6MaskTCPorUDPSrcPortMask, swACLIpv6MaskTCPorUDPDstPortMask in swACLIpv6MaskTable.
-- 2.add objects swACLIpv6RuleProtocol, swACLIpv6RuleSrcPort, swACLIpv6RuleDstPort in swACLIpv6RuleTable.
-- Requested by Kelvin Tao for project DGS3700.
--
-- Version 2.05, 2008/02/20, Kelvin
-- Description:
-- [New Object]
-- 1.add objects swACLEtherRuleVID in swACLEtherRuleTable.
-- 2.add objects swACLIpRuleVID in swACLIpRuleTable.
-- 3.add objects swACLPktContRuleVID in swACLPktContRuleTable.
-- 4.add objects swACLIpv6RuleVID in swACLIpv6RuleTable.
-- 5.add objects swACLPktContRuleOptionVID in swACLPktContRuleOptionTable.
-- Requested by Kelvin Tao for project DGS3700.
--
-- Version 2.04, 2008/01/15, Yan
-- Description:
-- [New Object]
-- 1.add objects swACLEtherRuleEnableReplaceTosPrecedence, swACLEtherRuleRepTosPrecedence in swACLEtherRuleTable.
-- 2.add objects swACLIpRuleEnableReplaceTosPrecedence, swACLIpRuleRepTosPrecedence in swACLIpRuleTable.
-- 3.add objects swACLPktContRuleEnableReplaceTosPrecedence, swACLPktContRuleRepTosPrecedence in swACLPktContRuleTable.
-- 4.add objects swACLIpv6RuleEnableReplaceDscp, swACLIpv6RuleRepDscp, swACLIpv6RuleEnableReplaceTosPrecedence and
-- swACLIpv6RuleRepTosPrecedence in swACLIpv6RuleTable.
-- 5.add objects swACLPktContRuleOptionEnableReplaceTosPrecedence, swACLPktContRuleOptionRepTosPrecedence in
-- swACLPktContRuleOptionTable.
-- Requested by Yan Zhang for project DES35XX.
--
-- Version 2.03, 2007/12/27 by Ronald Hsu
-- 1.Add 'lease-renew(4)' in the value list of object swACLPktContRulePermit.
-- Requested by Ronald Hsu for project DES3828R4.
--
-- Version 2.02, 2007/12/18, Jenny
-- Description:
-- [New Object]
-- 1.add object swACLPktContMaskOptionProfileName in swACLPktContMaskOptionTable.
-- 2.add object swACLIpv6MaskProfileName in swACLIpv6MaskTable.
-- 3.add object swACLIpProfileName in swACLIpTable.
-- 4.add object swACLEthernetProfileName in swACLEthernetTable.
-- 5.add object swACLPktContMaskProfileName in swACLPktContMaskTable.
-- Requested by Jenny for project DES35XX.
--
-- Version 2.01, 2007/05/15, Yan
-- Description:
-- [Modification]
-- 1. add Value List remark-dscp(4) of object swAclMeterActionForRateExceed, change the access
-- of objects swAclMeterRate and swAclMeterActionForRateExceed from read-write to read-create for CLI.
-- 2. change the access of object swACLIpRuleProtocol from read-only to read-write for supporting
-- the new chip of project DGS3600R2.
-- [New Object]
-- 1. add objects swACLIpSrcMacAddrMask, swACLIpRuleSrcMacAddress for supporting the lab-out project DGS3400R2.
-- 2. add tables swACLCounterTable, swACLPktContMaskOptionTable and swACLPktContRuleOptionTable for CLI.
-- 3. add read-only objects swACLTotalUsedRuleEntries, swACLTotalUnusedRuleEntries, swACLEthernetUnusedRuleEntries,
-- swACLIpUnusedRuleEntries, swACLPktContMaskUnusedRuleEntries, swACLIpv6MaskUnusedRuleEntries for CLI.
-- 4. add objects swCpuAclEtherRuleEtherPort, swCpuAclIpRulePort, swCpuAclPktContRulePort, swCpuAclIpv6RulePort for CLI.
-- 5. add object swCpuACLMaskDelAllState for supporting the lab-out project DGS3400R2.
-- 6. add objects swAclMeterRemarkDscp, swAclMeterBurstSize, swAclMeterMode, swAclMeterTrtcmCir, swAclMeterTrtcmCbs,
-- swAclMeterTrtcmPir, swAclMeterTrtcmPbs, swAclMeterTrtcmColorMode, swAclMeterTrtcmConformState, swAclMeterTrtcmConformReplaceDscp,
-- swAclMeterTrtcmConformCounterState, swAclMeterTrtcmExceedState, swAclMeterTrtcmExceedReplaceDscp, swAclMeterTrtcmExceedCounterState,
-- swAclMeterTrtcmViolateState, swAclMeterTrtcmViolateReplaceDscp, swAclMeterTrtcmViolateCounterState, swAclMeterSrtcmCir,
-- swAclMeterSrtcmCbs, swAclMeterSrtcmEbs, swAclMeterSrtcmColorMode, swAclMeterSrtcmConformState, swAclMeterSrtcmConformReplaceDscp,
-- swAclMeterSrtcmConformCounterState, swAclMeterSrtcmExceedState, swAclMeterSrtcmExceedReplaceDscp, swAclMeterSrtcmExceedCounterState,
-- swAclMeterSrtcmViolateState, swAclMeterSrtcmViolateReplaceDscp, swAclMeterSrtcmViolateCounterState, swAclMeterRowStatus for CLI.
-- 7. add objects swACLEtherRuleRxRate, swACLIpRuleRxRate, swACLPktContRuleRxRate, swACLIpv6RuleRxRate for supporting
-- the older CLI Command, and these objects could be used for some projects.
-- 8. add swIBPACLEthernetTable, swIBPACLIpTable, swIBPACLEtherRuleTable, swIBPACLIpRuleTable for keeping the OID
-- of lab-out project DGS3400R2, but these objects can not be used for other project, so the status is obsolete.
-- Requested by Yan for DGS3600R2.
--
-- Version 2.00, 2007/03/27, Yedda
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
                   �"This data type is used to model IPv6 addresses.
                This is a binary string of 16 octets in network
                byte-order."                                                                             g"The structure of Access Control List information for the
                    proprietary enterprise." "http://support.dlink.com"                  9"Enable or disable CPU Interface Filtering (also called Software ACL).
             The default is disabled. If enabled, the filtering entries in the
             swAclRuleMgmt tables will be set to active if its RuleSwAclState is
             enabled. If disabled, the software ACL function will be disabled."                       ,"The total number of used ACL rule entries."                       ."The total number of unused ACL rule entries."                          `"This table contains ACL mask Ethernet information.
             The access profile will be created on the switch to define which
             part of each incoming frame's layer 2 header will be examined
             by the switch. Masks entered will be combined with the
             values the switch finds in the specified frame header fields."                       3"A list of information about the ACL for Ethernet."                       o"The ID of the ACL mask entry unique to the mask list. The maximum value of this object depends on the device."             --read-create
         M"Specifies that the switch will examine the VLAN part of each packet header."                      �"This object indicates the status of the MAC address mask.
                other (1) - Neither source MAC address nor destination MAC address are masked.
                dst-mac-addr (2) - Destination MAC addresses within received frames are
                    to be filtered when matched with the MAC address entry for the table.
                src-mac-addr (3) - Source MAC addresses within received frames are to
                    be filtered when matched with the MAC address entry for the table.
                dst-src-mac-addr (4) - Source or destination MAC addresses within received
                    frames are to be filtered when matched with the MAC address entry of the table."                       H"This object specifies the MAC address mask for the source MAC address."                       M"This object specifies the MAC address mask for the destination MAC address."                       m"Specifies if the switch will examine the 802.1p priority value in the frame's header
              or not."                       l"Specifies if the switch will examine the Ethernet type value in each frame's header
              or not."                       1"This object indicates the status of this entry."       --swACLEthernetState
              @"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       D"The number of unused rule entries  of this Ethernet profile entry."                       9"The name of the ACL mask entry unique to the mask list."                       �"The mask used for the VLAN ID.
             Valid values are from 0x0000 to 0x0FFF.
             Default value is 0x0FFF
            "                      t"This table contains the ACL mask for IP information.
             Access profiles will be created on the switch to define which
             part of the incoming frame's IP layer packet header will be
             examined by the switch. Masks entered will be combined
             with the values the switch finds in the specified frame
             header fields."                       6"A list of information about the ACL of the IP Layer."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       E"This object indicates if the IP layer VLAN part is examined or not."                      �"This object indicates the status of IP address mask.

             other (1) - Neither source IP address nor destination IP address are
                masked.
             dst-ip-addr (2) - Destination IP addresses within received frames
                are to be filtered when matched with the IP address entry of the table.
             src-ip-addr (3) - Source IP addresses within received frames are
                to be filtered when matched with the IP address entry of the table.
             dst-src-ip-addr (4) - Destination or source IP addresses within received
                frames are to be filtered when matched with the IP address entry of the
                table."                       F"This object specifies the IP address mask for the source IP address."                       K"This object specifies the IP address mask for the destination IP address."                       i"This object indicates if the DSCP protocol in the packet header
             is to be examined or not."                       8"That object indicates which protocol will be examined."                      "This object indicates which fields are defined for ICMP.
             none (1)- Both fields are null.
             type (2)- Type field identified.
             code (3)- Code field identified.
             type-code (4)- Both ICMP fields identified.
            "                       ;"Indicates if the IGMP options field is identified or not."                      3"This object indicates the status of the filtered address of TCP.

             other (1) - Neither source port nor destination port are
                masked.
             dst-addr (2) - Packets will be filtered if this destination port
                is identified in received frames.
             src-addr (3) - Packets will be filtered if this source port is
                identified in received frames.
             dst-src-addr (4) - Packets will be filtered if this destination
                or source port is identified in received frames."                      4"This object indicates the status of the filtered address of UDP .

             other (1) - Neither source port nor destination port are
                masked.
             dst-addr (2) - Packets will be filtered if this destination port
                is identified in received frames.
             src-addr (3) - Packets will be filtered if this source port is
                identified in received frames.
             dst-src-addr (4) - Packets will be filtered if this destination
                or source port is identified in received frames."                       �"Specifies a TCP port mask for the source port if swACLIpUseProtoType is TCP
             Specifies a UDP port mask for the source port if swACLIpUseProtoType is UDP.
             "                       �"Specifies a TCP port mask for the destination port if swACLIpUseProtoType is TCP
             Specifies a UDP port mask for the destination port if swACLIpUseProtoType is UDP."                       '"Specifies a TCP connection flag mask."                      +"A value which indicates the set of TCP flags that this
             entity may potentially offer. The value is a sum of flag bits.
             This sum initially takes the value zero. Then, for each flag, L,
             is added in the range 1 through 6, for which this node performs
             transactions where 2^(L-1) is added to the sum.
             Note that values should be calculated accordingly:

                 Flag      functionality
                   6        urg bit
                   5        ack bit
                   4        psh bit
                   3        rst bit
                   2        syn bit
                   1        fin bit
             For example, if you want to enable urg bit and ack bit, you
             should set value 48{2^(5-1) + 2^(6-1)}."                       M"Specifies if the switch will examine each frame's protocol ID field or not."                       M"Specifies that the rule applies to the IP protocol ID behind the IP header."                       p"Specifies that the rule applies to the IP protocol ID and the mask options
             behind the IP header."                       1"This object indicates the status of this entry."                      A"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example, IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       H"This object specifies the MAC address mask for the source MAC address."                       :"The number of unused rule entries this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                       �"The mask used for the VLAN ID.
             Valid values are from 0x0000 to 0x0FFF.
             Default value is 0x0FFF.
            "                      �"This table contains the ACL mask for user-defined information.
             An access profile will be created on the switch to define which part
             of each incoming frame's user-defined part of the packet header
             will be examined by switch. Masks entered will be combined
              with the values the switch finds in the specified frame header fields."                       0"A list of information about user-defined ACLs."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."             --read-create
         i"Specifies that the rule applies to the packet content (Offset0to15) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset16to31) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset32to47) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset48to63) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset64to79) and
             the mask options."                       1"This object indicates the status of this entry."       --swACLEthernetState
              A"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example, IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       ="The number of unused rule entries of this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                      l"This table contains user-defined ACL mask information.
           An access profile will be created on the switch to define which
           parts of each incoming frame's IPv6 part of the packet header will
           be examined by the switch. Masks entered will be combined
           with the values the switch finds in the specified frame header fields."                       0"A list of information about user-defined ACLs."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."             --read-create
         O"Specifies that the rule applies to the IPv6 class field and the mask options."                       S"Specifies that the rule applies to the IPv6 flowlabel field and the mask options."                      	"This object indicates the status of the IPv6 address mask.

            other (1) - Neither source IPv6 address nor destination IPv6 address are
                masked.
            dst-ipv6-addr (2) - Received frame destination IPv6 address is
                currently used to be filtered as it meets with the IPv6
                address entry of the table.
            src-ipv6-addr (3) - Received frame source IPv6 address is currently
                used to be filtered as it meets with the IPv6 address entry of
                the table.
            dst-src-ipv6-addr (4) - Received frame destination IPv6 address or
                source IPv6 address is currently used to be filtered as it meets
                with the IPv6 address entry of the table."                       �"Specifies that the rule applies to the Source IPv6 address and the mask options.
            This should be a 16 byte octet string."                       �"Specifies that the rule applies to the Destination IPv6 address and the mask options.
            This should be a 16 byte octet string."                       1"This object indicates the status of this entry."                      >"The owner of the ACL mask entry. The type of ACL entry created. ACL type
            entries can only be modified when being configured through the same
            type command. For example, IP-MAC Binding entries can only be modified
            or deleted through the IP-MAC Binding configurations or commands."                       ="The number of unused rule entries of this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                       8"That object indicates which protocol will be examined."                      3"This object indicates the status of the filtered address of TCP.

             other (1) - Neither source port nor destination port are
                masked.
             dst-addr (2) - Packets will be filtered if this destination port
                is identified in received frames.
             src-addr (3) - Packets will be filtered if this source port is
                identified in received frames.
             dst-src-addr (4) - Packets will be filtered if this destination
                or source port is identified in received frames."                      2"This object indicates the status of the filtered address of UDP.

             other (1) - Neither source port nor destination port is
                masked.
             dst-addr (2) - Packets will be filtered if this destination port
                is identified in received frames.
             src-addr (3) - Packets will be filtered if this source port is
                identified in received frames.
             dst-src-addr (4) - Packets will be filtered if this destination
                or source port is identified in received frames."                       �"Specifies a TCP port mask for the source port if swACLIpv6MaskUseProtoType is TCP
             Specifies a UDP port mask for the source port if swACLIpv6MaskUseProtoType is UDP.
             "                       �"Specifies a TCP port mask for the destination port if swACLIpv6MaskUseProtoType is TCP
             Specifies a UDP port mask for the destination port if swACLIpv6MaskUseProtoType is UDP."                      "This object indicates which fields are defined for ICMP.
             none (1)- Both fields are null.
             type (2)- Type field identified.
             code (3)- Code field identified.
             type-code (4)- Both ICMP fields identified.
            "                       "Used to delete all ACL masks."                      �"This table contains IP-MAC-Binding ACL mask Ethernet information.
             Access profiles will be created on the switch by row creation and to
             define which parts of each incoming frame's layer 2 header part
             the switch will examine. Masks can be entered that will be combined
             with the values the switch finds in the specified frame header fields."                       /"A list of information about the Ethernet ACL."                       p"The ID of the ACL mask entry, unique in the mask list. The maximum value of this object depends on the device."                       l"Specifies if the switch will examine the Ethernet type value in each frame's header
              or not."                      �"This table contains IP-MAC-Binding IP ACL mask information.
             Access profiles will be created on the switch by row creation and to
             define which parts of each incoming frame's IP layer part of the header
             the switch will examine. Masks can be entered that will be combined
             with the values the switch finds in the specified frame header fields."                       6"A list of information about the IP layer of the ACL."                       p"The ID of the ACL mask entry, unique in the mask list. The maximum value of this object depends on the device."                       H"This object specifies the MAC address mask for the source MAC address."                       C"This object specifies IP address masks for the source IP address."                      �"This table contains the ACL mask for user-defined option information.
             An access profile will be created on the switch to define which part
             of each incoming frame's user-defined part of the packet header
             will be examined by switch. Masks entered will be combined
             with the values the switch finds in the specified frame header fields."                       3"A list of information about the user-defined ACL."                       p"The ID of the ACL mask entry, unique to the mask list. The maximum value of this object depends on the device."                        "Specifies the state of chunk1."                       /"Specifies the frame content offset of chunk1."                       -"Specifies the frame content mask of chunk1."                        "Specifies the state of chunk2."                       /"Specifies the frame content offset of chunk2."                       -"Specifies the frame content mask of chunk2."                        "Specifies the state of chunk3."                       /"Specifies the frame content offset of chunk3."                       -"Specifies the frame content mask of chunk3."                        "Specifies the state of chunk4."                       /"Specifies the frame content offset of chunk4."                       -"Specifies the frame content mask of chunk4."                       1"This object indicates the status of this entry."                      A"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example, IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       ="The number of unused rule entries of this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                          �"This table contains the ACL mask for user-defined option 2 information.
             An access profile will be created on the switch to define which part
             of each incoming frame's user-defined part of the packet header
             will be examined by switch. Masks entered will be combined
             with the values the switch finds in the specified frame header fields.
             
             To create a packet content field with respect to an offset, an entry in the
             swACLPktContMaskOption2OffsetsTable must be created first. 
             
             On row creation, all entries in the corresponding profile defined in the
             swACLPktContMaskOption2OffsetsTable will be associated to the profile mask.
             
	         If any rule is using the profile mask the entries cannot be modified.
	                      
             "                       3"A list of information about the user-defined ACL."                       p"The ID of the ACL mask entry, unique to the mask list. The maximum value of this object depends on the device."                       +"Specifies the mask for source MAC address"                       0"Specifies the mask for destination MAC address"                       X"Specifies the mask for customer VLAN tag, valid values are only from 0x0000 to 0xFFFF."                       W"Specifies the mask for service VLAN tag, valid values are only from 0x0000 to 0xFFFF."                      A"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example, IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       ="The number of unused rule entries of this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                       1"This object indicates the status of this entry."                      k"This table contains the ACL masks for the individual packet content offset user-defined option 2 information.
	         Entries created in this table will not set into the TCAM until a valid entry in the swACLPktContMaskOption2Table
	         is created.
	         
	         If any rule is using the profile mask the entries cannot be modified.
	        ."                       J"A list of information about the individual offsets for user-defined ACL."                       �"The ID of the ACL mask entry, unique to the mask list. 
             This is the profile id to which this packet content field entry will be associated to.
            "                       :"Specifies the offset number with respect to the profile."                      I"Specifies the reference of the offset.
						L2 - The offset will start counting from the byte 
						     after the end of the VLAN tags (start of ether type)
		                L3 - The offset will start counting right after the ether type field. 
		                     The packet must have a valid L2 header and a recognizeable ether type in 
		                     order to be recognized.
		                L4 - The offset will start counting right after the end of ip header. 
		                     The packet must have a valid IP header in order to be recognized.
				"                       N"Specifies the amount of bytes from the reference to the packet content field"                       1"Specifies the mask for the packet content field"                       1"This object indicates the status of this entry."                          W"This table contains the ACL mask for user-defined option 3 information.
             An access profile will be created on the switch to define which part
             of each incoming frame's user-defined part of the packet header
             will be examined by switch. Masks entered will be combined
             with the values the switch finds in the specified frame header fields.
             
             To create a packet content field with respect to an offset, an entry in the
             swACLPktContMaskOption3OffsetsTable must be created first. 
             
             On row creation, all entries in the corresponding profile defined in the
             swACLPktContMaskOption3OffsetsTable will be associated to the profile mask.
             
	     If any rule is using the profile mask the entries cannot be modified."                       3"A list of information about the user-defined ACL."                       p"The ID of the ACL mask entry, unique to the mask list. The maximum value of this object depends on the device."                       ,"Specifies the mask for source MAC address."                       1"Specifies the mask for destination MAC address."                       U"Specifies the mask for outer VLAN tag, valid values are only from 0x0000 to 0x0FFF."                      A"The owner of the ACL mask entry. The type of ACL entry created. ACL type
             entries can only be modified when being configured through the same
             type command. For example, IP-MAC Binding entries can only be modified
             or deleted through the IP-MAC Binding configurations or commands."                       ="The number of unused rule entries of this IP profile entry."                       9"The name of the ACL mask entry unique to the mask list."                       1"This object indicates the status of this entry."                      _"This table contains the ACL masks for the individual packet content offset user-defined option 3 information.
	         Entries created in this table will not set into the TCAM until a valid entry in the swACLPktContMaskOption3Table
	         is created.
	         
	         If any rule is using the profile mask the entries cannot be modified."                       J"A list of information about the individual offsets for user-defined ACL."                       �"The ID of the ACL mask entry, unique to the mask list. 
             This is the profile id to which this packet content field entry will be associated to.
            "                       :"Specifies the offset number with respect to the profile."                      I"Specifies the reference of the offset.
						L2 - The offset will start counting from the byte 
						     after the end of the VLAN tags (start of ether type)
		                L3 - The offset will start counting right after the ether type field. 
		                     The packet must have a valid L2 header and a recognizeable ether type in 
		                     order to be recognized.
		                L4 - The offset will start counting right after the end of ip header. 
		                     The packet must have a valid IP header in order to be recognized.
				"                       O"Specifies the amount of bytes from the reference to the packet content field."                       2"Specifies the mask for the packet content field."                       1"This object indicates the status of this entry."                           /"This table contains Ethernet ACL information."                       N"A list of information about the ACL rule of the layer 2 part of each packet."                       �"The ID of the ACL rule entry, which is unique to the mask list.
             The maximum value of this object depends on the device."                      �"The ID of the the ACL rule entry relates to the swACLEtherRuleProfileID.
             When row creation is set to 0, assignment of an Access ID for ports is automatic
             and the swACLEtherRulePort creates Rule entries for the swACLEtherRulePort accordingly.
             When set from 1 to 65535, an access ID will be created for the swACLEtherRulePort.
             The swACLEtherRulePort must be set to one port only otherwise the row creation will fail.
            "                       j"Specifies that the access rule will only apply to the packet with the VLAN ID indexed by this VLAN name."                       g"Specifies that the access rule will apply to only packets with
             this source MAC address."                       m"Specifies that the access rule will apply to only packets
              with this destination MAC address."                       �"Specifies that the access rule will apply only to packets with
              this 802.1p priority value. A value of -1 indicates that this node
              is not actively used."                       �"Specifies that the access rule will apply only to packets with this
             hexadecimal 802.1Q Ethernet type value in the packet header."                       ^"Specifies that the access rule will apply only to packets with
             priority value."                       z"Specifies that the priority will be changed in packets while the swACLEtherRuleEnablePriority
             is enabled ."                       �"Specifies if the switch will change priorities of packets that match the access profile
             802.1p priority tag field or not ."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      j"This object indicates if the result of the packet examination is 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets matching the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets matching the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that packets matching the access profile are copied to the mirror port.
                      Note : The ACL mirror function will start functioning after mirror has been enabled
                             and the mirror port has been configured.     
             set-drop-precedence - Specifies that packets that matching the access profile are set
                                   to drop precedence."                       �"Specifies that the access rule will only apply to port(s).
             This object and swACLEtherRuleVID can not be set together."       &--        SYNTAX  INTEGER (1..65535)
               1"This object indicates the status of this entry."       --swACLEtherRuleState
               E"The owner of the ACL rule entry. Only owners can modify this entry."                       d"Specifies the rx rate, 0 denotes no_limit. The maximum value of this object depends on the device."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies the VLAN-based ACL rule. There are two conditions:
             1. this rule will apply to all the ports;
             2. packets must belong to this VLAN.

             This object and swACLEtherRulePort can not be set together.
             When you set swACLEtherRulePort, the value of this object will automatically change to 0.
             And this object can not be set to 0."                       �"Specifies that the access rule will apply only to packets with
             this VLAN ID. It is applied to the specified ports configured by swACLEtherRulePort."                      *"Specifies the per rule mask for the VLAN field as defined in swACLEtherRuleVlan object.
             The value of this object when not in use is the corresponding mask in the profile mask.
             Once the value of this object is modified, the per rule mask will take effect.
            "                      L"Specifies the per rule mask for the source MAC addres field as defined in swACLEtherRuleSrcMacAddress object.
             The value of this object when not in use is the corresponding mask in the profile mask.            
             Once the value of this object is modified, the per rule mask will take effect.
            "                      Q"Specifies the per rule mask for the destination MAC addres field as defined in swACLEtherRuleDstMacAddress object.
             The value of this object when not in use is the corresponding mask in the profile mask.            
             Once the value of this object is modified, the per rule mask will take effect.
            "                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                       ""                       ""                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                      �"The ID of the ACL rule entry relates to swACLIPRuleProfileID.
             Row creation set to 0 indicates automatic assignment of the Access ID
             for the ports in the swACLIpRulePort to create Rule entries
             for swACLIpRulePort accordingly.
             Set to 1-65535 causes creation of an access ID for the swACLIpRulePort.
             The swACLIpRulePort must be set to one port only otherwise the row
             creation will fail."             --read-create
         g"Specifies that the access rule will only apply to packets with the VLAN ID indexed by this VLAN name."                       !"Specifies an IP source address."                       &"Specifies an IP destination address."                       �"Specifies the value of DSCP. The value can be configured from 0 to 63.
            A value of -1 indicates that this node is not actively used."                      x"Specifies the IP protocol.
             For some older chips, this object can not be set. When getting this object,
             it always returns the type which has been configured in swACLIpEntry.

             For some newer chips, this object can only set the type which
             has been configured in swACLIpEntry. The default value is none (1).
            "                       �"Specifies that the rule applies to the value of ICMP type traffic.
            A value of -1 denotes that this object is not active."                       �"Specifies that the rule applies to the value of ICMP code traffic.
            A value of -1 denotes that this object is not active."                       �"Specifies that the rule applies to the range of the TCP/UDP source ports.
            A value of -1 indicates that this node is not actively used."                       y"Specifies the TCP/UDP destination port range.
            A value of -1 indicates that this node is not actively used."                      -"A value which indicates the set of TCP flags that this
             entity may potentially offer. The value is a sum of flag bits.
             This sum initially takes the value zero. Then, for each flag, L
             is added in the range 1 through 6, for which this node performs
             transactions, where 2^(L - 1) is added to the sum.
             Note that values should be calculated accordingly:

                 Flag      functionality
                   6        urg bit
                   5        ack bit
                   4        psh bit
                   3        rst bit
                   2        syn bit
                   1        fin bit
             For example, it you want to enable urg bit and ack bit, you
             should set value 48{2^(5-1) + 2^(6-1)}."                       �"Specifies that the rule applies to the value of IP protocol ID traffic.
            A value of -1 indicates that this node is not actively used."                       t"Specifies that the rule applies to the IP protocol ID and the range of
             options behind the IP header."                       c"Specifies that the access rule will apply only to packets with this
             priority value."                       m"Specifies the priority will change in packets while the swACLIpRuleEnablePriority
             is enabled."                       �"Specifies whether the packets that match the access profile will change the
             802.1p priority tag field by the switch or not."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      e"This object indicates if the result of the packet examination is 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets matching the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets matching the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies the packets matching the access profile are copied
                      to the mirror port.
                      Note : The ACL mirror function will work after the mirror is enabled and the mirror port has
                             been configured. 
             set-drop-precedence - Specifies the packets that match the access profile are set
                               to drop precedence."                       �"Specifies that the access rule will only apply to port(s).
             This object and swACLIpRuleVID can not be set together. "       &--        SYNTAX  INTEGER (1..65535)
               1"This object indicates the status of this entry."                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       d"Specifies the rx-rate, 0 denotes no_limit. The maximum value of this object depends on the device."                       b"Specifies that the access will only apply to packets with
             this source MAC address."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
              that meets the criteria specified in the first part of the command.
              This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies the VLAN-based rule. There are two conditions:
             1. this rule will apply to all the ports;
             2. packets must belong to this VLAN.

             This object and swACLIpRulePort can not be set together.
             When you set swACLIpRulePort, the value of this object will automatically change to 0.
             And this object can not be set 0."                       �"Specifies that the access rule will apply only to packets with
             this VLAN ID. It is applied to the specified ports configured by swACLIpRulePort."                      ]"Specifies the per rule mask for the VLAN as defined in swACLIpRuleVlan object. 
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
            "                      _"Specifies the per rule mask for the source IP address as defined in swACLIpRuleSrcIpaddress object.
			The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
			"                      h"Specifies the per rule mask for the destination IP address as defined in swACLIpRuleMaskDstIpaddress object.
			The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
			"                      W"Specifies the per rule mask for the L4 source port as defined in swACLIpRuleSrcPort object.
			The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
			"                      n"Specifies the per rule mask for the L4 destination port as defined in swACLIpRuleDstPort object.
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
            "                       F"Specifies that the per rule mask for the bytes behind the IP header."                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                       C"This table contains ACL rules regarding user-defined information."                       S"A list of information about the ACL rule of the user-defined part of each packet."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                      "The ID of the ACL rule entry in relation to the swACLPktContRuleProfileID.
             When row creation is set to 0, an access ID is automatically created
             for the ports in the swACLPktContRulePort to create rule entries
             for swACLPktContRulePort accordingly.
             Set to 1-65535 indicates to creswACLPktContRuleRepDscpate the exact access ID
             for the swACLPktContRulePort. The swACLPktContRulePort must be set to
             one port only, otherwise the row creation will fail."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       c"Specifies that the access rule will apply only to packets with this
             priority value."                       y"Specifies the priority will change for the packets while the swACLPktContRuleReplacePriority
             is enabled ."                       �"Specifies if the switch will change priorities of packets that match the access profile
            802.1p priority tag or not."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.             "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      �"This object indicates if the result of the packet examination is 'permit' or 'deny'.
         The default is 'permit'.
             permit - Specifies that packets matching the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets matching the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that the packets matching the access profile are copied to
                      the mirror port.
                      Note : The ACL mirror function will function after mirror is enabled
                      and a mirror port has been configured.
             lease-renew - Specifies the packets matching the access profile are copied to
                           the CPU.
                           Note : After a user enables the port's lease-renew state, all kinds of DHCP packets
                                  (including unicast and broadcast DHCP packets) will be copied to the CPU
                                  (using user ACL mask and rule).  
             set-drop-precedence - Specifies that packets matching the access profile are set
                               to drop precedence."                       �"Specifies that the access rule will only apply to port(s).
              This object and swACLPktContRuleVID can not be set together. "       &--        SYNTAX  INTEGER (1..65535)
               1"This object indicates the status of this entry."                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       d"Specifies the rx-rate, 0 denotes no_limit. The maximum value of this object depends on the device."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies this rule only applies to the specified VLAN. There are two conditions:
             1.only the portlist that belongs to this VLAN will be included;
             2.packets must belong to this VLAN.

             This object and swACLPktContRulePort can not be set together.
             When you set swACLPktContRulePort, the value of this object will automatically change to 0.
             And this object can not be set 0."                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                       4"This table contains the IPv6 ACL rule information."                       O"A list of information about ACL rules regarding the IPv6 part of each packet."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                      �"The ID of the ACL rule entry relates to swACLIpv6RuleProfileID.
             When row creation is set to 0, this indicates the access ID
             will be assigned automatically for the ports in the swACLIpv6RulePort
             to create rule entries for swACLIpv6RulePort accordingly.
             Set to 1-65535 indicates creation of an access ID for the swACLIpv6RulePort.
             The swACLIpv6RulePort must be set to one port only, otherwise
             the row creation will fail."                       :"Specifies that the rule applies to the IPv6 class field."                       ?"Specifies that the rule applies to the IPv6 flow label field."                       q"Specifies that the rule applies to the source IPv6 address.
            This should be a 16 byte octet string."                       v"Specifies that the rule applies to the destination IPv6 address.
            This should be a 16 byte octet string."                       ^"Specifies that the access rule will apply only to packets with
             priority value."                       p"Specifies the priority will change in packets while the swACLIpv6RuleReplacePriority
             is enabled."                       �"Specifies if the switch will change priorities of packets that match the access profile
             802.1p priority tag or not."                      d"This object indicates if the result of the packet examination is 'permit' or 'deny'.
         The default is 'permit'.
             permit - Specifies that packets matching the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets matching the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that the packets matching the access profile are copied to
                      the mirror port.
                      Note : The ACL mirror function will function after mirror has been enabled
                      and a mirror port has been configured.    
             set-drop-precedence - Specifies the packets matching the access profile are set
                               to drop precedence."                       �"Specifies that the access rule will apply only to port(s).
             This object and swACLIpv6RuleVID can not be set together. "       &--        SYNTAX  INTEGER (1..65535)
               1"This object indicates the status of this entry."                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       d"Specifies the rx-rate, 0 denotes no_limit. The maximum value of this object depends on the device."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies this rule only applies to the specified VLAN. There are two conditions:
             1.only the portlist that belongs to this VLAN will be included;
             2.packets must belong to this VLAN.

             This object and swACLIpv6RulePort can not be set together.
             When you set swACLIpv6RulePort, the value of this object will automatically change to 0.
             And this object can not be set 0."                      ~"Specifies the IPv6 protocol.
             For some older chips, this object can not be set. When getting this object,
             it always returns the type which has been configured in swACLIpv6Entry.

             For some newer chips, this object can only set the type which
             has been configured in swACLIpv6Entry. The default value is none (1).
            "                       K"Specifies that the rule applies to the range of the TCP/UDP source ports."                       0"Specifies the TCP/UDP destination ports range."                      "Specifies that the per rule mask of swACLIpv6RuleSrcIpv6Addr.
            This should be a 16 byte octet string.
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
            "                       w"Specifies that the rule applies to the destination IPv6 address.
             This should be a 16 byte octet string."                      G"Specifies that the per rule mask of swACLIpv6RuleSrcPort.
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
            "                      B"Specifies the per rule mask of swACLIpv6RuleDstPort.
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            This object is writeable only once.            
            "                       �"Specifies that the rule applies to the value of ICMP type traffic.
            A value of -1 denotes that this object is not active."                       �"Specifies that the rule applies to the value of ICMP code traffic.
            A value of -1 denotes that this object is not active."                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                       C"This table contains IP-MAC-Binding Ethernet ACL Rule information."                       N"A list of information about the ACL rule of the layer 2 part of each packet."                       p"The ID of the ACL mask entry, unique in the mask list. The maximum value of this object depends on the device."                      �"The ID of the ACL rule entry in relation to swACLEtherRuleProfileID.
            When row creation is set to 0, this indicates automatically assigning an Access
            for the ports in the swACLEtherRulePort  to create rule entries for swACLEtherRulePort
            accordingly.
            Set to 1-65535 indicates to create the exact access ID for the swACLEtherRulePort
            and the swACLEtherRulePort must set one port only, otherwise the row creation will
            fail."                       �"Specifies that the access rule will apply only to packets with this
             802.1Q Ethernet type value in the packet header."                      �"This object indicates if the result of the examination is 'permit' or 'deny'.
             The default is 'permit' (1).
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       <"Specifies that the access rule will only apply to port(s)."                       ""                       ""                       p"The ID of the ACL mask entry, unique in the mask list. The maximum value of this object depends on the device."                      �"The ID of the ACL rule entry in relation to swACLIPRuleProfileID.
            When the row creation is set to 0, this indicates assigning an access ID automatically
            for the ports in the swACLIpRulePort  to create rule entries for swACLIpRulePort
            accordingly.
            Set to 1-65535 indicates to create the exact access ID for the swACLIpRulePort
            and the swACLIpRulePort must be set for one port only, otherwise the row creation will
            fail."             --read-create
         g"Specifies that the access rule will apply to only packets with
             this source MAC address."                       !"Specifies an IP source address."                      �"This object indicates if the result of the examination is 'permit' or 'deny'; the default is 'permit' (1)
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       <"Specifies that the access rule will only apply to port(s)."                       3"This table contains user-defined ACL information."                       Z"A list of information about the ACL rule regarding the user-defined part of each packet."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                      "The ID of the ACL rule entry in relation to the swACLPktContRuleProfileID.
             When row creation is set to 0, access ID is automatically created
             for the ports in the swACLPktContRulePort to create rule entries
             for swACLPktContRulePort accordingly.
             Set to 1-65535 indicates to creswACLPktContRuleRepDscpate the exact access ID
             for the swACLPktContRulePort. The swACLPktContRulePort must be set to
             one port only, otherwise the row creation will fail."                       ."Displays the frame content offset of chunk1."                       ("Specifies the frame content of chunk1."                       ."Displays the frame content offset of chunk2."                       ("Specifies the frame content of chunk2."                       ."Displays the frame content offset of chunk3."                       ("Specifies the frame content of chunk3."                       ."Displays the frame content offset of chunk4."                       ("Specifies the frame content of chunk4."                       c"Specifies that the access rule will only apply to packets with this
             priority value."                       z"Specifies that the priority will change for the packets while the swACLPktContRuleReplacePriority
         is enabled ."                       �"Specifies if the switch will change priorities of packets that match the access profile
            802.1p priority tag or not."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.                 "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      �"This object indicates if the result of the packet examination is 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that the packets that match the access profile are copied to
                      the mirror port.
                      Note: The ACL mirror function will function after mirror is enabled
                      and a mirror port has been configured."                       �"Specifies that the access rule will only apply to port(s).
              This object and swACLPktContRuleOptionVID can not be set together. "                       1"This object indicates the status of this entry."                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       d"Specifies the rx-rate, 0 denotes no_limit. The maximum value of this object depends on the device."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies this rule only applies to the specified VLAN. There are two conditions:
             1.only the portlist that belongs to this VLAN will be included;
             2.packets must belong to this VLAN.

             This object and swACLPktContRuleOptionPort can not be set together.
             When you set swACLPktContRuleOptionPort, the value of this object will automatically change to 0.
             And this object can not be set 0."                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                      "This table maintains counter information associated with a specific
                rule in the ACL rule table. Please refer to the swACLEtherRuleTable,
                swACLIpRuleTable, swACLIpv6RuleTable and swACLPktContRuleTable
                for detailed ACL rule information."                       ^"The entry maintains counter information associated with the ACL
                rule table."                       O"The ID of the ACL mask entry, which is unique in the mask
             list."                       U"The ID of the ACL rule entry as related to the
             swACLCounterProfileID."                      N"Specifies whether the counter feature will be enabled/disabled.
             1. This is optional. The default is disable.
             2. If the rule is not bound with flow_meter, then all packets that match will be counted.
             If the rule is bound with flow_meter, then the 'counter' will be overridden.
             "                        "The number of matched packets."                       &"The number of matched green packets."                       '"The number of matched yellow packets."                       $"The number of matched red packets."                          �"This table contains user-defined ACL information for option 2 type
            of packet contnet syntax.
            
		 	To qualify the data of a packet content field with respect to an offset, an entry in the
		 	swACLPktContRuleOption2OffsetsTable must be created first. 
		 	
		 	On row creation, all entries in the corresponding profile defined in the
		 	swACLPktContRuleOption2OffsetsTable will be associated to the ACL rule.            
            "                       Z"A list of information about the ACL rule regarding the user-defined part of each packet."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       �"The ID of the ACL rule entry in relation to the swACLPktContRuleOption2ProfileID.
             When row creation is set to 0, access ID is automatically assigned.
			"                       g"Specifies that the access rule will apply to only packets with
             this source MAC address."                       l"Specifies that the access rule will apply to only packets with
             this destination MAC address."                       d"Specifies the frame content of the customer VLAN tag, valid values are only from 0x0000 to 0xFFFF."                       c"Specifies the frame content of the service VLAN tag, valid values are only from 0x0000 to 0xFFFF."                       c"Specifies that the access rule will only apply to packets with this
             priority value."                       �"Specifies that the priority will change for the packets while the swACLPktContRuleOption2ReplacePriority
         is enabled ."                       �"Specifies if the switch will change priorities of packets that match the access profile
            802.1p priority tag or not."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported.                 "                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      �"This object indicates if the result of the packet examination is 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that the packets that match the access profile are copied to
                      the mirror port.
                      Note: The ACL mirror function will function after mirror is enabled
                      and a mirror port has been configured."                       �"Specifies that the access rule will only apply to port(s).
              This object and swACLPktContRuleOption2VID can not be set together. "                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies this rule only applies to the specified VLAN. There are two conditions:
             1.only the portlist that belongs to this VLAN will be included;
             2.packets must belong to this VLAN.

             This object and swACLPktContRuleOption2Port can not be set together.
             When you set swACLPktContRuleOption2Port, the value of this object will automatically change to 0.
             And this object can not be set 0."                       1"This object indicates the status of this entry."                      ;"Specifies the per rule mask of source MAC address field as defined in swACLPktContRuleOption2SrcMac object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                      ;"Specifies the per rule mask of source MAC address field as defined in swACLPktContRuleOption2DstMac object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                      <"Specifies the per rule mask of the customer VLAN tag field as defined in swACLPktContRuleOption2CTag object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                      ;"Specifies the per rule mask of the service VLAN tag field as defined in swACLPktContRuleOption2STag object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                      #"This table contains the ACL rules for the individual packet 
            content offset user-defined option 2 information. 
            
			Entries created in this table will not set into the TCAM until a 
			valid entry in the swACLPktContMaskOption2Table is created.            
			"                       Z"A list of information about the ACL rule regarding the user-defined part of each packet."                       Q"The ACL profile id to which this packet content field entry will be associated."                       P"The ACL access id to which this packet content field entry will be associated."                       P"The sequence number of the packet content field to qualify the packet content."                       '"The data of the packet content field."                       1"This object indicates the status of this entry."                      #"Specifies the per rule mask of the frame content of each packet content offset field.
           The value of this object when not in use is the corresponding mask in the profile mask.
           Once the value of this object is modified, the per rule mask will take effect.
           "                          �"This table contains user-defined ACL information for option 3 type
            of packet contnet syntax.
            
	    To qualify the data of a packet content field with respect to an offset, an entry in the
	    swACLPktContRuleOption3OffsetsTable must be created first. 
		 	
	    On row creation, all entries in the corresponding profile defined in the
	    swACLPktContRuleOption3OffsetsTable will be associated to the ACL rule."                       Z"A list of information about the ACL rule regarding the user-defined part of each packet."                       y"The ID of the ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       �"The ID of the ACL rule entry in relation to the swACLPktContRuleOption3ProfileID.
             When row creation is set to 0, access ID is automatically assigned."                       g"Specifies that the access rule will apply to only packets with
             this source MAC address."                       l"Specifies that the access rule will apply to only packets with
             this destination MAC address."                       a"Specifies the frame content of the outer VLAN tag, valid values are only from 0x0000 to 0x0FFF."                       c"Specifies that the access rule will only apply to packets with this
             priority value."                       �"Specifies that the priority will change for the packets while the swACLPktContRuleOption3ReplacePriority
         is enabled."                       �"Specifies if the switch will change priorities of packets that match the access profile
            802.1p priority tag or not."                       �"Specifies if the switch will change priorities of packets that match the access profile
             DSCP field or not.
             Replace DSCP and replace ToS precedence can not both be supported."                       �"Specifies a value to be written to the DSCP field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the DSCP field of the packet."                      �"This object indicates if the result of the packet examination is 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered.
             mirror - Specifies that the packets that match the access profile are copied to
                      the mirror port.
                      Note: The ACL mirror function will function after mirror is enabled
                      and a mirror port has been configured."                       �"Specifies that the access rule will only apply to port(s).
              This object and swACLPktContRuleOption3VID can not be set together. "                       E"The owner of the ACL rule entry. Only owners can modify this entry."                       �"Specifies if the switch will change priorities of packets that match the access profile
             ToS precedence field or not.
             Replace DSCP and replace ToS precedence can not both be supported.
            "                      "Specifies a value to be written to the ToS precedence field of an incoming packet
             that meets the criteria specified in the first part of the command.
             This value will over-write the value in the ToS precedence field of the packet."                      �"Specifies this rule only applies to the specified VLAN. There are two conditions:
             1.only the portlist that belongs to this VLAN will be included;
             2.packets must belong to this VLAN.

             This object and swACLPktContRuleOption3Port can not be set together.
             When you set swACLPktContRuleOption3Port, the value of this object will automatically change to 0.
             And this object can not be set 0."                       1"This object indicates the status of this entry."                      ;"Specifies the per rule mask of source MAC address field as defined in swACLPktContRuleOption3SrcMac object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                      ;"Specifies the per rule mask of source MAC address field as defined in swACLPktContRuleOption3DstMac object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                      ="Specifies the per rule mask of the outer VLAN tag field as defined in swACLPktContRuleOption3OuterTag object
            The value of this object when not in use is the corresponding mask in the profile mask.
            Once the value of this object is modified, the per rule mask will take effect.
            "                       �"Specifies that packets matching the access profile are copied to the mirror group target port.
             Note : The ACL mirror function will start functioning after mirror has been enabled
                    and the mirror group has been created."                      "This table contains the ACL rules for the individual packet 
            content offset user-defined option 3 information. 
            
	    Entries created in this table will not set into the TCAM until a 
	    valid entry in the swACLPktContMaskOption3Table is created."                       Z"A list of information about the ACL rule regarding the user-defined part of each packet."                       Q"The ACL profile id to which this packet content field entry will be associated."                       P"The ACL access id to which this packet content field entry will be associated."                       P"The sequence number of the packet content field to qualify the packet content."                       '"The data of the packet content field."                       1"This object indicates the status of this entry."                      "Specifies the per rule mask of the frame content of each packet content offset field.
           The value of this object when not in use is the corresponding mask in the profile mask.
           Once the value of this object is modified, the per rule mask will take effect."                          f"This table contains software ACL mask Ethernet information.
             Access profiles will be created on the switch to define which
             part of each incoming frame's layer 2 header will be examined by
             the switch. Masks entered will be combined with the values
             the switch finds in the specified frame header fields."                       1"A list of information about Ethernet ACL masks."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."             --read-create
         M"Specifies that the switch will examine the VLAN part of each packet header."                      �"This object indicates the status of the MAC address mask.

            other (1) - Neither source MAC addresses nor destination MAC addresses are
                masked.
            dst-mac-addr (2) - Destination MAC addresses within received frames are
                to be filtered when matched with the MAC address entry of the table.
            src-mac-addr (3) - Source MAC address within received frames are to
                be filtered when matched with the MAC address entry of the table.
            dst-src-mac-addr (4) - Source or destination MAC addresses within received
                frames are to be filtered when matched with the MAC address entry of this table."                       H"This object specifies the MAC address mask for the source MAC address."                       M"This object specifies the MAC address mask for the destination MAC address."                       m"Specifies if the switch will examine the 802.1p priority value in the frame's header
              or not."                       l"Specifies if the switch will examine the Ethernet type value in each frame's header
              or not."                       1"This object indicates the status of this entry."       --swCpuAclEthernetState
              d"This table contains software ACL mask IP information.
             Access profiles will be created on the switch to define which
             parts of each incoming frame's IP layer 2 header will be examined
             by the switch. Masks entered will be combined with the
             values the switch finds in the specified frame header fields."                       ?"A list of information about the software ACL of the IP Layer."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       E"This object indicates if the IP layer VLAN part is examined or not."                      �"This object indicates the status of IP address mask.

            other (1) - Neither source IP addresses nor destination IP address are
                masked.
            dst-ip-addr (2) - Destination IP addresses within received frames are
                to be filtered when matched with the IP address entry of this table.
            src-ip-addr (3) - Source IP addresses within received frames are to
                be filtered when matched with the IP address entry of this table.
            dst-src-ip-addr (4) - Destination or source IP addresses within received
                frames are to be filtered when matched with the IP address entry of the
                table."                       F"This object specifies the IP address mask for the source IP address."                       K"This object specifies the IP address mask for the destination IP address."                       ["This object indicates if the DSCP protocol in the packet header is to be examined or not."                       8"This object indicates which protocol will be examined."                      "This object indicates which fields are identified for ICMP.
            none (1)- Both fields are null.
            type (2)- Type field identified.
            code (3)- Code field identified.
            type-code (4)- Both ICMP fields identified.
            "                       G"This object indicates if the IGMP options field is identified or not."                      4"This object indicates the status of filtered addresses of TCP.

                other (1) - Neither source port nor destination port are
                masked.
             dst-addr (2) - Packets will be filtered if this destination port is
                identified in received frames.
             src-addr (3) - Packets will be filtered if this source port is
                identified in received frames.
             dst-src-addr (4) - Packets will be filtered is this destination or
                source port is identified in received frames."                      -"This object indicates the status of filtered addresses of UDP.

            other (1) - Neither source port nor destination port are
                masked.
            dst-addr (2) - Packets will be filtered if this destination port
                is identified in received frames.
            src-addr (3) - Packets will be filtered if this source port
                is identified in received frames.
            dst-src-addr (4) - Packets will be filtered if this destination
                or source port is identified in received frames."                       �"Specifies a TCP port mask for the source port if swCpuAclIpUseProtoType is TCP.
             Specifies a UDP port mask for the source port if swCpuAclIpUseProtoType is UDP.
             "                       �"Specifies a TCP port mask for the destination port if swCpuAclIpUseProtoType is TCP.
             Specifies a UDP port mask for the destination port if swCpuAclIpUseProtoType is UDP."                       '"Specifies a TCP connection flag mask."                      ,"A value which indicates the set of TCP flags that this
             entity may potentially offer. The value is a sum of flag bits.
             This sum initially takes the value zero. Then, for each flag, L
             is added in the range 1 through 6, for which this node performs
             transactions where 2^(L - 1) is added to the sum.
             Note that values should be calculated accordingly:

                 Flag      functionality
                   6        urg bit
                   5        ack bit
                   4        psh bit
                   3        rst bit
                   2        syn bit
                   1        fin bit
             For example, if you want to enable urg bit and ack bit, you
             should set value 48{2^(5-1) + 2^(6-1)}."                       M"Specifies if the switch will examine each frame's Protocol ID field or not."                       ""                       p"Specifies that the rule applies to the IP protocol ID and the mask options
             behind the IP header."                       1"This object indicates the status of this entry."                      ~"This table contains user-defined software ACL information.
             Access profiles will be created on the switch to define which
             part of each incoming frame's user-defined part of the packet header
             will be examined by the switch. Masks entered will be combined
              with the values the switch finds in the specified frame header fields."                       9"A list of information about user-defined software ACLs."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."             --read-create
         i"Specifies that the rule applies to the packet content (Offset0to15) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset16to31) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset32to47) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset48to63) and
             the mask options."                       j"Specifies that the rule applies to the packet content (Offset64to79) and
             the mask options."                       1"This object indicates the status of this entry."       --swCpuAclEthernetState
              s"This table contains IPv6 software ACL mask information.
             An access profile will be created on the switch to define which
             part of each incoming frame's IPv6 part of the packet header
             will be examined by switch. Masks entered will be combined
              with the values the switch finds in the specified frame header fields.  "                       9"A list of information about user-defined software ACLs."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."             --read-create
         O"Specifies that the rule applies to the IPv6 class field and the mask options."                       S"Specifies that the rule applies to the IPv6 flowlabel field and the mask options."                      �"This object indicates the status of IPv6 address mask.

            other (1) - Neither source IPv6 address nor destination IPv6 address are
                masked.
             dst-ipv6-addr (2) - Packets will be filtered if this destination IPv6 address
                is identified as a match in received frames.
             src-ipv6-addr (3) - Packets will be filtered if this source IPv6 address
                is identified as a match in received frames.
             dst-src-ipv6-addr (4) - Packets will be filtered if this destination or source
                IPv6 address is identified as a match in received frames."                       �"Specifies that the rule applies to the source IPv6 address and the mask options.
            This should be a 16 byte octet string."                       �"Specifies that the rule applies to the destination IPv6 address and the mask options.
            This should be a 16 byte octet string."                       1"This object indicates the status of this entry."                       ("Used to delete all software ACL masks."                           ="This table contains Ethernet software ACL rule information."                       W"A list of information about the software ACL rule of the layer 2 part of each packet."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       T"The ID of the software ACL rule entry as it relates to swCpuAclEtherRuleProfileID."                       g"Specifies that the access rule will only apply to packets with the VLAN ID indexed by this VLAN name."                       k"Specifies that the access rule will only apply to the packets with
             this source MAC address."                       p"Specifies that the access rule will only apply to the packets               with this destination MAC address."                       �"Specifies that the access rule will only apply to packets with
              this 802.1p priority value. A value of -1 indicates that this node is not actively used."                       �"Specifies that the access rule will only apply to packets with this
             802.1Q Ethernet type value in the packet header."                      �"This object indicates if the result of the packet examination is to 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       1"This object indicates the status of this entry."       --swCpuAclEtherRuleState
               <"Specifies that the access rule will only apply to port(s)."                       \"Specifies that the access rule will apply only to packets with
             this VLAN ID."                       9"This table contains IPv4 software ACL rule information."                       5"A list of information about this software ACL rule."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       5"The ID of the software ACL for the IPv4 rule entry."             --read-create
         g"Specifies that the access rule will apply only to packets with the VLAN ID indexed by this VLAN name."                       !"Specifies an IP source address."                       &"Specifies an IP destination address."                       �"Specifies the value of DSCP. The value can be configured from 0 to 63.
            A value of -1 indicates that this node is not actively used."                       I"Specifies the IP protocol which has been configured in swCpuAclIpEntry."                       �"Specifies the rule applies to the value of ICMP type traffic.
            A value of -1 indicates that this node is not actively used."                       �"Specifies the rule applies to the value of ICMP code traffic.
            A value of -1 indicates that this node is not actively used."                       �"Specifies the rule applies to the range of TCP/UDP source ports.
            A value of -1 indicates that this node is not actively used."                       }"Specifies the range of TCP/UDP destination ports.
            A value of -1 indicates that this node is not actively used."                      1"A value which indicates the set of TCP flags that this
             entity may potentially offer. The value is a sum of flag bits.
             This sum initially takes the value zero. Then, for each flag, L
             is added in the range 1 through 6, for which this node performs
             transactions where, 2^(L - 1) is added to the sum.
             Note that values should be calculated accordingly:

                 Flag      functionality
                   6        urg bit
                   5        ack bit
                   4        psh bit
                   3        rst bit
                   2        syn bit
                   1        fin bit
             For example, it you want to enable urg bit and ack bit, you
             should set the value 48{2^(5-1) + 2^(6-1)}."                       �"Specifies the rule applies to the value of IP protocol ID traffic.
            A value of -1 indicates that this node is not actively used."                       t"Specifies that the rule applies to the IP protocol ID and the range of
             options behind the IP header."                      �"This object indicates if the result of the packet examination is to 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       1"This object indicates the status of this entry."                       <"Specifies that the access rule will only apply to port(s)."                       \"Specifies that the access rule will apply only to packets with
             this VLAN ID."                       A"This table contains user-defined software ACL rule information."                       \"A list of information about the software ACL rule of the user-defined part of each packet."                       �"The ID of the software ACL mask entry, which is unique to the mask list. The maximum value of this object depends on the device."                       P"The ID of the software ACL rule entry related to swCpuAclPktContRuleProfileID."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                       ="Specifies that the rule applies to the user-defined packet."                      �"This object indicates if the result of the packet examination is to 'permit' or 'deny'.
             The default is 'permit'.
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       1"This object indicates the status of this entry."                       <"Specifies that the access rule will only apply to port(s)."                       8"This table contains user-defined ACL rule information."                       S"A list of information about the ACL rule of the user-defined part of each packet."                       x"The ID of the ACL mask entry. This is unique in the mask list. The maximum value of this object depends on the device."                       H"The ID of the ACL rule entry in relation to swCpuAclIpv6RuleProfileID."                       :"Specifies that the rule applies to the IPv6 class field."                       >"Specifies that the rule applies to the IPv6 flowlabel field."                       q"Specifies that the rule applies to the source IPv6 address.
            This should be a 16 byte octet string."                       v"Specifies that the rule applies to the destination IPv6 address.
            This should be a 16 byte octet string."                      �"This object indicates if the result of the examination is to 'permit' or 'deny'.
             The default is 'permit' (1).
             permit - Specifies that packets that match the access profile are
                      permitted to be forwarded by the switch.
             deny - Specifies that packets that match the access profile
                    are not permitted to be forwarded by the switch and will be filtered."                       1"This object indicates the status of this entry."                       <"Specifies that the access rule will apply only to port(s)."                          �"This table is used to configure the flow-based metering function.
                    The access rule must first be created before the parameters of this
                    function can be applied. Users may set the preferred bandwidth for
                    this rule, in Kbps; once the bandwidth has been exceeded, overflow
                    packets will be either dropped or set for a drop precedence,
                    depending on user configuration."                       l"This entry displays parameters and configurations set for the flow
                    metering function."                       r"The ID of the ACL mask entry is unique in the mask list. The maximum value of this object depends on the device."                       E"The ID of the ACL rule entry as related to the swAclMeterProfileID."                      "Specifies the committed bandwidth in Kbps for the flow.
                    NOTE:
                        1. Specifying 0 will disable this flow meter setting.
                        2. Users must set the swAclMeterActionForRateExceed object to activate this entry."                       �"Specifies the action to take for those packets exceeding the committed rate.
                    NOTE:
                        Users must also set the swAclMeterRate to activate this entry."                       �"Mark the packet with a specified DSCP.
                 It can be set when swAclMeterActionForRateExceed sets remark-dscp (3)."                      )"This specifies the burst size for the single rate two color mode.
                 The unit is Kbytes. That is to say, 1 means 1kbytes.
                 The set value range is 0..n, the value n is determined by project,
                 the value of 0 means to delete this flow_meter setting."                       n"tr-tcm: two rate three color mode;
                 sr-tcm: single rate three color mode.
                "                       r"Specifies the 'committed information rate' of 'two rate three color mode'.
                 The unit is 64Kbps."                      4"Specifies the 'committed burst size' of 'two rate three color mode'.
                 1.     The unit is Kbytes. That is to say, 1 means 1Kbytes.
                 2. This parameter is an optional parameter. The default value is 4*1024.
                 3. The max set value is 16*1024.
                "                       m"Specifies the 'Peak Information Rate' of 'two rate three color mode'.
                 The unit is 64Kbps."                      /"Specifies the 'peak burst size' of 'two rate three color mode'.
                 1.     The unit is Kbytes. That is to say, 1 means 1kbytes.
                 2. This parameter is an optional parameter. The default value is 4*1024.
                 3. The max set value is 16*1024.
                "                       �"Specifies the meter mode.
                 The default is color-blind mode. The final color of the packet is determined
                 by the initial color of the packet and the metering result."                       �"Specifies the action state when packet is in 'green color'.
                 permit: permit the packet.
                 replace-dscp:  change the DSCP value of packet.
                "                       M"Specifies the DSCP value of the packet when the packet is in 'green color'."                      m"Specifies the counter state when the packet is in 'green color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited so that the counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       �"Specifies the action state when packet is in 'yellow color'.
                 permit: permit the packet.
                 replace-dscp: change the DSCP value of the packet.
                 drop: drop the packet.
                "                       F"Specifies the DSCP value of packet when packet is in 'yellow color'."                      j"Specifies the counter state when packet is in 'yellow color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited so that the counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       �"Specifies the action state when packet is in 'red color'.
                 permit: permit the packet.
                 replace-dscp: change the DSCP value of packet.
                 drop: drop the packet.
                "                       G"Specifies the DSCP value of the packet when packet is in 'red color'."                      g"Specifies the counter state when packet is in 'red color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited so that the counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       u"Specifies the 'committed information rate' of 'single rate three color mode'.
                 The unit is 64Kbps."                       �"Specifies the 'committed burst size' of 'single rate three color mode'.
                 1.     The unit is Kbytes. That is to say, 1 means 1Kbytes.
                 2. The max set value is 16*1024.
                "                       �"Specifies the 'Excess burst size' of 'single rate three color mode'.
                 1.     The unit is Kbytes. That is to say, 1 means 1kbytes.
                 2. The max set value is 16*1024.
                "                       �"Specifies the meter mode.
                 The default is color-blind mode. The final color of packet is determined
                 by the initial color of the packet and the metering result."                       �"Specifies the action state when the packet is in 'green color'.
                 permit: permit the packet.
                 replace-dscp: change the DSCP value of packet.
                "                       I"Specifies the DSCP value of the packet when packet is in 'green color'."                      k"Specifies the counter state when the packet is in 'green color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited such that counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       �"Specifies the action state when the packet is in 'yellow color'.
                 permit: permit the packet.
                 replace-dscp: change the DSCP value of packet.
                 drop: drop the packet.
                "                       J"Specifies the DSCP value of the packet when packet is in 'yellow color'."                      l"Specifies the counter state when the packet is in 'yellow color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited such that counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       �"Specifies the action state when the packet is in 'red color'.
                 permit: permit the packet.
                 replace-dscp: change the DSCP value of packet.
                 drop: drop the packet.
                "                       G"Specifies the DSCP value of the packet when packet is in 'red color'."                      k"Specifies the counter state when the packet is in 'red color'.
                 1.     This is optional. The default is 'disable'.
                 2.     The resource may be limited so that the counter can not be turned on. The limitation is project dependent.
                 3.     counter will be cleared when the function is disabled.
                "                       1"This object indicates the status of this entry."                       �"Specifies the DSCP value of the packet when packet is in yellow and red for Tr-TCM.
                A value of -1 indicates that this node is not actively used."                       �"Specifies the DSCP value of the packet when packet is in yellow and red for Sr-TCM.
                A value of -1 indicates that this node is not actively used."                       �"Specifies the 'committed information rate' of 'two rate three color mode'.
                 The unit is 1Kbps. The range is project dependent."                       �"Specifies the 'peak information rate' of 'two rate three color mode'.
                 The unit is 1Kbps. The range is project dependent."                       �"Specifies the 'committed information rate' of 'single rate three color mode'.
                 The unit is 1Kbps. The range is project dependent."                       5"Used to display total entries of the flow metering."                      �"This table is used to configure the single rate two color metering function.
                    The access rule must first be created before the parameters of this
                    function can be applied. Users may set the preferred bandwidth for
                    this rule, in Kbps; once the bandwidth has been exceeded, overflow
                    packets will be either dropped or set for a drop precedence,
                    depending on user configuration."                       _"This entry displays parameters and configurations set for the single rate two color function."                       r"The ID of the ACL mask entry is unique in the mask list. The maximum value of this object depends on the device."                       D"The ID of the ACL rule entry as related to the swAclRateProfileID."                       �"Specifies the committed bandwidth in Kbps for the flow.
                    NOTE: 
                        Users must set the swAclRateActionForRateExceed object to activate this entry."                      8"This specifies the burst size for the single rate two color mode.
                 The unit is Kbytes. That is to say, 1 means 1kbytes.
                 The set value range is 0..n, the value n is determined by project.
                 This parameter is an optional parameter. The default value is 4kbytes."                       �"Specifies the action to take for those packets exceeding the committed rate.
                    NOTE:
                        Users must also set the swAclRate to activate this entry."                       "Mark the packet with a specified DSCP.
                 It can be set when swAclRateActionForRateExceed sets remark-dscp(3)."                       1"This object indicates the status of this entry."                      