y-- -----------------------------------------------------------------------------
-- MIB NAME : DHCP Server mib
-- FILE NAME: DHCPServer.mib
-- DATE     : 2007/07/27
-- VERSION  : 2.03
-- PURPOSE  : To construct the MIB structure of DHCP Server
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
--
-- Revision 2.05, 2007/10/16, Ronald
-- [Modification]
-- [1]Add object value(offering, bootp) in swDHCPServerBindingStatus object.
-- Notes: Requested by Ronald with DES3828/3852
--
-- Revision 2.04, 2007/10/08, Ronald
-- [Modification]
-- [1]Add swDHCPServerConflictIPTable.
-- Notes: Requested by Ronald with DES3828/3852
--
-- Revision 2.03, 2007/07/27, Nic
-- [Modification]
-- [1]Remove the redundant node swDHCPServerBindingMgmt.
--
-- Revision 2.02, 2007/06/08, Nic
-- [Modification]
-- [1]Change the access mode of swDHCPServerDefaultRouterIndex from
-- Read-Write to read-only
-- [2]Change the access mode of swDHCPServerManualBindingIpAddress from
-- Read-Write to read-only
-- Notes: Requested by Nic for project DGS3600R2.
--
-- Revision 2.01, 2007/05/30, Nic
-- [Modification]
-- [1]Modify swDHCPServerExcludedAddressTable. Delete node
-- swDHCPServerExcludedAddressIndex, and change the index of table from
-- swDHCPServerExcludedAddressIndex to swDHCPServerExcludedAddressBegin and
-- swDHCPServerExcludedAddressEnd.
-- Notes: Requested by Nic for project DGS3600R2.
--
-- Version 2.00, 2007/05/17, Nic
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
                                                                 R"The structure of DHCP Server management for the
		    		proprietary enterprise." "http://support.dlink.com"                   R"This object enables/disables the DHCP server status
             of the device."                       �"This object specifies the number of ping packets
            the DHCP server sends to an IP address before
            assigning this address to a requesting client. 
            0 means no ping test."                       �"This object specifies the amount of time in millisecond the
            DHCP server must wait before timing out a ping packet."                                   5"A table that contains DHCP server pool information."                       )"A list of DHCP server pool information."                       "The name of this pool entry."                       ."The network address of this DHCP pool entry."                       3"The network address mask of this DHCP pool entry."                       m"The domain name for the client if the server allocates
         the address for the client from this pool."                       4"The NetBIOS node type for a Microsoft DHCP client."                       �"The pool lease state. When configured to infinity, the following
        nodes include swDHCPServerPoolLeaseDay, swDHCPServerPoolLeaseHour
        and swDHCPServerPoolLeaseMinute have no significance any more."                       "The day of the lease."                       "The hour of the lease."                       "The minute of the lease."                       4"The name of the file that is used as a boot image."                       ="The next server to be used in the DHCP client boot process."                       !"This object manages this entry."                       c"A table that contains the IP address of a DNS server
        that is available to a DHCP client."                       #"A list of DNS server information."                       "The pool name of this entry."                        "The DNS server address number."                       D"The IP address of a DNS server that is available to a DHCP client."                       !"This object manages this entry."                       w"A table containing the NetBIOS WINS server address information
        that is available to a Microsoft DHCP client."                       +"A list of NetBIOS WINS server infomation."                       "The pool name of this entry."                       !"The NetBIOS WINS server number."                       ="The NetBIOS WINS server that is available to a DHCP client."                       !"This object manages this entry."                       O"A table that contains the IP address of the default router for a DHCP client."                       '"A list of default router information."                       "The pool name of this entry."                       "The default router index."                       6"The address of the default router for a DHCP client."                       !"This object manages this entry."                       m"A table that contains an IP addresses group that
        the DHCP server should not assign to DHCP client."                       Y"A group of IP addresses that the DHCP server
        should not assign to DHCP client."                       *"The start address of this address range."                       ("The end address of this address range."                       !"This object manages this entry."                       n"A table that contains information regarding the binding
        of a pool of IP addresses to a DHCP server."                        "A list of manual pool binding."                       "The pool name of this entry."                       8"IP address which will be assigned to specified client."                       "The MAC address of a client."                       �"The type of this manual binding.
         both(3): The client type has no significance, the entry
         	  could apply to either one.
        "                       !"This object manages this entry."                       m"A table that contains information regarding dynamic
        binding information of a pool of IP addresses."                       C"A pool of IP addresses that are listed as bound to a DHCP server."                       "The name of this pool entry."                       >"IP addresses which have been assigned to a specified client."                       "The MAC address of a client."                       "The type of this binding."                       "The status of this binding."                       `"The life time of the binding entry in seconds.
		 The value '-1' means an infinite life time."                       *"This object is used to clear this entry."                       /"A table that contains DHCP address conflicts."                       c"A DHCP address conflict means a duplication of use
         of the same IP address by two hosts."                       7"IP address which is duplicated and used by two hosts."                       "The server detects conflicts using ping. The client detects
		 conflicts using gratuitous Address Resolution Protocol (ARP)."                       4"The time at which a DHCP address conflict happens."                       *"This object is used to clear this entry."                      