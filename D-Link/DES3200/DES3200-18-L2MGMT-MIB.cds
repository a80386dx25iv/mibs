)-- -----------------------------------------------------------------------------
-- MIB NAME : L2 Management MIB
-- FILE NAME: L2mgmtDes3200.mib
-- DATE     : 2009/12/16
-- VERSION  : 2.33
-- PURPOSE  : To construct the MIB structure for L2 Management of the switch
--            for proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICATION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
-- -----------------------------------------------------------------------------
-- Version 2.33, 2009/12/16,Eli Lin
-- Description:
--  [New Object]
-- Add swL2DevCtrlWeb and swL2DevCtrlWebState
-- -----------------------------------------------------------------------------
-- Version 2.32, 2009/09/22,Eli Lin
-- Description:
--  [New Object]
--  Add swL2DevCtrlCFM, swL2DevCtrlCFMState swL2DevCtrlCFMPortTable 
--  swL2DevCtrlCFMPortIndex, swL2DevCtrlCFMPortState for supporting 802.1ag
-- -----------------------------------------------------------------------------
-- Version 2.31, 2009/07/19,Eli Lin
-- Description:
--  [New Object]
--  Add swL2PortLoopOccurred, swL2PortLoopRestart, swL2VlanLoopOccurred
--  swL2VlanLoopRestart, swL2LoopDetectMode, swL2LoopDetectTrapMode,
--  swL2LoopDetectPortLoopVLAN and swL2VlanLoopDetectVID for loop back 
--  detection VLAN base 
--  [Modification]
--  Modify swL2LoopDetectPortLoopStatus for loop back detection VLAN base 
--  Revise swl2NotificationBidings to swl2NotificationBindings
--  Revise swl2PortSecurityBidings to swl2PortSecurityBindings
-- -----------------------------------------------------------------------------
-- Version 2.30, 2009/05/20,Eli Lin
-- Move swIGMPSnoopingGroupTable to McastSnooping.mib 
-- -----------------------------------------------------------------------------
-- Version 2.29, 2009/04/23,Eli Lin
-- Remove swL2IGMPInfoTable,swIGMPSnoopingGroupTable has enough information. 
-- -----------------------------------------------------------------------------
-- Version 2.28, 2009/04/16,Eli Lin
-- Add swL2DevAlarmLinkChange for enable/disable link trap
-- -----------------------------------------------------------------------------
-- Version 2.27, 2009/04/15,Eli Lin
-- Add swL2PVIDAutoAssignmentState for PVID auto assign
-- -----------------------------------------------------------------------------
-- Version 2.26, 2009/04/12,Eli Lin
-- modify  swL2PortErrPortReason for the loopdetect feature.
-- -----------------------------------------------------------------------------
-- Version 2.25, 2009/04/13,Eli Lin
-- Add swL2DevPasswordEncryptionState for the password encryption function.
-- Support swIGMPSnoopingHostTable for show "igmp igmp_snooping host"
-- Add the new objects which are swL2IGMPMulticastVlanUntagSourcePort, 
-- swL2IGMPMulticastVlanRemapPriority and wL2IGMPMulticastVlanReplacePriority for 
-- the multicast enhancement.
-- -----------------------------------------------------------------------------
-- Version 2.24, 2009/04/12,Eli Lin
-- Remove swL2CosMacBasePRITable because this chip does not support this function
-- -----------------------------------------------------------------------------
-- Version 2.23, 2009/04/12,Eli Lin
-- Remove swL2IGMPv3Table and  swL2IGMPDynIPMultVlanState, because we do not support 
-- this features.
-- -----------------------------------------------------------------------------
-- Version 2.22, 2009/04/11,Eli Lin
-- Support swL2PortCtrlJumboFrame and swL2PortCtrlJumboFrameMaxSize for the jumbo frame
-- -----------------------------------------------------------------------------
-- Version 2.21, 2009/04/10,Marco Visaya
-- add swL2DevArpAgingTime
-- -----------------------------------------------------------------------------
-- Version 2.20, 2009/04/08,Eli Lin
-- remove swL2IGMPHostTimeout, swL2IGMPRouterTimeout and swL2IGMPLeaveTimer
-- -----------------------------------------------------------------------------
-- Version 2.19, 2009/01/14,Peter Hsieh
-- remove swL2CosPriorityPortPRI and swL2CosPortPRITable
-- -----------------------------------------------------------------------------
-- Version 2.18, 2009/01/13,Eli Lin
-- modify name of swL2IGMPRouterTimeout
-- modify STATUS of swL2DhcpRelayCtrlServer
-- add swL2PortSecurityEntryClearCtrl
-- -----------------------------------------------------------------------------
-- Version 2.17, 2009/01/09,Eli Lin
-- added swL2PortInfoFlowCtrlOperStatus
-- -----------------------------------------------------------------------------
-- Version 2.16, 2008/12/15,Pete Hsieh
-- modify bandwidth rate unit of swL2QOSBandwidthRxRate and swL2QOSBandwidthTxRate
-- -----------------------------------------------------------------------------
-- Version 2.14, 2008/11/03,Marco Visaya
-- modify range of swL2PortSecurityMaxLernAddr
-- -----------------------------------------------------------------------------
-- Version 2.13, 2008/10/29,Hunk Li
-- Add swL2McastFilterPortAccess,swL2McastFilterPortInfoAccess
-- Modify swL2McastFilterPortInfoProfileName to swL2McastFilterPortInfoProfileID
-- (line 4646 and 4660)
-- -----------------------------------------------------------------------------
-- Version 2.12, 2008/10/27, Marco Visaya
-- added description swL2QOSSchedulingMaxWeight
-- deactivated half-1Gigabps option in swL2PortInfoNwayStatus 
-- changed access of swL2PortInfoMediumType to read-only
-- -----------------------------------------------------------------------------
-- Version 2.11, 2008/10/20, Marco Visaya
-- change description of swL2DevInfoFrontPanelLedStatus 
-- -----------------------------------------------------------------------------
-- Version 2.10, 2008/10/14, Marco Visaya
-- added 'other' option to swL2PortCtrlMDIXState
-- -----------------------------------------------------------------------------
-- Version 2.09, 2008/09/26, Marco Visaya
-- added swL2QOS8021pRadiusPriority
-- -----------------------------------------------------------------------------
-- Version 2.08, 2008/09/12, Marco Visaya
-- added swL2QOSBandwidthRadiusRxRate
-- added swL2QOSBandwidthRadiusTxRate
-- swL2DhcpRelayMgmt made obsolete (use DHCP-RELAY-MGMT-MIB)
-- -----------------------------------------------------------------------------
-- Version 2.07, 2008/08/12, Marco Visaya
-- modified index of swL2IGMPAccessAuthTable
-- -----------------------------------------------------------------------------
-- Version 2.06, 2008/07/07, Marco Visaya
-- added swL2IGMPDynIPMultVlanState
-- added swL2IGMPDynIPMultVlanAge
-- added swIGMPSnoopingGroupReportCount
-- added swIGMPSnoopingGroupUpTime
-- added swIGMPSnoopingGroupExpiryTime
-- added swIGMPSnoopingGroupRouterPorts
-- added swL2IGMPDynIpMultMgmt
-- -----------------------------------------------------------------------------
-- Version 2.05, 2008/05/30, Marco Visaya
-- added swL2TrunkVLANTable
-- added swL2DevCtrlVLANTrunkState
-- -----------------------------------------------------------------------------
-- Version 2.04, 2008/05/29, Marco Visaya
-- added swL2IGMPSnoopingMulticastVlanState
-- added swL2IGMPMulticastVlanTable
-- added swL2IGMPMulticastVlanGroupTable
-- added swL2IGMPv3Table
-- added swIGMPSnoopingGroupTable
-- -----------------------------------------------------------------------------
-- Version 2.03, 2008/05/22, Marco Visaya
-- add swL2IGMPAccessAuthTable
-- -----------------------------------------------------------------------------
-- Version 2.02, 2008/03/25, Marco Visaya
-- add swL2DhcpLocalRelayMgmt
-------------------------------------------------------------------------------
-- Version 2.01, 2008/03/07, Marco Visaya
-- add swL2IGMPRouterPortForbiddenPortList
-------------------------------------------------------------------------------
-- Version 2.00, 2008/01/22, Marco Visaya
-- add swL2PortCtrlMDIXState for new feature request
-- configure MDIX setting of port
-------------------------------------------------------------------------------
-- Version 1.06, 2007/2/22, Marco Visaya
-- Add swL2DevCtrlAsymVlanState
-------------------------------------------------------------------------------
-- Version 1.05, 2007/1/28,Marco Visaya
-- Add s2L2VlanMultiply*
-------------------------------------------------------------------------------
-- Version 1.04, 2007/1/23,Grace Liu
-- Add swL2DevCtrlLLDPState
-------------------------------------------------------------------------------
-- Version 1.03, 2007/12/01, Peter Hsieh
-- Add swL2VlanAdvertisementTable swL2LoopDetectMgmt, swL2MultiFilter
-------------------------------------------------------------------------------
-- Version 1.02, 2005/4/8, Scott Slong
-- Add swL2DevCtrlManagementVlanId
-------------------------------------------------------------------------------
-- Version 1.01, 2004/9/21, Karen Huang
-- Add swL2PortCableDiagnosisTable
------------------------------------------------------------------------------- 	
-- Version 1.00
-- Remove swDevInfoSystemUptime, swDevInfoConsoleInuse,swL2DevCtrlUpDownloadState,swDevInfoSaveCfg
-- Remove swL2DevCtrlUpDownloadImageFileName ,swL2DevCtrlUpDownloadImageSourceAddr
-- Remove swL2DevCtrlUpDownloadImage , swL2DevCtrlSaveCfg
------------------------------------------------------------------------------- 	
-- Version 0.02, 2002/07/30, Tommy Wang
-- Renamed to DXxxxxx-MIB
-- Add swL2TrafficMgmt
-- Modified swL2DevCtrl
-- Removed swL2PortStTable
-- Removed swL2PortCtrlEntry->swL2PortCtrlBackPressState and 
-- swL2PortCtrlBMStromthreshold
-- Modified swL2MirrorMgmt
-- Removed swL2FilterAddrVid
-- Modified swL2IGMPCtrlTable
-- Removed swL2PriorityMgmt   
-- Modified swL2TrunkMasterPortIndex MAX-ACCESS read-only to read-write
-- Add swL2IGMPCtrState for invalid(4)
-- Modified swL2PortInfoType
-- Removed swDevInfoFrontPanelLedStatus about RPS and modified its SIZE
-- Removed swL2TrafficMgmt
-------------------------------------------------------------------------------
-- Version 0.01, 2000/07/03,Chrissy Chen   
-- Removed swL2PortTxPrioQueueCtrlTable
-- Modified swL2PortCtrlBMStromPercentage to swL2PortCtrlBMStromthreshold  
--   
-------------------------------------------------------------------------------
                                                                                     I"The Structure of Layer 2 Network Management Information for enterprise." "http://support.dlink.com"                      \"This object is a set of system LED indicators.
         
     The first two octets are defined as the system LED. The first indicator is 
     the power LED. The second indicator is the console LED. 

     The other octets are defined as follow:

     Starting on the third octet indicates the logical port LED (following 
     dot1dBasePort ordering). Every two bytes are presented to a port. The first 
     byte is presented to the Link/Activity LED. The second byte is presented to 
     the Speed LED.

     Link/Activity LED:

	 The most significant bit is used for blink/solid:
	 8 = The LED blinks.
	 
	 The second significant bit is used for link status:

	 1 = link fail.

	 2 = link pass.

	 Speed LED:

	 01 = 10Mbps.

	 02 = 100Mbps.

	 03 = 1000Mbps.

	 The four remaining bits are currently unused and must be 0."                           �"This object indicates the agent system reboot state. The agent always
                  returns other(1) when this object is read."                       ""This object indicates system IP."                       +"This object indicates system subnet mask."                       /"This object indicates system default gateway."                       k"This object controls the VLAN, including system IP. The VLAN
                  should have been created."                       �"This object indicates layer 2 Internet Group Management Protocol
                  (IGMP) capture function is enabled or disabled ."                       ~"As the object is set to active, all the statistic counters will be cleared.
                  If set to normal, do nothing."                      D"Indicates whether the SNMP entity is permitted to generate
                      authenticationFailure traps.  The value of this object
                      overrides any configuration information; as such, it
                      provides a means whereby all authenticationFailure traps may
                      be disabled.

                      Note that it is strongly recommended that this object be
                      stored in non-volatile memory so that it remains constant
                      across re-initializations of the network management system."                       ."This object can be enabled or disabled RMON."                       Y"Indicates  the status of automatically getting configuration from TFTP server on device"                       7"This object can enabled or disabled MAC Notification."                       p"This object indicates the history size of variation MAC in address table. 
	         The default value is 1 ."                       X"This object indicates the time interval in second for trigger the MAC notify message. "                      :"Specifies the state of the LLDP function.
             When this function is enabled, the switch can start to transmit
             LLDP packets and receive and process the LLDP packets.
	     The specific function of each port will depend on the per port
	     LLDP setting.
	     For the advertisement of LLDP packets, the switch announces the
	     information to its neighbor through ports. For the receiving of
	     LLDP packets, the switch will learn the information from the LLDP
	     packets advertised from the neighbor in the neighbor table.
	    "                       �"When lldp is disabled and lldp forward_message  is enabled, the received
              LLDP Data Units packet will be forwarded. "                       L"This object enables or disables the asymmetric VLAN feature of the device."                       C"This indicates the global state of the igmp_snoop multicast_vlan."                       M"This indicates the global state of the VLAN trunking feature of the device."                       _"The timeout period in minutes for aging out dynamically learned
            ARP information."                       _"If the password encryption is enabled, the password will be in 
             encrypted form."                           -"This object indicates the CFM global state."                       6"A table containing the CFM state of specified ports."                       0"The entry of the CFM state on specified ports."                       ("This object indicates the port number."                       ."This object indicates the CFM state by port."                           ""This object controls Web status."                          T"This object determines whether or not to send a trap message when the link              
              changes. If the object is enabled (3), the Link Change trap is sent by 
             the device when any of its port links change. The device does not send the 
             trap if this object value is disabled or in another state."                           5"A table that contains information about every port."                       4"A list of information for each port of the device."                       g"This object indicates the module's port number.(1..Max port number in
                   the module)"                       7"This object indicates the port type: fiber or copper."                       -"This object indicates the port link status."                       7"This object indicates the port speed and duplex mode."                      �"The current operational state of flow control. The none (1) state
                   indicates that no flow control is performed. The flow control
                   mechanism is different between full duplex mode and half duplex
                   mode. For full duplex mode, the IEEE 802.3x (2) flow control function
                   sends PAUSE frames and receives PAUSE frames. For half duplex mode,
                   backpressure (3) is performed."                       ="A table that contains control information about every port."                       <"A list of control information for each port of the device."                       g"This object indicates the module's port number.(1..Max port number in
                   the module)"                       7"This object indicates the port type: fiber or copper."                       >"This object decides whether the port is enabled or disabled."                       >"Choose the port speed, duplex mode, and N-Way function mode."                       7"Set the flow control function as enabled or disabled."                       )"The object describes the ports in text."                       J"This object decides whether the address learning is enabled or disabled."                       6"This object sets each port's MAC notification state."                       H"This object decides the multicast packet filtering mode on this port. "                       �"This object configures the MDIX setting of the port. 
		            The value 'other' is for those entries in which MDIX is not applicable."                       7"A table that contains information about the Err port."                       7"A list of information for the err port of the device."                       g"This object indicates the module's port number.(1..Max port number in
                   the module)"                       D"This object decides whether the port state is enabled or disabled."                       ="This object decides whether the PortStatus is err-disabled."                       I"This object decides whether the PortStatus STP is LBD or Storm control."                       )"The object describes the ports in text."                       ;"This object configures the switch's jumbo frame settings."                       >"This object describes how many bytes the max jumbo frame is."                           "."                       B"A list of information contained in swL2QOSBandwidthControlTable."                       "Indicates the port."                       ["Indicates the RX Rate(Kbit/sec) of the specified port. A value of 1024000 means no limit."                       \"Indicates the TX Rate(Kbit/sec) of the specified port. A value of 1024000 means no limit. "                       �"The Rx Rate value comes from the RADIUS server,
		       If an 802.1X port is authenticated, this value
		       will overwrite the locally configured Rx Rate. "                       �"The Tx Rate value comes from the RADIUS server,
		       If an 802.1X port is authenticated, this value
		       will overwrite the locally configured Tx Rate. "                       "."                       @"A list of information contained in the swL2QOSSchedulingTable."                       &"Indicates the hardware queue number."                       �"Indicates the maximum number of packets the above specified hardware
          	   priority queue will be allowed to transmit before allowing
          	   the next lowest priority queue to transmit its packets."                       ,"Indicates the mechanism of QOS scheduling."                       3"This object can control QOS scheduling Mechanism."                       "."                       G"A list of information contained in the swL2QOS8021pUserPriorityTable."                       "The 802.1p user priority."                       �"The number of the switch's hardware priority queue. The switch has four hardware 
		       priority queues available. They are numbered between 0 (the lowest priority)
		        and 3 (the highest priority)."                       "."                       J"A list of information contained in the swL2QOS8021pDefaultPriorityTable."                       "Indicates the port number."                       h"The priority value to assign to untagged packets received by the
		       switch ports on the switch."                       �"Indicates the value of 802.1p comes from RADIUS server.
	       If an 802.1X port is authenticated,
           this value will overwrite the local configured value."                           )"The max entries of the swPortTrunkTable"                       -"The max number of ports allowed in a trunk."                       A"This table specifies the port membership for each logical link."                       b"A list of information that specifies which port group forms
             a single logical link."                       ""The index of logical port trunk."                      �"The object indicates the master port number of the port trunk entry. The 
            first port of the trunk is implicitly configured to be the master logical 
            port. When using a Port Trunk, you can not configure the other ports of 
            the group except the master port. Their configuration must be the same as the
            master port (e.g. speed, duplex, enabled/disabled, flow control, and so on)."                       +"Indicate member ports of a logical trunk."                       /"This object indicates the type of this entry."                       7"This object indicates the active ports of this entry."                       �"This object indicates the status of this entry. 
            when the state is CreatAndGo (4),the type of trunk is static (1);
            when the state is CreatAndWait (5), the type of trunk is lacp(2).
            "                       #"The flooding port of every trunk."                       �"This object configures part of the packet to be examined by the 
            switch when selecting the egress port for transmitting 
            load-sharing data."                       s"This table specifies which port group a set of ports (up to 8)
            is formed into a single logical link."                       "A list of information specifies which port group a set of 
            ports (up to 8) is formed into a single logical link."                       &"The index of the logical port LACP. "                       %"The state of the logical port LACP."                       G"This table is used to manage the VLAN trunking feature of the device."                       L"This object is used to configure the VLAN trunking settings for each port."                       2"This object indicates the port being configured."                       %"The state of the logical port LACP."                           @"This object indicates the Rx port list of ports to be sniffed."                       @"This object indicates the Tx port list of ports to be sniffed."                      "This object indicates the switch whose port will sniff another port.
            A trunk port member cannot be configured as a target Snooping port.
            The port number is the sequential (logical) number which is also 
            applied to the bridge MIB, etc. "                       1"This object indicates the status of this entry."                           a"The maximum number of VLANs in the layer 2 IGMP control table
            (swL2IGMPCtrlTable)."                       �"The maximum number of multicast IP groups per VLAN in the layer 2 
            IGMP information table (swL2IGMPQueryInfoTable)."                      ]"The table controls the VLAN's IGMP function. Its scale depends
            on the current VLAN state (swL2VlanInfoStatus). If the VLAN mode is disabled, there is only one entry in the table, with index 1. If 
            VLAN is in Port-Based or 802.1q mode, the number of entries can 
            be up to 12, with an index range from 1 to 12."                       �"An entry in IGMP control table (swL2IGMPCtrlTable). The entry
            is effective only when IGMP captures the switch 
            (swL2DevCtrlIGMPSnooping) is enabled."                      �"This object indicates the IGMP control entry's VLAN ID. If VLAN
            is disabled, the VID is always 0 and cannot be changed by
            management users. If VLAN is in Port-Based mode, the VID is
            arranged from 1 to 12, fixed form. If VLAN is in 802.1q mode,  
            the VID setting can vary from 1 to 4094 by management user, and 
            the VID in each entry must be unique in the IGMP Control Table."                       ^"The frequency at which IGMP Host-Query packets are
             transmitted on this switch."                       1"The maximum query response time on this switch."                      "The Robustness Variable allows tuning for the expected
             packet loss on a subnet. If a subnet is expected to be
             lossy, the Robustness Variable may be increased. IGMP is
             robust to (Robustness Variable-1) packet losses."                       �"The Last Member Query Interval is the Max Response Time
            inserted into Group-Specific Queries sent in response to
            Leave Group messages, and is also the amount of time between
            Group-Specific Query messages."                       ?"This object decides if the IGMP query is enabled or disabled."                       5"This object indicates the current IGMP query state."                      x"This object indicates the status of this entry.

            other (1) - This entry is currently in use but the conditions under
                which it will remain so are different from each of the following
                values.
            disable (2) - IGMP function is disabled for this entry.
            enable (3) -  IGMP function is enabled for this entry."                       "  "                       `"This object is used to enable or disable aging on the dynamic IP multicast entry in this VLAN."                       �"The table contains the number of current IGMP query packets which
            are captured by this device, as well as the IGMP query packets
            sent by the device."                       �"Information about current IGMP query information, provided that
            swL2DevCtrlIGMPSnooping and swL2IGMPCtrState of associated VLAN
            entry are all enabled."                       �"This object indicates the VID of the associated IGMP info table
            entry. It follows swL2IGMPCtrlVid in the associated entry of 
            IGMP control table (swL2IGMPCtrlTable)."                       �"This object indicates the number of query packets received 
            since the IGMP function enabled, on a per-VLAN basis."                       �"This object indicates the send count of IGMP query messages, on
            a per-VLAN basis. In case of the IGMP timer expiring, the switch
            sends IGMP query packets to related VLAN member ports and
            increment this object by 1."                       +"The information of the router port table."                       +"The entry of the swL2IGMPRouterPortTable."                       ="This object indicates the VLAN ID of the router port entry."                       ?"This object indicates the VLAN name of the router port entry."                       E"This object indicates the static portlist of the router port entry."                       F"This object indicates the dynamic portlist of the router port entry."                       H"This object indicates the forbidden portlist of the router port entry."                       i"This table is used to manage the IGMP Access Authentication 
            configurations of the device."                       p"A list of manageable entities for IGMP Access Authentication.
            The configuration is done per port."                       n"The index of the swL2IGMPAccessAuthTable. 
           This object corresponds to the port being configured."                       K"This object denotes the status of IGMP Access Authentication of the port."                       ;"Information about the IGMP snooping multicast VLAN table."                       *"The entry of swL2IGMPMulticastVlanTable."                       ]"This object indicates the VLAN ID of the IGMP snooping multicast 
             VLAN entry."                       _"This object indicates the VLAN name of the IGMP snooping multicast 
             VLAN entry."                      *"This object indicates the port list of the source ports of the IGMP 
             snooping multicast VLAN. The source ports will be set as tag ports 
             of the VLAN entry and the IGMP control messages received from the
             member ports will be forwarded to the source ports."                      ."This object indicates the port list of the member ports of the IGMP 
             snooping multicast VLAN. The member ports will be set as untagged ports
             of the VLAN entry and the IGMP control messages received from the
             member ports will be forwarded to the source ports."                       q"This object indicates the port list of the tag member ports of the IGMP 
             snooping multicast VLAN."                       P"This object can be used to enable or disable the IGMP snooping multicast VLAN."                       3"The replacement source IP of this multicast VLAN."                       1"This object indicates the status of this entry."                       s"This object indicates the port list of the untag source ports of the IGMP 
             snooping multicast VLAN."                       �"This is the priority value (0 to 7) to be associated with the data traffic
            to be forwarded on the multicast VLAN.
            When set to -1, the packet's original priority will be used."                       �"This specifies that a packet's priority will be changed by the switch
            based on the remap priority. This flag will only take effect
            when remap priority is set."                       I"The table containing the IGMP snooping multicast VLAN group information"                       C"Information about the current IGMP snooping multicast VLAN group."                       J"This object indicates the VID of the IGMP snooping multicast VLAN group."                       5"Specifies the multicast address list for this VLAN."                       5"Specifies the multicast address list for this VLAN."                       1"This object indicates the status of this entry."                           c"This object specifies the maximum number of entries which can be learned by dynamic IP multicast."                           i"This object indicates the VLAN identifier where the data driven 
        entries will be removed from."                       ~"This object indicates the IP address of the IGMP snooping group from which
        the data driven entries will be removed."                      �"Setting this object will clear the data driven entries.
            	all - Remove all learned data driven groups. 
            	VLAN - Clear all data driven entries in the VLAN specified in 
            			swIGMPSnoopingClearDynIpMultVID.
            	group - Clear the group with the address specified in swL2IGMPSnoopingClearDynIpMultIP
            		    in the VLAN specified in swIGMPSnoopingClearDynIpMultVID.
            "                       @"The table contains the IGMP hosts that have joined the groups."                       ="The entry contains the IGMP host that has joined the group."                       `"This object indicates the VID of the individual IGMP snooping host 
             table entry."                       j"This object indicates the group address of the individual IGMP 
             snooping host table entry."                       h"This object indicates the port number of the individual IGMP 
             snooping host table entry."                       l"This object indicates the host IP address of the individual IGMP 
             snooping host table entry."                           a"This table specifies the port just can forward traffic to the 
            specific port list."                       W"A list of information specifies the port with its traffic 
            forward list."                       &"The port number of the logical port."                       ;"The port list that the specific port can forward traffic."                          W"The port security feature controls the address leaning capability and the
                traffic forwarding decision. Each port can have this function enabled or disabled.
                When it is enabled and a number is given said N, which allows N addresses to be
                learned at this port, the first N learned addresses are locked at this port as
                a static entry. When the learned address number reaches N, any incoming packet without
learned source addresses are discarded (e.g. dropped) and no more new addresses
                can be learned at this port."                       F"A list of information contained in the swL2PortSecurityControlTable."                       4"Indicates a secured port to lock address learning."                       I"Indicates the allowable number of addresses to be learned at this port."                      �"Indicates the mode of locking address.
                In deleteOnTimeout (3) mode - the locked addresses can be aged out after the aging timer
                expires. In this mode, when the locked address is aged
                out, the number of addresses that can be learned has to be
                increased by one.
                In deleteOnReset (4) mode - never age out the locked addresses unless restarting
                the system to prevent port movement or intrusion."                       7"Indicates an administration state of locking address."                       �"Used to clear port security entries by port.
			 Setting this value to 'start' will execute the clear action,
			 Once cleared, the value returns to 'other'."                       �"When enabled (2), whenever there's a new MAC that violates the pre-defined
                  port security configuration, a trap will be sent out and the relevant information
                  will be logged in the system."                           "Indicates the VLAN name."                       ="Indicates the port. 0 means the function dose not work now."                       "Specifies the MAC address."                       ""                               P"Used to show and configure per port priority-based QoS features on the switch."                       >"A list of information contained in the swL2CosPriorityTable."                       ""The port number of CoS Priority."                       �"Enable Ethernet frame based priority. 
                      When set ether8021p (2), enable 802.1p QoS; When set macBase (3), enable MAC-based QoS; 
                      When set disable (1), disable Ethernet frame based priority."                       �"Enable IP priority QoS.
                      When set tos (2), enable TOS based QoS; When set dscp (3), enable DSCP based QoS; 
                      When set disable (1), disable IP priority QoS."                       �"When read, it always returns invalid (2);
                      when write valid  (1), it disables all priority in this table."                       �"Used to show TOS value to traffic class mapping and
                       map the TOS value in the IP header of incoming packet to one of the four hardware queues available on the switch."                       <"A list of information contained in the swL2CosTosPRITable."                       $"Indicates the CoS Priority TosPRI."                      5"The number of the switch's hardware priority queue. The switch has 4 hardware priority queues available. 
                      They are numbered between 0 (the lowest priority queue) and 3 (the highest priority queue).
                      If you want to set one, you must have administrator privileges."                       �"Used to show DSCP value to traffic class mapping and
                      map the DSCP value in the IP header of incoming packet to one of the hardware queues available on the switch."                       ="A list of information contained in the swL2CosDscpPRITable."                       %"Indicates the CoS Priority DscpPRI."                      5"The number of the switch's hardware priority queue. The switch has 4 hardware priority queues available. 
                      They are numbered between 0 (the lowest priority queue) and 3 (the highest priority queue).
                      If you want to set one, you must have administrator privileges."                           ]"This object indicates whether the DHCP relay function is enabled or 
            disabled."                       h"This object indicates the maximum number of router hops that 
            the DHCP packets can cross."                       �"This object indicates the minimum time in seconds within which
            the switch must relay the DHCP request. If this time is 
            exceeded, the switch will drop the DHCP packet."                       m"This object indicates DHCP relay agent information option 82 
            function is enabled or disabled."                       ~"This object indicates the checking mechanism of DHCP relay agent 
            information option 82 is enabled or disabled."                      U"This object indicates the reforwarding policy of DHCP relay agent 
            information option 82.
            
            replace (1) - Replace the exiting option 82 field in messages.
            drop (2)  - Discard messages with existing option 82 field.
            keep (3)  - Retain the existing option 82 field in messages."                       g"This table specifies the IP address as a destination to forward
            (relay) DHCP packets to."                       r"A list of information specifies the IP address as a destination
            to forward (relay) DHCP packets to."                       "The name of the IP interface."                       "The DHCP server IP address."                      �"This object indicates the status of this entry.
 
            other (1) - This entry is currently in use but the conditions
                under which it will remain so are different from each of
                the following values.
            invalid (2) - Writing this value to the object, and then the
                corresponding entry will be removed from the table.
            valid (3) - This entry resides in the table."                               �"When the port_security trap is enabled, if there's a new MAC that violates the pre-defined 
            port security configuration, a trap will be sent out."                 H" This trap indicates the MAC address variations in the address table. "                 +"The trap is sent when a Port loop occurs."                 E"The trap is sent when a Port loop restarts after the interval time."                 6"The trap is sent when a Port with a VID loop occurs."                 P"The trap is sent when a Port with a VID loop restarts after the interval time."                     V"This object indicates the MAC address that violates the port security configuration."                           G"This object indicates the last time reboot information.
            "                       9"This object indicates the VID that detected a loopback."                               E"This object indicates the loopback detection status for the system."                       Q"This object indicates the interval value, the range is from 1 to 32767 seconds."                       �"This object indicates the recover time, the range is from 60 to 1000000.
             The value of 0 disables the recover function."                       C"This object indicates the loopback detection mode for the system."                       H"This object indicates the loopback detection trap mode for the system."                           H"The table specifies the loopback detection function specified by port."                       H"The table specifies the loopback detection function specified by port."                       �"This object indicates the module's port number. The range is from 1 to
             the maximum port number specified in the module"                       J"This object indicates the loopback detection function state on the port."                       C"This object indicates the VLAN list that has detected a loopback."                       ("This object indicates the port status."                           H" A table that contains information about the multicast filter address."                       C"A list of multicast filter mode information for each profile ID. "                       " index for each profile"                       #"The multicast filter description."                       '"The control multicast filter address."                       *"The multicast filter address group list."                       1"This object indicates the status of this entry."                       3" A table that is used to bind port to profile ID."                       A"A list of information that is used to bind port to profile ID. "                       "The port index."                       '"The control multicast filter profile."                       x"This object indicates the profile ID of this entry. 
		  When read, it is always 0. 
		  When set, 0 can not be set."                       �"This object indicates the access status for each port.
      When read, it is always none.
      When set, none can not be set."                       N" A table that contains information about the max group number based on port."                       7"A list of max group number information for each port."                       "The port index."                       ,"The max group numbers. The default is 256."                       U" A table that contains information about all of the multicast groups for the ports."                       H"A list of information about all of the multicast groups for each port."                       "The port index."                       *"The multicast filter address profile ID."                       8"This object indicates the access status for each port."                           �"A table containing the advertisement state for each
             VLAN configured into the device by (local or
             network) management."                       Q"The advertisement state for each VLAN configured
             into the device."                       9"The VLAN-ID or other identifier referring to this VLAN."                       \"An administratively assigned string, which may be used
             to identify the VLAN."                       @"This object indicates the advertise status of this VLAN entry."                           )"This object specifies the VLAN ID List."                       0"This object specifies the advertisement state."                       "Specifies the port list."                       d"Specifies the action for the port list which specified 
             by swL2VlanMultiplyPortList."                      �"Specifies the action for VLANs.
             other:     no action.
             create:    the VLANs specified by swL2VlanMultiplyVlanList 
                        would be created at a time.
             configure: the VLANs specified by swL2VlanMultiplyVlanList 
                        would be configured at a time.
             delete:    the VLANs specified by swL2VlanMultiplyVlanList 
                        would be deleted at a time.
           "                      �"This object controls the PVID auto assigment state.
    		 If 'Auto-assign PVID' is disabled, PVID only be changed
    		 by PVID configuration (user changes explicitly). The VLAN
    		 configuration will not automatically change PVID.

		 If 'Auto-assign PVID' is enabled, PVID will be possibly
		 changed by PVID or VLAN configuration. When user configures
		 a port to VLAN X's untagged membership, this port's PVID
		 will be updated with VLAN X. In the form of VLAN list command,
		 PVID is updated with last item of VLAN list. When user removes
		 a port from the untagged membership of the PVID's VLAN, the
		 port's PVID will be assigned with 'default VLAN'."                           `"This object indicates the status of the DHCP local relay 
            function of the switch."                       O"This table is used to manage the DHCP local relay status 
	 		for each VLAN."                       k"This object lists the current VLANs in the switch and their
	    	corresponding DHCP local relay status."                       @"This object shows the VIDs of the current VLANS in the switch."                       X"This object indicates the status of the DHCP relay function 
            of the VLAN."                                  