'-- D-Link Common MIBs
-- DLINK-ID-REC
-- -----------------------------------------------------------------------------
-- Version 1.3
-- 2009/1/15
-- Modify syntax of AgentNotifyLevel
-- for support 8 notification levels.
-- by Green Zhu
-- -----------------------------------------------------------------------------
-- Version 1.2       
-- 2008/8/11
-- Add dlink-broadband-products and dlink-broadband-mgmt.
-- by Green Zhu
-- -----------------------------------------------------------------------------
-- Version 1.1       
-- 2004/7/6
-- Modify syntax of AgentNotifyLevel
-- by Karen
-- ----------------------------------------------------------------------------- 
-- Version 1.0.12  01-29-2002
-- ----------------------------------------------------------------------------- 
   "Notification  leveling."                                                                    