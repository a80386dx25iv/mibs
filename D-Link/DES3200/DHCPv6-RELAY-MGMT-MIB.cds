4-- -----------------------------------------------------------------------------
-- MIB NAME : DHCPv6-RELAY-MGMT-MIB
-- FILE NAME: DHCPv6Relay.mib
-- DATE     : 2009/9/17
-- VERSION  : 1.00
-- PURPOSE  : To construct the MIB structure of DHCPv6 Relay function for
--            proprietary enterprise
-- -----------------------------------------------------------------------------
-- MODIFICTION HISTORY:
-- -----------------------------------------------------------------------------
-- Version, Date, Author
-- Description:
--  [New Object]
--  [Modification]
-- Notes: (Requested by who and which project)
-- 
-- Version 1.00, 2009/9/17, Sammy Xiao
-- This is the first formal version for universal MIB definition.
-- -----------------------------------------------------------------------------
                                                 J"The Structure of DHCPv6 relay management for the proprietary enterprise." "http://support.dlink.com"                   j"This object indicates the maximum number of router hops that
             the DHCPv6 packets can cross."                       B"This object indicates the global state of DHCPv6 Relay function."                       <"This table displays the current DHCPv6 relay static table."                       T"A list of information display the current DHCPv6 relay static 
            table."                       8"This object indicates the name of the relay interface."                       @"This object indicates the DHCPv6 Relay state of the interface."                       l"This table indicates the IPv6 address as a destination to forward
             (relay) DHCPv6 packets to."                       w"A list of information indicates the IPv6 address as a destination
             to forward (relay) DHCPv6 packets to."                       s"This object indicates the type of the DHCPv6 server IPv6 address or 
            next DHCPv6 relay IPv6 address."                       g"This object indicates the DHCPv6 server IPv6 address or 
            next DHCPv6 relay IPv6 address."                       1"This object indicates the status of this entry."                      