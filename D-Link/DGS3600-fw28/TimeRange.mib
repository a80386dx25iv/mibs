

TIMERANGE-MIB DEFINITIONS ::= BEGIN

     IMPORTS
        MODULE-IDENTITY, OBJECT-TYPE
            FROM SNMPv2-SMI
        IpAddress           			
        	FROM RFC1155-SMI
        DateAndTime ,RowStatus, DisplayString
            FROM SNMPv2-TC
		dlink-common-mgmt 
		    FROM DLINK-ID-REC-MIB;
	
	
     swTimeRangeMIB MODULE-IDENTITY
          LAST-UPDATED "0202140000Z"
          ORGANIZATION " "
          CONTACT-INFO
                       " "
          DESCRIPTION
           "Equipments absolute time."
          ::= { dlink-common-mgmt 50 }

    --
    -- OID Tree Allocation in bsoluteTime
    --
    swTimeRangeCtrl		OBJECT IDENTIFIER ::= { swTimeRangeMIB 1 }
    swTimeRangeInfo          OBJECT IDENTIFIER ::= { swTimeRangeMIB 2 }       
    swTimeRangeMgmt		OBJECT IDENTIFIER ::= { swTimeRangeMIB 3 }
 

    --
    -- Object Definition for 
    --
-- -------------------------------------------------------------------------------------------------
-- swTimeRangeMgmt
-- --------------------------------------------------------------------------------------------
	swTimeRangeMgmtTable OBJECT-TYPE
	    SYNTAX      SEQUENCE OF SwTimeRangeMgmtEntry
	    MAX-ACCESS  not-accessible
	    STATUS      current
	    DESCRIPTION
	        "A table that contains the informations of each time range."
	    ::= { swTimeRangeMgmt 1 }
	
	swTimeRangeMgmtEntry OBJECT-TYPE
	    SYNTAX      SwTimeRangeMgmtEntry
	    MAX-ACCESS  not-accessible
	    STATUS      current
	    DESCRIPTION
	        "A list of informations for each time range."
	    INDEX { swTimeRangeMgmtRangeName }
	    ::= { swTimeRangeMgmtTable 1 }


	SwTimeRangeMgmtEntry ::=
	    SEQUENCE {
	        swTimeRangeMgmtRangeName
	            DisplayString,
	        swTimeRangeMgmtSelectDays
	       	    BITS,
	        swTimeRangeMgmtStartTime
	            DisplayString,
	        swTimeRangeMgmtEndTime
	            DisplayString,
	        swimeRangeMgmtStatus
	            RowStatus
	        }
        	    
	swTimeRangeMgmtRangeName	OBJECT-TYPE
		SYNTAX	DisplayString (SIZE (1..32))
		MAX-ACCESS read-only
		STATUS	current
		DESCRIPTION
			"The range name of the time range."
		::= { swTimeRangeMgmtEntry 1 }
		
	swTimeRangeMgmtSelectDays	 OBJECT-TYPE
		SYNTAX	BITS{
			sunday(0),
			monday(1),
			tuesday(2),
			wednesday(3),
			thursday(4),
			friday(5),
			saturday(6)			
			}
		MAX-ACCESS read-create
		STATUS current
		DESCRIPTION
			"The bits from left to right express the days from Sunday to Saturday.
			when set, the first bit from right to left can only be set 0. When you want to 
			choose a day, the corresponding bit needs to be set 1.
			For example, if you set 'E0', you have selected the Sunday,Monday, Tuesday;
			if you set '94', you have selected the Sunday, Wednesday, Friday.
			When read, the bits show the days you have selected. 
			As same as setting, if the bit shows 1, represents the corresponding day has been selected."
		::= { swTimeRangeMgmtEntry 2 }
	
	swTimeRangeMgmtStartTime	OBJECT-TYPE
		SYNTAX	DisplayString (SIZE (8))
		MAX-ACCESS read-create
		STATUS current
		DESCRIPTION
			"The start time in day. The immovable format is hh:mm:ss, you can't set another format.
			The end time must be set later than start time.
			The default value is 00:00:00."
		::= { swTimeRangeMgmtEntry 3 }

	swTimeRangeMgmtEndTime	OBJECT-TYPE
		SYNTAX	DisplayString (SIZE (8))
		MAX-ACCESS read-create
		STATUS current
		DESCRIPTION
			"The end time in day. The immovable format is hh:mm:ss, you can't set another format.
			The end time must be set later than start time.
			The default value is 23:59:59."
		::= { swTimeRangeMgmtEntry 4 }
	        			
	swimeRangeMgmtStatus OBJECT-TYPE                                   
	    SYNTAX      RowStatus                                        
	    MAX-ACCESS  read-create                                      
	    STATUS      current                                          
	    DESCRIPTION                                                  
	        "This object indicates the RowStatus of this entry."    
	    ::= { swTimeRangeMgmtEntry  5 }                              

        




END
