

SSH-MIB DEFINITIONS ::= BEGIN

-- -------------------------------------------------------------
-- SSH System Access Control MIB
-- -------------------------------------------------------------


IMPORTS
 	IpAddress
 	    FROM RFC1155-SMI
	Ipv6Address	             	
           FROM IPV6-TC
    MODULE-IDENTITY, OBJECT-TYPE
        FROM SNMPv2-SMI
    RowStatus,DisplayString
            FROM SNMPv2-TC
    MODULE-COMPLIANCE, OBJECT-GROUP
        FROM SNMPv2-CONF
    SnmpAdminString
	FROM SNMP-FRAMEWORK-MIB
	dlink-common-mgmt	FROM DLINK-ID-REC-MIB;


swSSHMIB MODULE-IDENTITY
        LAST-UPDATED "0703270000Z"
    ORGANIZATION " "
    CONTACT-INFO
        "  "
    DESCRIPTION
        "The Secure Shell module MIB."
    ::= { dlink-common-mgmt 6}

swSSHMgmt OBJECT IDENTIFIER ::= { swSSHMIB 1 }

-- -------------------------------------------------------------
-- Textual Conventions
-- -------------------------------------------------------------

 swSSHVersion OBJECT-TYPE
        SYNTAX  INTEGER(1..10) 
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "This object indicates the SSH version supported in the system."
        ::= { swSSHMgmt 1}
        
 swSSHAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Secure Shell is enable or disable."
        ::= { swSSHMgmt 2}

 swSSHMaxAuthFailAttempts OBJECT-TYPE
        SYNTAX  INTEGER (2..20)
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the tolerance in times of Authentication failure."
        ::= { swSSHMgmt 3}
 swSSHSessionKeyRekeying OBJECT-TYPE
        SYNTAX  INTEGER {
                        never(0),
                        ten-min(10),
                        thirty-min(30),
                        sixty-min(60)
                       }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the time interval in minutes to negotiate new session key
            for client and server."
        ::= { swSSHMgmt 4}
  
 swSSHMaxSession OBJECT-TYPE
        SYNTAX  INTEGER(1..8) 
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the SSH supported max session the system."
        ::= { swSSHMgmt 5}
      
 swSSHConnectionTimeout OBJECT-TYPE
        SYNTAX  INTEGER(120..600) 
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates connection timeout."
        ::= { swSSHMgmt 6}    
        
 swSSHListenedPort OBJECT-TYPE
        SYNTAX  INTEGER(1..65535)
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates listened port."
        ::= { swSSHMgmt 7}
                    

-- -------------------------------------------------------------
-- groups in the SSH MIB
-- -------------------------------------------------------------


-- -------------------------------------------------------------
 swSSHCtrlAlgGroup OBJECT IDENTIFIER ::= { swSSHMIB 3 }
-- -------------------------------------------------------------
  swSSHEncryptAlgCtrl OBJECT IDENTIFIER ::= { swSSHCtrlAlgGroup 1 }

   swSSHEncryptAlg3DESAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the TDES encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 1}

   swSSHEncryptAlgBlowfishAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Blowfish encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 2}

   swSSHEncryptAlgAES128Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the AES128 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 3}

   swSSHEncryptAlgAES192Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the AES192 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 4}

   swSSHEncryptAlgAES256Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the AES256 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 5}

   swSSHEncryptAlgArcfourAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Arcfour encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 6}

   swSSHEncryptAlgCAST28Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the CAST128 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 7}

   swSSHEncryptAlgTwofish128Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Twofish128 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 8}

   swSSHEncryptAlgTwofish192Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Twofish192 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 9}

   swSSHEncryptAlgTwofish256Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates the Twofish256 encryption algorithm is enable or disable
			."
        ::= { swSSHEncryptAlgCtrl 10}


-- ****************************************************************************************************
  swSSHAuthenMethodCtrl OBJECT IDENTIFIER ::= { swSSHCtrlAlgGroup 2 }
-- ****************************************************************************************************

   swSSHAuthenMethodPasswdAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates password authentication enable or disable
			."
        ::= { swSSHAuthenMethodCtrl 1}

   swSSHAuthenMethodPubKeyAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates Public Key authentication enable or disable
			."
        ::= { swSSHAuthenMethodCtrl 2}

   swSSHAuthenMethodHostKeyAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates Host Key authentication enable or disable
			."
        ::= { swSSHAuthenMethodCtrl 3}
-- ****************************************************************************************************
   swSSHInteAlgCtrl OBJECT IDENTIFIER ::= { swSSHCtrlAlgGroup 3 }
-- ****************************************************************************************************
   swSSHInteAlgSHA1Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates HMAC-SHA1 algorithm enable or disable
			."
        ::= { swSSHInteAlgCtrl 1}

   swSSHInteAlgMD5Admin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates HMAC-MD5 algorithm enable or disable
			."
        ::= { swSSHInteAlgCtrl 2}



--  swSSHKeyExDAdmin OBJECT IDENTIFIER ::= { swSSHCtrlAlgGroup 4 }

--   swSSHKeyExDAdmin OBJECT-TYPE
--        SYNTAX  INTEGER {
--                    other(1),
--                    disabled(2),
--                    enabled(3)
--                }
--        MAX-ACCESS  read-write
--        STATUS  current
--        DESCRIPTION
--            "This object indicates  Diffie Hellman algorithm enable or disable
--			."
--        ::= { swSSHCtrlAlgGroup 4}
        
-- ****************************************************************************************************
swSSHPublKeyCtrl OBJECT IDENTIFIER ::= { swSSHCtrlAlgGroup 5 }        
-- ****************************************************************************************************
   swSSHPublKeyRSAAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates RSA algorithm enable or disable."
        ::= { swSSHPublKeyCtrl 1}

   swSSHPublKeyDSAAdmin OBJECT-TYPE
        SYNTAX  INTEGER {
                    other(1),
                    disabled(2),
                    enabled(3)
                }
        MAX-ACCESS  read-write
        STATUS  current
        DESCRIPTION
            "This object indicates DSA algorithm enable or disable."
        ::= { swSSHPublKeyCtrl 2}

-- -----------------------------------------------------------------------------
-- SSH User Info Management
-- -----------------------------------------------------------------------------
    swSSHUserMgmt 	OBJECT IDENTIFIER ::= {swSSHMIB 4}

    swSSHUserCtrlTable OBJECT-TYPE
    	SYNTAX SEQUENCE OF SshUserEntry
    		MAX-ACCESS not-accessible
            STATUS current
            DESCRIPTION
            	"The SSH user table."
            ::= { swSSHUserMgmt 1 }

    swSSHUserCtrlEntry OBJECT-TYPE
    		SYNTAX SshUserEntry
            MAX-ACCESS not-accessible
            STATUS current
            DESCRIPTION
            	"The entries of SSH user table."
            INDEX  { swSSHUserName }
            ::= { swSSHUserCtrlTable 1 }

    SshUserEntry ::=
    	SEQUENCE {
    		swSSHUserName 		SnmpAdminString,
    		swSSHUserAuthMode	INTEGER,
    		swSSHUserHostName	SnmpAdminString,
    		swSSHUserHostIPAddr 	IpAddress,
    		swSSHUserHostIPv6Addr 	Ipv6Address
    	}

    swSSHUserName OBJECT-TYPE
    	SYNTAX	SnmpAdminString (SIZE(1..15))
    	MAX-ACCESS read-only
    	STATUS	current
    	DESCRIPTION
    		"SSH user name."
    	::= { swSSHUserCtrlEntry 1 }

    swSSHUserAuthMode OBJECT-TYPE
    	SYNTAX INTEGER {
    		hostbased(1),
    		password(2),
    		publickey(3)
    		}
    	MAX-ACCESS read-write
    	STATUS current
    	DESCRIPTION
    		"Specifies the auth mode for a user."
    	::= { swSSHUserCtrlEntry 2 }

    swSSHUserHostName OBJECT-TYPE
    	SYNTAX SnmpAdminString (SIZE(1..32))
    	MAX-ACCESS read-write
    	STATUS current
    	DESCRIPTION
    		"Specifies the host name of host-based auth mode."
    	::= { swSSHUserCtrlEntry 3 }

    swSSHUserHostIPAddr OBJECT-TYPE
    	SYNTAX IpAddress
    	MAX-ACCESS read-write
    	STATUS current
    	DESCRIPTION
    		"Specifies the host IP address of host-based auth mode."
    	::= { swSSHUserCtrlEntry 4 }

   swSSHUserHostIPv6Addr OBJECT-TYPE
        SYNTAX  	Ipv6Address
        MAX-ACCESS  read-write
        STATUS  	current
        DESCRIPTION
    		"Specifies the host IP address of host-based auth mode."
    	::= { swSSHUserCtrlEntry 5 }
END