RADLAN-SMON-MIB DEFINITIONS ::= BEGIN

-- Title:                RADLAN ROS
--                       Private SMON MIB
-- Version:              7.46
-- Date:                 15-Jan-2007

IMPORTS
    rnd                                                     FROM RADLAN-MIB
    OBJECT-TYPE, MODULE-IDENTITY                            FROM SNMPv2-SMI
    TruthValue                                              FROM SNMPv2-TC
    dot1dBasePort                                           FROM BRIDGE-MIB
    VlanId                                                  FROM Q-BRIDGE-MIB
	RowStatus, TEXTUAL-CONVENTION                           FROM SNMPv2-TC;

CopyModeType ::= TEXTUAL-CONVENTION
    STATUS current
    DESCRIPTION    "copy destination mode type:
                    1- monitor-only
                    2- network."
    SYNTAX INTEGER {
        monitor-only(1),
        network     (2)
}

CopyRemoteDirectionType ::= TEXTUAL-CONVENTION
    STATUS current
    DESCRIPTION    "copy remote direction type:
                    1- Rx
                    2- Tx"
    SYNTAX INTEGER {
        copyRemoteRx(1),
        copyRemoteTx(2)
}

rlSmon MODULE-IDENTITY
                LAST-UPDATED "200701020000Z"
                ORGANIZATION "Radlan - a MARVELL company.
                              Marvell Semiconductor, Inc."
                CONTACT-INFO
                      "www.marvell.com"
                DESCRIPTION
                      "This private MIB module defines SMON private MIBs."
                REVISION "200701020000Z"
                DESCRIPTION
                      "Initial revision."
        ::= { rnd 84 }

rlPortCopyMibVersion OBJECT-TYPE
    SYNTAX  INTEGER
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
        "MIB's version, the current version is 1."
    ::= { rlSmon 1 }

rlPortCopySupport OBJECT-TYPE
   SYNTAX INTEGER {
       supported(1),
       notSupported(2)
   }
   MAX-ACCESS read-only
   STATUS current
   DESCRIPTION
       "supported - The standard portCopy is supported.
        notSupported - the standard portCopy is not supported.
                       only basic portCopy operation is supported. "
   ::= { rlSmon 2 }

rlPortCopyVlanTaggingTable OBJECT-TYPE
    SYNTAX  SEQUENCE OF RlPortCopyVlanTaggingEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "A supplementing table for portCopyTable.
         For every portCopyDest a vlan-tagging option is available."
    ::= { rlSmon 3 }

rlPortCopyVlanTaggingEntry OBJECT-TYPE
    SYNTAX  RlPortCopyVlanTaggingEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "Each entry specify how  mirrored packets will transmit from
         the portCopyDest:   Tagged or unTagged.
         The values in this entry will be valid only when the
         dot1dBasePort will be configured as a portCopyDest
         in the portCopyTable."
    INDEX   { dot1dBasePort }
    ::= { rlPortCopyVlanTaggingTable 1 }

RlPortCopyVlanTaggingEntry ::=
    SEQUENCE {
        rlPortCopyVlanTagging     TruthValue
    }

rlPortCopyVlanTagging  OBJECT-TYPE
    SYNTAX  TruthValue
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
        "TRUE  - Mirrored packets will transmit from portCopyDest - Tagged
         FALSE - Mirrored packets will transmit from portCopyDest - unTagged"
    DEFVAL { false }
    ::= { rlPortCopyVlanTaggingEntry 1 }

rlPortCopyMode          OBJECT-TYPE
      SYNTAX                CopyModeType
      MAX-ACCESS            read-write
      STATUS                current
      DESCRIPTION "This scalar defined a mode of the copy
                   destination port"
      ::= { rlSmon 4}


rlPortCopyRemoteTable OBJECT-TYPE
    SYNTAX  SEQUENCE OF RlPortCopyRemoteEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "A supplementing table for portCopyRemoteTable."
    ::= { rlSmon 5 }

rlPortCopyRemoteEntry OBJECT-TYPE
    SYNTAX  RlPortCopyRemoteEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "Each entry specifies vlan tag and user priority added
        by analyzer to a mirrored Rx/Tx packets."
    INDEX   { rlPortCopyRemoteDirection }
    ::= { rlPortCopyRemoteTable 1 }

RlPortCopyRemoteEntry ::=
    SEQUENCE {
        rlPortCopyRemoteDirection   CopyRemoteDirectionType,
		rlPortCopyRemoteVlan        VlanId,
        rlPortCopyRemotePrio        INTEGER,
		rlPortCopyRemoteStatus      RowStatus
    }

rlPortCopyRemoteDirection  OBJECT-TYPE
      SYNTAX                CopyRemoteDirectionType
      MAX-ACCESS            not-accessible
      STATUS                current
      DESCRIPTION "This field defines a direction"
      ::= { rlPortCopyRemoteEntry 1}

rlPortCopyRemoteVlan  OBJECT-TYPE
      SYNTAX                VlanId
      MAX-ACCESS            read-write
      STATUS                current
      DESCRIPTION "This field defines an RSPAN VLAN id"
      ::= { rlPortCopyRemoteEntry 2}

rlPortCopyRemotePrio  OBJECT-TYPE
      SYNTAX                INTEGER ( 0 .. 7 )
      MAX-ACCESS            read-write
      STATUS                current
      DESCRIPTION "This field defines an RSPAN user priority"
      DEFVAL { 0 }
      ::= { rlPortCopyRemoteEntry 3}

rlPortCopyRemoteStatus  OBJECT-TYPE
      SYNTAX                RowStatus
      MAX-ACCESS            read-create
      STATUS                current
      DESCRIPTION "This field defines a row status"
      ::= { rlPortCopyRemoteEntry 4}

END
