--
-- Eltex Enterprise Specific MIB: WOP, WEP MIB
--
-- Copyright (c) 2014, Eltex Enterprise, Ltd.
-- All rights reserved.
--
-- The contents of this document are subject to change without notice.
-- MIB checked by http://wwwsnmp.cs.utwente.nl/ietf/mibs/validate/
--

-- .60 - WOP12AC
-- .61 - WEP12AC

ELTEX-WP-MIB DEFINITIONS ::= BEGIN

IMPORTS
	MODULE-IDENTITY, OBJECT-TYPE, NOTIFICATION-TYPE,
	Integer32
		FROM SNMPv2-SMI
	DisplayString, MacAddress, TruthValue, TEXTUAL-CONVENTION, RowStatus
        	FROM SNMPv2-TC
	OBJECT-GROUP, NOTIFICATION-GROUP
    		FROM SNMPv2-CONF
	elHardware
		FROM ELTEX-SMI-ACTUAL
	sysObjectID, sysName
		FROM SNMPv2-MIB
	InetAddressType, InetAddress
		FROM INET-ADDRESS-MIB
	apSysConfVersion, apSysConfBaseMac, apSysConfSerialNumber, apManagementLocation
		FROM FASTPATH-WLAN-ACCESS-POINT-MIB;

wop MODULE-IDENTITY
	LAST-UPDATED	"201409240000Z"
	ORGANIZATION	"Eltex Co"
	CONTACT-INFO 	"Homepage: www.eltex.nsk.ru
					 E-mail: eltex@gcom.ru"
	DESCRIPTION		"Eltex WOP, WEP mib
						Used like sysOid for WOP devices"
	REVISION    	"201409240000Z"
	DESCRIPTION 	"first version of parametrs"
		::= { elHardware 60 }

-- *********************************************************************
-- TEXTUAL-CONVENTION
-- *********************************************************************
		
DevTempType ::= TEXTUAL-CONVENTION
  STATUS current
  DESCRIPTION "Error code or temperature in Celsius degrees"
  SYNTAX INTEGER  {
      undefined(250), -- unknown error while getting data
  	  sensorNotAvailable(251),
  	  sensorError(252) }
  	  
DevPowerType ::= TEXTUAL-CONVENTION
  STATUS current
  DESCRIPTION "Power type"
  SYNTAX INTEGER {
	ac(1),
	dc(2)
  }

-- *********************************************************************
-- Additional systemOid's
-- *********************************************************************

-- Used like sysOid for WEP devices
wop12ac         OBJECT IDENTIFIER ::= { elHardware 60 }
wep12ac         OBJECT IDENTIFIER ::= { elHardware 61 }
wop12acLr      	OBJECT IDENTIFIER ::= { elHardware 62 }
wop12acEr      	OBJECT IDENTIFIER ::= { elHardware 63 }
wop12acRevB    	OBJECT IDENTIFIER ::= { elHardware 64 }
wop2ac          OBJECT IDENTIFIER ::= { elHardware 65 }
wep2ac          OBJECT IDENTIFIER ::= { elHardware 66 }
wep12acRevB    	OBJECT IDENTIFIER ::= { elHardware 69 }
wop12acLrRevB 	OBJECT IDENTIFIER ::= { elHardware 84 }
wop12acLrRevC 	OBJECT IDENTIFIER ::= { elHardware 85 }
wop12acLrRevD 	OBJECT IDENTIFIER ::= { elHardware 87 }
wep2acSmart    	OBJECT IDENTIFIER ::= { elHardware 93 }
wop12acRevC    	OBJECT IDENTIFIER ::= { elHardware 94 }
wep12acRevC    	OBJECT IDENTIFIER ::= { elHardware 95 }

-- *********************************************************************
-- Trap messages
-- *********************************************************************

wpNotifications OBJECT IDENTIFIER ::= { wop 1 }

-- *********************************************************************
-- 1. Associated clients traps
-- FASTPATH-WLAN-ACCESS-POINT-MIB::apAssocTable
-- *********************************************************************

associatedClientsTrap OBJECT IDENTIFIER
                ::= { wpNotifications 1 }

associatedClientsTrapPrefix OBJECT IDENTIFIER
                ::= { associatedClientsTrap 1 }

associatedClientsTrapObjects OBJECT IDENTIFIER
                ::= { associatedClientsTrap 2 }


associatedClientsTrapMessage OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "This string contains the contents of trap message."
		::= { associatedClientsTrapObjects 1 }
		
associatedClientsTrapStationMac OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Client's MAC address."
		::= { associatedClientsTrapObjects 2 }
		
associatedClientsTrapInterface OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Wireless interface name."
		::= { associatedClientsTrapObjects 3 }
		
associatedClientsTrapUsername OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Client's user name."
		::= { associatedClientsTrapObjects 4 }
		
associatedClientsTrapSsid OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "SSID."
		::= { associatedClientsTrapObjects 5 }
		
associatedClientsTrapDomain OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Domain."
		::= { associatedClientsTrapObjects 6 }
		
associatedClientsTrapLastRssi OBJECT-TYPE
	SYNTAX          Integer32
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Last RSSI."
		::= { associatedClientsTrapObjects 7 }
		
associatedClientsTrapDisconnectCause OBJECT-TYPE
	SYNTAX          INTEGER
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Disconnect cause."
		::= { associatedClientsTrapObjects 8 }
		
associatedClientsTrapDisconnectInitiator OBJECT-TYPE
	SYNTAX          INTEGER {
						ap(0),
						client(1)
						}
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Disconnect initiator."
		::= { associatedClientsTrapObjects 9 }
		
associatedClientsTrapFftStatus OBJECT-TYPE
	SYNTAX          INTEGER
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "FFT trap status."
		::= { associatedClientsTrapObjects 10 }
		
associatedClientsTrapRoamingType OBJECT-TYPE
	SYNTAX          INTEGER {
						none(0),
						overDs(1),
						overAir(2) }
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Roaming type. 
					From WEP 1.6.3"
		::= { associatedClientsTrapObjects 11 }


clientAssociatedNotification  NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid
	}
    STATUS  current
    DESCRIPTION "Client associated"
	::= { associatedClientsTrapPrefix 1 }
	
clientAuthenticatedNotification  NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapUsername, -- for 'wpa enter' only
        associatedClientsTrapSsid,
        associatedClientsTrapDomain,
        associatedClientsTrapLastRssi,
        associatedClientsTrapRoamingType
	}
    STATUS  current
    DESCRIPTION "Client authenticated successfully"
	::= { associatedClientsTrapPrefix 2 }
	
clientAuthenticationFailedNotification  NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapUsername, -- for 'wpa enter' only
        associatedClientsTrapSsid,
        associatedClientsTrapDomain
	}
    STATUS  current
    DESCRIPTION "Client authentication failed"
	::= { associatedClientsTrapPrefix 3 }
	
clientDisassociatedNotification  NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapUsername, -- for 'wpa enter' only
        associatedClientsTrapSsid,
        associatedClientsTrapDomain,
        associatedClientsTrapLastRssi,
        associatedClientsTrapDisconnectCause,
        associatedClientsTrapDisconnectInitiator
	}
    STATUS  current
    DESCRIPTION "Client disassociated"
	::= { associatedClientsTrapPrefix 4 }
	
-- SNMP_TRAP_CLIENT_AUTH_FFT_AIR
clientAuthFftAirNotification NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid,
        associatedClientsTrapFftStatus,
        associatedClientsTrapLastRssi
        }
    STATUS current
    DESCRIPTION "Client try to roaming over air"
	::= { associatedClientsTrapPrefix 5 }
	
-- SNMP_TRAP_CLIENT_AUTH_FFT_DS
clientAuthFftDsNotification NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid,
        associatedClientsTrapFftStatus
        }
    STATUS current
    DESCRIPTION "Client try to roaming over ds"
	::= { associatedClientsTrapPrefix 6 }
	
-- SNMP_TRAP_CLIENT_REQUEST_RRB
clientRrbRequestedNotification NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid
        }
    STATUS current
    DESCRIPTION "RRB requested by client Action"
	::= { associatedClientsTrapPrefix 7 }
	
-- SNMP_TRAP_CLIENT_REQUEST_RRB_FAIL
clientRrbRequestFailedNotification NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid
        }
    STATUS current
    DESCRIPTION "RRB request failed"
	::= { associatedClientsTrapPrefix 8 }
	
-- SNMP_TRAP_CLIENT_RESPONSE_RRB
clientRrbResponceReceivedNotification NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid
        }
    STATUS current
    DESCRIPTION "RRB responce received"
	::= { associatedClientsTrapPrefix 9 }

clientDisassociatedLogoff NOTIFICATION-TYPE
	OBJECTS {
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapSsid
        }
    STATUS current
    DESCRIPTION "logoff"
	::= { associatedClientsTrapPrefix 10 }
	
-- *********************************************************************
-- 2. Not initialized AP
-- *********************************************************************

notInitializedApTrap OBJECT IDENTIFIER
                ::= { wpNotifications 2 }

notInitializedApTrapPrefix OBJECT IDENTIFIER
                ::= { notInitializedApTrap 1 }

notInitializedApTrapObjects OBJECT IDENTIFIER
                ::= { notInitializedApTrap 2 }


notInitializedApTrapMessage OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "This string contains the contents of trap message."
		::= { notInitializedApTrapObjects 1 }
		
notInitializedApTrapBoard OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "This string contains 'Board' from factory."
		::= { notInitializedApTrapObjects 2 }
		
notInitializedApNotification  NOTIFICATION-TYPE
	OBJECTS {
		notInitializedApTrapMessage,
		sysObjectID,
		notInitializedApTrapBoard,
		sysName,
		apSysConfVersion,
		apSysConfBaseMac,
		apSysConfSerialNumber,
		apManagementLocation
	}
    STATUS  current
    DESCRIPTION "Its not initialized AP"
	::= { notInitializedApTrapPrefix 1 }
	
	
-- *********************************************************************
-- 3. Configuration upload/download process-/result-trap
-- *********************************************************************		

configurationTransferTrap OBJECT IDENTIFIER
                ::= { wpNotifications 3 }

configurationTransferTrapPrefix OBJECT IDENTIFIER
                ::= { configurationTransferTrap 1 }

configurationTransferTrapObjects OBJECT IDENTIFIER
                ::= { configurationTransferTrap 2 }


configurationTransferTrapMessage OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "This string contains the contents of trap message."
		::= { configurationTransferTrapObjects 1 }
		
configurationTransferTrapFile OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Config file name."
		::= { configurationTransferTrapObjects 2 }
		
configurationTransferTrapServer OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "TFTP server IP."
		::= { configurationTransferTrapObjects 3 }
		
configurationTransferTrapError OBJECT-TYPE
	SYNTAX          INTEGER {
		noError(0),
		tftpTransferFailed(1),
		movingFailed(2),
		validationFailed(3),
		applyFailed(4),
		saveFailed(5)
	}
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Error index."
		::= { configurationTransferTrapObjects 4 }
		
configurationUploadSuccessNotification  NOTIFICATION-TYPE
	OBJECTS {
		configurationTransferTrapMessage,
		configurationTransferTrapFile,
		configurationTransferTrapServer
	}
    STATUS  current
    DESCRIPTION " "
	::= { configurationTransferTrapPrefix 1 }
	
configurationUploadFailedNotification  NOTIFICATION-TYPE
	OBJECTS {
		configurationTransferTrapMessage,
		configurationTransferTrapFile,
		configurationTransferTrapServer,
		configurationTransferTrapError
	}
    STATUS  current
    DESCRIPTION " "
	::= { configurationTransferTrapPrefix 2 }
	
configurationDownloadSuccessNotification  NOTIFICATION-TYPE
	OBJECTS {
		configurationTransferTrapMessage,
		configurationTransferTrapFile,
		configurationTransferTrapServer
	}
    STATUS  current
    DESCRIPTION " "
	::= { configurationTransferTrapPrefix 3 }
	
configurationDownloadFailedNotification  NOTIFICATION-TYPE
	OBJECTS {
		configurationTransferTrapMessage,
		configurationTransferTrapFile,
		configurationTransferTrapServer,
		configurationTransferTrapError
	}
    STATUS  current
    DESCRIPTION " "
	::= { configurationTransferTrapPrefix 4 }
	
-- *********************************************************************
-- 4. Firmware update process-/result-trap
-- *********************************************************************		

firmwareUpgradeTrap OBJECT IDENTIFIER
                ::= { wpNotifications 4 }

firmwareUpgradeTrapPrefix OBJECT IDENTIFIER
                ::= { firmwareUpgradeTrap 1 }

firmwareUpgradeTrapObjects OBJECT IDENTIFIER
                ::= { firmwareUpgradeTrap 2 }

firmwareUpgradeTrapMessage OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "This string contains the contents of trap message."
		::= { firmwareUpgradeTrapObjects 1 }
		
firmwareUpgradeTrapFile OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "FW file name."
		::= { firmwareUpgradeTrapObjects 2 }
		
firmwareUpgradeTrapServer OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "TFTP server IP."
		::= { firmwareUpgradeTrapObjects 3 }
		
firmwareUpgradeTrapError OBJECT-TYPE
	SYNTAX          INTEGER {
		noError(0),
		tftpTransferFailed(1),
		validationFailed(2),
		upgradeFailed(3),
		invalidTransferType(4)
	}
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Error index."
		::= { firmwareUpgradeTrapObjects 4 }
		
firmwareUpgradeTrapUrl OBJECT-TYPE
	SYNTAX          DisplayString
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "FW file URL."
		::= { firmwareUpgradeTrapObjects 5 }
		
firmwareUpgradeTrapProtocol OBJECT-TYPE
	SYNTAX          INTEGER {
		tftp(1),
		http(2)
	}
	MAX-ACCESS      accessible-for-notify
	STATUS          current
	DESCRIPTION     "Protocol."
		::= { firmwareUpgradeTrapObjects 6 }
		
firmwareUpgradeSuccessNotification  NOTIFICATION-TYPE
	OBJECTS {
		firmwareUpgradeTrapMessage,
		firmwareUpgradeTrapFile,
		firmwareUpgradeTrapServer,
		firmwareUpgradeTrapUrl,
		firmwareUpgradeTrapProtocol
	}
    STATUS  current
    DESCRIPTION " "
	::= { firmwareUpgradeTrapPrefix 1 }
	
firmwareUpgradeFailedNotification  NOTIFICATION-TYPE
	OBJECTS {
		firmwareUpgradeTrapMessage,
		firmwareUpgradeTrapFile,
		firmwareUpgradeTrapServer,
		firmwareUpgradeTrapError,
		firmwareUpgradeTrapUrl,
		firmwareUpgradeTrapProtocol
	}
    STATUS  current
    DESCRIPTION " "
	::= { firmwareUpgradeTrapPrefix 2 }

-- *********************************************************************
-- Eltex config
-- *********************************************************************

wpInitializationSettings OBJECT IDENTIFIER ::= { wop 2 }

	wpSendNotInitializatedNotification OBJECT-TYPE
			SYNTAX     TruthValue
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "set False to turn off 'notInitializedApNotification' sending"
			::= { wpInitializationSettings 1 }
			
wpMonitoring OBJECT IDENTIFIER ::= { wop 3 }

	wpDeviceTemperature OBJECT-TYPE
			SYNTAX     DevTempType
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Error code or temperature in Celsius degrees"
			::= { wpMonitoring 1 }
			
	wpDevicePower OBJECT-TYPE
			SYNTAX     DevPowerType
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Power: AC, DC"
			::= { wpMonitoring 2 }

-- *********************************************************************
-- Clustering config
-- *********************************************************************			
			
wpClustering OBJECT IDENTIFIER ::= { wop 4 }

	wpClusteringMain OBJECT IDENTIFIER ::= { wpClustering 1 }
	
		clustered OBJECT-TYPE
			SYNTAX INTEGER {
				on(1),
				off(2),
				softwlc(3)
			}
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "Start/Stop clustering
					1.5 - 1-on, 2-off
					1.6.1 - 1-on, 2-off, 3-softwlc"
				::= { wpClusteringMain 1 }
			
		accessPointLocation OBJECT-TYPE
			SYNTAX     DisplayString (SIZE (0..64))
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "Enter a description of where the access point is physically located"
				::= { wpClusteringMain 2 }
		
		clusterName OBJECT-TYPE
			SYNTAX     DisplayString (SIZE (0..64))
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "Enter the name of the cluster for the AP to join.
					The cluster name is not sent to other APs in the cluster. 
					You must configure the same cluster name on each AP that is a member of the cluster. 
					The cluster name must be unique for each cluster you configure on the network."
				::= { wpClusteringMain 3 }
		
		clusterIpVersion OBJECT-TYPE
			SYNTAX     InetAddressType
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "Specify the IP version that the APs in the cluster use to communicate with each other."
				::= { wpClusteringMain 4 }
		
		clusterPriority OBJECT-TYPE
			SYNTAX     Integer32(0..255)
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "This is supported only for ipv4. Specifies the Priority of the cluster member. 
				Configurable by user.The higher number indicates the higher preference for this AP 
				to become the dominant AP.In case of tie, lowest MAC address becomes dominant.
				Range: 0 to 255. Default value is 0."
				::= { wpClusteringMain 5 } 
		
		clusterManagementAddress OBJECT-TYPE
			SYNTAX     InetAddress
			MAX-ACCESS read-write
			STATUS     current
			DESCRIPTION "This is supported only for ipv4. In order to access the cluster with a single IP, 
				The Cluster can be configured with an option of Cluster IP address. This is part of 
				the global configuration of the cluster in section It has to be statically configured 
				by the Cluster Administrator. The Cluster IP managemenetmanagement address should be 
				part of the same subnet as the clustered AP management IP addresses. 
				The Cluster IP address is configured as secondary IP address to the management 
				interface of the Dominant AP. The Dominant AP user interface is accessible using the Cluster IP address. 
				When the Cluster IP address is set as secondary IP address on the Dominant AP, 
				it sends Gratuitous ARPs on the management VLAN so that the mapping between the new 
				IP address and the Mac-address is established in the subnet. 
				The Cluster IP address configuration is shared among all the clustered APs."
				::= { wpClusteringMain 6 } 
	
	wpClusteringState OBJECT IDENTIFIER ::= { wpClustering 2 }

		clusteringAllowed OBJECT-TYPE
			SYNTAX     TruthValue
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Whether clustering is allowed for this node"
				::= { wpClusteringState 1 }	
		
		compat OBJECT-TYPE
			SYNTAX     DisplayString
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION " "
				::= { wpClusteringState 2 }	
		
		operationalMode OBJECT-TYPE
			SYNTAX     Integer32
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION " "
				::= { wpClusteringState 3 }	
		
		memberCount OBJECT-TYPE
			SYNTAX     Integer32
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Number of cluster members"
				::= { wpClusteringState 4 }	
	
-- *********************************************************************
-- SNMP agent config
-- *********************************************************************			
			
wpSnmp OBJECT IDENTIFIER ::= { wop 5 }

	-- cli snmp status
	snmpAdminStatus OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION "Administrative status"
		::= { wpSnmp 1 }

	-- operational-status
	snmpOperationalStatus OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION "Operational status"
		::= { wpSnmp 2 }

	-- ro-community 
	snmpRoCommunity OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Read-only community name (for permitted SNMP get operations)"
		::= { wpSnmp 3 }

	-- rw-status
	snmpRwStatus OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION "Allow SNMP set requests"
		::= { wpSnmp 5 }

	-- port 
	snmpPort OBJECT-TYPE
		SYNTAX Integer32 (1..65535)
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Port number the SNMP agent will listen to"
		DEFVAL { 161 }
		::= { wpSnmp 6 }	

	-- source-status
	snmpRestrictSource OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Restrict the source of SNMP requests to only the designated hosts or subnets"
		::= { wpSnmp 7 }

	-- source 
	snmpSource OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Hostname, address, or subnet of Network Management System"
		::= { wpSnmp 8 }

	-- logs-output	Log to (-L): e - standard error, o - standard output, n - don't log (by default), f - file, s - syslog
	snmpLogsOutput OBJECT-TYPE
		SYNTAX INTEGER {
			none(0),
			standartError(1),
			standartOutput(2),
			file(3),
			syslog(4)
		}
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Logs destination. 'none' by default - don't log"
		DEFVAL { 0 }
		::= { wpSnmp 9 }

	-- logs-priority
	snmpLogsPriority OBJECT-TYPE
		SYNTAX INTEGER {
			emerg(0),
			alert(1),
			crit(2),
			err(3),
			warning(4),
			notice(5),
			info(6),
			debug(7)
		}
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Log priority: pri - level 'pri' and above (for snmpLogsOutput values=1,2,3)"
		DEFVAL { 0 }
		::= { wpSnmp 10 }

	-- logs-prio-range
	snmpLogsPrioRange OBJECT-TYPE
		SYNTAX DisplayString(SIZE(3))
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Log priority range: p1-p2 - log levels 'p1' to 'p2' (syslog only).
				Values of snmpLogsPriority"
		DEFVAL { "0-0" }
		::= { wpSnmp 11 }

	-- logs-file 
	snmpLogsFile OBJECT-TYPE
		SYNTAX DisplayString(SIZE(0..256))
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Logs to specified files: '/var/log/snmpd.log' by default"
		::= { wpSnmp 12 }

	-- dump	
	snmpDump OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Dump sent and received SNMP packets"
		::= { wpSnmp 13 }

	-- debug-token 
	snmpDebugToken OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Debugging output tokens (-D): empty string for 'no debug', 'ALL', or 'traps,send' - any tokens without spaces"
		::= { wpSnmp 14 }

	-- transport 
	snmpTransport OBJECT-TYPE
		SYNTAX   BITS {
			udp(0),
			tcp(1),
			udp6(2),
			tcp6(3)
		}
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "SNMP Transport"
		::= { wpSnmp 15 }

-- *********************************************************************
-- Channel planner settings
-- *********************************************************************

wpChannelPlanner OBJECT IDENTIFIER ::= { wop 6 }

	-- cli channel-planner status
	chPlannerStatus OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Whether channel planning is enabled"
		::= { wpChannelPlanner 1 }

	-- refresh
	chPlannerRefresh OBJECT-TYPE
		SYNTAX TruthValue
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION "Refresh channel plan when add new AP in cluster"
		::= { wpChannelPlanner 2 }

	-- change-threshold
	-- 5..75 %
	chPlannerChangeThreshold OBJECT-TYPE
		SYNTAX     Integer32
		MAX-ACCESS read-only
		STATUS     current
		DESCRIPTION "Percentage change in error required to propagate new channel assignments"
		::= { wpChannelPlanner 3 }	

	-- interval (minute)
	-- 1,5,30,60,240,480,720,1440,2880,7200,10080,20160,40320,80640,120960,241920
	chPlannerInterval OBJECT-TYPE
		SYNTAX     Integer32
		MAX-ACCESS read-only
		STATUS     current
		DESCRIPTION "Minutes between each attempt to optimize channel assignments"
		::= { wpChannelPlanner 4 }

-- *********************************************************************
-- SNMP Traps
-- *********************************************************************

wpTraphost OBJECT IDENTIFIER ::= { wop 7 }

	wpTraphostTable OBJECT-TYPE
	    SYNTAX      SEQUENCE OF WpTraphostEntry
	    MAX-ACCESS  not-accessible
	    STATUS      current
	    DESCRIPTION "A list of traphost configuration entries."
	    ::= { wpTraphost 1 }

	wpTraphostEntry OBJECT-TYPE
	    SYNTAX      WpTraphostEntry
	    MAX-ACCESS  not-accessible
	    STATUS      current
	    DESCRIPTION
		    "An entry containing configuration information applicable 
		    to a particular traphost."
	    INDEX   { wpTraphostId }
	    ::= { wpTraphostTable 1 }

	WpTraphostEntry ::= SEQUENCE {
		wpTraphostHost 			DisplayString,
		wpTraphostCommunity 	DisplayString,
		wpTraphostTrapVersion 	INTEGER, -- snmpV2(0), informRequest(1)
		wpTraphostRowStatus 	RowStatus
	}

	wpTraphostHost OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION ""
		::= { wpTraphostEntry 1 }

	wpTraphostCommunity OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION ""
		::= { wpTraphostEntry 2 }

	wpTraphostTrapVersion OBJECT-TYPE
		SYNTAX INTEGER {
			snmpV2(0),
			informRequest(1)
		}
		MAX-ACCESS read-write
		STATUS current
		DESCRIPTION ""
		::= { wpTraphostEntry 3 }

	wpTraphostRowStatus OBJECT-TYPE
		SYNTAX RowStatus
		MAX-ACCESS read-create
		STATUS current
		DESCRIPTION ""
		::= { wpTraphostEntry 4 }

-- *********************************************************************
-- SFP Ports
-- *********************************************************************

wpSfp OBJECT IDENTIFIER ::= { wop 8 }

wpSfpInfoTable OBJECT-TYPE
	SYNTAX		SEQUENCE OF wpSfpInfoEntry
	MAX-ACCESS	not-accessible
	STATUS		current
	DESCRIPTION	"WEP 1.8"
		::= { wpSfp 1 }

	wpSfpInfoEntry OBJECT-TYPE
		SYNTAX		wpSfpInfoEntry
		MAX-ACCESS	not-accessible
		STATUS		current
		DESCRIPTION ""
		INDEX	{ ifIndex  }
		::= { wpSfpInfoTable 1 }

		wpSfpInfoEntry ::= SEQUENCE {
			wpSfpInfoI2cData		INTEGER,
			wpSfpInfoLink			INTEGER,
			wpSfpInfoStatus			INTEGER,
			wpSfpInfoTemperature	Integer32,
			wpSfpInfoVoltage		DisplayString,
			wpSfpInfoCurrent		DisplayString,
			wpSfpInfoRXPower		DisplayString,
			wpSfpInfoTXPower		DisplayString,
			wpSfpInfoSpeed			Integer32
		}
		
		wpSfpInfoI2cData OBJECT-TYPE
			SYNTAX     INTEGER {
				yes(1),
				no(2)
			}
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Diag i2c data."
			::= { wpSfpInfoEntry 1 }

		wpSfpInfoLink OBJECT-TYPE
			SYNTAX     INTEGER {
				up(1),
				down(2)
			}
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Status of SFP interface."
			::= { wpSfpInfoEntry 2 }

		wpSfpInfoStatus OBJECT-TYPE
			SYNTAX     INTEGER {
				on(1),
				off(2)
			}
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Presence of SFP module."
			::= { wpSfpInfoEntry 3 }

		wpSfpInfoTemperature OBJECT-TYPE
			SYNTAX     Integer32
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Temperature of SFP module, in celsius degrees."
			::= { wpSfpInfoEntry 4 }

		wpSfpInfoVoltage OBJECT-TYPE
			SYNTAX     DisplayString
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Voltage of SFP module, in V."
			::= { wpSfpInfoEntry 5 }

		wpSfpInfoCurrent OBJECT-TYPE
			SYNTAX     DisplayString
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Magnitude of the laser bias power setting current of SFP module, in mA."
			::= { wpSfpInfoEntry 6 }

		wpSfpInfoRXPower OBJECT-TYPE
			SYNTAX     DisplayString
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Receive power of SFP module, in dBm."
			::= { wpSfpInfoEntry 7 }

		wpSfpInfoTXPower OBJECT-TYPE
			SYNTAX     DisplayString
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Transmit power of SFP module, in dBm."
			::= { wpSfpInfoEntry 8 }

		wpSfpInfoSpeed OBJECT-TYPE
			SYNTAX     Integer32
			MAX-ACCESS read-only
			STATUS     current
			DESCRIPTION "Current speed of SFP interface, in Mbps."
			::= { wpSfpInfoEntry 9 }

-- *********************************************************************
-- Groups
-- *********************************************************************

wopGroup OBJECT IDENTIFIER ::= { wop 100 } 

wopTrapObjectsGroup OBJECT-GROUP 
	OBJECTS { 
		associatedClientsTrapMessage,
		associatedClientsTrapStationMac,
        associatedClientsTrapInterface,
        associatedClientsTrapUsername,
        associatedClientsTrapSsid,
        associatedClientsTrapDomain,
        associatedClientsTrapLastRssi,
		associatedClientsTrapDisconnectCause,
		associatedClientsTrapDisconnectInitiator,
		associatedClientsTrapFftStatus,
		
		notInitializedApTrapMessage,
		notInitializedApTrapBoard,
		
		configurationTransferTrapMessage,
		configurationTransferTrapFile,
		configurationTransferTrapServer,
		configurationTransferTrapError,
		
		firmwareUpgradeTrapMessage,
		firmwareUpgradeTrapFile,
		firmwareUpgradeTrapServer,
		firmwareUpgradeTrapError,
		firmwareUpgradeTrapUrl,
		firmwareUpgradeTrapProtocol
	} 
    STATUS current
    DESCRIPTION "The group of objects for notifications." 
    ::= { wopGroup 1 }	
    
wopNotificationsGroup NOTIFICATION-GROUP 
	NOTIFICATIONS { 
		clientAssociatedNotification,
		clientAuthenticatedNotification,
		clientAuthenticationFailedNotification,
		clientDisassociatedNotification,
		clientAuthFftAirNotification,
		clientAuthFftDsNotification,
		clientRrbRequestedNotification,
		clientRrbRequestFailedNotification,
		clientRrbResponceReceivedNotification,
		clientDisassociatedLogoff,
		
		notInitializedApNotification,
		
		configurationUploadSuccessNotification,
		configurationUploadFailedNotification,
		configurationDownloadSuccessNotification,
		configurationDownloadFailedNotification,
		
		firmwareUpgradeSuccessNotification,
		firmwareUpgradeFailedNotification
	} 
    STATUS current
    DESCRIPTION "The group of notifications." 
    ::= { wopGroup 2 }	
    
wopSettingsGroup OBJECT-GROUP 
	OBJECTS { 
		wpSendNotInitializatedNotification
	} 
    STATUS current
    DESCRIPTION "The group of objects." 
    ::= { wopGroup 3 }	
    
wopMonitoringGroup OBJECT-GROUP 
	OBJECTS { 
		wpDeviceTemperature,
		wpDevicePower
	} 
    STATUS current
    DESCRIPTION "The group of objects." 
    ::= { wopGroup 4 }	
    
wopClusteringGroup OBJECT-GROUP 
	OBJECTS { 
		clustered,
		accessPointLocation,
		clusterName,
		clusterIpVersion,
		clusterPriority,
		clusterManagementAddress,
		
		clusteringAllowed,
		compat,
		operationalMode,
		memberCount
	} 
    STATUS current
    DESCRIPTION "The group of objects." 
    ::= { wopGroup 5 }

wopSnmpGroup OBJECT-GROUP 
	OBJECTS { 
		snmpAdminStatus,
		snmpOperationalStatus,
		snmpRoCommunity,
		snmpRwStatus,
		snmpPort,
		snmpRestrictSource,
		snmpSource,
		snmpLogsOutput,
		snmpLogsPriority,
		snmpLogsPrioRange,
		snmpLogsFile,
		snmpDump,
		snmpDebugToken,
		snmpTransport,

		wpTraphostHost,
		wpTraphostCommunity,
		wpTraphostTrapVersion,
		wpTraphostRowStatus
	} 
    STATUS current
    DESCRIPTION "The group of objects." 
    ::= { wopGroup 6 }

wopChannelPlannerGroup OBJECT-GROUP 
	OBJECTS { 
		chPlannerStatus,
		chPlannerRefresh,
		chPlannerChangeThreshold,
		chPlannerInterval
	} 
    STATUS current
    DESCRIPTION "The group of objects." 
    ::= { wopGroup 7 }

END
