#!/usr/bin/perl
$metafile = shift(@ARGV);
$flexfile = shift(@ARGV);
$mibfile = shift(@ARGV);

if ($metafile eq '' || $flexfile eq '' || $mibfile eq '')
{
   print "Usage: perl processmib.pl <metafile> <flex.h> <output>\n";
}

unless (open(META, $metafile))
{
   print "Could not open metafile '$metafile' for reading.";
   exit;
}

unless (open(FLEX, $flexfile))
{
   print "Could not open flex.h at '$flexfile' for reading.";
   exit;
}

unless (open(MIB, ">$mibfile"))
{
   print "Could not open output file '$mibfile' for writing.";
   exit;
}

my %tags = ();

foreach my $line (<FLEX>)
{
   chomp($line);
   if ($line =~ /export\s+?(.*?)\s*=\s*1/)
   {
      $tags{lc($1)} = 1;
#      print "Add tag: ".lc($1)."\n";
   }
}
close(FLEX);

my $conditional=0; # if 1, then in #if block
my $conditional_val=0; #if 1, and $conditional=1,then allow, swap on #else

foreach my $line (<META>)
{
  if ($line =~ /\s*--\s*#IFDEF\s+?([a-zA-Z0-9,\-_!]*?)\s*$/i)
  {
     $conditional = 1;
     $conditional_val = &checkconditional($1);
#     print "Check: '$1' ->".$conditional_val."\n";
     next;
  }
  elsif ($line =~ /\s*--\s*#ELSE\s*$/i)
  {
     $conditional_val = $conditional_val ? 0 : 1;
     next;
  }
  elsif ($line =~ /\s*--\s*#ENDIF\s*$/i)
  {
     $conditional = 0;
     next;
  }
  elsif ($conditional && !$conditional_val)
  {
     next;
  }
  print MIB $line;
}


sub checkconditional($keywords)
{
   my ($keywords) = @_;

   my @keywords = split(/\s*,\s*/,$keywords);

   foreach my $keyword (@keywords)
   {
      my $val = 1;
      if ($keyword =~ /^\!(.*)$/) {
         $val = 0;
         $keyword = $1;
      }
      
      # take care of any possible value returned by defined
      if (defined($tags{lc($keyword)}))
      {
         return 0 if (!$val);
      }
      else
      {
         return 0 if ($val);
      }
   }
   return 1;
}
