ELTEX-MES-BRIDGE-EXT-MIB DEFINITIONS ::= BEGIN

-- Title:      ELTEX MES Bridge Extension Private
-- Version:    1.0
-- Date:       17 Oct 2012
-- 21-Sep-2012 - Added eltBridgeExtMacLearningVlanTable

IMPORTS
    eltMesBridgeExtMIB                                      FROM ELTEX-MES-MNG-MIB
    VlanIndex                                               FROM Q-BRIDGE-MIB
    MODULE-IDENTITY, OBJECT-TYPE                            FROM SNMPv2-SMI
    TruthValue                                              FROM SNMPv2-TC
    rldot1sMstpInstanceEntry                                FROM RADLAN-BRIDGEMIBOBJECTS-MIB;


eltMesBridgeExtMIBObjects  OBJECT IDENTIFIER
    ::= { eltMesBridgeExtMIB 0 }


eltMesBridgeExtMacLearning  OBJECT IDENTIFIER
    ::= { eltMesBridgeExtMIBObjects 0 }

eltMesBridgeMstp  OBJECT IDENTIFIER
    ::= { eltMesBridgeExtMIBObjects 3 }

eltMesdot1dTp  OBJECT IDENTIFIER
    ::= { eltMesBridgeExtMIBObjects 4 }

eltMesBridgeStp OBJECT IDENTIFIER
	::= { eltMesBridgeExtMIBObjects 5 }
--
-- eltBridgeExtMacLearningVlanTable
--

eltBridgeExtMacLearningVlanTable OBJECT-TYPE
    SYNTAX          SEQUENCE OF EltBridgeExtMacLearningVlanEntry
    MAX-ACCESS      not-accessible
    STATUS          current
    DESCRIPTION        "Per VLAN MAC learning table."
    ::= { eltMesBridgeExtMacLearning 1 }

eltBridgeExtMacLearningVlanEntry OBJECT-TYPE
    SYNTAX          EltBridgeExtMacLearningVlanEntry
    MAX-ACCESS      not-accessible
    STATUS          current
    DESCRIPTION        "VLAN MAC learning entry."
    INDEX           { eltBridgeExtMacLearningVlanIndex }
    ::= { eltBridgeExtMacLearningVlanTable 1 }

EltBridgeExtMacLearningVlanEntry ::= SEQUENCE {
        eltBridgeExtMacLearningVlanIndex   VlanIndex,
        eltBridgeExtMacLearningVlanEnabled TruthValue
}

eltBridgeExtMacLearningVlanIndex OBJECT-TYPE
        SYNTAX          VlanIndex
        MAX-ACCESS      not-accessible
        STATUS          current
        DESCRIPTION        "Indicates the VLAN number."
    ::= { eltBridgeExtMacLearningVlanEntry 1 }

eltBridgeExtMacLearningVlanEnabled OBJECT-TYPE
    SYNTAX          TruthValue
    MAX-ACCESS      read-create
    STATUS          current
    DESCRIPTION        "Indicates whether the MAC addresses learning is enabled
                                                in this VLAN or not."
    DEFVAL          { true }
    ::= { eltBridgeExtMacLearningVlanEntry 2 }

--
-- eltdot1sMstpPendingGroupTable
--

eltdot1sMstpPendingGroupTable OBJECT-TYPE
    SYNTAX  SEQUENCE OF Eltdot1sMstpPendingGroupEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "A table that contains information about the alocation of vlans to groups."
    ::= { eltMesBridgeMstp 3 }

eltdot1sMstpPendingGroupEntry OBJECT-TYPE
    SYNTAX  Eltdot1sMstpPendingGroupEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "Entry of eltdot1sMstpPendingGroupTable"
    INDEX   { eltdot1sMstpPendingGroup }
    ::= { eltdot1sMstpPendingGroupTable 1 }

Eltdot1sMstpPendingGroupEntry ::= SEQUENCE {
      eltdot1sMstpPendingGroup       INTEGER,
      eltdot1sMstpVlanId1To1024      OCTET STRING,
      eltdot1sMstpVlanId1025To2048   OCTET STRING,
      eltdot1sMstpVlanId2049To3072   OCTET STRING,
      eltdot1sMstpVlanId3073To4094   OCTET STRING
}

eltdot1sMstpPendingGroup OBJECT-TYPE
    SYNTAX  INTEGER
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
        "The pending group number."
    ::= { eltdot1sMstpPendingGroupEntry 1 }

eltdot1sMstpVlanId1To1024 OBJECT-TYPE
    SYNTAX  OCTET STRING (SIZE(0..128))
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
        "first VlanId List of specific group number."
    ::= { eltdot1sMstpPendingGroupEntry 2 }

eltdot1sMstpVlanId1025To2048 OBJECT-TYPE
    SYNTAX  OCTET STRING (SIZE(0..128))
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
        "second VlanId List of specific group number."
    ::= { eltdot1sMstpPendingGroupEntry 3 }

eltdot1sMstpVlanId2049To3072 OBJECT-TYPE
    SYNTAX  OCTET STRING (SIZE(0..128))
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
        "third VlanId List of specific group number."
    ::= { eltdot1sMstpPendingGroupEntry 4 }

eltdot1sMstpVlanId3073To4094 OBJECT-TYPE
    SYNTAX  OCTET STRING (SIZE(0..128))
    MAX-ACCESS  read-write
    STATUS  current
    DESCRIPTION
        "fourth VlanId List of specific group number."
    ::= { eltdot1sMstpPendingGroupEntry 5 }

--
-- eltdot1dTpAgingTimeVlanTable
--

eltdot1dTpVlanTable OBJECT-TYPE
    SYNTAX          SEQUENCE OF Eltdot1dTpVlanEntry
    MAX-ACCESS      not-accessible
    STATUS          current
    DESCRIPTION        "Table that contains configuration of mac aging time for each VLAN."
    ::= { eltMesdot1dTp 1 }

eltdot1dTpVlanEntry OBJECT-TYPE
    SYNTAX          Eltdot1dTpVlanEntry
    MAX-ACCESS      not-accessible
    STATUS          current
    DESCRIPTION        "Each entry contains the aging out configuration
	                    and the configuration source of the aging out value
                        applied for each VLAN."
    INDEX           { eltdot1dTpVlanIndex }
    ::= { eltdot1dTpVlanTable 1 }

Eltdot1dTpVlanEntry ::= SEQUENCE {
        eltdot1dTpVlanIndex            VlanIndex,
        eltdot1dTpVlanAgingTime        INTEGER,
		eltdot1dTpVlanAgingFromGlobal  TruthValue
}

eltdot1dTpVlanIndex OBJECT-TYPE
	SYNTAX          VlanIndex
	MAX-ACCESS      not-accessible
	STATUS          current
	DESCRIPTION        "VLAN number."
    ::= { eltdot1dTpVlanEntry 1 }

eltdot1dTpVlanAgingTime OBJECT-TYPE
    SYNTAX          INTEGER
    MAX-ACCESS      read-write
    STATUS          current
    DESCRIPTION        "The timeout period in seconds for aging out
	                    dynamically learned mac address at this VLAN."
	DEFVAL          { 300 }
    ::= { eltdot1dTpVlanEntry 2 }

eltdot1dTpVlanAgingFromGlobal OBJECT-TYPE
	SYNTAX          TruthValue
	MAX-ACCESS      read-write
	STATUS          current
	DESCRIPTION        "Indicates whether the aging time applied at
                        this VLAN is from dot1dTpAgingTime object or not."
    DEFVAL          { true }
    ::= { eltdot1dTpVlanEntry 3 }

--
--   The Multiple Spanning Tree Instance Table
--

eltdot1sMstpInstanceTable OBJECT-TYPE
    SYNTAX  SEQUENCE OF Eltdot1sMstpInstanceEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
            "A table that contains Mstp instance specific information
            for the Multiple Spanning Tree Protocol."
    ::= { eltMesBridgeMstp 4 }

eltdot1sMstpInstanceEntry OBJECT-TYPE
    SYNTAX  Eltdot1sMstpInstanceEntry
    MAX-ACCESS  not-accessible
    STATUS  current
    DESCRIPTION
        "A list of information maintained by every instance
         about the multiple Spanning Tree Protocol state for
         that instance."
    AUGMENTS { rldot1sMstpInstanceEntry }
    ::= { eltdot1sMstpInstanceTable 1 }

Eltdot1sMstpInstanceEntry ::= SEQUENCE {
	eltdot1sMstpInstanceLastTopologyChangePort   INTEGER
}

eltdot1sMstpInstanceLastTopologyChangePort OBJECT-TYPE
	SYNTAX  INTEGER
    MAX-ACCESS  read-only
	STATUS  current
	DESCRIPTION
    		"This value contain port ifIndex from
            which the request about the last change of topology came.
            If value is 0 - topology wasn't changed yet."
    ::= { eltdot1sMstpInstanceEntry 1}


eltdot1dStpLastTopologyChangePort OBJECT-TYPE
    SYNTAX  INTEGER
    MAX-ACCESS  read-only
    STATUS  current
    DESCRIPTION
            "This value contain port ifIndex from
            which the request about the last change of topology came.
            If value is 0 - topology wasn't changed yet."
    ::= { eltMesBridgeStp 2 }

END
