-- =======================================================================
-- Version info
--
-- Version 1.0 Created 2009.07.8 by LONGYAGN
-- This version of MIB is created just for the Management of RRCP .
--
-- Copyright(c) 2002-2005 by QTECH TECH, Ltd.
-- modify history
-- 01,20100409,hongding,add "QTECHRrcpSoftVersion" mib object
-- =======================================================================

QTECH-RRCP-MIB  DEFINITIONS ::= BEGIN

IMPORTS    
        Counter32               		  FROM SNMPv2-SMI    -- [RFC2578] 
        QTECHRrcp                            FROM QTECH-RRCP-VLAN-MIB
        MacAddress, TruthValue            FROM SNMPv2-TC
        EnableVar                         FROM SWITCH-TC;
 
--QTECHRrcp             OBJECT IDENTIFIER ::= { QTECHSwitch 52 }

QTECHRrcpProtocol          MODULE-IDENTITY
    LAST-UPDATED   "201004090000Z"                -- 9 April 2010
    ORGANIZATION   "QTECH, Inc."
    CONTACT-INFO  
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 86-010-82884499

                E-mail: longyang@QTECH.com"
    DESCRIPTION
        "This MIB module defines objects to RRCP Management"
        
    --  Revision history
    
    REVISION   "201004090000Z"                     -- 9 April 2010
    DESCRIPTION
            "The updated revision of this MIB.
                - add a mib object 'QTECHRrcpSoftVersion' into 
                  QTECHRrcpDeviceEntry "

    REVISION   "200907060000Z"                     -- 6 July 2009 
    DESCRIPTION
            "The initial revision of this MIB."
        ::={ QTECHRrcp 1 } 

------------------------------------------------------------------------------
--	define groups in QTECH-RRCP-MIB
------------------------------------------------------------------------------
------------------------------------------------------------------------------
QTECHRrcpMibNotifications    OBJECT IDENTIFIER ::= { QTECHRrcpProtocol  1 }  
QTECHRrcpMibObjects    OBJECT IDENTIFIER ::= { QTECHRrcpProtocol 2 }      
QTECHRrcpGlobalGroup    OBJECT IDENTIFIER ::= { QTECHRrcpMibObjects 1 }    
QTECHRrcpCopyGroup    OBJECT IDENTIFIER ::= { QTECHRrcpMibObjects 2 }    

 ------------------------------------------------------------------------------
--	QTECHRrcpMibNotifications
------------------------------------------------------------------------------
------------------------------------------------------------------------------

      QTECHRrcpDeviceUp NOTIFICATION-TYPE
        OBJECTS { QTECHRrcpInterfaceIndex, 
                  QTECHRrcpMacAddress, 
                  QTECHRrcpDeviceId,
                  QTECHRrcpDeviceType
                }
        STATUS  current
        DESCRIPTION
          "Remode device is up."
       ::= { QTECHRrcpMibNotifications 1 } 
   
   
      QTECHRrcpDeviceDown NOTIFICATION-TYPE
        OBJECTS { QTECHRrcpInterfaceIndex, 
                  QTECHRrcpMacAddress, 
                  QTECHRrcpDeviceId,
                  QTECHRrcpDeviceType
                }
        STATUS  current
        DESCRIPTION
          "Remode device is down."
       ::= { QTECHRrcpMibNotifications 2 } 

------------------------------------------------------------------------------
--	QTECHRrcpGlobalGroup
------------------------------------------------------------------------------
------------------------------------------------------------------------------
     QTECHRrcpCurrentNumDevices OBJECT-TYPE
              SYNTAX  INTEGER
              ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The number of current devices. "
              ::= { QTECHRrcpGlobalGroup 1 }

     QTECHRrcpNumDevices OBJECT-TYPE
              SYNTAX  INTEGER
              ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The number of devices have been exist. "
              ::= { QTECHRrcpGlobalGroup 2 }

     QTECHRrcpTrapEnable OBJECT-TYPE
              SYNTAX  EnableVar
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "Enable or disable configuration RRCP Trap. "
              ::= { QTECHRrcpGlobalGroup 3 }
           
     QTECHRrcpHelloTime OBJECT-TYPE
              SYNTAX  INTEGER (0..65535)  
              UNITS   "minutes"
              ACCESS  read-write
              STATUS  current
              --DEFVAL      { 5 }
              DESCRIPTION
                      "The interval of sending hello packets."
              ::= { QTECHRrcpGlobalGroup 4 }  
              
     QTECHRrcpDeviceUpdate OBJECT-TYPE
              SYNTAX  TruthValue
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "Start to updating remote devices. "
              ::= { QTECHRrcpGlobalGroup 5 }     
              
     QTECHRrcpStatsClear OBJECT-TYPE
              SYNTAX  TruthValue
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "Clear statistic of RRCP packets. "
              ::= { QTECHRrcpGlobalGroup 6 }                              
------------------------------------------------------------------------------
--	QTECHRrcpCopyGroup
------------------------------------------------------------------------------
------------------------------------------------------------------------------
              
     QTECHRrcpSourceDeviceId OBJECT-TYPE
              SYNTAX  INTEGER
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "The No. of source device."
              ::= { QTECHRrcpCopyGroup 1 }  
              
     QTECHRrcpDestinationDeviceList OBJECT-TYPE
              SYNTAX  OCTET STRING
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "The list of destination devices. "
              ::= { QTECHRrcpCopyGroup 2 } 
              
     QTECHRrcpCopyStatus OBJECT-TYPE
              SYNTAX  INTEGER {
                      start(1),
                      busy(2),
                      completed(3),
                      error(4)
                    }
              ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "The status of copy function. "
              ::= { QTECHRrcpCopyGroup 3 }  
              
     QTECHRrcpCopyFailDeviceList OBJECT-TYPE
              SYNTAX  OCTET STRING
              ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The list of failed devices. "
              ::= { QTECHRrcpCopyGroup 4 }
    
 ------------------------------------------------------------------------------
--	QTECHRrcpInterfaceTable
------------------------------------------------------------------------------
------------------------------------------------------------------------------
     QTECHRrcpInterfaceTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHRrcpInterfaceEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of RRCP interface."
			::=	{  QTECHRrcpMibObjects 3 }

	QTECHRrcpInterfaceEntry  OBJECT-TYPE
			SYNTAX		QTECHRrcpInterfaceEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"A list of RRCP interface information and configuration."
			INDEX {	QTECHRrcpInterfaceIndex }
			::=	{ QTECHRrcpInterfaceTable 1 }

 	QTECHRrcpInterfaceEntry ::= SEQUENCE {
			QTECHRrcpInterfaceIndex       INTEGER,
			QTECHRrcpInterfaceDescription OCTET STRING,
			QTECHRrcpInterfaceEnable      EnableVar
			}

    QTECHRrcpInterfaceIndex   OBJECT-TYPE
  	        SYNTAX      INTEGER
         	MAX-ACCESS 	not-accessible
        	STATUS 		current
        	DESCRIPTION
                	"The index of interface."
         	::= { QTECHRrcpInterfaceEntry 1 }

    QTECHRrcpInterfaceDescription   OBJECT-TYPE
  	        SYNTAX      OCTET STRING (SIZE(0..63))
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The description of interface."
         	::= { QTECHRrcpInterfaceEntry 2 }

    QTECHRrcpInterfaceEnable   OBJECT-TYPE
  	        SYNTAX      EnableVar
         	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"Enable or disable configuration RRCP on the interface."
         	::= { QTECHRrcpInterfaceEntry 3 }

 ------------------------------------------------------------------------------
--	QTECHRrcpDeviceTable
------------------------------------------------------------------------------
------------------------------------------------------------------------------
     QTECHRrcpDeviceTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHRrcpDeviceEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of RRCP remote devices."
			::=	{  QTECHRrcpMibObjects 4 }

	QTECHRrcpDeviceEntry  OBJECT-TYPE
			SYNTAX		QTECHRrcpDeviceEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"A list of RRCP remote devices information on the interface."
			INDEX {	QTECHRrcpInterfaceIndex, QTECHRrcpMacAddress}
			::=	{ QTECHRrcpDeviceTable 1 }

 	QTECHRrcpDeviceEntry ::= SEQUENCE {
			QTECHRrcpMacAddress    MacAddress,
			QTECHRrcpDeviceId      INTEGER,
			QTECHRrcpDeviceType    OCTET STRING,
			QTECHRrcpDownlinkPort  INTEGER,
			QTECHRrcpUplinkPort    INTEGER,
			QTECHRrcpUplinkMac     MacAddress,
			QTECHRrcpSoftVersion   INTEGER
			}

    QTECHRrcpMacAddress   OBJECT-TYPE
  	        SYNTAX      MacAddress
         	MAX-ACCESS 	not-accessible
        	STATUS 		current
        	DESCRIPTION
                	"The MAC address of remote device."
         	::= { QTECHRrcpDeviceEntry 1 }

    QTECHRrcpDeviceId   OBJECT-TYPE
  	        SYNTAX      INTEGER
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The No. of remote device."
         	::= { QTECHRrcpDeviceEntry 2 }

    QTECHRrcpDeviceType   OBJECT-TYPE
  	        SYNTAX      OCTET STRING (SIZE(0..15))
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The type of remote device."
         	::= { QTECHRrcpDeviceEntry 3 }

    QTECHRrcpDownlinkPort   OBJECT-TYPE
  	        SYNTAX      INTEGER
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The link port of remote device."
         	::= { QTECHRrcpDeviceEntry 4 }

    QTECHRrcpUplinkPort   OBJECT-TYPE
  	        SYNTAX      INTEGER
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The link port of uplink device."
         	::= { QTECHRrcpDeviceEntry 5 }

    QTECHRrcpUplinkMac   OBJECT-TYPE
  	        SYNTAX      MacAddress
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"The MAC address of uplink device."
         	::= { QTECHRrcpDeviceEntry 6 }                    
         		
    QTECHRrcpSoftVersion  OBJECT-TYPE
  	        SYNTAX     INTEGER
         	MAX-ACCESS read-only
         	STATUS   current
         	DESCRIPTION
                	"The software bin version of remote device."
         	::= { QTECHRrcpDeviceEntry 7 }

 ------------------------------------------------------------------------------
--	QTECHRrcpStatsTable
------------------------------------------------------------------------------
------------------------------------------------------------------------------
     QTECHRrcpStatsTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHRrcpStatsEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of RRCP statistics."
			::=	{  QTECHRrcpMibObjects 5 }

	QTECHRrcpStatsEntry  OBJECT-TYPE
			SYNTAX		QTECHRrcpStatsEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"A list of RRCP statistics parameters on the interface."
			INDEX {	QTECHRrcpInterfaceIndex }
			::=	{ QTECHRrcpStatsTable 1 }

	QTECHRrcpStatsEntry ::= SEQUENCE {
			QTECHRrcpHelloTx       		 Counter32,
			QTECHRrcpGetTx       			 Counter32,
			QTECHRrcpSetTx       			 Counter32,
			QTECHRrcpGetReplyRx       		 Counter32,
			QTECHRrcpHelloReplyRx       	 Counter32			
			}
	 		
     QTECHRrcpHelloTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of Hello packets transmitted 
           			on this interface."            			
          	::= { QTECHRrcpStatsEntry 1 }

     QTECHRrcpGetTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of Get packets transmitted on 
           			this interface." 
          	::= { QTECHRrcpStatsEntry 2 }

     QTECHRrcpSetTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of Set packets transmitted on 
           			this interface." 
          	::= { QTECHRrcpStatsEntry 3 }

     QTECHRrcpGetReplyRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of Get Reply packets received 
           			on this interface."
          	::= { QTECHRrcpStatsEntry 4 }

     QTECHRrcpHelloReplyRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of Hello Reply packets received 
           			on this interface."
          	::= { QTECHRrcpStatsEntry 5 }         

END
