-- *****************************************************************
-- QTECH-POE-MIB.mib
--
-- june 2009, wangying
--
-- Copyright(c) 2003-2005 by QTECH TECH, Ltd. 
-- All rights reserved.
-- *****************************************************************
                      

QTECH-POE-MIB DEFINITIONS ::= BEGIN      
IMPORTS
    Unsigned32               			FROM SNMPv2-SMI    -- [RFC2578] 
    pethPsePortGroupIndex , 
    pethPsePortIndex,
    pethMainPseGroupIndex               FROM POWER-ETHERNET-MIB  -- [RFC3621]rfc3621-power-ethernet
    InterfaceIndex					    FROM IF-MIB        -- [RFC2863]
    EnableVar                           FROM SWITCH-TC
    QTECHSwitch                         FROM QTECH-BASE-MIB
    ;
QTECHPoe  MODULE-IDENTITY
    LAST-UPDATED   "200711020000Z"
    ORGANIZATION   "QTECH, Inc."
    CONTACT-INFO  
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 
                E-mail: wangying@QTECH.com"
    DESCRIPTION
        "This MIB module defines objects to POE "
        REVISION   "200711020000Z"
        DESCRIPTION
            "The initial revision of this MIB."
        ::={ QTECHSwitch 51 } 

                                                                                                
-- POE Interface Table   

QTECHPsePortTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHPsePortEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "POE Interface table"
    ::= { QTECHPoe 1 }

QTECHPsePortEntry   OBJECT-TYPE
    SYNTAX      QTECHPsePortEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "An entry in the QTECHPsePortTable. It contains
        POE status in each interface port."
	INDEX        { pethPsePortGroupIndex , pethPsePortIndex  }    
    ::={ QTECHPsePortTable 1 }
    
    QTECHPsePortEntry ::= SEQUENCE {
   	QTECHPsePortPeakPower					    Unsigned32,
	QTECHPsePortAveragePower					Unsigned32,        
	QTECHPsePortCurrentPower                   Unsigned32, 
	QTECHPsePortCurrentVoltage                 Unsigned32,
	QTECHPsePortCurrent	                    Unsigned32,
	QTECHPsePortPowerLimit                     Unsigned32,
	QTECHPsePortOperStatus					EnableVar	
}

QTECHPsePortPeakPower        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The peak power of the PSE port in milliWatts."
    ::= { QTECHPsePortEntry 1 }    
     
QTECHPsePortAveragePower        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The average power of the PSE port in milliWatts."
    ::= { QTECHPsePortEntry 2 }      
QTECHPsePortCurrentPower        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The current power of the PSE port in milliWatts."
    ::= { QTECHPsePortEntry 3 }    
QTECHPsePortCurrentVoltage        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The current voltage of the PSE port in volt."
    ::= { QTECHPsePortEntry 4 }    
QTECHPsePortCurrent        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The current ciQTECHuit of the PSE port in Milliampere."
    ::= { QTECHPsePortEntry 5 }    
QTECHPsePortPowerLimit        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The max power of the PSE port in milliWatts.the value is configured by manual."
    ::= { QTECHPsePortEntry 6 }    
QTECHPsePortOperStatus        OBJECT-TYPE
	SYNTAX      EnableVar
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The operational status of the port PSE.1:enable,2:disable"
    ::= { QTECHPsePortEntry 7 }   


 -- POE Main Table   

QTECHMainPseTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHMainPseEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "A table of objects that display and control attributes
            of the main power souQTECHe in a PSE  device.  Ethernet
            switches are one example of boxes that would support
            these objects."
    ::= { QTECHPoe 2 }

QTECHMainPseEntry   OBJECT-TYPE
    SYNTAX      QTECHMainPseEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "A set of objects that display and control the Main
             power of a PSE."
	INDEX        { pethMainPseGroupIndex}    
    ::={ QTECHMainPseTable 1 }
    
    QTECHMainPseEntry ::= SEQUENCE {
   	QTECHMainPseAveragePower			   Unsigned32,
	QTECHMainPsePeakPower					Unsigned32,   
	QTECHMainPseLegacyDetectionEnable         EnableVar,
	QTECHMainPseManageMode                   INTEGER, 
	QTECHMainPseTemperatureProtect                 EnableVar,
	QTECHMainPseModuleOverTemp	                    EnableVar
}
 
QTECHMainPseAveragePower        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The average power of the PSE in milliWatts."
    ::= { QTECHMainPseEntry 1 }    
 
QTECHMainPsePeakPower        OBJECT-TYPE
	SYNTAX      Unsigned32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The peak power of the PSE in milliWatts."
    ::= { QTECHMainPseEntry 2 }     
    
QTECHMainPseLegacyDetectionEnable        OBJECT-TYPE
	SYNTAX      EnableVar
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Legacy CAP Detection��1-enable��2-disable"
    ::= { QTECHMainPseEntry 3 }     
 
QTECHMainPseManageMode        OBJECT-TYPE
	SYNTAX INTEGER   {
               auto(1),
               manual(2)
          }
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The management mode of power of PSE. 1-auto mode,2-manual mode."
    ::= { QTECHMainPseEntry 4 }   
        
QTECHMainPseTemperatureProtect        OBJECT-TYPE
	SYNTAX      EnableVar
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The swich of over tempreture protection.1-enable;2-disable."
    ::= { QTECHMainPseEntry 5 } 

QTECHMainPseModuleOverTemp        OBJECT-TYPE
	SYNTAX INTEGER   {
               normal(1),
               alarm(2)
          }

    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The status of over tempreture alarm for PSE,1-normal;2-alarm."
    ::= { QTECHMainPseEntry 6 }

-- trap notification 
QTECHPoeNotifications           OBJECT IDENTIFIER ::= { QTECHPoe 3 }
QTECHMainPseOverTempreture
NOTIFICATION-TYPE
    OBJECTS     { QTECHMainPseModuleOverTemp
                }
    STATUS      current
    DESCRIPTION
       "This Notification indicates if Pse is over tempreture or below tempreture.  
       This Notification SHOULD be sent on every status change.
              "
    REFERENCE
       ""
    ::= { QTECHPoeNotifications 1 }

END      


