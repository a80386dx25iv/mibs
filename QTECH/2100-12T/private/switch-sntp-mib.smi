--MibName=QTECHSntp
------------------------------------------------------------------------------
------------------------------------------------------------------------------
--
--  File         : sntp.mib
--  Description  : ose software system command  mib defination 
--  Version      : 0.1   
--  Date         : Dec 20, 2004     
--
--  Copyright (c) 2003-2004 Beijing QTECH Science & Technology Co., ltd  All Rights Reserved.
--
------------------------------------------------------------------------------
------------------------------------------------------------------------------

    SWITCH-SNTP-MIB DEFINITIONS ::= BEGIN

    IMPORTS
        MODULE-IDENTITY, OBJECT-TYPE,
        IpAddress                    FROM SNMPv2-SMI 
        EnableVar				   FROM SWITCH-TC  
        InterfaceIndex             FROM IF-MIB                           
        QTECHSwitch                FROM QTECH-BASE-MIB; 

    QTECHSntp MODULE-IDENTITY
    
        LAST-UPDATED    "0412200000Z"  -- Dec 20, 2004
        ORGANIZATION    "QTECH Science & Technology Co., ltd"
        CONTACT-INFO    "QTECH Science & Technology Co., ltd.
                         E-mail: support@QTECH.com"

        DESCRIPTION     "OSE SYSinfo Enterprise MIB definition."

        REVISION        "0412200000Z"  -- Dec 20, 2004
        DESCRIPTION     "Initial MIB creation."

                ::= { QTECHSwitch 8 }      
                
----groups---------------------------------                
QTECHSntpServer        OBJECT IDENTIFIER ::= { QTECHSntp 1 }  
QTECHSntpClient       	OBJECT IDENTIFIER ::= { QTECHSntp 2 }
--------------------------------------------------   

----------QTECHSntpServer    
--QTECHSntpServer
	--QTECHSntpServerEnable
	--QTECHSntpServerBroadcastAddress
	--QTECHSntpServerSendInterval        

QTECHSntpServerEnable OBJECT-TYPE
        SYNTAX EnableVar        
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "enable system to send sntp message or disable such function  "
        DEFVAL {disable}
        ::= { QTECHSntpServer 1 }
        
QTECHSntpServerBroadcastAddress OBJECT-TYPE
        SYNTAX IpAddress
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "system sntp server host ip address  ."
        ::= { QTECHSntpServer 2 }


QTECHSntpServerSendInterval OBJECT-TYPE
        SYNTAX INTEGER(1..65535)
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "the interval for  system to send sntp message. "
        ::= { QTECHSntpServer 3 }




--------------------------------------------------------------------------------------
--sysSntpClient  tempory defination for snmp trap server
--------------------------------------------------------------------------------------
--QTECHSntpClient
	--QTECHSntpClientAddress
	--QTECHSntpClientGet
	--QTECHSntpClientListenEnable

QTECHSntpClientAddress OBJECT-TYPE
        SYNTAX IpAddress
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "system sntp server host ip address which to get sntp message ."
        ::= { QTECHSntpClient 1 }

QTECHSntpClientGet OBJECT-TYPE
        SYNTAX INTEGER{
        	enable(1),disable(2)        	
        	}        
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The operation of system to specific sntp server host ip address which to get sntp message ."
        ::= { QTECHSntpClient 2 }
        
QTECHSntpClientListenEnable OBJECT-TYPE
        SYNTAX EnableVar
        MAX-ACCESS read-write
        STATUS deprecated
        DESCRIPTION
            "enable system to listen broadcast sntp message or disable such function  "
        DEFVAL {disable}
        ::= { QTECHSntpClient 3 }      
        
QTECHSntpClientMode OBJECT-TYPE
        SYNTAX INTEGER{
        	unicast(0),broadcast(1),multicast(2)       	
        	}        
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The operation of system to specific which mode the sntp client works."
        ::= { QTECHSntpClient 4 }  
             
--start of interface config  
 
QTECHSntpInterfaceTable 	OBJECT-TYPE
        SYNTAX      SEQUENCE OF QTECHSntpInterfaceEntry
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"This table provides universal information of SNTP clock."
        ::= { QTECHSntpClient 5 }  

QTECHSntpInterfaceEntry 	OBJECT-TYPE
        SYNTAX      QTECHSntpInterfaceEntry
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"Each port entry provides SNTP interface information. Entries
	                are automatically created when the system running."
        INDEX   	{ QTECHSntpInterfaceIndex }
        ::= { QTECHSntpInterfaceTable 1 }     
        
QTECHSntpInterfaceEntry ::= SEQUENCE {   
        QTECHSntpInterfaceIndex                            InterfaceIndex,
        QTECHSntpInterfaceEnable			                INTEGER
        }
  
QTECHSntpInterfaceIndex OBJECT-TYPE
     SYNTAX        InterfaceIndex
     MAX-ACCESS    not-accessible
     STATUS        current
     DESCRIPTION
         "SNTP interface entry interface index."
     ::= { QTECHSntpInterfaceEntry 1 }
        
QTECHSntpInterfaceEnable OBJECT-TYPE
        SYNTAX INTEGER{
        	enable(1),disable(2)        	
        	}        
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "SNTP interface entry sntp enable."
        ::= { QTECHSntpInterfaceEntry 2 }  

--end of interface config

--
-- END of SWITCH-SNTP-MIB                
--

END    

