-- *****************************************************************
-- QTECH-IPMCAST-MIB.mib:  IPMCAST MIB
--
-- January 2012, xuguiping
--
-- Copyright (c) 1996-2009 by QTECH Technology Co., Ltd.
-- All rights reserved.
--
-- *****************************************************************
--

QTECH-IPMCAST-MIB DEFINITIONS ::= BEGIN

IMPORTS
        MODULE-IDENTITY, OBJECT-TYPE, Integer32 	FROM SNMPv2-SMI
        TEXTUAL-CONVENTION, MacAddress, RowStatus	FROM SNMPv2-TC
        EnableVar	  								FROM SWITCH-TC
        QTECHPortIndex							        FROM SWITCH-SYSTEM-MIB
        QTECHAgent                           	FROM QTECH-BASE-MIB
        InetAddress, InetAddressType, 
        InetAddressIPv6                             FROM INET-ADDRESS-MIB    -- [RFC4001]       
        InterfaceIndexOrZero,
        InterfaceIndex                              FROM IF-MIB;             -- [RFC2863]

QTECHIpmcast   MODULE-IDENTITY
        LAST-UPDATED    "201201050000Z"
        ORGANIZATION    "QTECH Technology Co., Ltd."
        CONTACT-INFO
		                "QTECH Science & Technology Co., ltd.
		                 E-mail: support@QTECH.com"
        DESCRIPTION
		        		"This MIB module defines a MIB which provides
		                mechanisms to the IPMCAST."

        REVISION     	"201201050000Z"
        DESCRIPTION
            		 	"version 1.0"

        ::= { QTECHAgent 71 }

QTECHIpmcastNotifications	OBJECT IDENTIFIER ::= {	QTECHIpmcast 1 }
QTECHIpmcastObjects			OBJECT IDENTIFIER ::= {	QTECHIpmcast 2 }   
QTECHIpmcastConformance		OBJECT IDENTIFIER ::= {	QTECHIpmcast 3 }  
                                                                                                   
QTECHIpMcastScalar		    OBJECT IDENTIFIER ::= {	QTECHIpmcastObjects 1 }  
        
QTECHIpMcastRouteLimit OBJECT-TYPE           
        SYNTAX      INTEGER
		MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The limit of multicast route table. 
                The upper limit is depended on device." 
        ::= { QTECHIpMcastScalar 1 }   
        
QTECHIpMcastOifLimitPerRoute OBJECT-TYPE           
        SYNTAX      INTEGER 
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The limit of downstream interface per multicast route entry.
                The upper limit is depended on device."     
        ::= { QTECHIpMcastScalar 2 }
        
QTECHIpMcastStaticTable 	OBJECT-TYPE
        SYNTAX      SEQUENCE OF QTECHIpMcastStaticEntry
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"This table provides the static mroute information."
        ::= { QTECHIpmcastObjects 2 }
 
QTECHIpMcastStaticEntry 	OBJECT-TYPE
        SYNTAX      QTECHIpMcastStaticEntry
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"Each entry provides the static mroute information."
        INDEX   	{ QTECHIpMcastStaticSAddressType,
        			  QTECHIpMcastStaticSAddress,
        			  QTECHIpMcastStaticSAddressPrefix}
        ::= { QTECHIpMcastStaticTable 1 }

QTECHIpMcastStaticEntry ::= SEQUENCE { 
		QTECHIpMcastStaticSAddressType		InetAddressType,
		QTECHIpMcastStaticSAddress    		InetAddress,
		QTECHIpMcastStaticSAddressPrefix 	INTEGER,
		QTECHIpMcastStaticNAddressType		InetAddressType,
		QTECHIpMcastStaticNAddress       	InetAddress,
		QTECHIpMcastStaticIfIndex       	    InterfaceIndexOrZero,
		QTECHIpMcastStaticPreference        	INTEGER,
		QTECHIpMcastStaticRowStatus       	RowStatus
        }
  
QTECHIpMcastStaticSAddressType 	OBJECT-TYPE
        SYNTAX      InetAddressType
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"The address type of the multicast source."
        ::= { QTECHIpMcastStaticEntry 1 }
        
QTECHIpMcastStaticSAddress  OBJECT-TYPE    
        SYNTAX      InetAddress
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"The address of the multicast source."  
        ::= { QTECHIpMcastStaticEntry 2 }  
        
QTECHIpMcastStaticSAddressPrefix  OBJECT-TYPE
        SYNTAX      INTEGER
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
	        		"The prefix length of the multicast source address."
        ::= { QTECHIpMcastStaticEntry 3 }     
        
QTECHIpMcastStaticNAddressType OBJECT-TYPE
        SYNTAX      InetAddressType
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
	        		"The address type of the next hop."
        ::= { QTECHIpMcastStaticEntry 4 }           
   
QTECHIpMcastStaticNAddress OBJECT-TYPE
        SYNTAX      InetAddress
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
	        		"The address of the next hop."
        ::= { QTECHIpMcastStaticEntry 5 }    
        
QTECHIpMcastStaticIfIndex OBJECT-TYPE
        SYNTAX      InterfaceIndexOrZero
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
	        		"The interface index of the next hop. A value of 0 
	        		indicates that the next hop interface is unknown."
        ::= { QTECHIpMcastStaticEntry 6 }   
        
QTECHIpMcastStaticPreference OBJECT-TYPE
        SYNTAX      INTEGER 
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
	        		"The preference of the static route protocol."   
	   	DEFVAL {0}

        ::= { QTECHIpMcastStaticEntry 7 } 
        
QTECHIpMcastStaticRowStatus OBJECT-TYPE
        SYNTAX      RowStatus
        MAX-ACCESS  read-create
        STATUS      current
        DESCRIPTION
	        		"The row status of this entry."
        ::= { QTECHIpMcastStaticEntry 8 }
        
--
-- END of QTECH-IPMCAST-MIB
--

END

