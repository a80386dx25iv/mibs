--MibName=QTECHRemoteUpgrade
-- =======================================================================
-- Version info 
--
-- Version 0.1 Created 2006.9.11 by LIAOBIN
-- This version of MIB is created just for management of extend OAM remote
-- upgrade. 
-- Copyright(c) 2003-2006 by QTECH TECH, Ltd. 
-- =======================================================================

-- ===========================================================================

--
-- QTECH base management objects
--            

QTECH-EXTEND-OAM-UPGRADE-MIB DEFINITIONS ::= BEGIN

    IMPORTS
		MODULE-IDENTITY,
		OBJECT-TYPE,
		NOTIFICATION-TYPE ,
		Integer32,
		IpAddress,
		Unsigned32
			FROM SNMPv2-SMI
		DisplayString,
		TimeStamp,
		RowStatus,
		TruthValue
			FROM SNMPv2-TC    
		DateAndTime,
		EnableVar
			FROM SWITCH-TC       
		QTECHAgent		
			FROM QTECH-BASE-MIB;
	
	QTECHRemoteUpgrade    MODULE-IDENTITY
		LAST-UPDATED 	"200608110000Z"
		ORGANIZATION 	"QTECH TECH, Ltd."
		CONTACT-INFO 	"www.QTECH.com"
		DESCRIPTION  	
		    "The MIB module defining objects for remote upgrade management"
		::= { QTECHAgent  11}  
                
	--groups                 
	QTECHExtendOamUpgradeGroup	OBJECT IDENTIFIER ::= {QTECHRemoteUpgrade 1}
	
	
    -- QTECHExtendOamUpgradeGroup
    		
   	QTECHExtendOamUpgradeMibObjects OBJECT IDENTIFIER ::= { QTECHExtendOamUpgradeGroup 1 }
	QTECHExtendOamMibTraps   OBJECT IDENTIFIER ::= { QTECHExtendOamUpgradeGroup 2 }

    QTECHExtendOamUpgradeNextIndex OBJECT-TYPE
		    SYNTAX Unsigned32(1..2147483647)
		    ACCESS read-only
		    STATUS mandatory
		    DESCRIPTION
		        "Object specifying a unique entry in the QTECHExtendOamUpgradeTable. 
		        A management station wishing to initiate an upgrade to remote 
		        device with extend OAM should get this object value as an index 
		        to create an request entry in the following table."
		    ::= { QTECHExtendOamUpgradeMibObjects 1 }
   
   
	QTECHExtendOamUpgradeTable OBJECT-TYPE
		    SYNTAX SEQUENCE OF QTECHExtendOamUpgradeEntry
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "A table of Upgrade requests."
		    ::= { QTECHExtendOamUpgradeMibObjects 2 }

	QTECHExtendOamUpgradeEntry OBJECT-TYPE
		    SYNTAX QTECHExtendOamUpgradeEntry
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "A Upgrade request."
		    INDEX { QTECHExtendOamUpgradeIndex }
		    ::= { QTECHExtendOamUpgradeTable 1 }

	QTECHExtendOamUpgradeEntry ::= SEQUENCE {
		    QTECHExtendOamUpgradeIndex 			Unsigned32,
		    QTECHExtendOamUpgradeType	 		INTEGER,
		    QTECHExtendOamUpgradeFileType	 	INTEGER,
		    QTECHExtendOamUpgradeFileName 		DisplayString,
		    QTECHExtendOamUpgradeNotificationOnComplete 	TruthValue,
		    QTECHExtendOamUpgradeState 			INTEGER,    
		    QTECHExtendOamUpgradeDevices			OCTET STRING,
		    QTECHExtendOamUpgradeEntryRowStatus 	RowStatus
	    	    }

	QTECHExtendOamUpgradeIndex OBJECT-TYPE
		    SYNTAX Unsigned32(1..2147483647)
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "Object, of which value is generated from the scalar 
		        QTECHExtendOamUpgradeNextIndex, specifies a unique entry 
		        in the QTECHExtendOamUpgradeTable.  "
		    ::= { QTECHExtendOamUpgradeEntry 1 }

	QTECHExtendOamUpgradeType OBJECT-TYPE
		    SYNTAX INTEGER {download(1),upload(2)}
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "Specify the direction of the upgrade. 
		        download means write file to remote device. 
		        upload means get file from remote device."
		    ::= { QTECHExtendOamUpgradeEntry 2 }

	QTECHExtendOamUpgradeFileType OBJECT-TYPE
		    SYNTAX INTEGER {
	        		   image (1),
                       startupconfig (2),
                       runningconfig (3),
                       others (4),
                       bootstrap (5),
                       fpga (6)
	        		   }
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "File type to be upgraded."
		        		       
		    ::= { QTECHExtendOamUpgradeEntry 3 }

	 QTECHExtendOamUpgradeFileName OBJECT-TYPE
		    SYNTAX DisplayString
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "Name of file on local device to be upgraded. If upload, it specifies 
		        the name of file and the type of storage to be saved, otherwise the 
		        name and storage of source file to be downloaded."
		    ::= {  QTECHExtendOamUpgradeEntry 4 }

	 QTECHExtendOamUpgradeNotificationOnComplete OBJECT-TYPE
		    SYNTAX TruthValue
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "Specifies whether or not a  QTECHExtendOamUpgradeNotificationOnCompleteTrap       
		        notification should be issued on completion of transfer. If such 
		        notification is desired, it is the responsibility of the management entity 
		        to ensure that the SNMP administrative model is configured in the way that 
		        allows the notification to be delivered."
		    DEFVAL { false }
		    ::= {  QTECHExtendOamUpgradeEntry 5 }

	 QTECHExtendOamUpgradeState OBJECT-TYPE
		    SYNTAX  INTEGER {
				        waiting(1),
				        getsource(2),
				        writedest(3),
				        completed(4)
				     }
		    ACCESS read-only
		    STATUS mandatory
		    DESCRIPTION
		        "Specifies the state of this Upgrade request.
		        This value of this object is instantiated only after the row
		        has been instantiated, i.e. after the  QTECHExtendOamUpgradeEntryRowStatus
		        has been made active."
		    ::= {  QTECHExtendOamUpgradeEntry 6 }

	 QTECHExtendOamUpgradeDevices OBJECT-TYPE
		    SYNTAX OCTET STRING (SIZE(2..255))
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "Specifies the Devices to be upgraded ."
		    ::= {  QTECHExtendOamUpgradeEntry 7 }

	 QTECHExtendOamUpgradeEntryRowStatus OBJECT-TYPE
		    SYNTAX RowStatus
		    ACCESS read-create
		    STATUS mandatory
		    DESCRIPTION
		        "The status of this table entry.  Once the entry status is
		        set to active, the associated entry cannot be modified until
		        the request completes ."
		    ::= {  QTECHExtendOamUpgradeEntry 8 }
	

	QTECHExtendOamUpgradeStatusTable OBJECT-TYPE
		    SYNTAX SEQUENCE OF QTECHExtendOamUpgradeStatusEntry
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "A table of Upgrade statuses."
		    ::= { QTECHExtendOamUpgradeMibObjects 3 }

	QTECHExtendOamUpgradeStatusEntry OBJECT-TYPE
		    SYNTAX QTECHExtendOamUpgradeStatusEntry
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "A Upgrade status."
		    INDEX { QTECHExtendOamUpgradeStatusIndex, QTECHExtendOamUpgradeStatusDevice }
		    ::= { QTECHExtendOamUpgradeStatusTable 1 }

	QTECHExtendOamUpgradeStatusEntry ::= SEQUENCE {
		    QTECHExtendOamUpgradeStatusIndex 			Unsigned32,
		    QTECHExtendOamUpgradeStatusDevice	 		INTEGER,
		    QTECHExtendOamUpgradeFailCause 		INTEGER
	    	    }


 	QTECHExtendOamUpgradeStatusIndex OBJECT-TYPE
		    SYNTAX Unsigned32(1..2147483647)
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "Object which specifies a unique entry in the
		        QTECHExtendOamUpgradeStatusTable."
		    ::= { QTECHExtendOamUpgradeStatusEntry 1 }

	QTECHExtendOamUpgradeStatusDevice OBJECT-TYPE
		    SYNTAX INTEGER 
		    ACCESS not-accessible
		    STATUS mandatory
		    DESCRIPTION
		        "Specify the direction of the upgrade. 
		        download means write file to remote device. 
		        upload means get file from remote device."
		    ::= { QTECHExtendOamUpgradeStatusEntry 2 }

	 QTECHExtendOamUpgradeFailCause OBJECT-TYPE
		    SYNTAX  INTEGER {
		    			noerror(1),
				        invalidfilename(2),
				        fileopenfail(3),
				        filewritefail(4),
				        nomem(5),
				        filetoolarge(6),
				        oamlinkbusy(7),
				        oamtimeout(8),
				        oamnotconnnected(9),
				        remotenotsupport (10),
				        unknown(11),
				        invalidupgradetype(12),
				        invalidfiletype(13),
				        filecheckfail(14)		        
				    }
		    ACCESS read-only
		    STATUS mandatory
		    DESCRIPTION
		        "The reason why the Upgrade operation failed.
		        This object is instantiated only when the 
		        QTECHExtendOamUpgradeState for this  entry is 
		        in the completed state."
		    ::= {  QTECHExtendOamUpgradeStatusEntry 3 }

		    
	QTECHExtendOamUpgradeCompletion NOTIFICATION-TYPE
           OBJECTS {QTECHExtendOamUpgradeDevices}
           STATUS  current
           DESCRIPTION
               "Notify snmp manager that completion of upgrade for specific devices."
       		::= { QTECHExtendOamMibTraps 1 }
              
END
