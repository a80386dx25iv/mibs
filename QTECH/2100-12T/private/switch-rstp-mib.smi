--MibName=QTECHRstp
SWITCH-RSTP-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE    	 	FROM SNMPv2-SMI
    TruthValue  	FROM SNMPv2-TC   
    dot1dStpPortEntry   			FROM BRIDGE-MIB 
    QTECHSwitch	            			FROM QTECH-BASE-MIB;


QTECHRstp MODULE-IDENTITY
    LAST-UPDATED "9411010000Z"
    ORGANIZATION "IETF SNMPv2 Working Group"
    CONTACT-INFO
            " 
             Phone:  01082884499
             Email:  support@QTECH.com"
    DESCRIPTION
            "The MIB module for managing  trunk  in QTECH2126."
    REVISION      "9103310000Z"
    DESCRIPTION
            ""
    ::= { QTECHSwitch 9}

-----------------------------------------------------------------
	QTECHRstpConfig       	OBJECT IDENTIFIER ::= { QTECHRstp 1 }  
	QTECHRstpStatistics     	OBJECT IDENTIFIER ::= { QTECHRstp 2 }
-----------------------------------------------------------------


QTECHRstpEnable OBJECT-TYPE
    SYNTAX      TruthValue
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "."    
    DEFVAL { 1}
    ::= { QTECHRstpConfig 1 }

-- QTECHRstpPortConfigTable

QTECHRstpPortConfigTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHRstpPortConfigEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Rstp config table"
    ::= { QTECHRstpConfig 2 }

QTECHRstpPortConfigEntry OBJECT-TYPE
    SYNTAX      QTECHRstpPortConfigEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Rstp config table"
    AUGMENTS    { dot1dStpPortEntry }
    ::= { QTECHRstpPortConfigTable 1 }

QTECHRstpPortConfigEntry ::= SEQUENCE {
        QTECHRstpPortRstpEnable          TruthValue,
        QTECHRstpPortOperEnable          TruthValue
        }

QTECHRstpPortRstpEnable OBJECT-TYPE
    SYNTAX      TruthValue
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "port Rstp enable"
    ::= { QTECHRstpPortConfigEntry 1 } 
    
QTECHRstpPortOperEnable OBJECT-TYPE
    SYNTAX      TruthValue
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port Rstp enable operater state"
    ::= { QTECHRstpPortConfigEntry 2 }

-- QTECHRstpStatistics     
QTECHRstpPortStatsTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHRstpPortStatsEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Rstp config table"
    ::= { QTECHRstpStatistics 1 }

QTECHRstpPortStatsEntry OBJECT-TYPE
    SYNTAX      QTECHRstpPortStatsEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Rstp config table"
    AUGMENTS    { dot1dStpPortEntry }
    ::= { QTECHRstpPortStatsTable 1 }

QTECHRstpPortStatsEntry ::= SEQUENCE {
        QTECHRstpPortRxStpBPDUs       Counter,
        QTECHRstpPortRxTCNs           Counter,
        QTECHRstpPortRxRstpBPDUs      Counter,
        QTECHRstpPortTxStpBPDUs        Counter,
        QTECHRstpPortTxTCNs             Counter,
        QTECHRstpPortTxRstpBPDUs        Counter,
        QTECHRstpPortStatisticsClear          TruthValue
        }

QTECHRstpPortRxStpBPDUs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port receive STP BPDUs"
    ::= { QTECHRstpPortStatsEntry 1 }   
    
    QTECHRstpPortRxTCNs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port receive TCNs"
    ::= { QTECHRstpPortStatsEntry 2 }

QTECHRstpPortRxRstpBPDUs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port receive RSTP BPDUs"
    ::= { QTECHRstpPortStatsEntry 3 }

QTECHRstpPortTxStpBPDUs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port send STP BPDUs"
    ::= { QTECHRstpPortStatsEntry 4 }

QTECHRstpPortTxTCNs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port send TCNs"
    ::= { QTECHRstpPortStatsEntry 5 }

QTECHRstpPortTxRstpBPDUs OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "port send RSTP BPDUs"
    ::= { QTECHRstpPortStatsEntry 6 }     
    
QTECHRstpPortStatisticsClear OBJECT-TYPE
    SYNTAX      TruthValue
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "clear port statistics"
    ::= { QTECHRstpPortStatsEntry 7 }
END
