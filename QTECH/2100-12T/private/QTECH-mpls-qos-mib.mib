--MibName=QTECHMplsQosMIB
-- *****************************************************************
-- QTECH-MPLS-OS-MIB.MIB:  QTECH Mpls QOS MIB file
--
-- May. 2011, huochao
--
-- Copyright (c) 1994-2011 by QTECH, Inc.
-- All rights reserved.
-- 01,20111231,yangkewei,ROS00008218,advent mib������
-- *****************************************************************
QTECH-MPLS-QOS-MIB  DEFINITIONS ::= BEGIN

IMPORTS                      
	MODULE-IDENTITY, OBJECT-TYPE,
        Integer32, Counter64                    FROM SNMPv2-SMI
        
        TruthValue, RowStatus
        					FROM SNMPv2-TC
        EnableVar,ObjName
        					FROM SWITCH-TC
        MODULE-COMPLIANCE, OBJECT-GROUP         FROM SNMPv2-CONF    
        
        SnmpAdminString
           FROM SNMP-FRAMEWORK-MIB          -- RFC3411

        QTECHQosMIB
                FROM QTECH-QOS-MIB; 
                 
    
	QTECHMplsQosMIB MODULE-IDENTITY
        LAST-UPDATED    "201105040000Z"
        ORGANIZATION    "QTECH, Inc."
        CONTACT-INFO
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 86-010-82884499

                E-mail: huochao@QTECH.com"
        DESCRIPTION
                "Description of Mpls QOS object."     
                
        REVISION   "201105040000Z"  
        DESCRIPTION                       
        "Mpls qos is a submodule of qos.Mpls qos processes the qos priority information between user's network and provider's network.
        Including the following details:
        1.Mapping the qos information of user's network to provider's,including internal priority to Tunnel EXP and VC EXP.
        2.Mapping the qos information of provider's network to user's,including Tunnel EXP and VC EXP to internel priority.
        3.Mannul config the Tunenl EXP,VC EXP value."
                ::= { QTECHQosMIB 4 }   
------------------------------------------------------------------------------
--  define groups in QTECHQosMIB
------------------------------------------------------------------------------
    QTECHMplsQosNotifications     OBJECT IDENTIFIER ::={ QTECHMplsQosMIB 1 } 
    QTECHMplsQosObjects           OBJECT IDENTIFIER ::={ QTECHMplsQosMIB 2 }
    QTECHMplsQosConformance       OBJECT IDENTIFIER ::={ QTECHMplsQosMIB 3 }   
    
    QTECHMplsQosScalerObject      OBJECT IDENTIFIER ::={ QTECHMplsQosObjects 1 }       
------------------------------------------------------------------------------
----QTECHMplsQosObjects group------------
----QTECHMplsQosScalerObject group scale MIB--
------------------------------------             
QTECHMplsQosLocalPriMappingToIpPrecedenceEnable OBJECT-TYPE
        SYNTAX  EnableVar
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "Indicates whether the internal priority mapping to IP precedence is enabled on the device.
            enable(1) means that the mapping is enabled on this device.
            disable(2) means that the mapping is disabled." 
        DEFVAL { disable }
        ::= { QTECHMplsQosScalerObject 1 }

     
QTECHMplsQosLocalPriMappingToDscpEnable OBJECT-TYPE
        SYNTAX  EnableVar
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "Indicates whether the internal priority mapping to dscp is enabled on the device.
            enable(1) means that the mapping is enabled on this device.
            disable(2) means that the mapping is disabled." 
        DEFVAL { disable }
        ::= { QTECHMplsQosScalerObject 2 }     
                
----------------------------------------------
----QTECHMplsQosObjects group QTECHMplsQosLocalPriMappingTable----
----------------------------------------------
QTECHMplsQosLocalPriMappingTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosLocalPriMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "A mapping table of local priority to EXP,including tunnel EXP and VC EXP."
        ::= { QTECHMplsQosObjects 2 }

QTECHMplsQosLocalPriMappingEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosLocalPriMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry for the internal priority maping to EXP table."
        INDEX { QTECHMplsQosLocalPriMappingLocalPriority }
        ::= { QTECHMplsQosLocalPriMappingTable 1 }

QTECHMplsQosLocalPriMappingEntry ::= SEQUENCE {
        QTECHMplsQosLocalPriMappingLocalPriority   INTEGER, 
        QTECHMplsQosLocalPriMappingTunnelExp       INTEGER,
        QTECHMplsQosLocalPriMappingVcExp	     	INTEGER, 
        QTECHMplsQosLocalPriMappingIpPrecedence    INTEGER, 
        QTECHMplsQosLocalPriMappingDscp            INTEGER
        }

QTECHMplsQosLocalPriMappingLocalPriority OBJECT-TYPE
        SYNTAX INTEGER(0..7)
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "the local priority for mapping."
        ::= { QTECHMplsQosLocalPriMappingEntry 1}

QTECHMplsQosLocalPriMappingTunnelExp OBJECT-TYPE
        SYNTAX      INTEGER(0..7)
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "the tunnel EXP value of the mpls packet."   
        DEFVAL { 0 }
        ::= { QTECHMplsQosLocalPriMappingEntry 2}   

QTECHMplsQosLocalPriMappingVcExp OBJECT-TYPE
        SYNTAX      INTEGER(0..7)
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "the VC EXP value of the mpls packet."  
        DEFVAL { 0 }
        ::= { QTECHMplsQosLocalPriMappingEntry 3}  
                                                            
QTECHMplsQosLocalPriMappingIpPrecedence OBJECT-TYPE
        SYNTAX      INTEGER(0..7)
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "the IP precedence value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosLocalPriMappingEntry 4}  
                           
QTECHMplsQosLocalPriMappingDscp OBJECT-TYPE
        SYNTAX      INTEGER(0..63)
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
            "the dscp value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosLocalPriMappingEntry 5}  
                           
                           
-----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosTunnelExpConfigTable----
-----------------------------------------------------------
QTECHMplsQosTunnelExpConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosTunnelExpConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "This table describes the tunnel exp configuration based on tunnel label."
        ::= { QTECHMplsQosObjects 3}

QTECHMplsQosTunnelExpConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosTunnelExpConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry for tunnel EXP configuration."
        INDEX { QTECHMplsQosTunnelExpConfigTunnelName }
        ::= { QTECHMplsQosTunnelExpConfigTable 1 }           

QTECHMplsQosTunnelExpConfigEntry ::= SEQUENCE {
        QTECHMplsQosTunnelExpConfigTunnelName	   SnmpAdminString,
        QTECHMplsQosTunnelExpConfigExp            INTEGER, 
        QTECHMplsQosTunnelExpConfigMode           INTEGER,
        QTECHMplsQosTunnelExpConfigRowStatus      RowStatus
    }

QTECHMplsQosTunnelExpConfigTunnelName OBJECT-TYPE
        SYNTAX      SnmpAdminString(SIZE(1..255))

        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "The tunnel name."
        ::= { QTECHMplsQosTunnelExpConfigEntry 1}
        
QTECHMplsQosTunnelExpConfigExp OBJECT-TYPE
        SYNTAX 		INTEGER(0..7)
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The tunnel EXP value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosTunnelExpConfigEntry 2 } 
           
QTECHMplsQosTunnelExpConfigMode OBJECT-TYPE
        SYNTAX 		INTEGER{fixed(0),mapping(1)}
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The tunnel EXP generate mode,
            fixed(0) means using the config exp value,
            mapping(1) means using local priority mapping to exp." 
        DEFVAL { mapping }
        ::= { QTECHMplsQosTunnelExpConfigEntry 3 } 

QTECHMplsQosTunnelExpConfigRowStatus OBJECT-TYPE
        SYNTAX 		RowStatus
        MAX-ACCESS read-create
        STATUS     current
        DESCRIPTION
            "This object allows entries to be created and deleted in the QTECHMplsQosTunnelExpConfigTable."
        ::= { QTECHMplsQosTunnelExpConfigEntry 4 }   
                 
-----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosVcExpConfigTable----
-----------------------------------------------------------
QTECHMplsQosVcExpConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosVcExpConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "This table describes the VC EXP configuration based on VC label."
        ::= { QTECHMplsQosObjects 4 }

QTECHMplsQosVcExpConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosVcExpConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry for VC EXP configuration."
        INDEX { QTECHMplsQosVcExpConfigVcId }
        ::= { QTECHMplsQosVcExpConfigTable 1 }           

QTECHMplsQosVcExpConfigEntry ::= SEQUENCE {
        QTECHMplsQosVcExpConfigVcId	       INTEGER,
        QTECHMplsQosVcExpConfigExp            INTEGER,     
        QTECHMplsQosVcExpConfigMode           INTEGER,
        QTECHMplsQosVcExpConfigRowStatus      RowStatus
    }

QTECHMplsQosVcExpConfigVcId OBJECT-TYPE
        SYNTAX      INTEGER(0..1048575)
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "The VC id value."
        ::= { QTECHMplsQosVcExpConfigEntry 1}
        
QTECHMplsQosVcExpConfigExp OBJECT-TYPE
        SYNTAX 		INTEGER(0..7)
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The VC EXP value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosVcExpConfigEntry 2 }   
        
QTECHMplsQosVcExpConfigMode OBJECT-TYPE
        SYNTAX 		INTEGER{fixed(0),mapping(1)}
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The VC EXP generate mode,
            fixed(0) means using the config exp value,
            mapping(1) means using local priority mapping to exp."  
        DEFVAL { mapping }
        ::= { QTECHMplsQosVcExpConfigEntry 3 } 


QTECHMplsQosVcExpConfigRowStatus OBJECT-TYPE
        SYNTAX 		RowStatus
        MAX-ACCESS read-create
        STATUS     current
        DESCRIPTION
            "This object allows entries to be created and deleted in the QTECHMplsQosVcExpConfigTable."
        ::= { QTECHMplsQosVcExpConfigEntry 4 }  
         
-----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosTunnelExpMappingTable----
-----------------------------------------------------------   
QTECHMplsQosTunnelExpMappingTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosTunnelExpMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "A mapping table of tunnel EXP to local priority."
        ::= { QTECHMplsQosObjects 5 }

QTECHMplsQosTunnelExpMappingEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosTunnelExpMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry for tunnel EXP to local priority mapping table."
        INDEX { QTECHMplsQosTunnelExpMappingExp }
        ::= { QTECHMplsQosTunnelExpMappingTable 1 }           

QTECHMplsQosTunnelExpMappingEntry ::= SEQUENCE {
        QTECHMplsQosTunnelExpMappingExp	              INTEGER,
        QTECHMplsQosTunnelExpMappingLocalPriority        INTEGER,  
        QTECHMplsQosTunnelExpMappingColor                INTEGER
       }

QTECHMplsQosTunnelExpMappingExp OBJECT-TYPE
        SYNTAX      INTEGER(0..7)
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "The tunnel EXP value for mapping."
        ::= { QTECHMplsQosTunnelExpMappingEntry 1}
        
QTECHMplsQosTunnelExpMappingLocalPriority OBJECT-TYPE
        SYNTAX 		INTEGER(0..7)
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The local priority."  
        DEFVAL { 0 }
        ::= { QTECHMplsQosTunnelExpMappingEntry 2 }    

QTECHMplsQosTunnelExpMappingColor OBJECT-TYPE
        SYNTAX 	   INTEGER {green(1),yellow(2),red(3)}	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The color value."
        DEFVAL { null }
        ::= { QTECHMplsQosTunnelExpMappingEntry 3 } 

-----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosVcExpMappingTable----
-----------------------------------------------------------
QTECHMplsQosVcExpMappingTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosVcExpMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "A mapping table of VC EXP to local priority."
        ::= { QTECHMplsQosObjects 6 }

QTECHMplsQosVcExpMappingEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosVcExpMappingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry for VC EXP to local priority mapping table."
        INDEX { QTECHMplsQosVcExpMappingExp }
        ::= { QTECHMplsQosVcExpMappingTable 1 }           

QTECHMplsQosVcExpMappingEntry ::= SEQUENCE {
        QTECHMplsQosVcExpMappingExp	           INTEGER,
        QTECHMplsQosVcExpMappingLocalPriority     INTEGER,  
        QTECHMplsQosVcExpMappingColor             INTEGER
       }

QTECHMplsQosVcExpMappingExp OBJECT-TYPE
        SYNTAX      INTEGER(0..7)
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "The VC EXP value."
        ::= { QTECHMplsQosVcExpMappingEntry 1}
        
QTECHMplsQosVcExpMappingLocalPriority OBJECT-TYPE
        SYNTAX 		INTEGER(0..7)
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "The local priority." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosVcExpMappingEntry 2 }    
        
QTECHMplsQosVcExpMappingColor OBJECT-TYPE
        SYNTAX 	   INTEGER {green(1),yellow(2),red(3)}	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the color value."  
        DEFVAL { null }
        ::= { QTECHMplsQosVcExpMappingEntry 3 } 


-----------------------------------------------------------   
----QTECHMplsQosObjects group QTECHMplsQosTunnelLocalPriConfigTable---- 
-----------------------------------------------------------
 QTECHMplsQosTunnelLocalPriConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosTunnelLocalPriConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "The local priority configuration based on tunnel label."
        ::= { QTECHMplsQosObjects 7}

QTECHMplsQosTunnelLocalPriConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosTunnelLocalPriConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "table entry for local priority configuration based on tunnel label."
        INDEX { QTECHMplsQosTunnelLocalPriConfigTunnelName }
        ::= {QTECHMplsQosTunnelLocalPriConfigTable 1 }           

QTECHMplsQosTunnelLocalPriConfigEntry ::= SEQUENCE {
        QTECHMplsQosTunnelLocalPriConfigTunnelName	       SnmpAdminString,
        QTECHMplsQosTunnelLocalPriConfigLocalPriority     INTEGER, 
        QTECHMplsQosTunnelLocalPriConfigMode              INTEGER,
        QTECHMplsQosTunnelLocalPriConfigRowStatus         RowStatus
       }

QTECHMplsQosTunnelLocalPriConfigTunnelName OBJECT-TYPE
        SYNTAX      SnmpAdminString(SIZE(1..255))
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "the tunnel name."
        ::= { QTECHMplsQosTunnelLocalPriConfigEntry 1}
        
QTECHMplsQosTunnelLocalPriConfigLocalPriority OBJECT-TYPE
        SYNTAX 	   INTEGER(0..7)	
        MAX-ACCESS read-create
        STATUS     current
        DESCRIPTION
            "the local priority." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosTunnelLocalPriConfigEntry 2 }   
        
QTECHMplsQosTunnelLocalPriConfigMode OBJECT-TYPE
        SYNTAX 	   INTEGER{not-change(0),fixed(1),mapping(2)}
        MAX-ACCESS read-create
        STATUS     current
        DESCRIPTION
            "the local priority generate mode,
            not-change(0) means not change the local priority,
            fixed(1) means using the static config local priority value,
            mapping(2) means using exp mapping to local priority." 
        DEFVAL { mapping }
        ::= { QTECHMplsQosTunnelLocalPriConfigEntry 3 } 
        
QTECHMplsQosTunnelLocalPriConfigRowStatus OBJECT-TYPE
        SYNTAX 	   RowStatus
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "This object allows entries to be created and deleted in the QTECHMplsQosTunnelLocalPriConfigTable."
        ::= { QTECHMplsQosTunnelLocalPriConfigEntry 4 } 

-----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosVcLocalPriConfigTable---- 
-----------------------------------------------------------
 QTECHMplsQosVcLocalPriConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosVcLocalPriConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "The local priority configuration based on VC label."
        ::= { QTECHMplsQosObjects 8}

QTECHMplsQosVcLocalPriConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosVcLocalPriConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "table entry for local priority configuration based on VC label."
        INDEX { QTECHMplsQosVcLocalPriConfigVcId }
        ::= {QTECHMplsQosVcLocalPriConfigTable 1 }           

QTECHMplsQosVcLocalPriConfigEntry ::= SEQUENCE {
        QTECHMplsQosVcLocalPriConfigVcId	            INTEGER,
        QTECHMplsQosVcLocalPriConfigLocalPriority      INTEGER,  
        QTECHMplsQosVcLocalPriConfigMode               INTEGER, 
        QTECHMplsQosVcLocalPriConfigRowStatus          RowStatus
       }

QTECHMplsQosVcLocalPriConfigVcId OBJECT-TYPE
        SYNTAX      INTEGER(0..1048575)
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
            "the VC id value."
        ::= { QTECHMplsQosVcLocalPriConfigEntry 1}
        
QTECHMplsQosVcLocalPriConfigLocalPriority OBJECT-TYPE
        SYNTAX 	   INTEGER(0..7)	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the local priority."  
        DEFVAL { 0 }
        ::= { QTECHMplsQosVcLocalPriConfigEntry 2 } 

QTECHMplsQosVcLocalPriConfigMode OBJECT-TYPE
        SYNTAX 	   INTEGER{not-change(0),fixed(1),mapping(2)}	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the local priority generate mode,
            not-change(0) means not change the local priority,
            fixed(1) means using the static config local priority value,
            mapping(2) means using exp mapping to local priority." 
        DEFVAL { mapping }
        ::= { QTECHMplsQosVcLocalPriConfigEntry 3 } 


QTECHMplsQosVcLocalPriConfigRowStatus OBJECT-TYPE
        SYNTAX 	   RowStatus
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the local priority configuration based on VC label table row status."
        ::= { QTECHMplsQosVcLocalPriConfigEntry 4 }    
        
        
 -----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosPortStatusConfigTable---- 
-----------------------------------------------------------
 QTECHMplsQosPortStatusConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosPortStatusConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "The enable/disable of local pri to dscp or ip precedence in port."
        ::= { QTECHMplsQosObjects 9}

QTECHMplsQosPortStatusConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosPortStatusConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "table entry for the enable/disable of local pri to dscp or ip precedence in port."
        INDEX { QTECHMplsQosPortId }
        ::= {QTECHMplsQosPortStatusConfigTable 1 }           

QTECHMplsQosPortStatusConfigEntry ::= SEQUENCE {
        QTECHMplsQosPortId	            INTEGER,
        QTECHMplsQosPortLocalPriMappingToIpPrecedenceEnable      INTEGER,  
        QTECHMplsQosPortLocalPriMappingToDscpEnable               INTEGER
             }

QTECHMplsQosPortId OBJECT-TYPE
        SYNTAX      INTEGER
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
        	"An index that uniquely identifies a configuration about mpls qos."
        ::= { QTECHMplsQosPortStatusConfigEntry 1}
        
QTECHMplsQosPortLocalPriMappingToIpPrecedenceEnable OBJECT-TYPE
        SYNTAX 	   EnableVar	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "Indicates whether the internal priority mapping to IP precedence is enabled on the port.
            enable(1) means that the mapping is enabled on this device.
            disable(2) means that the mapping is disabled." 

        DEFVAL { disable }
        ::= { QTECHMplsQosPortStatusConfigEntry 2 } 

QTECHMplsQosPortLocalPriMappingToDscpEnable OBJECT-TYPE
        SYNTAX 	   EnableVar	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "Indicates whether the internal priority mapping to IP dscp is enabled on the port.
            enable(1) means that the mapping is enabled on this device.
            disable(2) means that the mapping is disabled." 
        DEFVAL { disable }
        ::= { QTECHMplsQosPortStatusConfigEntry 3 }    
        
        
 -----------------------------------------------------------   
 ----QTECHMplsQosObjects group QTECHMplsQosPortLocalPriMappingConfigTable---- 
-----------------------------------------------------------
 QTECHMplsQosPortLocalPriMappingConfigTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHMplsQosPortLocalPriMappingConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "The enable/disable of local pri to dscp or ip precedence in port."
        ::= { QTECHMplsQosObjects 10}

QTECHMplsQosPortLocalPriMappingConfigEntry OBJECT-TYPE
        SYNTAX QTECHMplsQosPortLocalPriMappingConfigEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "table entry for the enable/disable of local pri to dscp or ip precedence in port."
        INDEX { QTECHMplsQosPortLocalPriMappingPortId, QTECHMplsQosPortLocalPriMappingLocalPri }
        ::= {QTECHMplsQosPortLocalPriMappingConfigTable 1 }           

QTECHMplsQosPortLocalPriMappingConfigEntry ::= SEQUENCE {
        QTECHMplsQosPortLocalPriMappingPortId	            INTEGER,
        QTECHMplsQosPortLocalPriMappingLocalPri      INTEGER,  
        QTECHMplsQosPortLocalPriMappingDscp               INTEGER ,
        QTECHMplsQosPortLocalPriMappingIpPre          INTEGER 

             }

QTECHMplsQosPortLocalPriMappingPortId OBJECT-TYPE
        SYNTAX      INTEGER
        MAX-ACCESS  not-accessible
        STATUS      current
        DESCRIPTION
        	"An index that uniquely identifies a configuration about mpls qos."
        ::= { QTECHMplsQosPortLocalPriMappingConfigEntry 1}
        
QTECHMplsQosPortLocalPriMappingLocalPri OBJECT-TYPE
        SYNTAX 	   INTEGER(0..7)
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
        	"An index that uniquely identifies a configuration about mpls qos."
        ::= { QTECHMplsQosPortLocalPriMappingConfigEntry 2 } 

QTECHMplsQosPortLocalPriMappingDscp OBJECT-TYPE
        SYNTAX 	   INTEGER(0..63)	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the dscp value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosPortLocalPriMappingConfigEntry 3 } 
        
QTECHMplsQosPortLocalPriMappingIpPre OBJECT-TYPE
        SYNTAX 	   INTEGER(0..7)	
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "the IP precedence value." 
        DEFVAL { 0 }
        ::= { QTECHMplsQosPortLocalPriMappingConfigEntry 4 }     

 
END

