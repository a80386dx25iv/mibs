--MibName=QTECHPortStatistics
-- *****************************************************************
-- QTECH-PORTSTATISTIC-MIB.mi2:  QTECH port statistic MIB file
--
-- Nov 2004, gaohongwei
--
-- Copyright (c) 1994-2000, 2004 by QTECH, Inc.
-- All rights reserved.
-- 
-- *****************************************************************
SWITCH-PORTSTATISTIC-MIB  DEFINITIONS ::= BEGIN
 
IMPORTS
       QTECHSwitch         				FROM QTECH-BASE-MIB 
       TruthValue  	FROM SNMPv2-TC
       NOTIFICATION-TYPE,Counter64      FROM SNMPv2-SMI; 
                
	QTECHPortStatistics MODULE-IDENTITY
        LAST-UPDATED    "200412200000Z"
        ORGANIZATION    "QTECH, Inc." 
        CONTACT-INFO
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 86-010-82884499

                E-mail: surport@QTECH.com"
        DESCRIPTION
                "port statistic mib."
                ::= { QTECHSwitch 7}
-------------------------------------------------------------------------
  
    QTECHPortStatsTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHPortStatsEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "A table of port statistic."
        ::= { QTECHPortStatistics 1 }

    QTECHPortStatsEntry OBJECT-TYPE
        SYNTAX QTECHPortStatsEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry of port statistic."
        INDEX { QTECHStatsPortIndex }
        ::= { QTECHPortStatsTable 1 }

    QTECHPortStatsEntry ::= SEQUENCE {
        QTECHStatsPortIndex               	INTEGER,
        QTECHStatsInPacket			Counter64,
        QTECHStatInPktType			INTEGER,
        QTECHStatsOutPacket		Counter64,
        QTECHStatOutPktType		INTEGER,
        QTECHStatsClear			TruthValue
        }      
     
   	QTECHStatsPortIndex OBJECT-TYPE
        SYNTAX INTEGER 
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "An index based 1 that uniquely identifies a system interface."
        ::= { QTECHPortStatsEntry 1 }
                    
   	QTECHStatsInPacket OBJECT-TYPE
        SYNTAX  Counter64 
        ACCESS read-only
        STATUS current
        DESCRIPTION
                "count of packets received. "
        ::= { QTECHPortStatsEntry 2 }   
                        
        QTECHStatInPktType OBJECT-TYPE
    	SYNTAX      INTEGER {
                    		goodPacket(1),
			        badPacket(2),
			        localPacket(3)
                    } 
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "port ingress packet Statistic type"  
    	::= { QTECHPortStatsEntry 3 }           
   	 
                                     
   	QTECHStatsOutPacket OBJECT-TYPE
        SYNTAX  Counter64 
        ACCESS read-only
        STATUS current
        DESCRIPTION
                "count of packets transmitted. "
        ::= { QTECHPortStatsEntry 4 }   
                                   
  	QTECHStatOutPktType OBJECT-TYPE
   	SYNTAX      INTEGER {
                    		goodPacket(1),
			        badPacket(2),
			        abortPacket(3)
                    } 
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "port egress packet Statistic type"  
    	::= { QTECHPortStatsEntry 5 } 	
        
        QTECHStatsClear OBJECT-TYPE
    	SYNTAX      TruthValue
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "clear port statistics"
   	::= { QTECHPortStatsEntry 6 }       
   	
  --  QTECHPortStatsAllBitsTable

   	QTECHPortStatsAllBitsTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHPortStatsAllBitsEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "A table of port statistic about all bits transmitted or received."
        ::= { QTECHPortStatistics 2 }

    QTECHPortStatsAllBitsEntry OBJECT-TYPE
        SYNTAX QTECHPortStatsAllBitsEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Table entry of port statistic about all bits transmitted or received."
        INDEX { QTECHStatsPortIndex }
        ::= { QTECHPortStatsAllBitsTable 1 }

    QTECHPortStatsAllBitsEntry ::= SEQUENCE {
        QTECHStatsInAllBits              Counter64,
        QTECHStatsOutAllBits             Counter64
        }        
        
   QTECHStatsInAllBits OBJECT-TYPE
       SYNTAX      Counter64
       MAX-ACCESS  read-only
       STATUS      current
       DESCRIPTION
               "The total number of bits received on the interface,
               including framing characters.

               Discontinuities in the value of this counter can occur
               at re-initialization of the management system, and at
               other times as indicated by the value of
               ifCounterDiscontinuityTime."
       ::= { QTECHPortStatsAllBitsEntry 1 }   
        
    QTECHStatsOutAllBits OBJECT-TYPE
       SYNTAX      Counter64
       MAX-ACCESS  read-only
       STATUS      current
       DESCRIPTION
               "The total number of bits transmitted out of the
               interface, including framing characters.

               Discontinuities in the value of this counter can occur
               at re-initialization of the management system, and at
               other times as indicated by the value of
               ifCounterDiscontinuityTime."
       ::= { QTECHPortStatsAllBitsEntry 2 } 	
END
