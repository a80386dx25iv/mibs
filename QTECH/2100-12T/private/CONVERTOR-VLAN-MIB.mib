--MibName=QTECHmcVlanConfig
-- =======================================================================
-- Version info 
--
-- Version 1.0 Created 2005.07.27 by LONGYAGN
-- This version of MIB is created just for the use of Network Management Systems
-- display and set the system configuration of convertor .
--
-- Copyright(c) 2002-2005 by QTECH TECH, Ltd. 
-- =======================================================================



CONVERTOR-VLAN-MIB DEFINITIONS ::= BEGIN

	IMPORTS
        MODULE-IDENTITY 
                	FROM SNMPv2-SMI
		OBJECT-TYPE     
					FROM SNMPv2-SMI
		QTECHMediaConvertor    
					FROM QTECH-BASE-MIB
        PortList,EnableVar	
                    FROM SWITCH-TC;

   QTECHmcVlanConfig MODULE-IDENTITY
    
        LAST-UPDATED    "200507270000Z"  
        ORGANIZATION    "QTECH Science & Technology Co., ltd"
        CONTACT-INFO    "QTECH Science & Technology Co., ltd.
                         E-mail: support@QTECH.com"

        DESCRIPTION     "QTECH Convertor system Enterprise MIB definition."
        ::= { QTECHMediaConvertor 2 }      
        
	QTECHmcVlanCoreTagType  OBJECT-TYPE
        	SYNTAX  	INTEGER(0..65535) 
        	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"The value of core tag type."
        	DEFVAL  { 37120 }
        	::= { QTECHmcVlanConfig 1 }
  		
     QTECHmcVlanSwitchMode	OBJECT-TYPE
         	SYNTAX		INTEGER
         	{
            	transparent(1),
        		dot1q-vlan(2),
        		double-tagged-vlan(3)
         	}
         	MAX-ACCESS  read-write
         	STATUS      current
         	DESCRIPTION
           			"The switch mode of the device is transparent(1) or vlan(2)."
        	::= { QTECHmcVlanConfig 2 } 
          
     QTECHmcVlanPortConfigTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHmcVlanPortConfigEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of ports' vlan information and associated properties."
			::=	{  QTECHmcVlanConfig 3 } 
				
	QTECHmcVlanPortConfigEntry  OBJECT-TYPE
			SYNTAX		QTECHmcVlanPortConfigEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"Table entry for ports' vlan information."
			INDEX {	QTECHmcVlanPortIndex }
			::=	{ QTECHmcVlanPortConfigTable 1 } 	  
						            
	QTECHmcVlanPortConfigEntry ::= SEQUENCE {
			QTECHmcVlanPortIndex       			 Integer32,
			QTECHmcVlanNative     					 INTEGER,
			QTECHmcVlanNativeOverride    			 EnableVar,
			QTECHmcVlanDoubleTagEnable      		 EnableVar,
			QTECHmcVlanIngressFilter				 EnableVar,
			QTECHmcVlanAcceptFrameType     	   	 INTEGER,  
			QTECHmcVlanEgressDefault      		     INTEGER
			}
        
     QTECHmcVlanPortIndex  OBJECT-TYPE
 	      	SYNTAX 	  	Integer32 
	      	MAX-ACCESS  not-accessible
	      	STATUS      current
 	      	DESCRIPTION
           			"An index based 1 that uniquely identifies a port entry."
          	::= { QTECHmcVlanPortConfigEntry 1 }    
          	
     QTECHmcVlanNative  OBJECT-TYPE
        	SYNTAX  	INTEGER(1..4094) 
        	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"Native vlan."
        	::= { QTECHmcVlanPortConfigEntry 2 }
     
     QTECHmcVlanNativeOverride	OBJECT-TYPE
        	SYNTAX  	EnableVar        
        	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"If enable, set all ingress packets' vlan ID  
                	 to native vlan. Else, do nothing."
        	DEFVAL  { disable }
        	::= { QTECHmcVlanPortConfigEntry 3 }
	     
	 QTECHmcVlanDoubleTagEnable   OBJECT-TYPE
        	SYNTAX  	EnableVar        
        	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"Set double tag to enable(1) or disable(2)."
        	DEFVAL  { disable }
        	::= { QTECHmcVlanPortConfigEntry 4 } 

     QTECHmcVlanIngressFilter  OBJECT-TYPE
  	        SYNTAX  INTEGER
 	        {
 	        	none(1),
                notmember(2),
                unkown(3)
            }
         	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"Ingress filter packets type."
         	::= { QTECHmcVlanPortConfigEntry 5 }
    	
     QTECHmcVlanAcceptFrameType     OBJECT-TYPE
 	        SYNTAX  INTEGER
 	        {
 	        	all(1),
                tag(2),
                untag(3)
            }
         	MAX-ACCESS 	read-write
        	STATUS 		current
        	DESCRIPTION
                	"Accept frame type."
         	::= { QTECHmcVlanPortConfigEntry 6 }
  	  
	 QTECHmcVlanEgressDefault   OBJECT-TYPE
        	SYNTAX  INTEGER
        	{
        		unmodify(1),
        	 	tag(2),
            	untag(3),
            	disable(4)
			}
        	MAX-ACCESS 	read-write
        	STATUS 		current   
        	DESCRIPTION
            		"The object indicates how to process egress packets 
            		if their vlan dosen't exist."
        	DEFVAL  { unmodify }
        	::= { QTECHmcVlanPortConfigEntry 7 }
            
END  
