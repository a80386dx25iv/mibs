--MibName=QTECHLinkStateTrack
-- *****************************************************************
-- switch-linkStateTrack-mib.MIB:	 switch link state track MIB file
--
-- feb 2008, zhangjun 
-- 修改
-- 01，20110420，yangkewei, ESW00001260，增加QTECHLinkStateTrackGroupTable中的 QTECHLinkStateTrackMep 和
--						QTECHLinkStateTrackElps8031LinkId  取值范围  ，修改 QTECHLinkStateTrackSourceFault权限为read-only
-- Copyright (c) 1994-2004,	2001 by	QTECH, Inc.
-- All rights reserved.
--
-- *****************************************************************

SWITCH-LINKSTATETRACK-MIB DEFINITIONS ::= BEGIN

IMPORTS
        QTECHSwitch			
         				FROM QTECH-BASE-MIB
        RowStatus,TruthValue
                FROM SNMPv2-TC
        EnableVar	
                FROM SWITCH-TC
        InterfaceIndexOrZero     
                FROM IF-MIB
        ifIndex  
        		    FROM IF-MIB;
                 
     QTECHLinkStateTrack MODULE-IDENTITY
        LAST-UPDATED    "200802280000Z"
        ORGANIZATION    "QTECH, Inc."
        CONTACT-INFO
                "QTECH Systems
                        
                Postal: Beijing,
                        China

                Tel: 86-010-82884499

                E-mail: zhangjun@QTECH.com"
        DESCRIPTION
                "description of link state track group manage object."
                ::= { QTECHSwitch 33}  
                
-- QTECHLinkStateTrackGroupTable

	QTECHLinkStateTrackGroupTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHLinkStateTrackGroupEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "A table of link state track group"
    ::={ QTECHLinkStateTrack 1 }
    

	QTECHLinkStateTrackGroupEntry OBJECT-TYPE
    SYNTAX      QTECHLinkStateTrackGroupEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "An entry in the QTECHLinkStateTrackGroupTable provides objects .
        "
    INDEX        { QTECHLinkStateTrackGroupIndex }
    ::={ QTECHLinkStateTrackGroupTable 1 }
    
	QTECHLinkStateTrackGroupEntry ::= SEQUENCE {
		QTECHLinkStateTrackGroupIndex  INTEGER,
		QTECHLinkStateTrackState	INTEGER,
		QTECHLinkStateTrackGroupEnable	INTEGER,
		QTECHLinkStateTrackRowStatus	RowStatus,
		QTECHLinkStateTrackMep INTEGER,
		QTECHLinkStateTrackElps8031LinkId INTEGER,
		QTECHLinkStateTrackSourceFault INTEGER
	}


	QTECHLinkStateTrackGroupIndex OBJECT-TYPE
		SYNTAX	INTEGER(1..10)
		ACCESS	not-accessible
		STATUS	current
		DESCRIPTION
			"An index that uniquely identifies an entry in
             the link state track group table."
		::=	{ QTECHLinkStateTrackGroupEntry 1 }


	QTECHLinkStateTrackState OBJECT-TYPE
		SYNTAX	INTEGER { normal(1), failover(2) }
		ACCESS	read-only
		STATUS	current
		DESCRIPTION
			"The state of link state tracking for a Link-State-Tracking group, Normal or Failover"
		::=	{ QTECHLinkStateTrackGroupEntry 2 }
		
	QTECHLinkStateTrackGroupEnable OBJECT-TYPE
		SYNTAX	EnableVar
		ACCESS	read-create
		STATUS	current
		DESCRIPTION
			"The state of this link state track group, Enable or Disable"
		::=	{ QTECHLinkStateTrackGroupEntry 3 }
		
	QTECHLinkStateTrackRowStatus OBJECT-TYPE
		SYNTAX	RowStatus
		ACCESS	read-create
		STATUS	current
		DESCRIPTION
			"The status	of this	entry"
		::=	{ QTECHLinkStateTrackGroupEntry 4 }
	
	QTECHLinkStateTrackMep OBJECT-TYPE
		SYNTAX	INTEGER (1..8191)
		ACCESS	read-create
		STATUS	deprecated
		DESCRIPTION
			"The UpStream Mep. If the fault group is not based on mep, it is 0."
		::=	{ QTECHLinkStateTrackGroupEntry 5 }
		
	QTECHLinkStateTrackElps8031LinkId OBJECT-TYPE
		SYNTAX	INTEGER (1..8)
		ACCESS	read-create
		STATUS	deprecated
		DESCRIPTION
			"The UpStream 8031LinkId. If the fault group is not based on mep, it is 0."
		::=	{ QTECHLinkStateTrackGroupEntry 6 }
		
	QTECHLinkStateTrackSourceFault OBJECT-TYPE
		SYNTAX	INTEGER
		ACCESS	read-only
		STATUS	current
		DESCRIPTION
			"The Fault reason.0:no fault;1:shutdown,2:mep shutdown;3:mep vlan stg change;4:mep lost;5:elps link change."
		::=	{ QTECHLinkStateTrackGroupEntry 7 }	
		
-- QTECHLinkStateTrackIfTable

	QTECHLinkStateTrackIfTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHLinkStateTrackIfEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "A table of link state track interface config"
    ::={ QTECHLinkStateTrack 2 }
    
  QTECHLinkStateTrackIfEntry OBJECT-TYPE
    SYNTAX      QTECHLinkStateTrackIfEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "An entry in the QTECHLinkStateTrackIfTable provides objects.
        "
    INDEX        { ifIndex }
    ::={ QTECHLinkStateTrackIfTable 1 }
    
    QTECHLinkStateTrackIfEntry ::= SEQUENCE {
		QTECHLinkStateTrackIfOwerGroup	INTEGER,
		QTECHLinkStateTrackIfStreamType	INTEGER,
		QTECHLinkStateTrackIfFaultProcesseMode INTEGER,
		QTECHLinkStateTrackIfModifyPvid INTEGER,
		QTECHLinkStateTrackIfDeleteVlan INTEGER,
		QTECHLinkStateTrackIfSuspendVlan INTEGER
	}
			
	QTECHLinkStateTrackIfOwerGroup OBJECT-TYPE
		SYNTAX	INTEGER(0..10)
		ACCESS	read-write
		STATUS	current
		DESCRIPTION
			"link state group number of the interface.
			 Set '0' means that the port dose not belong to any group."
		::=	{ QTECHLinkStateTrackIfEntry 2 }
		
	QTECHLinkStateTrackIfStreamType OBJECT-TYPE
		SYNTAX	INTEGER { none(0), upstream(1), downstream(2) }
		ACCESS	read-write
		STATUS	current
		DESCRIPTION
			"Interface type in the link state track group,Upstream or Downstream."
		::=	{ QTECHLinkStateTrackIfEntry 3 }		

	QTECHLinkStateTrackIfFaultProcesseMode OBJECT-TYPE
		SYNTAX	INTEGER
		ACCESS	read-write
		STATUS	current
		DESCRIPTION
			"DownStream fault processing mode.1:shutdown;2:trap-only;3:delete port vlan;4:suspend vlan;5:modify pvid"
		::=	{ QTECHLinkStateTrackIfEntry 4 }	

	QTECHLinkStateTrackIfModifyPvid OBJECT-TYPE
		SYNTAX	INTEGER
		ACCESS	read-write
		STATUS	deprecated
		DESCRIPTION
			"Pvid value. If fault process mode is not 5, it is 0."
		::=	{ QTECHLinkStateTrackIfEntry 5 }	

	QTECHLinkStateTrackIfDeleteVlan OBJECT-TYPE
		SYNTAX	INTEGER
		ACCESS	read-write
		STATUS	deprecated
		DESCRIPTION
			"Vlan value.If fault process mode is not 3, it is 0."
		::=	{ QTECHLinkStateTrackIfEntry 6 }	

	QTECHLinkStateTrackIfSuspendVlan OBJECT-TYPE
		SYNTAX	INTEGER
		ACCESS	read-write
		STATUS	deprecated
		DESCRIPTION
			"Vlan value. If fault process mode is not 4, it is 0."
		::=	{ QTECHLinkStateTrackIfEntry 7 }	

-- trap notification 
QTECHLinkStateTrackNotifications           OBJECT IDENTIFIER ::= { QTECHLinkStateTrack 3 }
QTECHLinkStateTrackFault
NOTIFICATION-TYPE
    OBJECTS { QTECHLinkStateTrackIfOwerGroup, QTECHLinkStateTrackState, QTECHLinkStateTrackSourceFault}
    STATUS      current
    DESCRIPTION
       "When finding fault or restoring fault, it need send trap."
    ::= { QTECHLinkStateTrackNotifications 1 }
END
	
