--MibName=QTECHWebServer
-- *****************************************************************
-- QTECH-webserver-mib.mib:  QTECH WEB SERVER MIB file
--
-- Aug 2011, zhangjiguang
--
-- Copyright (c) 1994-2000, 2001 by QTECH, Inc.
-- All rights reserved.
-- 
-- *****************************************************************

QTECH-WEBSERVER-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE,
    Integer32     	 			           FROM SNMPv2-SMI    
    EnableVar	                        FROM SWITCH-TC  
    QTECHAgent                    FROM QTECH-BASE-MIB;         
    QTECHWebServer MODULE-IDENTITY
    LAST-UPDATED "201102180000Z"
    ORGANIZATION "QTECH Group"
    CONTACT-INFO
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 86-010-82884499

                E-mail: zhangjiguang@QTECH.com"
    DESCRIPTION
            "The MIB module for Web Module."
    ::= { QTECHAgent 46}

QTECHWebServerNotifications	OBJECT IDENTIFIER ::= {	QTECHWebServer 1 }
QTECHWebServerObjects			OBJECT IDENTIFIER ::= {	QTECHWebServer 2 }   
QTECHWebServerConformance		OBJECT IDENTIFIER ::= {	QTECHWebServer 3 }  

QTECHWebServerScalar		OBJECT IDENTIFIER ::= {	QTECHWebServerObjects 1 }
QTECHHttpClientsTable		OBJECT IDENTIFIER ::= {	QTECHWebServerObjects 2 }

--
-- QTECHWebServerScalar
--

QTECHHttpServerEnable OBJECT-TYPE
        SYNTAX EnableVar
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The state of global http server. 
             It has two values,one is enable(1),which indicates that the system start http server; 
             the other is disable(2) that means web server is invalid in this system. 
             The default value is disable(2)."
        DEFVAL { disable }
        ::= { QTECHWebServerScalar 1 }

QTECHHttpsServerEnable OBJECT-TYPE
        SYNTAX EnableVar
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The state of global https server. 
             It has two values,one is enable(1),which indicates that the system start https server; 
             the other is disable(2) that means https is invalid in this system. 
             The default value is disable(2)."
        DEFVAL { disable }
        ::= { QTECHWebServerScalar 2 }

QTECHHttpPort OBJECT-TYPE           
        SYNTAX        INTEGER(0..65535)  
		  MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The port value for http service.  
               The default value is 80, the vlaue user set must be between 0 to 65535."
        DEFVAL { 80 }
        ::= { QTECHWebServerScalar 3 }

QTECHHttpsPort OBJECT-TYPE           
        SYNTAX        INTEGER(0..65535)  
		  MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The port value for https service.  
               The default value is 443, the vlaue user set must be between 0 to 65535."
        DEFVAL { 443 }
        ::= { QTECHWebServerScalar 4 }

QTECHHttpAuth OBJECT-TYPE           
       SYNTAX INTEGER
                {
                    local(1),
                    radius (2),
                    tacacs(3)                   
                }
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
             "The method of web user authentication.
             The type local(1) is for the local authentication. 
             The type radius(2) is for the radius authentication.
             The type tacacs(3) is for the tacacs authentication.
             The default type is local."            
        DEFVAL { 1 }
        ::= { QTECHWebServerScalar 5 }
        
QTECHHttpTimeout OBJECT-TYPE           
        SYNTAX INTEGER(300..1200)                
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
           "if the client do nothing within this timeout after logon, he must re-logon after the time out."
        DEFVAL { 400 }
        ::= { QTECHWebServerScalar 6 }
   
--        
--QTECHHttpClientsTable
--            
          QTECHHttpClientsTable OBJECT-TYPE
              SYNTAX  SEQUENCE OF QTECHHttpClientsEntry
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "A table that contains web client information, include ID, IP, name."
              ::= { QTECHWebServerObjects 2 }
              
          QTECHHttpClientsEntry OBJECT-TYPE
              SYNTAX  QTECHHttpClientsEntry
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "An entry that contains web client information, include ID, IP, name."
              INDEX   { QTECHHttpClientsIndex }
              ::= { QTECHHttpClientsTable 1 }
            
          QTECHHttpClientsEntry ::=
              SEQUENCE {
                  QTECHHttpClientsIndex	   INTEGER,
                  QTECHHttpClientsID           INTEGER,
                  QTECHHttpClientsIP           IpAddress,
                  QTECHHttpClientsName      OCTET STRING
                  }
               
          QTECHHttpClientsIndex OBJECT-TYPE
              SYNTAX  INTEGER 
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "The index of web client."
              ::= { QTECHHttpClientsEntry 1 }
              
          QTECHHttpClientsID OBJECT-TYPE
              SYNTAX  INTEGER
              MAX-ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The ID of web client."
              ::= { QTECHHttpClientsEntry 2 }
              
           QTECHHttpClientsIP OBJECT-TYPE
              SYNTAX  IpAddress 
              MAX-ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The ip address of web client."
              ::= { QTECHHttpClientsEntry 3 }
              
          QTECHHttpClientsName OBJECT-TYPE
              SYNTAX  OCTET STRING
              MAX-ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The name of web client."
              ::= { QTECHHttpClientsEntry 4 }
  
END
