--MibName=QTECHRcmp
QTECH-RCMP-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE     FROM SNMPv2-SMI
    MacAddress,
    RowStatus          		     FROM SNMPv2-TC
    EnableVar					FROM SWITCH-TC
    QTECHAgent				FROM QTECH-BASE-MIB;

QTECHCluster OBJECT IDENTIFIER ::= {QTECHAgent 6}

QTECHRcmp MODULE-IDENTITY
    LAST-UPDATED "0412210000Z"
    ORGANIZATION "IETF SNMPv2 Working Group"
    CONTACT-INFO
            "
             Phone:  01082884499
             Email:  support@QTECH.com"
    DESCRIPTION
            "The MIB module for managing  rcmp protocol."
    REVISION      "0412210000Z"
    DESCRIPTION
            ""
    ::= {QTECHCluster 1}

QTECHRcmpClusterEnable OBJECT-TYPE
     SYNTAX  EnableVar
     ACCESS read-write
     STATUS current
     DESCRIPTION
        "DURABLE: enable(1) or disable(2) rcmp protocol on the switch."
     DEFVAL  { disable }
        ::= { QTECHRcmp 1 }


QTECHRcmpIdentity OBJECT-TYPE
    SYNTAX      INTEGER {
                    member(1),
                    candidate(2),
                    commander(3)
                    }
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The identity of the switch."
    ::= { QTECHRcmp 2 }

QTECHRcmpCommanderMac OBJECT-TYPE
    SYNTAX      MacAddress
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The mac address of the commander."
    ::= { QTECHRcmp 3 }

QTECHRcmpAutoActiveEnable OBJECT-TYPE
    SYNTAX  EnableVar
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Enable auto active function."
    DEFVAL  { disable }
    ::= { QTECHRcmp 4 }

QTECHRcmpAutoActiveCommanderMac OBJECT-TYPE
    SYNTAX      MacAddress
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The mac address of the commander that can auto active it'self."
    ::= { QTECHRcmp 5 }


-- QTECHRcmpMemberTable

QTECHRcmpMemberTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHRcmpMemberEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Cluster member talbe."
    ::= { QTECHRcmp 6 }

QTECHRcmpMemberEntry OBJECT-TYPE
    SYNTAX      QTECHRcmpMemberEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Cluster member table entry."
    INDEX   {QTECHRcmpMacAddress}
    ::= { QTECHRcmpMemberTable 1 }

QTECHRcmpMemberEntry ::= SEQUENCE {
        QTECHRcmpMacAddress 		MacAddress,
        QTECHRcmpHostName     	OCTET STRING,
        QTECHRcmpActiveEnable   	EnableVar,
        QTECHRcmpOperationState	INTEGER,
        QTECHRcmpUdpPortNumber    	INTEGER,
        QTECHRcmpUserName		OCTET STRING,
        QTECHRcmpPassword		OCTET STRING,
        QTECHRcmpRowStatus		RowStatus
        }

QTECHRcmpMacAddress OBJECT-TYPE
    SYNTAX      MacAddress
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
    "The mac addr of the member."
    ::= { QTECHRcmpMemberEntry 1 }

QTECHRcmpHostName OBJECT-TYPE
    SYNTAX    OCTET STRING
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "The hostname of the member."
    ::= { QTECHRcmpMemberEntry 2 }

QTECHRcmpActiveEnable OBJECT-TYPE
    SYNTAX  EnableVar
    MAX-ACCESS read-write
    STATUS current
    DESCRIPTION
        "The hostname of the member."
    ::=  { QTECHRcmpMemberEntry 3 }

QTECHRcmpOperationState OBJECT-TYPE
    SYNTAX  INTEGER {
                 down(1),
                 up(2)
                }
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The operation state of the member."
    ::=  { QTECHRcmpMemberEntry 4 }

QTECHRcmpUdpPortNumber OBJECT-TYPE
    SYNTAX  INTEGER
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The udp port of the member."
    ::=  { QTECHRcmpMemberEntry 5 }

QTECHRcmpUserName OBJECT-TYPE
    SYNTAX   OCTET STRING (SIZE (1..16))
    MAX-ACCESS read-write
    STATUS current
    DESCRIPTION
        "The user name of the member."
    ::=  { QTECHRcmpMemberEntry 6 }

QTECHRcmpPassword OBJECT-TYPE
    SYNTAX   OCTET STRING (SIZE (1..48))
    MAX-ACCESS read-write
    STATUS current
    DESCRIPTION
        "The password of the member."
    ::=  { QTECHRcmpMemberEntry 7 }

QTECHRcmpRowStatus OBJECT-TYPE
    SYNTAX  RowStatus
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "The row status  of the member table."
    ::=  { QTECHRcmpMemberEntry 8 }

QTECHRcmpSessionTimeout OBJECT-TYPE
    SYNTAX      INTEGER (120..600)
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Cluster session timeout time."
    ::= { QTECHRcmp 7 }

QTECHRcmpMaxSession OBJECT-TYPE
    SYNTAX      INTEGER (1..2048)
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Max sessions of the Cluster."
    ::= { QTECHRcmp 8 }

QTECHRcmpMaxSessionPerMember OBJECT-TYPE
    SYNTAX      INTEGER (1..50)
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Max sessions of a  cluster member."
    ::= { QTECHRcmp 9 }

QTECHRcmpMaxMember OBJECT-TYPE
    SYNTAX      INTEGER (1..512)
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "Max number of the Cluster member."
    ::= { QTECHRcmp 10 }

QTECHRcmpID OBJECT-TYPE
    SYNTAX      MacAddress
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "Cluster member ID."
    ::= { QTECHRcmp 11 }

QTECHRcmpStatisticsTotalSession OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "Total cluster sessions."
    ::= { QTECHRcmp 12 }

QTECHRcmpStatisticsCurrentSession OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "current cluster sessions."
    ::= { QTECHRcmp 13 }

QTECHRcmpStatisticsMaxSession OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "max cluster sessions."
    ::= { QTECHRcmp 14 }

QTECHRcmpStatisticsTimeoutSession OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "timeout cluster sessions."
    ::= { QTECHRcmp 15 }

QTECHRcmpStatisticsInPkts OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "incoming cluster  packets."
    ::= { QTECHRcmp 16 }

QTECHRcmpStatisticsOutPkts OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "outgoing cluster  packets."
    ::= { QTECHRcmp 17 }

QTECHRcmpStatisticsDiscardPkts OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "discard cluster  packets."
    ::= { QTECHRcmp 18 }

QTECHRcmpStatisticsErrorPkts OBJECT-TYPE
    SYNTAX      Counter
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "error cluster packets."
    ::= { QTECHRcmp 19 }

QTECHRcmpDefaultVlan   OBJECT-TYPE
    SYNTAX      INTEGER
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "default vlan for cluster."
    ::= { QTECHRcmp 20 }

QTECHRcmpClearStatistics OBJECT-TYPE
    SYNTAX  INTEGER {
                 clear(1)
               }
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "clear statistics."
    ::= { QTECHRcmp 21 }
END

