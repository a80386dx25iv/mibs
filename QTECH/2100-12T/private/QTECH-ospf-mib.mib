--MibName=QTECHOspf
-- *****************************************************************
-- QTECH-OSPF-MIB.MIB
--
-- JULY. 2003, CHENYU
--
-- Copyright QTECH Telecom.
-- All rights reserved.
-- 
-- *****************************************************************--  

QTECH-OSPF-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE,IpAddress     FROM SNMPv2-SMI
    TEXTUAL-CONVENTION,RowStatus   FROM SNMPv2-TC
    QTECHSwitch	            	FROM QTECH-BASE-MIB;

QTECHOspf MODULE-IDENTITY
    LAST-UPDATED "0412200000Z"
    ORGANIZATION "QTECH"
    CONTACT-INFO
                 ""
    DESCRIPTION
                 "The MIB module for ospf."
    REVISION     "0412200000Z"
    DESCRIPTION
                 ""
    ::= { QTECHSwitch 18}      

-------textual convention-----------
AreaID ::= TEXTUAL-CONVENTION
    STATUS       current
    DESCRIPTION
            " An OSPF Area Identifier."
    SYNTAX  IpAddress
 
RouterID ::= TEXTUAL-CONVENTION
    STATUS       current
    DESCRIPTION
            " A OSPF Router Identifier."
    SYNTAX  IpAddress   
           
         
           
--  OSPF Interface MD5 Authentication Table
QTECHOspfIfMd5AuthTable OBJECT-TYPE
    SYNTAX SEQUENCE OF QTECHOspfIfMd5AuthEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Interface Authentication Table describes the parameters
           required for MD5 cryptographic Authentication."
    REFERENCE
        "OSPF Version 2, Appendix D.3  Cryptographic Authentication"
    ::=  { QTECHOspf 1 }

QTECHOspfIfMd5AuthEntry OBJECT-TYPE
    SYNTAX QTECHOspfIfMd5AuthEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Interface MD5 Auththentication Entry describes one  
          Authentication Key from the viewpoint of OSPF."
    INDEX {QTECHOspfIfMd5AuthIpAddress,QTECHOspfIfMd5AuthAddressLessIf,QTECHOspfIfMd5AuthKeyId}
    ::=  { QTECHOspfIfMd5AuthTable 1 }

QTECHOspfIfMd5AuthEntry ::= SEQUENCE {
    QTECHOspfIfMd5AuthIpAddress IpAddress,
    QTECHOspfIfMd5AuthAddressLessIf    INTEGER,
    QTECHOspfIfMd5AuthKeyId    INTEGER,
    QTECHOspfIfMd5AuthKey    OCTET STRING,
    QTECHOspfIfMd5AuthKeyRowStatus    RowStatus
    }

QTECHOspfIfMd5AuthIpAddress OBJECT-TYPE
    SYNTAX IpAddress
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The IP address of this OSPF interface."
    ::=  { QTECHOspfIfMd5AuthEntry 1 }

QTECHOspfIfMd5AuthAddressLessIf OBJECT-TYPE
    SYNTAX INTEGER (0..16)
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "For the purpose of easing  the  instancing  of
           addressed   and  addressless  interfaces;  This
           variable takes the value 0 on  interfaces  with
           IP  Addresses,  and  the corresponding value of
           ifIndex for interfaces having no IP Address."
    ::=  { QTECHOspfIfMd5AuthEntry 2 }

QTECHOspfIfMd5AuthKeyId OBJECT-TYPE
    SYNTAX INTEGER  (0..65535)
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        " This object identifies the secret key used to create the 
          message digest appended to the OSPF packet."
    ::=  { QTECHOspfIfMd5AuthEntry 3 }

QTECHOspfIfMd5AuthKey OBJECT-TYPE
    SYNTAX OCTET STRING (SIZE (0..16))
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        " This is the secret key which is used to create the 
          message digest appended to the OSPF packet."
    ::=  { QTECHOspfIfMd5AuthEntry 4 }



QTECHOspfIfMd5AuthKeyRowStatus OBJECT-TYPE
    SYNTAX RowStatus
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        " Identifies the status of the key "
    ::=  { QTECHOspfIfMd5AuthEntry 5 }



--  OSPF Virtual Interface Md5 Authentication Table

QTECHOspfVirtIfMd5AuthTable OBJECT-TYPE
    SYNTAX SEQUENCE OF QTECHOspfVirtIfMd5AuthEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Interface Authentication Table describes the parameters
           required for Md5 cryptographic Authentication."
    REFERENCE
        "OSPF Version 2, Appendix D.3  Cryptographic Authentication"
    ::=  { QTECHOspf 2 }

QTECHOspfVirtIfMd5AuthEntry OBJECT-TYPE
    SYNTAX QTECHOspfVirtIfMd5AuthEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Interface Md5 Auththentication Entry describes one  
          Authentication Key from the viewpoint of OSPF."
    INDEX {QTECHOspfVirtIfMd5AuthAreaId,QTECHOspfVirtIfMd5AuthNeighbor,QTECHOspfVirtIfMd5AuthKeyId}
    ::=  { QTECHOspfVirtIfMd5AuthTable 1 }

QTECHOspfVirtIfMd5AuthEntry ::= SEQUENCE {
    QTECHOspfVirtIfMd5AuthAreaId  AreaID,
    QTECHOspfVirtIfMd5AuthNeighbor    RouterID,
    QTECHOspfVirtIfMd5AuthKeyId    INTEGER,
    QTECHOspfVirtIfMd5AuthKey    OCTET STRING,
    QTECHOspfVirtIfMd5AuthKeyRowStatus    RowStatus
    }

QTECHOspfVirtIfMd5AuthAreaId OBJECT-TYPE
    SYNTAX AreaID
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The  Transit  Area  that  the   Virtual   Link
           traverses.  By definition, this is not 0.0.0.0."
    ::=  { QTECHOspfVirtIfMd5AuthEntry 1 }

QTECHOspfVirtIfMd5AuthNeighbor OBJECT-TYPE
    SYNTAX RouterID
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
               "The Router ID of the Virtual Neighbor."
    ::=  { QTECHOspfVirtIfMd5AuthEntry 2 }

QTECHOspfVirtIfMd5AuthKeyId OBJECT-TYPE
    SYNTAX INTEGER (0..65535)
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        " This object identifies the secret key used to create the 
          message digest appended to the OSPF packet."
    ::=  { QTECHOspfVirtIfMd5AuthEntry 3 }

QTECHOspfVirtIfMd5AuthKey OBJECT-TYPE
    SYNTAX OCTET STRING (SIZE (0..16))
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        " This is the secret key which is used to create the 
          message digest appended to the OSPF packet."
    ::=  { QTECHOspfVirtIfMd5AuthEntry 4 }



QTECHOspfVirtIfMd5AuthKeyRowStatus OBJECT-TYPE
    SYNTAX RowStatus
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        " Identifies the status of the key "
    ::=  { QTECHOspfVirtIfMd5AuthEntry 5}


--ospf nssa table
QTECHOspfNssaAreaTable OBJECT-TYPE
    SYNTAX SEQUENCE OF QTECHOspfNssaAreaEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Nssa Area Table describes nssa area's parameters."
    REFERENCE
        "OSPF Version 2, Appendix D.3  Cryptographic Authentication"
    ::=  { QTECHOspf 3 }

QTECHOspfNssaAreaEntry OBJECT-TYPE
    SYNTAX QTECHOspfNssaAreaEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "The OSPF Nssa Area Entry describes one  
          OSPF Nssa Area."
    INDEX {QTECHOspfNssaAreaId,QTECHOspfNssaTos}
    ::=  { QTECHOspfNssaAreaTable 1 }

QTECHOspfNssaAreaEntry ::= SEQUENCE {
    QTECHOspfNssaAreaId  AreaID,
    QTECHOspfNssaTos    INTEGER,
    QTECHOspfNssaDefaultCost    INTEGER,
    QTECHOspfNssaRowStatus     RowStatus
    }

QTECHOspfNssaAreaId OBJECT-TYPE
    SYNTAX AreaID
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The  Nssa  Area  Id."
    ::=  { QTECHOspfNssaAreaEntry 1 }  
    
QTECHOspfNssaTos   OBJECT-TYPE
    SYNTAX INTEGER (0..7)
    MAX-ACCESS read-only
    STATUS current
    DESCRIPTION
        "The  Nssa  Area  TOS."
    ::=  { QTECHOspfNssaAreaEntry 2 }         
    
QTECHOspfNssaDefaultCost   OBJECT-TYPE
    SYNTAX INTEGER  (0..16777215)
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "The  Type  of  Service  associated  with   the
 	metric.   On creation, this can be derived from
 	the instance."
    ::=  { QTECHOspfNssaAreaEntry 3 }  
                                                   
QTECHOspfNssaRowStatus   OBJECT-TYPE
    SYNTAX RowStatus
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "This variable displays the status of  the  en-
 	try.  Setting it to 'invalid' has the effect of
 	rendering it inoperative.  The internal  effect
 	(row removal) is implementation dependent."  
 	
    ::=  { QTECHOspfNssaAreaEntry 4 }   
 
------ospf config-------
QTECHOspfConfig  OBJECT IDENTIFIER ::= {QTECHOspf 4 }            
    
QTECHOspfDistance   OBJECT-TYPE
    SYNTAX INTEGER  (1..200)
    MAX-ACCESS read-write
    STATUS current
    DESCRIPTION
        "distance of ospf,value range is 1--200."  
 	DEFVAL { 110 }
    ::=  {QTECHOspfConfig 1 } 

--  OSPF Network Table

QTECHOspfNetworkTable OBJECT-TYPE
    SYNTAX SEQUENCE OF QTECHOspfNetworkEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "Before a interface startup ospf protocol,the interface must be added to an ospf area.
        The OSPF network Table add a ospf interface into an ospf area."
    ::=  { QTECHOspf 5 }                                                             

QTECHOspfNetworkEntry OBJECT-TYPE
    SYNTAX QTECHOspfNetworkEntry
    MAX-ACCESS not-accessible
    STATUS current
    DESCRIPTION
        "Before a interface startup ospf protocol,the interface must be added to an ospf area.
        The OSPF network Table add a ospf interface into an ospf area."
    INDEX {QTECHOspfNetworkIfIpAddress,QTECHOspfNetworkIfMask}
    ::=  { QTECHOspfNetworkTable 1 }

QTECHOspfNetworkEntry ::= SEQUENCE {
    QTECHOspfNetworkIfIpAddress IpAddress,           
    QTECHOspfNetworkIfMask      IpAddress,
    QTECHOspfNetworkAreaId    IpAddress,
    QTECHOspfNetworkRowStatus    RowStatus
    }

QTECHOspfNetworkIfIpAddress OBJECT-TYPE
    SYNTAX IpAddress
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "The IP address of this OSPF interface."
    ::=  { QTECHOspfNetworkEntry 1 }
                                     
QTECHOspfNetworkIfMask OBJECT-TYPE
    SYNTAX IpAddress
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "mask of this OSPF interface."
    ::=  { QTECHOspfNetworkEntry 2 }

QTECHOspfNetworkAreaId OBJECT-TYPE
    SYNTAX IpAddress
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        "an ospf area that the ospf interface is in."
    ::=  { QTECHOspfNetworkEntry 3 }

QTECHOspfNetworkRowStatus OBJECT-TYPE
    SYNTAX RowStatus
    MAX-ACCESS read-create
    STATUS current
    DESCRIPTION
        " This variable displays the status of  the  entry. "
    ::=  { QTECHOspfNetworkEntry 4 }

END
