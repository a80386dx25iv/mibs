--MibName=QTECHTrunk
SWITCH-TRUNK-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE     	FROM SNMPv2-SMI
    RowStatus  							FROM SNMPv2-TC 
    PortList,EnableVar					FROM SWITCH-TC     
    QTECHSwitch	            			FROM QTECH-BASE-MIB;


	QTECHTrunk MODULE-IDENTITY
    	LAST-UPDATED "200412200000Z"
    	ORGANIZATION "QTECH Group"
    	CONTACT-INFO
            	" 
             	Phone:  01082884499
             	Email:  support@QTECH.com"
    	DESCRIPTION
            	"The MIB module for managing  trunk  in QTECH2026."
    	REVISION      "9103310000Z"
    	DESCRIPTION
            ""
    	::= { QTECHSwitch 6}  
    	
------------------------------------------------------------------
	QTECHTrunkEnable OBJECT-TYPE
    	SYNTAX   EnableVar
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
              "Enable or disable trunking."
    	DEFVAL  { disable }
    	::= { QTECHTrunk 1 }

	QTECHTrunkLoadingSharingMode OBJECT-TYPE
    	SYNTAX      INTEGER {
                    		srcMAC(1),
			        destMAC(2),
			        srcXORDestMAC(3),
			        srcIP(4),
			        destIP(5),
			        srcXORDestIP(6),
			        SrcXORDestMACXORSrcPort(7)
                    } 
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            " The Rule to be applied to this Link Aggregator's Trunk Group
			Table. The rules are based on the following selections SrcMAC (1),
			means that the last three bits of the Source MAC Address are used to
			index the Trunk Group to get the destination port. DestMAC (2), means
			the last three bits of the Destination MAC Address are used to index 
			into the Trunk Group to get the destination port. SrcXORDestMAC (3),
			means that the last three bits of the Source MAC Address are logically
			XORed with the last three bits of the Destination MAC Address and used
			to index into the Trunk Group to get the destination port. SrcIP (4),
			means the last three bits of the Source IP Address are used to index
			into the Trunk Group to get the destination port. DestIP (5), means
			the last three bits of the Destination IP Address are used to index
			into the Trunk Group to get the destination port. SrcXORDestIP (6),
			means the last three bits of the Source IP Address are logically XORed
			with the last three bits of the Destination IP Address and used to 
			index into the Trunk Group to get the destination port. This rule
			defines the distribution algorithm applied to the aggregated link."      
    	DEFVAL { 1 }
    	::= { QTECHTrunk 2 }          
	
	QTECHTrunkMaxGroup OBJECT-TYPE
    	SYNTAX   INTEGER (0..8)
    	MAX-ACCESS  read-only
    	STATUS      current
    	DESCRIPTION
              "The number of maximum trunking group that system supports."
        ::= { QTECHTrunk 3 }
 
	QTECHTrunkTicketGenerationAlgorithm OBJECT-TYPE
    	SYNTAX      INTEGER {
                    direct-map(1),  
                    crc(2) 
                    }
   		MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "There are two algorithm to generate trunk ticket: direct-map and crc.
            Direct-map uses the last two bits of mac address as the trunk ticket;
            Crc uses crc algorithm on all mac address to generate trunk ticket."    
    	DEFVAL { 1}
    	::= { QTECHTrunk 4 }

-- QTECHTrunkGroupTable

	QTECHTrunkGroupTable OBJECT-TYPE
    	SYNTAX  SEQUENCE OF QTECHTrunkGroupEntry
    	MAX-ACCESS  not-accessible
    	STATUS      current
    	DESCRIPTION
            "Some ports participate in a trunking group. The range of group ID is 1-8."
    	::= { QTECHTrunk 5 }

	QTECHTrunkGroupEntry OBJECT-TYPE
    	SYNTAX      QTECHTrunkGroupEntry      
    	MAX-ACCESS  not-accessible
    	STATUS      current
    	DESCRIPTION
            "The entry is used to set some ports into a trunking group."
    	INDEX   { QTECHTrunkGroupID }
    	::= { QTECHTrunkGroupTable 1 }

	QTECHTrunkGroupEntry ::= SEQUENCE {
        QTECHTrunkGroupID          	INTEGER,
        QTECHTrunkGroupSetPorts    	PortList, 
        QTECHTrunkGroupCurrentPortInOperation    PortList,
        QTECHTrunkGroupMode            INTEGER
        }

	QTECHTrunkGroupID OBJECT-TYPE
    	SYNTAX      INTEGER  (1..8)
    	MAX-ACCESS  not-accessible
    	STATUS      current
    	DESCRIPTION
            "The trunking group ID."
    	::= { QTECHTrunkGroupEntry 1 }

	QTECHTrunkGroupSetPorts OBJECT-TYPE
    	SYNTAX      PortList
    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "When a bit of PortList is set, the port this bit specified participate in 
            a appointed trunking group."
    ::= { QTECHTrunkGroupEntry 2 }

	QTECHTrunkGroupCurrentPortInOperation OBJECT-TYPE
    	SYNTAX      PortList
    	MAX-ACCESS  read-only
    	STATUS      current
    	DESCRIPTION
            "Show these ports that are in 'up' status in a trunking group."
    	::= { QTECHTrunkGroupEntry 3 }

	QTECHTrunkGroupMode OBJECT-TYPE
    	SYNTAX      INTEGER {
                    manual(1),  
                    lacp-static(2) 
                    }

    	MAX-ACCESS  read-write
    	STATUS      current
    	DESCRIPTION
            "Trunking group support two mode, manual and static lacp mode."
    ::= { QTECHTrunkGroupEntry 4 }
    	
END
