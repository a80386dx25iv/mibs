--MibName=QTECHIgmpSnoop
SWITCH-IGMPSNOOP-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE,
    Integer32     	 			FROM SNMPv2-SMI
    RowStatus,TruthValue		FROM SNMPv2-TC 
    PortList,Vlanset,EnableVar	FROM SWITCH-TC 
    dot1qVlanIndex,     
    dot1qStaticMulticastAddress,
    dot1qStaticMulticastReceivePort
  					FROM Q-BRIDGE-MIB   
    QTECHSwitch	            	FROM QTECH-BASE-MIB;
         
QTECHIgmpSnoop MODULE-IDENTITY
    LAST-UPDATED "0412200000Z"
    ORGANIZATION "QTECH Group"
    CONTACT-INFO
            " 
             Phone:  01082884499
             Email:  support@QTECH.com"
    DESCRIPTION
            "The MIB module for igmp snooping."
    REVISION      "0412200000Z"
    DESCRIPTION
            ""
    ::= { QTECHSwitch 11}   
    
------------------------------------------------------------------------------
--
--  QTECHCommonIgmpSnoop - IGMP Snooping Parameters
--
--  This group is used to instrument the Layer 2 IGMP Snooping support.
--
------------------------------------------------------------------------------
--
    QTECHIgmpSnoopEnable  OBJECT-TYPE
        SYNTAX EnableVar
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "DURABLE: { enabled:all }
             Setting this object to enabled(1) enables IGMP Snooping.  Setting
             it to disabled(2) disables IGMP Snooping.
             
             Note that IGMP Snooping can function with or without GVRP and
             GMRP enabled."  
        DEFVAL {enable}
        ::= { QTECHIgmpSnoop 1 }


    QTECHIgmpSnoopAlerts  OBJECT-TYPE
        SYNTAX TruthValue
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "DURABLE: { false }
             Setting this object to true(1) enables the IP Router Alert
             Option (as defined in RFC2113) for transmitted IGMP packets.
             Setting it to false(2) disables this option."
        ::= { QTECHIgmpSnoop 2 }

    QTECHIgmpSnoopAging  OBJECT-TYPE
        SYNTAX Integer32 (0|30..3600)
        UNITS "second"
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "DURABLE: { 300 }
             The timeout period in seconds for aging out Multicast Groups
             dynamically learned with IGMP Snooping.  Note that aging operates
             on a per interface per VLAN per multicast group basis.
             0 means never aging"
        ::= { QTECHIgmpSnoop 3 }

    QTECHIgmpSnoopVlan  OBJECT-TYPE
        SYNTAX Vlanset
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The enabled/disabled  status of igmp snooping of vlan."
        ::= { QTECHIgmpSnoop 4 }        
     
    QTECHIgmpSnoopLeave  OBJECT-TYPE
        SYNTAX Vlanset
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "The enabled/disabled  status of igmp snooping immediate leave of vlan."
        ::= { QTECHIgmpSnoop 5 }   
           
    QTECHIgmpSnoopFilter  OBJECT-TYPE
        SYNTAX TruthValue
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "true indicates forward all unregistered multicast;
             false indicates filter all unregistered multicast"
        ::= { QTECHIgmpSnoop 6 } 
        
----     QTECHIgmpSnoopTable:multicast based on mac address,rfc2674q------------
    QTECHIgmpSnoopTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpSnoopEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "This table, which provides IGMP Snooping information, augments
             the 'dot1qStaticMulticastTable' in the Q-MIB (RFC2674)."
        ::= { QTECHIgmpSnoop 7 }

    QTECHIgmpSnoopEntry OBJECT-TYPE
        SYNTAX QTECHIgmpSnoopEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Displays by VLAN, Multicast Group, and Multicast receive port
             the set of ports enabled to forward Multicast Group traffic as
             determined by the IGMP Snooping task."
    	INDEX   {
       	 	dot1qVlanIndex,
        	dot1qStaticMulticastAddress,
        	dot1qStaticMulticastReceivePort
    	}
        ::= { QTECHIgmpSnoopTable 1 }

    QTECHIgmpSnoopEntry ::= SEQUENCE {
        QTECHIgmpSnoopEgressPorts   PortList
    }

    QTECHIgmpSnoopEgressPorts  OBJECT-TYPE
        SYNTAX PortList
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "This read-only object displays the set of ports enabled to
             forward specific Multicast Group traffic as determined by the
             IGMP Snooping task.
            
             It should be noted that the IGMP Snooping task generates a pseudo-
             static (i.e., not saved in NVM) port list similar to the RFC2674
             Q-MIB 'dot1qStaticMulticastStaticEgressPorts' object. Consequently,
             a port will not be a member of 'QTECHCommonIgmpSnoopEgressPorts' if
             it is a member of 'dot1qStaticMulticastForbiddenEgressPorts'."
        ::= { QTECHIgmpSnoopEntry 1 }    

--------    QTECHIgmpSnoopMrouterTable  ------------------    
    QTECHIgmpSnoopMrouterTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpSnoopMrouterEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "This table, which provides IGMP Snooping information, 
            whitch is multicast router port."
        ::= { QTECHIgmpSnoop 8 }

    QTECHIgmpSnoopMrouterEntry OBJECT-TYPE
        SYNTAX QTECHIgmpSnoopMrouterEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Displays by VLAN and Multicast router port."
        INDEX { QTECHIgmpSnoopMrouterVlan, QTECHIgmpSnoopMrouterPort }
        ::= { QTECHIgmpSnoopMrouterTable 1 }

    QTECHIgmpSnoopMrouterEntry ::= SEQUENCE {
        QTECHIgmpSnoopMrouterVlan   	INTEGER,
        QTECHIgmpSnoopMrouterPort   INTEGER,
        QTECHIgmpSnoopMrouterStatus 		RowStatus
    	}

	QTECHIgmpSnoopMrouterVlan OBJECT-TYPE
		SYNTAX INTEGER
		MAX-ACCESS not-accessible
		STATUS current
		DESCRIPTION
			"the multicast router vlan."
		::= { QTECHIgmpSnoopMrouterEntry 1 }
		
	QTECHIgmpSnoopMrouterPort OBJECT-TYPE
		SYNTAX INTEGER 
		MAX-ACCESS not-accessible
		STATUS current
		DESCRIPTION
			"the multicast router port ."
		::= { QTECHIgmpSnoopMrouterEntry 2 }
		
	QTECHIgmpSnoopMrouterStatus OBJECT-TYPE
		SYNTAX RowStatus 
		MAX-ACCESS read-create
		STATUS current
		DESCRIPTION
			"the status of multicast router."
		::= { QTECHIgmpSnoopMrouterEntry 3 }  
		
--------     QTECHIgmpSnoopStaticMulticastTable:multicast based on ip address ---------------
QTECHIgmpSnoopStaticMulticastTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF QTECHIgmpSnoopStaticMulticastEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "A table containing filtering information for Multicast
        and Broadcast IP addresses for each VLAN, configured
        into the device by (local or network) management
        specifying the set of ports to which frames received
        from specific ports and containing specific Multicast
        and Broadcast destination addresses are allowed to be
        forwarded.  A value of zero in this table as the port
        number from which frames with a specific destination
        address are received, is used to specify all ports for
        which there is no specific entry in this table for that
        particular destination address.  Entries are valid for
        Multicast and Broadcast addresses only."
    ::= { QTECHIgmpSnoop 9 }

QTECHIgmpSnoopStaticMulticastEntry OBJECT-TYPE
    SYNTAX      QTECHIgmpSnoopStaticMulticastEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "Filtering information configured into the device by
        (local or network) management specifying the set of
        ports to which frames received from this specific port
        for this VLAN and containing this Multicast or Broadcast
        destination address are allowed to be forwarded."
    INDEX   {
        dot1qVlanIndex,
        QTECHIgmpSnoopStaticMulticastAddress
    }
    ::= { QTECHIgmpSnoopStaticMulticastTable 1 }

QTECHIgmpSnoopStaticMulticastEntry ::=
    SEQUENCE {
        QTECHIgmpSnoopStaticMulticastAddress
            IpAddress,
        QTECHIgmpSnoopStaticMulticastStaticEgressPorts
            PortList,
        QTECHIgmpSnoopStaticMulticastStatus
            INTEGER
    }

QTECHIgmpSnoopStaticMulticastAddress OBJECT-TYPE
    SYNTAX      IpAddress
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "The destination IP address in a frame to which this
        entry's filtering information applies.  This object must
        take the value of a Multicast or Broadcast address."
    ::= { QTECHIgmpSnoopStaticMulticastEntry 1 }

QTECHIgmpSnoopStaticMulticastStaticEgressPorts OBJECT-TYPE
    SYNTAX      PortList
    MAX-ACCESS  read-write
    STATUS      current

    DESCRIPTION
        "The set of ports to which frames received from a
        specific port and destined for a specific Multicast or
        Broadcast IP address must be forwarded, regardless of
        any dynamic information e.g. from GMRP.  
        The default value of this object is a string of ones of
        appropriate length."
    ::= { QTECHIgmpSnoopStaticMulticastEntry 2 }

QTECHIgmpSnoopStaticMulticastStatus OBJECT-TYPE
    SYNTAX      INTEGER {
                    other(1),
                    invalid(2),
                    permanent(3),
                    deleteOnReset(4),
                    deleteOnTimeout(5)
                }
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
        "This object indicates the status of this entry.
            other(1) - this entry is currently in use but
                the conditions under which it will remain
                so differ from the following values.
            invalid(2) - writing this value to the object
                removes the corresponding entry.
            permanent(3) - this entry is currently in use
                and will remain so after the next reset of
                the bridge.

            deleteOnReset(4) - this entry is currently in
                use and will remain so until the next
                reset of the bridge.
            deleteOnTimeout(5) - this entry is currently in
                use and will remain so until it is aged out."
    DEFVAL      { permanent }
    ::= { QTECHIgmpSnoopStaticMulticastEntry 3 }
END
