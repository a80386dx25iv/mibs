--MibName=QTECHDhcpRelay
-- *****************************************************************
-- switch-dhcprelay-mib.smi:  QTECH DHCP Relay MIB file
--
-- Apr 2007, wumingyu
--
-- Copyright (c) 1999-2007 by QTECH Technology Co., Ltd. 
-- All rights reserved.
-- 
-- *****************************************************************

DHCP-RELAY-MIB DEFINITIONS ::= BEGIN

    IMPORTS
        MODULE-IDENTITY, TimeTicks, OBJECT-TYPE
             FROM SNMPv2-SMI       
        RowStatus                   FROM SNMPv2-TC 
        EnableVar,Vlanset			FROM SWITCH-TC       
        QTECHSwitch  FROM QTECH-BASE-MIB;

    QTECHDhcpRelay MODULE-IDENTITY
          LAST-UPDATED "200412200000Z"  -- Dec 20, 2004
          ORGANIZATION "QTECH."
          CONTACT-INFO
              "QTECH
                  E-mail: "

          DESCRIPTION "dhcp module management."

          REVISION    "200412200000Z"  -- Dec 20, 2004
          DESCRIPTION "Initial Version"

          ::= { QTECHSwitch 13 }
          
    QTECHDhcpRelayMibObjects	OBJECT IDENTIFIER ::= { QTECHDhcpRelay 1 }
    QTECHDhcpRelayGroup      	OBJECT IDENTIFIER ::= { QTECHDhcpRelayMibObjects 1 }
    QTECHDhcpRelayStatistics  OBJECT IDENTIFIER ::= { QTECHDhcpRelayMibObjects 2 }
 

--------------dhcp relay group--------------------
          
          QTECHDhcpRelayEnable OBJECT-TYPE
              SYNTAX  EnableVar
              MAX-ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "The enabled/disabled status of the dhcps relay."
              DEFVAL { disabled }
              ::= { QTECHDhcpRelayGroup 1 }
                                            
    	QTECHDhcpRelayStartTime  OBJECT-TYPE
      	  	SYNTAX TimeTicks
      	  	MAX-ACCESS read-only
     	   	STATUS mandatory
       	 	DESCRIPTION
            	"This read-only object displays the DHCP Relay startup time."
        	::= { QTECHDhcpRelayGroup 2 } 
        
              
------------------dhcp relay server table----------------
          QTECHDhcpRelayServerTable OBJECT-TYPE
              SYNTAX  SEQUENCE OF QTECHDhcpRelayServerEntry
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "A table that contains target server information
                      for the DHCP Relay."
              ::= { QTECHDhcpRelayGroup 3 }
              
          QTECHDhcpRelayServerEntry OBJECT-TYPE
              SYNTAX  QTECHDhcpRelayServerEntry
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "A entry that contains target server information
                      for the DHCP Relay."
              INDEX   { QTECHDhcpRelayServerIndex }
              ::= { QTECHDhcpRelayServerTable 1 }
            
          QTECHDhcpRelayServerEntry ::=
              SEQUENCE {
                  QTECHDhcpRelayServerIndex  INTEGER,
                  QTECHDhcpRelayServerAddress IpAddress,
                  QTECHDhcpRelayServerRowStatus RowStatus
                  }
                  
          QTECHDhcpRelayServerIndex OBJECT-TYPE
              SYNTAX  INTEGER
              MAX-ACCESS  not-accessible
              STATUS  current
              DESCRIPTION
                      "index of target server."
              REFERENCE
                      "SOUCE-CODE OF DHCP SERVER"
              ::= { QTECHDhcpRelayServerEntry 1 }                  
              
          QTECHDhcpRelayServerAddress OBJECT-TYPE
              SYNTAX  IpAddress
              MAX-ACCESS  read-create
              STATUS  current
              DESCRIPTION
                      "the ip address of target server."
              REFERENCE
                      "SOUCE-CODE OF DHCP SERVER"
              ::= { QTECHDhcpRelayServerEntry 2 } 
              
          QTECHDhcpRelayServerRowStatus OBJECT-TYPE
              SYNTAX  RowStatus              
              MAX-ACCESS  read-create
              STATUS  current
              DESCRIPTION
                      "DURABLE:
                      the rows tatus of target server address table."
              REFERENCE
                      "SOUCE-CODE OF DHCP SERVER."
              ::= { QTECHDhcpRelayServerEntry 3 }                  
  

-------------other scalars--------------------  
          QTECHDhcpRelayServerNextIndex OBJECT-TYPE
              SYNTAX  INTEGER (1..8)
              MAX-ACCESS  read-only
              STATUS  current
              DESCRIPTION
                      "The next index of Target server."
              REFERENCE
                      "DHCP SERVER SOUCE-CODE"
              ::= { QTECHDhcpRelayGroup 4 }              
              
              
          QTECHDhcpRelayVlans OBJECT-TYPE
              SYNTAX  Vlanset 
              MAX-ACCESS  read-write
              STATUS  current
              DESCRIPTION
                      "The enabled/disabled  status of dhcp-relay of vlan."
              REFERENCE
                      "DHCP SERVER SOUCE-CODE"
              ::= { QTECHDhcpRelayGroup 5 }                    
              
-------------dhcp Relay statistics---------------
    QTECHDhcpRelayStatsBootps  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Bootps num."
        ::= {QTECHDhcpRelayStatistics 1 }  
        
    QTECHDhcpRelayStatsDiscovers  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Discovers num."
        ::= { QTECHDhcpRelayStatistics 2 }        
        
    QTECHDhcpRelayStatsRequests  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Requests num."
        ::= { QTECHDhcpRelayStatistics 3 }        

    QTECHDhcpRelayStatsReleases  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Releases num."
        ::= { QTECHDhcpRelayStatistics 4 }                

    QTECHDhcpRelayStatsOffers  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Offers num."
        ::= { QTECHDhcpRelayStatistics 5 }        

    QTECHDhcpRelayStatsAcks  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Acks num."
        ::= { QTECHDhcpRelayStatistics 6 }        

    QTECHDhcpRelayStatsNacks  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Nacks num."
        ::= { QTECHDhcpRelayStatistics 7 }     
           
    QTECHDhcpRelayStatsDeclines  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Declines num."
        ::= {QTECHDhcpRelayStatistics 8 }  

    QTECHDhcpRelayStatsInformations  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the Informations num."
        ::= {QTECHDhcpRelayStatistics 9 } 
                
    QTECHDhcpRelayStatsUnknows  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the unknowed packets num."
        ::= {QTECHDhcpRelayStatistics 10 }  
                
    QTECHDhcpRelayStatsPackets  OBJECT-TYPE
        SYNTAX Counter
        MAX-ACCESS read-only
        STATUS mandatory
        DESCRIPTION
            "This read-only object displays the DHCP Relay receive the total packets num."
        ::= {QTECHDhcpRelayStatistics 11 }  
                                                               
END
