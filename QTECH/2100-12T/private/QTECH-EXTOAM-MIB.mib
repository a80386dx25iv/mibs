--MibName=QTECHExtendOam
-- =======================================================================
-- Version info
--
-- Version 1.0 Created 2005.07.27 by LONGYAGN
-- This version of MIB is created just for the use of Network Management Systems
-- display and set the system configuration of convertor .
--
-- Copyright(c) 2002-2005 by QTECH TECH, Ltd.
-- =======================================================================



QTECH-EXTOAM-MIB DEFINITIONS ::= BEGIN

	IMPORTS 
	    EnableVar
			        FROM SWITCH-TC   
		QTECHAgent
					FROM QTECH-BASE-MIB
		ifIndex
					FROM RFC1213-MIB
		Counter32
					FROM SNMPv2-SMI;

   QTECHExtendOam MODULE-IDENTITY

        LAST-UPDATED    "200507270000Z"
        ORGANIZATION    "QTECH Science & Technology Co., ltd"
        CONTACT-INFO    "QTECH Science & Technology Co., ltd.
                         E-mail: support@QTECH.com"

        DESCRIPTION     "QTECH extend oam Enterprise MIB definition."
        ::= { QTECHAgent 10 }

------------------------------------------------------------------------------
--	define groups in QTECH-EXTOAM-MIB
------------------------------------------------------------------------------
------------------------------------------------------------------------------

     QTECHOamExtStatsTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHOamExtStatsEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of extend OAM statistics."
			::=	{  QTECHExtendOam 1 }

	QTECHOamExtStatsEntry  OBJECT-TYPE
			SYNTAX		QTECHOamExtStatsEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"A list of extend OAM statistics parameters."
			INDEX {	ifIndex }
			::=	{ QTECHOamExtStatsTable 1 }

	QTECHOamExtStatsEntry ::= SEQUENCE {
			QTECHOamExtVariableGetTx       			 Counter32,
			QTECHOamExtVariableGetRx       			 Counter32,
			QTECHOamExtVariableSetTx       			 Counter32,
			QTECHOamExtVariableSetRx       			 Counter32,
			QTECHOamExtVariableGetResponseTx       	 Counter32,
			QTECHOamExtVariableGetResponseRx       	 Counter32,
			QTECHOamExtVariableSetResponseTx       	 Counter32,
			QTECHOamExtVariableSetResponseRx       	 Counter32,
			QTECHOamExtFileReadTx       			     Counter32,
			QTECHOamExtFileReadRx       			     Counter32,
			QTECHOamExtFileWriteTx       			 Counter32,
			QTECHOamExtFileWriteRx       			 Counter32,
			QTECHOamExtFileTransferDataRx       		 Counter32,
			QTECHOamExtFileTransferDataTx       		 Counter32,
			QTECHOamExtFileTransferAckTx       		 Counter32,
			QTECHOamExtFileTransferAckRx       		 Counter32,
			QTECHOamExtNotificationTx       			 Counter32,
			QTECHOamExtNotificationRx       			 Counter32,
			QTECHOamExtStaticInfoTx                   Counter32,
			QTECHOamExtStaticInfoRx                   Counter32,
			QTECHOamExtDynamicInfoTx                  Counter32,
			QTECHOamExtDynamicInfoRx                  Counter32,
			QTECHOamExtConfTx                         Counter32,
			QTECHOamExtConfRx                         Counter32,
			QTECHOamExtCmdTx                          Counter32,
			QTECHOamExtCmdRx                          Counter32,
			QTECHOamExtConnectTx                      Counter32,
			QTECHOamExtConnectRx                      Counter32,
			QTECHOamExtUnknownRx       			     Counter32
			}

     QTECHOamExtVariableGetTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM variable get
           			 OAMPDUs transmitted on this interface."            			
          	::= { QTECHOamExtStatsEntry 1 }

     QTECHOamExtVariableGetRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of extend-OAM variable get
           			 OAMPDUs received on this interface." 
          	::= { QTECHOamExtStatsEntry 2 }

     QTECHOamExtVariableSetTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of extend-OAM variable set
           			 OAMPDUs transmitted on this interface." 
          	::= { QTECHOamExtStatsEntry 3 }

     QTECHOamExtVariableSetRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM variable set
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 4 }

     QTECHOamExtVariableGetResponseTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of extend-OAM variable get
           			responses OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 5 }

     QTECHOamExtVariableGetResponseRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM variable get
           			responses OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 6 }

     QTECHOamExtVariableSetResponseTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM variable set
           			responses OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 7 }

     QTECHOamExtVariableSetResponseRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM variable set
           			responses OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 8 }

     QTECHOamExtFileReadTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of extend-OAM file read
           			requests OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 9 }

     QTECHOamExtFileReadRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file read
           			requests OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 10 }

     QTECHOamExtFileWriteTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file write
           			requests OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 11 }

    QTECHOamExtFileWriteRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file write
           			requests OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 12 }

    QTECHOamExtFileTransferDataRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file transfer
           			data OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 13 }

    QTECHOamExtFileTransferDataTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file transfer
           			data OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 14 }

    QTECHOamExtFileTransferAckTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION           			
           			"A count of the number of extend-OAM file transfer
           			acks OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 15 }

    QTECHOamExtFileTransferAckRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM file transfer
           			acks OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 16 }

    QTECHOamExtNotificationTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION            			
           			"A count of the number of extend-OAM notifications
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 17 }

    QTECHOamExtNotificationRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of extend-OAM notifications
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 18 }
 
    QTECHOamExtStaticInfoTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of static infomation
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 19 }
       
    QTECHOamExtStaticInfoRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of static infomation 
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 20 }
         
    QTECHOamExtDynamicInfoTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of dynamic infomation 
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 21 }
          	
    QTECHOamExtDynamicInfoRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of dynamic infomation 
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 22 } 
          	
    QTECHOamExtConfTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of configuration infomation 
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 23 }
          	
    QTECHOamExtConfRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of configuration infomation 
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 24 }
          	
    QTECHOamExtCmdTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of command infomation 
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 25 }
          	
    QTECHOamExtCmdRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of command infomation 
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 26 }
          	
    QTECHOamExtConnectTx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of connect infomation 
           			 OAMPDUs transmitted on this interface."
          	::= { QTECHOamExtStatsEntry 27 }
          	
    QTECHOamExtConnectRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"A count of the number of connect infomation 
           			 OAMPDUs received on this interface."
          	::= { QTECHOamExtStatsEntry 28 }

     QTECHOamExtUnknownRx  OBJECT-TYPE
 	      	SYNTAX 	  	Counter32
	      	MAX-ACCESS  read-only
	      	STATUS      current
 	      	DESCRIPTION
           			"The total number of unknown extend-OAM packets
           			received on this interface."
          	::= { QTECHOamExtStatsEntry 29 }



     QTECHOamExtStatusTable  OBJECT-TYPE
			SYNTAX		SEQUENCE OF	QTECHOamExtStatusEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
				 "A table of extended OAM status."
			::=	{  QTECHExtendOam 2 }

	QTECHOamExtStatusEntry  OBJECT-TYPE
			SYNTAX		QTECHOamExtStatusEntry
			MAX-ACCESS	not-accessible
			STATUS		current
			DESCRIPTION
			   	"A list of extend OAM status parameters."
			INDEX {	ifIndex }
			::=	{ QTECHOamExtStatusTable 1 }

 	QTECHOamExtStatusEntry ::= SEQUENCE {
			QTECHOamExtStatus       			 INTEGER
			}

    QTECHOamExtStatus   OBJECT-TYPE
  	        SYNTAX  INTEGER
 	        {
 	           nonoperative(1),
               invariableInfoGet(2),
               invariableInfoGetError(3),
               operational(4),
               fileTransfer(5)
            }
         	MAX-ACCESS 	read-only
        	STATUS 		current
        	DESCRIPTION
                	"Extend-OAM status."
         	::= { QTECHOamExtStatusEntry 1 }

--QTECHOamExtRemoteMibObjects	
    QTECHOamExtRemoteMibObjects OBJECT IDENTIFIER ::= { QTECHExtendOam 3 }   
    
	QTECHOamNotificationEnable OBJECT-TYPE
		    SYNTAX  EnableVar
		    ACCESS read-write
		    STATUS current
		    DESCRIPTION
		        "Specifies whether or not send OAM notification to local."
		    ::= {  QTECHOamExtRemoteMibObjects 1 }

    QTECHOamExtConfigReqEnable OBJECT-TYPE
		    SYNTAX  EnableVar
		    ACCESS read-write
		    STATUS current
		    DESCRIPTION
		        "Enable or disable configuration request function."
		    ::= {  QTECHOamExtRemoteMibObjects 2 }
END
