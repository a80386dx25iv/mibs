--MibName=QTECHEthRing
-- *****************************************************************
-- QTECH-ETHERNET-RING-MIB.MIB:  QTECH Ethernet Ring MIB file
--
-- Oct. 2008, guoxiaodan
--
-- Copyright (c) 1994-2008, 2008 by QTECH, Inc.
-- All rights reserved.
--
-- 04,20100714,wangjian, add the description of QTECHEthRingEntry and modified the 
-- access of the nodes in QTECHEthRingEntry
-- 03,20100714,liweixing,line 472,delete the comma  
--02, 20100423, huochao,the preversion had Compile errors, I have changed  it at:
--    QTECHEthRingEntry ::= SEQUENCE {...
--        QTECHEthRingBridgeState            INTEGER, 
--        QTECHEthRingState                  INTEGER, 
--        QTECHEthRingDbState                INTEGER,   
--        ...
--    QTECHEthRingPortEntry ::= SEQUENCE {
--        QTECHEthRingPortState              INTEGER,
--        QTECHEthRingPortForwardState       INTEGER,
--        QTECHEthRingDiscoverPeerState      INTEGER,
--    QTECHEthRingDeviceListEntry ::= SEQUENCE {
--        QTECHEthRingDeviceListBridgeState    INTEGER,
-- 01, 20100401, gxd, add node QTECHEthRingExtInfoGroup
-- *****************************************************************
SWITCH-ERING-MIB  DEFINITIONS ::= BEGIN

IMPORTS
       QTECHSwitch
		   FROM QTECH-BASE-MIB 
	   MODULE-IDENTITY, OBJECT-TYPE, Integer32,
       Unsigned32, NOTIFICATION-TYPE
           FROM SNMPv2-SMI                  -- RFC2578
       RowStatus, MacAddress, TruthValue, DisplayString
           FROM SNMPv2-TC                   -- RFC2579
       EnableVar    FROM SWITCH-TC 
       ClearVar     FROM SWITCH-TC;
       
QTECHEthRing MODULE-IDENTITY
    LAST-UPDATED "0808120000Z"
    ORGANIZATION "QTECH China World"
    CONTACT-INFO
            " 
             Phone:  01082884499
             Email:  support@QTECH.com"
    DESCRIPTION
            "The MIB module for Ethernet Ring protocol."
    REVISION      "0808050000Z"
    DESCRIPTION
            "Ethernet Ring MIB."
    ::= {QTECHSwitch 39}
   
  QTECHEthRingObjects           OBJECT IDENTIFIER ::= { QTECHEthRing 1 }
  QTECHEthRingNotifications     OBJECT IDENTIFIER ::= { QTECHEthRing 2 }   
     
--------------------- ERING Table -------------------------------
    QTECHEthRingTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHEthRingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "QTECH ethernet ring table."
        ::= { QTECHEthRingObjects 1 }

    QTECHEthRingEntry OBJECT-TYPE
        SYNTAX QTECHEthRingEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "QTECH ethernet ring table entry. The QTECHEthRingPrimaryPort and QTECHEthRingSecondaryPort can not be modified once the row has created."
        INDEX { QTECHEthRingIndex }
        ::= { QTECHEthRingTable 1 }

    QTECHEthRingEntry ::= SEQUENCE {
        QTECHEthRingIndex                  Integer32,
        QTECHEthRingPrimaryPort            Unsigned32,
        QTECHEthRingSecondaryPort          Unsigned32,
        QTECHEthRingPriority               Unsigned32,
        QTECHEthRingHelloTime              Unsigned32,
        QTECHEthRingRestoreDelay           Unsigned32,
        QTECHEthRingHoldTime               Unsigned32,
        QTECHEthRingProtocolVlan           Unsigned32,
        QTECHEthRingEnable                 EnableVar,
        QTECHEthRingDescription            OCTET STRING,  
        QTECHEthRingBridgeState            INTEGER, 
        QTECHEthRingState                  INTEGER,             
        QTECHEthRingCurStateDuration       Unsigned32,  
        QTECHEthRingDbMac                  MacAddress,
        QTECHEthRingDbPri                  Unsigned32,
        QTECHEthRingDbState                INTEGER,   
        QTECHEthRingClearStatistics        ClearVar,
        QTECHEthRingRowStatus              RowStatus
     }

    QTECHEthRingIndex OBJECT-TYPE
        SYNTAX  Integer32(1..8)
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "An index uniquely identifies a ERING Ring, which ranges from 1~8. 
            This value can't be modified after created."
        ::= { QTECHEthRingEntry 1 }

    QTECHEthRingPrimaryPort OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The primary port blocked firstly when ring node state changes. 
            It can't be modified after enabled."
        ::= { QTECHEthRingEntry 2 }
          
    QTECHEthRingSecondaryPort OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The secondary port; It can't be modified after enabled."
        ::= { QTECHEthRingEntry 3 }  
        
    QTECHEthRingPriority OBJECT-TYPE
        SYNTAX  Unsigned32 (0..255)
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The priority of bridge,The value 0 signifies the lowest priority 
            and 255 signifies the highest priority."
        ::= { QTECHEthRingEntry 4 }

    QTECHEthRingEnable OBJECT-TYPE
        SYNTAX  EnableVar
        ACCESS read-create
        STATUS current
        DESCRIPTION
            "This attribute, while TRUE, indicates that the  function 
            of ethernet ring is enabled(1). The function is disabled(2), otherwise."
        DEFVAL { disable }
        ::= { QTECHEthRingEntry 5 }

    QTECHEthRingDescription OBJECT-TYPE
        SYNTAX  OCTET STRING
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The description information of ethernet ring."     
        ::= { QTECHEthRingEntry 6 }

    QTECHEthRingHelloTime OBJECT-TYPE
        SYNTAX  Unsigned32 (1..10)
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The value indicates the interval between two hello packets sent by ethernet ring node, 
            and its unit is second.This value ranges from 1s~10s."
        ::= { QTECHEthRingEntry 7 }  
        
    QTECHEthRingRestoreDelay OBJECT-TYPE
        SYNTAX  Unsigned32 (1..180)
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The value indicates the delay time when up happens, and its unit is second.
            This value ranges from 1s~180s."
        ::= { QTECHEthRingEntry 8 }
      
     QTECHEthRingHoldTime OBJECT-TYPE
        SYNTAX  Unsigned32 (3..360)
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "This time value determines the interval length during which no more than two protocol 
            packet received by this port. "
        ::= { QTECHEthRingEntry 9 }

     QTECHEthRingProtocolVlan OBJECT-TYPE
        SYNTAX  Unsigned32 (2..4094)
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The Vlan of protocol packet."
        ::= { QTECHEthRingEntry 10 }
                          
    QTECHEthRingBridgeState OBJECT-TYPE
        SYNTAX INTEGER{
                       invalid(0),
                       down(1),
                       block(2),
                       forward(3)
                       } 
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The state of designated bridge, including forward,block,down and invalid. 
            The default  bridge state is invalid(0) before ring was created.
            Each bridge have two ports,down(1) if at least one port is in the state of DOWN;
            Block(2) if at least one port is blocked when two ports are in DISCOVER state;
            Forward(3) when two ports are in the state of forwarding;"
        ::= { QTECHEthRingEntry 11 }

    QTECHEthRingState OBJECT-TYPE
        SYNTAX INTEGER{
                       down(0),
                       unstable(1),
                       enclosed(2),
                       unenclosed(3)             
                      }
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Ethernet ring state.
            Down(0)-- The default state of ring state when the ring was created;
            Error(1)-- The state before achieve stability after enable the ring;
            Enclosed(2)-- The ring state is stable and enclosed.
            Unenclosed(3)-- The ring state is stable but unenclosed."
        ::= { QTECHEthRingEntry 12 }
               
    QTECHEthRingCurStateDuration OBJECT-TYPE
        SYNTAX Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "It means duration since ring state got into stability if the current ring state is stable, 
            unstable state means duration of the last stable state."   
        ::= { QTECHEthRingEntry 13 }

    QTECHEthRingDbMac OBJECT-TYPE
        SYNTAX MacAddress  
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Mac address of the designated bridge."
        ::= { QTECHEthRingEntry 14 }
    
    QTECHEthRingDbPri OBJECT-TYPE
        SYNTAX   Unsigned32 (0..255)
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The DB priority of the Ethernet ring."
        ::= { QTECHEthRingEntry 15 }

    QTECHEthRingDbState OBJECT-TYPE
        SYNTAX INTEGER{
                       invalid(0),
                       down(1),
                       block(2),
                       forward(3)
                       } 
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The state of designated bridge, including forward,block,down and invalid. 
            The default  bridge state is invalid(0) before ring was created.
            Each bridge have two ports,down(1) if at least one port is in the state of DOWN;
            Block(2) if at least one port is blocked when two ports are in DISCOVER state;
            Forward(3) when two ports are in the state of forwarding;"
        DEFVAL{ 0 }
        ::= { QTECHEthRingEntry 16 }
          
    QTECHEthRingClearStatistics OBJECT-TYPE
        SYNTAX ClearVar
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "The purpose of this object is to clear all the ring statistics. 
            Set the value to clear(1) means clear current ring statistics and begin the next recalculation."
        DEFVAL { clear }
        ::= { QTECHEthRingEntry 17 }
    
    QTECHEthRingRowStatus OBJECT-TYPE
        SYNTAX RowStatus
        MAX-ACCESS read-create
        STATUS current
        DESCRIPTION
            "This object is responsible for managing the creation, 
            deletion and modification of rows, which support active status and CreatAndGo, destroy operation."
       ::= { QTECHEthRingEntry 18 }
   
--------------------- ERING Port Table -------------------------------
    QTECHEthRingPortTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHEthRingPortEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "QTECH ethernet ring Port Table."
        ::= { QTECHEthRingObjects 2 }

    QTECHEthRingPortEntry OBJECT-TYPE
        SYNTAX QTECHEthRingPortEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "QTECH ethernet ring port table entry."
        INDEX { QTECHEthRingIndex, QTECHEthRingPortFlagIndex }
        ::= { QTECHEthRingPortTable 1 }

    QTECHEthRingPortEntry ::= SEQUENCE {
        QTECHEthRingPortFlagIndex          Unsigned32,
        QTECHEthRingPort                   Unsigned32,
        QTECHEthRingPortState              INTEGER,
        QTECHEthRingPortForwardState       INTEGER,
        QTECHEthRingDiscoverPeerState      INTEGER,
        QTECHEthRingPortStateSwitchCount   Unsigned32,
        QTECHEthRingCurrStateDuration      Unsigned32,  
        QTECHEthRingRecvHelloPkts        	Unsigned32,
        QTECHEthRingRecvChangePkts       	Unsigned32,  
        QTECHEthRingRecvChangeRelayPkts    Unsigned32,
        QTECHEthRingRecvFlushPkts          Unsigned32,  
        QTECHEthRingSendHelloPkts        	Unsigned32,
        QTECHEthRingSendChangePkts       	Unsigned32,  
        QTECHEthRingSendChangeRelayPkts    Unsigned32,
        QTECHEthRingSendFlushPkts          Unsigned32
        }

    QTECHEthRingPortFlagIndex OBJECT-TYPE
        SYNTAX  Unsigned32(1..2)                        
                         --1,Index of the primary port statistics
                         --2,--Index of the secondary port statistics  
    MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "Index of the port table which corresponding to the specific port statistics uniquely."
        ::= { QTECHEthRingPortEntry 1 }

    QTECHEthRingPort OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Port number corresponding to the index."
        ::= { QTECHEthRingPortEntry 2 }

    QTECHEthRingPortState OBJECT-TYPE
        SYNTAX  INTEGER{
                         active(1),--port is active in the ring
                         inactive(2)--port is inactive in the ring
                         }
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "This value means whether port is active in the ring or not."
        ::= { QTECHEthRingPortEntry 3 } 
     
    QTECHEthRingPortForwardState OBJECT-TYPE
        SYNTAX  INTEGER{
                      block(1),
                      forward(2)
                      }
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "State of ethernet ring port, including block and forward."
        ::= { QTECHEthRingPortEntry 4 }

    QTECHEthRingDiscoverPeerState OBJECT-TYPE
        SYNTAX INTEGER{ 
                    none(1),  
                    discover(2),
                    full(3) 
                     }
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Port state of ethernet ring,including none,discover and full.
            none(1)-- two ports of the bridge did not receive any packets from opposite ports;
            discover(2)-- at least one port of the bridge received packet before got into stable status;
            full(3)-- Port received the same packet three times continuously."
        ::= { QTECHEthRingPortEntry 5 } 
                      
    QTECHEthRingPortStateSwitchCount OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Times of port state switch between block and forward state."
        ::= { QTECHEthRingPortEntry 6 }
        
    QTECHEthRingCurrStateDuration OBJECT-TYPE
        SYNTAX Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "It means the duration since port got into current state."
        ::= { QTECHEthRingPortEntry 7 }


    QTECHEthRingRecvHelloPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 8 }   
           
    QTECHEthRingRecvChangePkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 9 }   
         
    QTECHEthRingRecvChangeRelayPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 10 }
                        
    QTECHEthRingRecvFlushPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 11 }
                              
    QTECHEthRingSendHelloPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 12 }   
           
    QTECHEthRingSendChangePkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 13 }   
         
    QTECHEthRingSendChangeRelayPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 14 }
                        
    QTECHEthRingSendFlushPkts OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Packet number which received from peer device."
        ::= { QTECHEthRingPortEntry 15 }
        
--------------------- ERING Device Table -------------------------------
    QTECHEthRingDeviceListTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHEthRingDeviceListEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "EthRing Device Table."
        ::= { QTECHEthRingObjects 3 }

    QTECHEthRingDeviceListEntry OBJECT-TYPE
        SYNTAX QTECHEthRingDeviceListEntry
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "QTECH ethernet ring device table entry."
        INDEX { QTECHEthRingIndex, QTECHEthRingPortFlagIndex, QTECHEthRingDeviceListIndex }
        ::= { QTECHEthRingDeviceListTable 1 }

    QTECHEthRingDeviceListEntry ::= SEQUENCE {
        QTECHEthRingDeviceListIndex          Unsigned32,
        QTECHEthRingDeviceListMac            MacAddress,
        QTECHEthRingDeviceListPort1          Integer32,
        QTECHEthRingDeviceListPort2          Integer32,
        QTECHEthRingDeviceListPriority       Unsigned32 ,
        QTECHEthRingDeviceListBridgeState    INTEGER
        }

    QTECHEthRingDeviceListIndex OBJECT-TYPE
        SYNTAX  Unsigned32
        MAX-ACCESS not-accessible
        STATUS current
        DESCRIPTION
            "An entry in this table is created when hello packet is received. 
            An entry is removed from this table when its corresponding QTECHEthRingEntry is deleted.
            An implementation MUST start assigning QTECHEthRingDeviceListIndex values at 1 and wrap 
            after exceeding the maximum possible value, as defined by the limit of this object ('ffffffff'h)."
        ::= { QTECHEthRingDeviceListEntry 1 }

    QTECHEthRingDeviceListMac OBJECT-TYPE
        SYNTAX  MacAddress
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The mac address of bridge in the ethernet ring."
        ::= { QTECHEthRingDeviceListEntry 2 }

    QTECHEthRingDeviceListPort1 OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS read-only
        STATUS deprecated
        DESCRIPTION
            "One of active port of the device."
        ::= { QTECHEthRingDeviceListEntry 3 } 
            
    QTECHEthRingDeviceListPort2 OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS read-only
        STATUS deprecated
        DESCRIPTION
            "Another active port of the device."
        ::= { QTECHEthRingDeviceListEntry 4 }

    QTECHEthRingDeviceListPriority OBJECT-TYPE
        SYNTAX  Unsigned32 (0..255)
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The priority of bridge,The value 0 signifies the lowest priority 
            and 255 signifies the highest priority."
        ::= { QTECHEthRingDeviceListEntry 5 } 

    QTECHEthRingDeviceListBridgeState OBJECT-TYPE
        SYNTAX INTEGER{
                       invalid(0),
                       down(1),  
                       block(2),
                       forward(3)
                       } 
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The state of designated bridge, including forward,block,alone,down and invalid. 
            The default  bridge state is invalid(0) before ring was created.
            Each bridge have two ports,down(1) if at least one port is in the state of DOWN;
             Block(2) if at least one port is blocked when two ports are in DISCOVER state;
            Forward(3) when two ports are in the state of forwarding;"  
        ::= { QTECHEthRingDeviceListEntry 6 }      
       
-- Extend Info Group Definition section    

    QTECHEthRingExtInfoGroup OBJECT IDENTIFIER ::= { QTECHEthRingObjects 4 }      

    QTECHEthRingUpStreamGroupList OBJECT-TYPE
        SYNTAX  DisplayString(SIZE(4))
        MAX-ACCESS read-write
        STATUS current
        DESCRIPTION
            "Upstream group list of ethernet ring which is corresponding to the link state track group.
            A bit mask corresponding to the upstream group. Set corresponding bit to 1 to create the 
            group, set bit to 0 delete the group."
        ::= { QTECHEthRingExtInfoGroup 1 }   
     
-- Notification Definition section 

   QTECHEthRingStateChange NOTIFICATION-TYPE
         OBJECTS {
           QTECHEthRingIndex,
           QTECHEthRingState,
           QTECHEthRingDbMac
                }
         STATUS  current
         DESCRIPTION
             "Generated when ring state changes between enclosed and other status;"
         ::= { QTECHEthRingNotifications 1 }        
  END
