--MibName=QTECHIgmp
-- *****************************************************************
-- QTECH-igmpl2-mib.mib:  QTECH IGMP MIB file
--
-- Feb 2011, aiyingjie
--
-- Copyright (c) 1994-2000, 2001 by QTECH, Inc.
-- All rights reserved.
-- 01,20111231,yangkewei,ROS00008218,advent mib������
-- *****************************************************************

QTECH-IGMPL2-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE,
    Integer32     	 			    FROM SNMPv2-SMI
    RowStatus,TruthValue		    FROM SNMPv2-TC 
    PortList, Vlanset, EnableVar	FROM SWITCH-TC 
    InetAddress, InetAddressType    FROM INET-ADDRESS-MIB  
    VlanIndex,VlanId                FROM Q-BRIDGE-MIB
    QTECHAgent                   FROM QTECH-BASE-MIB;         
QTECHIgmpL2 MODULE-IDENTITY
    LAST-UPDATED "201102180000Z"
    ORGANIZATION "QTECH Group"
    CONTACT-INFO
                "       Raise Systems
                        
                Postal: Beijing,
                        China

                   Tel: 86-010-82884499

                E-mail: aiyingjie@QTECH.com"
    DESCRIPTION
            "The MIB module for Igmp Module."
    ::= { QTECHAgent 28}   


  
QTECHIgmpL2Notifications	OBJECT IDENTIFIER ::= {	QTECHIgmpL2 1 }
QTECHIgmpL2Objects			OBJECT IDENTIFIER ::= {	QTECHIgmpL2 2 }   
QTECHIgmpL2Conformance		OBJECT IDENTIFIER ::= {	QTECHIgmpL2 3 }  

   
------------------------------------------------------------------------------
QTECHIgmpBase			OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 1 }
QTECHIgmpSnooping		OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 2 }   
QTECHIgmpMvr		    	OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 3 } 
QTECHIgmpVlanCopy		OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 4 } 
QTECHIgmpProxy		    OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 5 } 
QTECHIgmpFilter			OBJECT IDENTIFIER ::= {	QTECHIgmpL2Objects 6 } 
------------------------------------------------------------------------------   



QTECHIgmpBaseScalar		OBJECT IDENTIFIER ::= {	QTECHIgmpBase 1 }
QTECHIgmpSnoopingScalar	OBJECT IDENTIFIER ::= {	QTECHIgmpSnooping 1 }
QTECHIgmpMvrScalar		OBJECT IDENTIFIER ::= {	QTECHIgmpMvr 1 }
QTECHIgmpVlanCopyScalar	OBJECT IDENTIFIER ::= {	QTECHIgmpVlanCopy 1 }
QTECHIgmpProxyScalar		OBJECT IDENTIFIER ::= {	QTECHIgmpProxy 1 }
QTECHIgmpFilterScalar	OBJECT IDENTIFIER ::= {	QTECHIgmpFilter 1 }



--
-- BASE Config Group
--
QTECHIgmpAging OBJECT-TYPE           
        SYNTAX        INTEGER(5..3600 | -1)  
		        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "The timeout period in seconds for aging out Multicast Groups dynamically learned with IGMP Snooping.  
				Note that aging operates on a per interface per VLAN per multicast group basis.
				Value '0XFFFFFFFF' means never timeout."
        DEFVAL { 300 }
        ::= { QTECHIgmpBaseScalar 1 }


QTECHIgmpRingPortList OBJECT-TYPE
        SYNTAX     PortList
        MAX-ACCESS read-write
        STATUS     current
        DESCRIPTION
            "Set the ring port."
        ::= { QTECHIgmpBaseScalar 2 }

QTECHIgmpImmediateLeaveTable OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpImmediateLeaveEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
            "Config immediate leave on port or port and vlan"
        ::= { QTECHIgmpBase 2 }    
        
QTECHIgmpImmediateLeaveEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpImmediateLeaveEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "Indicates immediate leave on port or port and vlan"
        INDEX      { QTECHIgmpImmediateLeavePort }
        ::= { QTECHIgmpImmediateLeaveTable 1 }   

QTECHIgmpImmediateLeaveEntry ::= SEQUENCE {    
        QTECHIgmpImmediateLeavePort        INTEGER,
		QTECHIgmpImmediateLeaveType		  INTEGER,
        QTECHIgmpImmediateLeaveVlanList    Vlanset,
        QTECHIgmpImmediateLeaveRowStatus   RowStatus
        }

        
QTECHIgmpImmediateLeavePort OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    not-accessible 
        STATUS        current
	DESCRIPTION  
		"."
        ::= { QTECHIgmpImmediateLeaveEntry 1 }

QTECHIgmpImmediateLeaveType OBJECT-TYPE
        SYNTAX        INTEGER 
                      { port(1),
                        port-vlan(2)
                      } 
        MAX-ACCESS    read-create 
        STATUS        current
	DESCRIPTION  
		"."
        ::= { QTECHIgmpImmediateLeaveEntry 2 }
    
QTECHIgmpImmediateLeaveVlanList OBJECT-TYPE
        SYNTAX        Vlanset  
        MAX-ACCESS    read-create 
        STATUS        current
	DESCRIPTION  
		"."
        ::= { QTECHIgmpImmediateLeaveEntry 3 }

QTECHIgmpImmediateLeaveRowStatus OBJECT-TYPE
        SYNTAX        RowStatus
        MAX-ACCESS    read-create
        STATUS        current
	DESCRIPTION  
		"The mulcast router status"
        ::= { QTECHIgmpImmediateLeaveEntry 4 }

-------------------------------------------------------
QTECHIgmpMrouterTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpMrouterEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "Indicates the igmp router ports."
        ::= { QTECHIgmpBase 3 }

QTECHIgmpMrouterEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpMrouterEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "Indicates the igmp router ports."
        INDEX      { QTECHIgmpMrouterPort, QTECHIgmpMrouterVlan}
        ::= { QTECHIgmpMrouterTable 1 }

QTECHIgmpMrouterEntry ::= SEQUENCE {    
        QTECHIgmpMrouterPort        INTEGER,
		QTECHIgmpMrouterVlan		   VlanIndex,
        QTECHIgmpMrouterLiveTime    INTEGER(5..3600),
        QTECHIgmpMrouterMRStatus    INTEGER,
        QTECHIgmpMrouterRowStatus   RowStatus
        }
        
QTECHIgmpMrouterPort OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    not-accessible 
        STATUS        current
	DESCRIPTION  
		"The mulcast router port. The index of this entry."
        ::= { QTECHIgmpMrouterEntry 1 }

QTECHIgmpMrouterVlan OBJECT-TYPE
        SYNTAX        VlanIndex  
        MAX-ACCESS    not-accessible 
        STATUS        current
	DESCRIPTION  
		"The mulcast router vlan. The index of this entry."
        ::= { QTECHIgmpMrouterEntry 2 }
    
QTECHIgmpMrouterLiveTime OBJECT-TYPE
        SYNTAX        INTEGER(5..3600)  
        MAX-ACCESS    read-only 
        STATUS        current
	DESCRIPTION  
		"The mulcast router port live time."
        ::= { QTECHIgmpMrouterEntry 3 }

QTECHIgmpMrouterMRStatus OBJECT-TYPE
        SYNTAX        INTEGER
        MAX-ACCESS    read-only 
        STATUS        current
	DESCRIPTION  
		"The mulcast router port stsatus, 0x10 indicates dynamic ,0x20 indicates static."
        ::= { QTECHIgmpMrouterEntry 4 }

        
QTECHIgmpMrouterRowStatus OBJECT-TYPE
	SYNTAX	RowStatus
	ACCESS	read-create
	STATUS	current
	DESCRIPTION
		"The row status of this entry."
	::= { QTECHIgmpMrouterEntry 5 }

-----------------------------------------------------------
QTECHIgmpMemberTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpMemberEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
        "This table record the igmp members learnd by the device."
        ::= { QTECHIgmpBase 4 }

QTECHIgmpMemberEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpMemberEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
        "Record the igmp members learnd by the device."
        INDEX      {QTECHIgmpMemberPort, QTECHIgmpMemberUserVlan, QTECHIgmpMemberGroupIpType, 
        			QTECHIgmpMemberGroup, QTECHIgmpMemberMVlan }
        ::= { QTECHIgmpMemberTable 1 }

QTECHIgmpMemberEntry ::= SEQUENCE { 
		QTECHIgmpMemberPort           INTEGER,
		QTECHIgmpMemberUserVlan       VlanIndex,
		QTECHIgmpMemberGroupIpType    InetAddressType,
		QTECHIgmpMemberGroup          InetAddress,
		QTECHIgmpMemberMVlan          INTEGER,
		QTECHIgmpMemberLiveTime       INTEGER(5..3600),
		QTECHIgmpMemberSource         INTEGER,
		QTECHIgmpMemberRowStatus         RowStatus
        }   
        
QTECHIgmpMemberPort OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpMemberEntry 1 }

 QTECHIgmpMemberUserVlan OBJECT-TYPE
        SYNTAX        VlanIndex  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpMemberEntry 2 }   
        
QTECHIgmpMemberGroupIpType OBJECT-TYPE
        SYNTAX        InetAddressType  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Indicates the IP address type."
        ::= { QTECHIgmpMemberEntry 3 }
 
QTECHIgmpMemberGroup OBJECT-TYPE
        SYNTAX        InetAddress  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Indicates the IP multicast group that join."
        ::= { QTECHIgmpMemberEntry 4 }

QTECHIgmpMemberMVlan OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Indicates the multicast VLAN that allow user join."
        ::= { QTECHIgmpMemberEntry 5 }

QTECHIgmpMemberLiveTime OBJECT-TYPE
        SYNTAX        INTEGER(5..3600)  
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"Indicates the multicast member live time."
        ::= { QTECHIgmpMemberEntry 6 }

QTECHIgmpMemberSource OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"Indicates the module of this member."
        ::= { QTECHIgmpMemberEntry 7 }

QTECHIgmpMemberRowStatus OBJECT-TYPE
        SYNTAX        RowStatus  
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"The row status of this entry."
        ::= { QTECHIgmpMemberEntry 8 }

--------------------------------------------------------------
QTECHIgmpPortStatisticsTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpPortStatisticsEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
        "This table indicates the statistics of igmp packets on prots."
        ::= { QTECHIgmpBase 5 }

QTECHIgmpPortStatisticsEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpPortStatisticsEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
        "Indicates the statistics of igmp packets on prots."
        INDEX      { QTECHIgmpPortStatisticsPortNum }
        ::= { QTECHIgmpPortStatisticsTable 1 }
   
   
QTECHIgmpPortStatisticsEntry ::= SEQUENCE { 

		QTECHIgmpPortStatisticsPortNum   INTEGER,
		QTECHIgmpPortStatisticsClear     INTEGER,
		QTECHIgmpPortStatisticsRecvQuery   Counter,
		QTECHIgmpPortStatisticsRecvReport   Counter,
		QTECHIgmpPortStatisticsRecvLeave      Counter,
		QTECHIgmpPortStatisticsFilterDropQuery   Counter,
		QTECHIgmpPortStatisticsFilterDropReport    Counter,
		QTECHIgmpPortStatisticsFilterDropLeave    Counter,
		QTECHIgmpPortStatisticsSnoopDealQuery      Counter,
		QTECHIgmpPortStatisticsSnoopDealReport      Counter,
		QTECHIgmpPortStatisticsSnoopDealLeave        Counter,
		QTECHIgmpPortStatisticsMvrDealQuery         Counter,
		QTECHIgmpPortStatisticsMvrDealReport        Counter,
		QTECHIgmpPortStatisticsMvrDealLeave         Counter,
		QTECHIgmpPortStatisticsVlanCPDealQuery       Counter,
		QTECHIgmpPortStatisticsVlanCPDealReport     Counter,
		QTECHIgmpPortStatisticsVlanCPDealLeave       Counter,
		QTECHIgmpPortStatisticsReplaceCount          Counter
        }

QTECHIgmpPortStatisticsPortNum OBJECT-TYPE
        SYNTAX        INTEGER  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"The port on which statistics be gathered. Index identifying this entry."
        ::= { QTECHIgmpPortStatisticsEntry 1 }

 QTECHIgmpPortStatisticsClear OBJECT-TYPE
        SYNTAX        INTEGER {clear(1) } 
        MAX-ACCESS    read-write 
        STATUS        current
		DESCRIPTION  
		"Set this object 1 to clear all the information of MVR port statistics.
		Get this object will always return 1.
		"
        ::= { QTECHIgmpPortStatisticsEntry 2 }

QTECHIgmpPortStatisticsRecvQuery OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of query packet received on this port."
        ::= { QTECHIgmpPortStatisticsEntry 3 }

 QTECHIgmpPortStatisticsRecvReport OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of report packet received on this port."
        ::= { QTECHIgmpPortStatisticsEntry 4 }

QTECHIgmpPortStatisticsRecvLeave OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of leave packet received on this port."
        ::= { QTECHIgmpPortStatisticsEntry 5 }

QTECHIgmpPortStatisticsFilterDropQuery OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of query packet droped on this port."
        ::= { QTECHIgmpPortStatisticsEntry 6 }

QTECHIgmpPortStatisticsFilterDropReport OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of report packet droped on this port."
        ::= { QTECHIgmpPortStatisticsEntry 7 }

QTECHIgmpPortStatisticsFilterDropLeave OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of leave packet droped on this port."
        ::= { QTECHIgmpPortStatisticsEntry 8 }

QTECHIgmpPortStatisticsSnoopDealQuery OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of query packets dealed by snooping."
        ::= { QTECHIgmpPortStatisticsEntry 9 }

QTECHIgmpPortStatisticsSnoopDealReport OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of report packets dealed by snooping."
        ::= { QTECHIgmpPortStatisticsEntry 10 }

QTECHIgmpPortStatisticsSnoopDealLeave OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of leave packets dealed by snooping."
        ::= { QTECHIgmpPortStatisticsEntry 11 }

QTECHIgmpPortStatisticsMvrDealQuery OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of query packets dealed by mvr."
        ::= { QTECHIgmpPortStatisticsEntry 12 }

QTECHIgmpPortStatisticsMvrDealReport OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of report packets dealed by mvr."
        ::= { QTECHIgmpPortStatisticsEntry 13 }

QTECHIgmpPortStatisticsMvrDealLeave OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of leave packets dealed by mvr."
        ::= { QTECHIgmpPortStatisticsEntry 14 }

QTECHIgmpPortStatisticsVlanCPDealQuery OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of query packets dealed by vlan-copy."
        ::= { QTECHIgmpPortStatisticsEntry 15 }

QTECHIgmpPortStatisticsVlanCPDealReport OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of report packets dealed by vlan-copy."
        ::= { QTECHIgmpPortStatisticsEntry 16 }

QTECHIgmpPortStatisticsVlanCPDealLeave OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note the count of leave packets dealed by vlan-copy."
        ::= { QTECHIgmpPortStatisticsEntry 17 }

QTECHIgmpPortStatisticsReplaceCount OBJECT-TYPE
        SYNTAX        Counter 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"The object note count of replace taken place on this port"
        ::= { QTECHIgmpPortStatisticsEntry 18 }

--
-- Snooping Group
--
QTECHIgmpSnoopingEnable OBJECT-TYPE
		SYNTAX      EnableVar
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "Set this object to enable(1) to enable IGMP Snooping.  Set it to disable(2) to disable IGMP Snooping.
				Note that IGMP Snooping can work with or without GVRP and GMRP enabled.
				"
        DEFVAL { 2 }
        ::= { QTECHIgmpSnoopingScalar 1 } 
        
QTECHIgmpSnoopingEnableVlanList OBJECT-TYPE
		SYNTAX      Vlanset
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "Enable/Disable igmp snooping of vlan."
        ::= { QTECHIgmpSnoopingScalar 2 }



--
-- Mvr Group
--
QTECHIgmpMvrEnable OBJECT-TYPE
		SYNTAX      EnableVar
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "This object controls whether the MVR is enabled on the device. 
               A disable(2) value will prevent the MVR on the device."   
        DEFVAL { 2 }
        ::= { QTECHIgmpMvrScalar 1 }


QTECHIgmpMvrEnablePortList OBJECT-TYPE
		SYNTAX      PortList
        MAX-ACCESS  read-write
        STATUS      current
        DESCRIPTION
               "Indicates the ports has enabled igmp mvr."
        ::= { QTECHIgmpMvrScalar 2 }

-------------------------------------------------------------- 
QTECHIgmpMvrMVlanGroupTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpMvrMVlanGroupEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "This table indicates the relation between multicast-vlan and igmp member."
        ::= { QTECHIgmpMvr 2 }

QTECHIgmpMvrMVlanGroupEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpMvrMVlanGroupEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "Indicates the relation between multicast-vlan and igmp member."
        INDEX      {QTECHIgmpMvrGroupIpType, QTECHIgmpMvrGroup }
        ::= { QTECHIgmpMvrMVlanGroupTable 1 }

QTECHIgmpMvrMVlanGroupEntry ::= SEQUENCE {  
         QTECHIgmpMvrGroupIpType    InetAddressType,
         QTECHIgmpMvrGroup          InetAddress,
         QTECHIgmpMvrMVlan          VlanId,
         QTECHIgmpMvrGroupRowStatus    RowStatus
		}

 QTECHIgmpMvrGroupIpType OBJECT-TYPE
        SYNTAX        InetAddressType
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Indicates the IP address type."
        ::= { QTECHIgmpMvrMVlanGroupEntry 1 }


 QTECHIgmpMvrGroup OBJECT-TYPE
        SYNTAX        InetAddress
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
			"Index identifying this entry.
			the multicast vlan of this groups which user want to receive.
			"
        ::= { QTECHIgmpMvrMVlanGroupEntry 2 }

  QTECHIgmpMvrMVlan OBJECT-TYPE
        SYNTAX        VlanId 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Indicates the multicast VLAN ID, which is connected to multicast router."
        ::= { QTECHIgmpMvrMVlanGroupEntry 3 }

 QTECHIgmpMvrGroupRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"The row status of this entry."
        ::= { QTECHIgmpMvrMVlanGroupEntry 4 }
  
  
  
--
-- Clan-Copy Group
--
 QTECHIgmpVlanCopyEnable	OBJECT-TYPE
        SYNTAX          EnableVar
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object controls whether the igmp vlan-copy is enabled on the device. 
                A disable(2) value will prevent the vlan-copy on the device." 
        DEFVAL { 2 }
        ::= { QTECHIgmpVlanCopyScalar 1 }
 
QTECHIgmpVlanCopyEnablePortList	OBJECT-TYPE
        SYNTAX          PortList
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "Indicates the ports has enabled igmp vlan-copy."
        ::= { QTECHIgmpVlanCopyScalar 2 }
 
 
-------------------------------------------------------------- 
QTECHIgmpVlanCopyVlanGroupTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpVlanCopyVlanGroupEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "This table indicates the relation between multicast-vlan and igmp member."
        ::= { QTECHIgmpVlanCopy 2 }

QTECHIgmpVlanCopyVlanGroupEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpVlanCopyVlanGroupEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "Indicates the relation between multicast-vlan and igmp member."
        INDEX      {QTECHIgmpVlanCopyGroupIpType, QTECHIgmpVlanCopyGroup }
        ::= { QTECHIgmpVlanCopyVlanGroupTable 1 }

QTECHIgmpVlanCopyVlanGroupEntry ::= SEQUENCE {  
         QTECHIgmpVlanCopyGroupIpType   InetAddressType,
         QTECHIgmpVlanCopyGroup       InetAddress,  
         QTECHIgmpVlanCopyMcastVlan    VlanId,
         QTECHIgmpVlanCopyGroupRowStatus    RowStatus
		}        
		
QTECHIgmpVlanCopyGroupIpType OBJECT-TYPE
        SYNTAX        InetAddressType
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Indicates the IP address type."
        ::= { QTECHIgmpVlanCopyVlanGroupEntry 1 }

QTECHIgmpVlanCopyGroup OBJECT-TYPE
        SYNTAX        InetAddress 
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
			"Index identifying this entry.
			the multicast vlan of this groups which user want to receive.
			"
        ::= { QTECHIgmpVlanCopyVlanGroupEntry 2 }   
        
QTECHIgmpVlanCopyMcastVlan OBJECT-TYPE
        SYNTAX        VlanId 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Indicates the multicast VLAN ID, which is connected to multicast router."
        ::= { QTECHIgmpVlanCopyVlanGroupEntry 3 }


 QTECHIgmpVlanCopyGroupRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"The row status of this entry."
        ::= { QTECHIgmpVlanCopyVlanGroupEntry 4 }
 
 
 
--
-- Proxy Group
--
 QTECHIgmpProxyEnable	OBJECT-TYPE
        SYNTAX          EnableVar
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object controls whether the proxy is enabled on the device. 
                A disable(2) value will prevent the proxy on the device."
        DEFVAL { 2 }
        ::= { QTECHIgmpProxyScalar 1 }
 
 QTECHIgmpProxySuppressionEnable	OBJECT-TYPE
        SYNTAX          EnableVar
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object controls whether the proxy suppression is enabled on the device. 
                A disable(2) value will prevent the proxy suppression on the device."
       DEFVAL { 2 }
        ::= { QTECHIgmpProxyScalar 2 }


 QTECHIgmpProxyQuerierEnable	OBJECT-TYPE
        SYNTAX          EnableVar
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object controls whether the IGMP querier is enabled on the device.
				A disable(2) value will prevent the IGMP querier on the device.
				"    
		DEFVAL { 2 }
        ::= { QTECHIgmpProxyScalar 3 }


 QTECHIgmpProxySourceIpType	OBJECT-TYPE
        SYNTAX          InetAddressType
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "Indicates the IP address type."
        DEFVAL { 1 }
        ::= { QTECHIgmpProxyScalar 4 }


 QTECHIgmpProxySourceIpAddress	OBJECT-TYPE
        SYNTAX          InetAddress
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object specify the source IP address for IGMP proxy and querier. 
                If not configure this object ,the default value is the IP address on IP interface 0.
                Then if the IP address on IP interface 0 is none, the default value is 0.0.0.0."
        ::= { QTECHIgmpProxyScalar 5 }


 QTECHIgmpProxyQueryInterval	OBJECT-TYPE
        SYNTAX          INTEGER(10..3600)
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object specify interval time that querier send general query packet.
				The default value is 60s.
				"
        ::= { QTECHIgmpProxyScalar 6 }


 QTECHIgmpProxyQueryMaxReponseInterval	OBJECT-TYPE
        SYNTAX          INTEGER(1..25)
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object specify max response time for host to response the IGMP proxy.
				The default value is 10s.
				"
        ::= { QTECHIgmpProxyScalar 7 }


 QTECHIgmpProxyQueryLastMemberInterval	OBJECT-TYPE
        SYNTAX          INTEGER(1..25)
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "This object specify last member query interval.
                When last member leave the group,IGMP proxy will send the special group query packet.
                If IGMP proxy receive none report packet of this group in the last member query interval, 
                IGMP proxy will delete this group from multicast transmit table.The default value is 1s."
        ::= { QTECHIgmpProxyScalar 8 }
 
 
 
--
-- Filter Group
--
QTECHIgmpFilterEnableFilter	OBJECT-TYPE
        SYNTAX          EnableVar
        MAX-ACCESS      read-write
        STATUS current
        DESCRIPTION
                "The state of global Igmp Filter" 
        DEFVAL { 2 }
        ::= { QTECHIgmpFilterScalar 1 }

QTECHIgmpFilterMaxProfileNum	OBJECT-TYPE
        SYNTAX          INTEGER
        MAX-ACCESS      read-only
        STATUS current
        DESCRIPTION
                "Max Profile number that you can set and used " 
        DEFVAL { 100 }
        ::= { QTECHIgmpFilterScalar 2 }


QTECHIgmpFilterCurrentProfileNum	OBJECT-TYPE
        SYNTAX          INTEGER
        MAX-ACCESS      read-only
        STATUS current
        DESCRIPTION
                "Current number of profiles that are already created"  
        DEFVAL { 0  }
        ::= { QTECHIgmpFilterScalar 3 }


-------------------------------------------------------------- 
QTECHIgmpFilterProfileTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpFilterProfileEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "description"
        ::= { QTECHIgmpFilter 2 } 
        
        
QTECHIgmpFilterProfileEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpFilterProfileEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "description"
        INDEX      { QTECHIgmpFilterProfileIndex }
        ::= { QTECHIgmpFilterProfileTable 1 }

QTECHIgmpFilterProfileEntry ::= SEQUENCE {  
         QTECHIgmpFilterProfileIndex    INTEGER(1..100),
         QTECHIgmpFilterProfileAct      INTEGER,
         QTECHIgmpFilterProfileRowStatus   RowStatus
		} 
		
QTECHIgmpFilterProfileIndex OBJECT-TYPE
        SYNTAX        INTEGER(1..100) 
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"the index of the Filter Profile Table"
        ::= { QTECHIgmpFilterProfileEntry 1 }

QTECHIgmpFilterProfileAct OBJECT-TYPE
        SYNTAX        INTEGER{permit(1),deny(2)} 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Igmp profile Filter Action" 
		 DEFVAL { 2 }
        ::= { QTECHIgmpFilterProfileEntry 2 }

QTECHIgmpFilterProfileRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Row Status of Igmp Filter Profile Table"
        ::= { QTECHIgmpFilterProfileEntry 3 }

       
--------------------------------------------------------------
QTECHIgmpFilterPortTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpFilterPortEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "description"
        ::= { QTECHIgmpFilter 3 }

QTECHIgmpFilterPortEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpFilterPortEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "description"
        INDEX      { QTECHIgmpFilterPortIndex }
        ::= { QTECHIgmpFilterPortTable 1 }

QTECHIgmpFilterPortEntry ::= SEQUENCE {  
         QTECHIgmpFilterPortIndex    INTEGER,  
         QTECHIgmpFilterPortProfileIndex   INTEGER(0..100),
         QTECHIgmpFilterPortMaxGroups    INTEGER(0..2147483647) ,
         QTECHIgmpFilterPortCurrentGroups   INTEGER,
         QTECHIgmpFilterPortMaxGroupsAct      INTEGER,
         QTECHIgmpFilterPortRowStatus     RowStatus
		}
 
QTECHIgmpFilterPortIndex OBJECT-TYPE
        SYNTAX        INTEGER 
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index of QTECHIgmpFilterIFTable"
        ::= { QTECHIgmpFilterPortEntry 1 }


QTECHIgmpFilterPortProfileIndex OBJECT-TYPE
        SYNTAX        INTEGER(0..100) 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"RSpecifies which IGMP filter profile applies to this vlan. 
		If the value of this MIB object matches the value of QTECHIgmpFilterProfileIndex in QTECHIgmpFilterTable,
		the corresponding profile configuration will apply to 	this vlan.
		A value of zero indicates no profile is associated with corresponding vlan.
		"   
		DEFVAL { 0 }
        ::= { QTECHIgmpFilterPortEntry 2 }


QTECHIgmpFilterPortMaxGroups OBJECT-TYPE
        SYNTAX        INTEGER(0..2147483647) 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Specifies the max multicast group numbers that allow join on this port. 
		A value of zero indicates no max limitation is associated with corresponding port"  
		DEFVAL { 0 }
        ::= { QTECHIgmpFilterPortEntry 3 }


QTECHIgmpFilterPortCurrentGroups OBJECT-TYPE
        SYNTAX        INTEGER 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"Specifies the current multicast group numbers that this port has joined"
        ::= { QTECHIgmpFilterPortEntry 4 }


QTECHIgmpFilterPortMaxGroupsAct OBJECT-TYPE
        SYNTAX        INTEGER  {drop(1),replace(2)} 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Specifies which action will take place on the vlan when exceed the max multicast group numbers. 
		A value of deny(1) indicates deny join the multicast group when exceed the max multicast group numbers. 
		A value of replace(2) indicates allow to replace the first multicast group on the vlan when exceed the max multicast group numbers."
        DEFVAL { 1 }
        ::= { QTECHIgmpFilterPortEntry 5 }


QTECHIgmpFilterPortRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Row Status of this Table"
        ::= { QTECHIgmpFilterPortEntry 6 }
 
 
--------------------------------------------------------------
QTECHIgmpFilterPortVlanTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpFilterPortVlanEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "description"
        ::= { QTECHIgmpFilter 4 }   
 
QTECHIgmpFilterPortVlanEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpFilterPortVlanEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "description"
        INDEX      { QTECHIgmpFilterPortVlanPortIndex, QTECHIgmpFilterPortVlanVlanIndex}
        ::= { QTECHIgmpFilterPortVlanTable 1 }

QTECHIgmpFilterPortVlanEntry ::= SEQUENCE {  
         QTECHIgmpFilterPortVlanPortIndex    INTEGER,
         QTECHIgmpFilterPortVlanVlanIndex   VlanIndex,
         QTECHIgmpFilterPortVlanProfileIndex    INTEGER(0..100),
         QTECHIgmpFilterPortVlanMaxGroups    INTEGER(0..2147483647) ,
         QTECHIgmpFilterPortVlanCurrentGroups   INTEGER,
         QTECHIgmpFilterPortVlanMaxGroupsAct    INTEGER,
         QTECHIgmpFilterPortVlanRowStatus    RowStatus
		}
 
QTECHIgmpFilterPortVlanPortIndex OBJECT-TYPE
        SYNTAX        INTEGER 
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpFilterPortVlanEntry 1 }


QTECHIgmpFilterPortVlanVlanIndex OBJECT-TYPE
        SYNTAX        VlanIndex 
        MAX-ACCESS    not-accessible
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpFilterPortVlanEntry 2 }


QTECHIgmpFilterPortVlanProfileIndex OBJECT-TYPE
        SYNTAX        INTEGER(0..100) 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Specifies which IGMP filter profile applies to this vlan. 
		If the value of this MIB object matches the value of QTECHIgmpFilterProfileIndex in QTECHIgmpFilterTable,
		the corresponding profile configuration will apply to 	this vlan.
		A value of zero indicates no profile is associated with corresponding vlan.
		"   
		DEFVAL { 0 }
        ::= { QTECHIgmpFilterPortVlanEntry 3 }


QTECHIgmpFilterPortVlanMaxGroups OBJECT-TYPE
        SYNTAX        INTEGER(0..2147483647)  
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Specifies the max multicast group numbers that allow this vlan join on this port. 
		A value of zero indicates no max limitation is associated with corresponding vlan on this port."   
		DEFVAL { 0 }
        ::= { QTECHIgmpFilterPortVlanEntry 4 }


QTECHIgmpFilterPortVlanCurrentGroups OBJECT-TYPE
        SYNTAX        INTEGER 
        MAX-ACCESS    read-only 
        STATUS        current
		DESCRIPTION  
		"Specifies the current multicast group numbers that this  vlan has joined on this port."
        ::= { QTECHIgmpFilterPortVlanEntry 5 }


QTECHIgmpFilterPortVlanMaxGroupsAct OBJECT-TYPE
        SYNTAX        INTEGER {drop(1),replace(2)}
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Specifies which action will take place on the vlan when exceed the max multicast group numbers. 
		A value of deny(1) indicates deny join the multicast group when exceed the max multicast group numbers. 
		A value of replace(2) indicates allow to replace the first multicast group on the vlan when exceed the max multicast group numbers."
        DEFVAL { 1 }
        ::= { QTECHIgmpFilterPortVlanEntry 6 }


QTECHIgmpFilterPortVlanRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Row Status of this Table"
        ::= { QTECHIgmpFilterPortVlanEntry 7 }
 
 
--------------------------------------------------------------        
QTECHIgmpFilterIpProfileTable	OBJECT-TYPE
        SYNTAX SEQUENCE OF QTECHIgmpFilterIpProfileEntry
        MAX-ACCESS      not-accessible
        STATUS current
        DESCRIPTION
                "description"
        ::= { QTECHIgmpFilter 5 }

QTECHIgmpFilterIpProfileEntry OBJECT-TYPE
	    SYNTAX     QTECHIgmpFilterIpProfileEntry
        MAX-ACCESS not-accessible
        STATUS     current
        DESCRIPTION
                "description"
        INDEX      { QTECHIgmpFilterIpProfileIndex, QTECHIgmpFilterIpProfileRangeIndex}
        ::= { QTECHIgmpFilterIpProfileTable 1 }

QTECHIgmpFilterIpProfileEntry ::= SEQUENCE {  
         QTECHIgmpFilterIpProfileIndex    INTEGER(1..100),
         QTECHIgmpFilterIpProfileRangeIndex   INTEGER(1..100),
         QTECHIgmpFilterIpProfileStartAddress  InetAddress,
         QTECHIgmpFilterIpProfileEndAddress     InetAddress,
         QTECHIgmpFilterIpProfileIpType      InetAddressType,
         QTECHIgmpFilterIpProfileRowStatus     RowStatus
		}   
		                              
QTECHIgmpFilterIpProfileIndex OBJECT-TYPE
        SYNTAX        INTEGER(1..100) 
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpFilterIpProfileEntry 1 }
 
QTECHIgmpFilterIpProfileRangeIndex OBJECT-TYPE
        SYNTAX        INTEGER(1..100)  
        MAX-ACCESS    not-accessible 
        STATUS        current
		DESCRIPTION  
		"Index identifying this entry."
        ::= { QTECHIgmpFilterIpProfileEntry 2 }
 
QTECHIgmpFilterIpProfileStartAddress OBJECT-TYPE
        SYNTAX        InetAddress 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Start IP address of Filter Profile"
        ::= { QTECHIgmpFilterIpProfileEntry 3 }

QTECHIgmpFilterIpProfileEndAddress OBJECT-TYPE
        SYNTAX        InetAddress 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"EndIP address of Filter Profile"
        ::= { QTECHIgmpFilterIpProfileEntry 4 }

QTECHIgmpFilterIpProfileIpType OBJECT-TYPE
        SYNTAX        InetAddressType 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"IP address Type of Filter Profile"  
        ::= { QTECHIgmpFilterIpProfileEntry 5 }

QTECHIgmpFilterIpProfileRowStatus OBJECT-TYPE
        SYNTAX        RowStatus 
        MAX-ACCESS    read-create 
        STATUS        current
		DESCRIPTION  
		"Row Status of this Table"
        ::= { QTECHIgmpFilterIpProfileEntry 6 }
 
  
END
